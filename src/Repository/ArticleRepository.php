<?php

namespace App\Repository;

use App\Entity\Categorie;

/**
 * ArticleRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */
class ArticleRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get article with parameter.
     *
     * @param null  $limit    limit
     * @param null  $page     page
     * @param null  $category category
     * @param array $types    type
     *
     * @return mixed
     */
    public function getArticles($limit = null, $page = null, $category = null, $types = array(), $revueId = null, $tag = null, $search = null, $home= null, $dateEvent = null)
    {
        $query = $this->createQueryBuilder('a')
        ->leftJoin("a.archive", "ac")
        ->andWhere("ac.article is null")
            ;

        if ($limit) {
            $query->setMaxResults($limit);
        }

        if ($page) {
            if (null == $limit) {
                $limit = 1;
            }

            $offset = ($page - 1) * $limit;
            $query->setFirstResult($offset);
        }

        if ($category) {
            $query->join('a.categorie', 'categorie');
            //check if categorie parent
            $dql = "SELECT c FROM App:Categorie c where c.id=".$category."";
            $categorieParent = $this->getEntityManager()->createQuery($dql)->getResult();

            if ($categorieParent) {
                $tabIdChildren = array();
                $catChildren = $categorieParent[0]->getChildren();
                if($catChildren) {
                    $tabIdChildren[] = $category;
                    foreach ($catChildren as $cat) {
                        $tabIdChildren[] = $cat->getId();
                        if($cat->getChildren()) {
                            foreach ($cat->getChildren() as $catChild) {
                                $tabIdChildren[] = $catChild->getId();
                            }
                        }
                    }
                }
                if ($tabIdChildren) {
                    $query->andWhere('categorie.id in ('.implode(",", $tabIdChildren).')');

                } else {
                    $query->andWhere('categorie.id = :categoryId')
                        ->setParameter('categoryId', $category);
                }


            } else {
                $query->andWhere('categorie.id = :categoryId')
                    ->setParameter('categoryId', $category);
            }


        }


        if ($types) {
            $typesString = '';
            foreach ($types as $type) {
                $typesString .= "'" . $type . "',";
            }
            $typesString = trim($typesString, ',');

            $query->andWhere("a.type in($typesString)");
        } else {
            $query->andWhere("a.type not in ('Événements')");
        }

        if ($revueId) {
            $query->join("a.revue","revue")
                ->join('a.positionArticle','positionArticle')
                ->andWhere("revue.id = :revue")
            ->setParameter("revue",$revueId);

            $query->andWhere("a.revuePage = 1");

        } else {
            if ($home == 1) {
                $query->andWhere("a.homePage = 1");
            }


        }

        if ($tag) {
            $query->join("a.tags","t")
                ->andWhere("t.slug = :slug")
                ->setParameter("slug",$tag);
        }

        if ($search) {
            $query->andWhere("a.titre LIKE :search OR a.resume LIKE :search OR a.contenu LIKE :search")
                ->setParameter('search', '%'.$search.'%');

        }

        $query->andWhere("a.status = 'publie' AND a.type not in ('Edito', 'edito')");
        if ($revueId) {
            $query->orderBy('positionArticle.position', 'ASC');
        } else {
            $query->orderBy('a.datePublication', 'DESC');
        }

        if (null!= $dateEvent) {
            $dateEvent = substr($dateEvent, 0,4)."-".substr($dateEvent, 4,2);
            $query->andWhere("a.dateDebutEvent LIKE :dateEvent")
            ->setParameter('dateEvent', '%'.$dateEvent.'%');
            $query->orderBy('a.dateDebutEvent', 'ASC');
        }
        $query->andWhere("a.datePublication <='".date("Y-m-d H:i:s")."'");


        $articles = $query->getQuery()->getResult();

        if($articles) {
            foreach ($articles as $article) {
                if ($article->getVideoArticle()) {
                    $article->setUrlExist(true);
                }

                if ($article->getType() == "Calameo") {
                    $article->setType("Articles");
                }

                if ( null == $article->getCategorie()) { //Pour un article sans categorie
                    $categorie = new Categorie();
                    $categorie->setNom("undefined");
                    $categorie->setSlug("undefined");
                    $article->setCategorie($categorie);
                }
            }
        }


        return $articles;
    }

    /**
     * Get article categorie
     *
     * @param $slug categorie
     *
     * @return array
     */
    public function getArticleCategorie($slug)
    {
        $query = $this->createQueryBuilder('a')
            ->join('a.categorie', 'c')
            ->where('c.slug = :slug')
            ->andWhere("a.status = 'publie' ")
            ->setParameter('slug', $slug)
            ->orderBy('a.datePublication', 'DESC')
            ->groupBy('a.id');

        return $query->getQuery()->getResult();
    }

    public function getArticleTag($slug)
    {
        $query = $this->createQueryBuilder('a')
            ->join('a.tags', 't')
            ->where('t.slug = :slug')
            ->andWhere("a.status = 'publie' ")
            ->setParameter('slug', $slug)
            ->orderBy('a.id', 'DESC')
            ->groupBy('a.id');

        $articles = $query->getQuery()->getResult();
    
        foreach ($articles as $article) {
            if ($article->getVideoArticle()) {
                $article->setUrlExist(true);
            }            
        }

        return $articles;
    }

    public function getLatestVideosArticle()
    {
        /*$query = $this->createQueryBuilder('a')
            ->join('a.videoArticle', 'v')
            ->where("a.type = 'Vidéos'")
            ->andWhere("a.status = 'publie' ")
            //->andWhere("v.lien IS NOT NULL")
            ->orderBy('a.datePublication','DESC')
            ->setFirstResult(0)
            ->setMaxResults(3)
            ;*/

        $query = $this->createQueryBuilder('a')
            ->where("a.type = 'Vidéos'")
            ->andWhere("a.status = 'publie' ")
            ->orderBy('a.datePublication', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(3);

        return $query->getQuery()->getResult();
    }

    public function getArticleFullContent($slug) 
    {
        $query = $this->createQueryBuilder('a')
            ->where("a.slug = :slug")
            ->andWhere("a.status = 'publie' ")
            ->setParameter('slug', $slug);

        $article = $query->getQuery()->getOneOrNullResult();
      
        if (method_exists($article,'getVideoArticle')  && $article->getVideoArticle()) {
            $article->setUrlExist(true);
        }            
        
        return $article;
    }

    public function getDataByFilter($data, $user, $session) {

        $query = $this->createQueryBuilder('a')
            ->leftJoin("a.archive", "ac")
            ->where('a.titre is not null')
            ->andWhere("ac.article is null")
            ;
        if ($user) {
            $query->andWhere("a.user=:user")
            ->setParameter('user',$user['user']);
        }

        $session->set('titre', null);
        if (isset($data['article_filter']['titre']) && null != $data['article_filter']['titre']) {
            $titre = $data['article_filter']['titre'];
            $session->set('titre', $data['article_filter']['titre']);
            $query->andWhere("a.titre LIKE :search OR a.resume LIKE :search OR a.contenu LIKE :search")
                ->setParameter('search', '%'.$titre.'%');
        }

        $session->set('categorie', null);
        if (isset($data['article_filter']['categorie']) && null != $data['article_filter']['categorie']) {
            $categorie = $data['article_filter']['categorie'];


            $session->set('categorie', $categorie);
            $query->innerJoin('a.categorie','c');
            $query->andWhere("c.id in (".implode(",",$categorie).")");
                //->setParameter("categorie", implode(",",$categorie));
        }

        $session->set('type', null);
        if (isset($data['article_filter']['type'])  && null != $data['article_filter']['type']) {
            $type = $data['article_filter']['type'];
            $session->set('type', $type);
            $type_2 = [];
            foreach ($type as $unType) {
                $type_2[] = "'".addslashes($unType)."'";
            }

            $query->andWhere("a.type in (".implode(",", $type_2).")");
        }

        $session->set('status', null);
        if (isset($data['article_filter']['status']) && null != $data['article_filter']['status']) {
            $status = $data['article_filter']['status'];
            $session->set('status', $status);

            $status_2 = [];
            foreach ($status as $unStatus) {
                $status_2[] = "'".addslashes($unStatus)."'";
            }

            $query->andWhere("a.status in (".implode(",", $status_2).")");

        }

        $session->set('auteur', null);
        if (isset($data['article_filter']['auteur']) && null != $data['article_filter']['auteur']) {
            $auteur = $data['article_filter']['auteur'];
            $session->set('auteur', $auteur);

            $auteur_2 = [];
            foreach ($auteur as $unAuteur) {
                $auteur_2[] = "'".addslashes($unAuteur)."'";
            }

            $query->andWhere("a.auteur in (".implode(",", $auteur_2).")");

        }

        $session->set('premium', null);

        if (isset($data['article_filter']['premium']) && null!=$data['article_filter']['premium']) {
            $premium = $data['article_filter']['premium'];
            $session->set('premium', $premium);
            $query->andWhere("a.premium=:premium")
                ->setParameter("premium", $premium);
        }



        $session->set('dateDebutCreation', null);
        $session->set('dateFinCreation', null);

        if (isset($data['date_creation_debut']) && null != $data['date_creation_debut']) {

            $dateDebutCreation = $data['date_creation_debut'];
            if (null != $data['date_creation_fin']) {
                $dateFinCreation = $data['date_creation_fin'];
            } else {
                $dateFinCreation = $data['date_creation_debut'];
            }

            $session->set('dateDebutCreation', $dateDebutCreation);
            $session->set('dateFinCreation', $dateFinCreation);
            //$dateCreation = $data->getDateCreation();
            //$dateAvantN = $dateCreation->format("Y-m-d");
            //$dateAvant = date('Y-m-d', strtotime($dateAvantN. ' + 10 days'));

            //$dateApres = date('Y-m-d', strtotime($dateAvantN. ' - 10 days'));

            $dateAvant = $dateDebutCreation;
            $dateApres = $dateFinCreation;
            $query->andWhere("a.dateCreation BETWEEN :dateAvant AND :dateApres")
                ->setParameter("dateAvant", $dateAvant)
                ->setParameter("dateApres", $dateApres)
            ;
        }

        $session->set('dateDebutPublication', null);
        $session->set('dateFinPublication', null);
        if (isset($data['date_publication_debut']) && null != $data['date_publication_debut']) {
            $dateDebutPublication = $data['date_publication_debut'];

            if (null != $data['date_publication_fin']) {
                $dateFinPublication = $data['date_publication_fin'];
            } else {
                $dateFinPublication = $data['date_publication_debut'];
            }


            $session->set('dateDebutPublication', $dateDebutPublication);
            $session->set('dateFinPublication', $dateFinPublication);

            //$dateCreation = $data->getDatePublication();
            //$dateAvantN = $dateCreation->format("Y-m-d");
            //$dateAvant = date('Y-m-d', strtotime($dateAvantN. ' + 10 days'));

            //$dateApres = date('Y-m-d', strtotime($dateAvantN. ' - 10 days'));
            $dateAvant = $dateDebutPublication;
            $dateApres = $dateFinPublication;

            $query->andWhere("a.datePublication BETWEEN :dateAvant AND :dateApres")
                ->setParameter("dateAvant", $dateAvant)
                ->setParameter("dateApres", $dateApres)
            ;
        }



        return $query->getQuery()->getResult();
    }

    public function getEventCenter($limit=null)
    {
        if (null == $limit) {
            $limit = 4;
        }
        $query = $this->createQueryBuilder('a')
            ->leftJoin("a.archive", "ac")
            ->where("ac.article is null")
            ->andWhere('a.dateDebutEvent >= :now')
            ->andWhere("a.type='Événements' AND a.status='publie'")
            ->orderBy('a.dateDebutEvent', 'ASC')
            ->setMaxResults($limit)
        ->setParameter('now', new \DateTime('now'));


        return $query->getQuery()->getResult();
    }

    /**
     * Get all article with comment
     *
     * @return mixed
     */
    public function getArticleWithComment()
    {
        $query = $this->createQueryBuilder('a')
            ->join('a.comments', 'c')
            ->orderBy('a.dateCreation', 'DESC')
            ;

        return $query->getQuery()->getResult();
    }
}
