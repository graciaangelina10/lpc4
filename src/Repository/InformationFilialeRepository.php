<?php

namespace App\Repository;

/**
 * InformationFilialeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InformationFilialeRepository extends \Doctrine\ORM\EntityRepository
{
    public function findInformation($cabinet, $annee = null)
    {
        $query = $this->createQueryBuilder('i')
            ->join('i.cabinet', 'ca')
            ->where('ca.id = :cabinet')
            ->setParameter('cabinet', $cabinet)
        ;

        if (null != $annee) {
            $query = $query->andWhere('i.annee = :annee')
                ->setParameter('annee', $annee);
        }

        $result = $query->getQuery()->getResult();

        //On prend le premier date
        if ($result) {
            $result = $result[0];
        }

        return $result;
    }
}
