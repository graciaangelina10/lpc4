<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

class ProduitRepository extends \Doctrine\ORM\EntityRepository
{
//    private $em;
//
//    public function __construct(EntityManagerInterface $em, ClassMetadata $class)
//    {
//       $this->em = $em;
//    }

    public function findAllIndexedById()
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from(Produit::class, 'p', 'p.id')
            ->where('p.disponible = :disponible')
            ->setParameter('disponible', true)
            ->orderBy('p.dateParution', 'DESC');

        return $query->getQuery()->getResult();
    }
}
