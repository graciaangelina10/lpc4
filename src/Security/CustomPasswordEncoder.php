<?php
namespace App\Security;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CustomPasswordEncoder implements PasswordEncoderInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {

        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function encodePassword($raw, $salt)
    {

    }

    public function isPasswordValid($encoded, $raw, $salt)
    {

    }
}
