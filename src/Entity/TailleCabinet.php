<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TailleCabinet
 *
 * @ORM\Table(name="taille_cabinet")
 * @ORM\Entity(repositoryClass="App\Repository\TailleCabinetRepository")
 */
class TailleCabinet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=255)
     */
    private $annee;

    /**
     * @var int|null
     *
     * @ORM\Column(name="effectif", type="integer", nullable=true)
     */
    private $effectif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbClient", type="integer", nullable=true)
     */
    private $nbClient;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbBureau", type="integer", nullable=true)
     */
    private $nbBureau;

    /**
     * @var \string|null
     *
     * @ORM\Column(name="dateCloture", type="string", nullable=true)
     */
    private $dateCloture;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbHommeEc", type="integer", nullable=true)
     */
    private $nbHommeEc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbFemmeEc", type="integer", nullable=true)
     */
    private $nbFemmeEc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbHommeAs", type="integer", nullable=true)
     */
    private $nbHommeAs;

    /**
     * @var int
     *
     * @ORM\Column(name="nbFemmeAs", type="integer", nullable=true)
     */
    private $nbFemmeAs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="groupeNational", type="string", length=255, nullable=true)
     */
    private $groupeNational;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reseauNational", type="string", length=255, nullable=true)
     */
    private $reseauNational;

    /**
     * @var string|null
     *
     * @ORM\Column(name="localisation", type="text", nullable=true)
     */
    private $localisation;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="cas")
     */
    private $cabinet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return TailleCabinet
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set effectif.
     *
     * @param int|null $effectif
     *
     * @return TailleCabinet
     */
    public function setEffectif($effectif = null)
    {
        $this->effectif = $effectif;

        return $this;
    }

    /**
     * Get effectif.
     *
     * @return int|null
     */
    public function getEffectif()
    {
        return $this->effectif;
    }

    /**
     * Set nbClient.
     *
     * @param int|null $nbClient
     *
     * @return TailleCabinet
     */
    public function setNbClient($nbClient = null)
    {
        $this->nbClient = $nbClient;

        return $this;
    }

    /**
     * Get nbClient.
     *
     * @return int|null
     */
    public function getNbClient()
    {
        return $this->nbClient;
    }

    /**
     * Set nbBureau.
     *
     * @param int|null $nbBureau
     *
     * @return TailleCabinet
     */
    public function setNbBureau($nbBureau = null)
    {
        $this->nbBureau = $nbBureau;

        return $this;
    }

    /**
     * Get nbBureau.
     *
     * @return int|null
     */
    public function getNbBureau()
    {
        return $this->nbBureau;
    }

    /**
     * Set dateCloture.
     *
     * @param \string|null $dateCloture
     *
     * @return TailleCabinet
     */
    public function setDateCloture($dateCloture = null)
    {
        $this->dateCloture = $dateCloture;

        return $this;
    }

    /**
     * Get dateCloture.
     *
     * @return \string|null
     */
    public function getDateCloture()
    {
        return $this->dateCloture;
    }

    /**
     * Set nbHommeEc.
     *
     * @param int|null $nbHommeEc
     *
     * @return TailleCabinet
     */
    public function setNbHommeEc($nbHommeEc = null)
    {
        $this->nbHommeEc = $nbHommeEc;

        return $this;
    }

    /**
     * Get nbHommeEc.
     *
     * @return int|null
     */
    public function getNbHommeEc()
    {
        return $this->nbHommeEc;
    }

    /**
     * Set nbFemmeEc.
     *
     * @param int|null $nbFemmeEc
     *
     * @return TailleCabinet
     */
    public function setNbFemmeEc($nbFemmeEc = null)
    {
        $this->nbFemmeEc = $nbFemmeEc;

        return $this;
    }

    /**
     * Get nbFemmeEc.
     *
     * @return int|null
     */
    public function getNbFemmeEc()
    {
        return $this->nbFemmeEc;
    }

    /**
     * Set nbHommeAs.
     *
     * @param int|null $nbHommeAs
     *
     * @return TailleCabinet
     */
    public function setNbHommeAs($nbHommeAs = null)
    {
        $this->nbHommeAs = $nbHommeAs;

        return $this;
    }

    /**
     * Get nbHommeAs.
     *
     * @return int|null
     */
    public function getNbHommeAs()
    {
        return $this->nbHommeAs;
    }

    /**
     * Set nbFemmeAs.
     *
     * @param int $nbFemmeAs
     *
     * @return TailleCabinet
     */
    public function setNbFemmeAs($nbFemmeAs)
    {
        $this->nbFemmeAs = $nbFemmeAs;

        return $this;
    }

    /**
     * Get nbFemmeAs.
     *
     * @return int
     */
    public function getNbFemmeAs()
    {
        return $this->nbFemmeAs;
    }

    /**
     * Set groupeNational.
     *
     * @param string|null $groupeNational
     *
     * @return TailleCabinet
     */
    public function setGroupeNational($groupeNational = null)
    {
        $this->groupeNational = $groupeNational;

        return $this;
    }

    /**
     * Get groupeNational.
     *
     * @return string|null
     */
    public function getGroupeNational()
    {
        return $this->groupeNational;
    }

    /**
     * Set reseauNational.
     *
     * @param string|null $reseauNational
     *
     * @return TailleCabinet
     */
    public function setReseauNational($reseauNational = null)
    {
        $this->reseauNational = $reseauNational;

        return $this;
    }

    /**
     * Get reseauNational.
     *
     * @return string|null
     */
    public function getReseauNational()
    {
        return $this->reseauNational;
    }

    /**
     * Set localisation.
     *
     * @param string|null $localisation
     *
     * @return TailleCabinet
     */
    public function setLocalisation($localisation = null)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation.
     *
     * @return string|null
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }
}
