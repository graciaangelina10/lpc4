<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation as Serialize;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * @Vich\Uploadable
 * @Serialize\ExclusionPolicy("all")
 */
class Produit
{
    const TVA = 2.1;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * String image.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     *
     * @Serialize\Expose
     */
    private $pdf;

    /**
     * String pdfFile.
     *
     * @Assert\File(
     *     maxSize = "20M",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     *
     * @Vich\UploadableField(mapping="produit_pdf", fileNameProperty="pdf")
     *
     * @var File
     */
    private $pdfFile;

    /**
     * String image.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     *
     * @Serialize\Expose
     */
    private $image;

    /**
     * String imageFile.
     *
     * @Assert\Image(
     *      maxSize="20M",
     *      mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/jpg"},
     *     mimeTypesMessage = "Please upload a valid Image"
     *     )
     *
     * @Vich\UploadableField(mapping="produit_image", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $auteur;

    /**
     * @ORM\Column(type="float")
     */
    protected $prix;

    /**
     * @ORM\OneToMany(targetEntity="Achat", mappedBy="produit", cascade={"persist"})
     */
    protected $achatList;

    /**
     * @ORM\ManyToMany(targetEntity="Panier", mappedBy="produitList")
     */
    protected $panierList;

    /**
     * Achat (si existant) de l'User en cours
     * Renseigné dynamiquement par le controller
     */
    protected $currentAchat;

    /**
     * Date mise à jour.
     *
     * @ORM\Column(name="updatedAt",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="rubrique", type="string", length=55)
     */
    protected $rubrique;

    /**
     * Date mise à jour.
     *
     * @ORM\Column(name="dateParution",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $dateParution;

    /**
     * Boolean Disponibilité
     *
     * @ORM\Column(name="disponible",type="boolean",nullable=true)
     *
     * @var bool
     */
    private $disponible = false;

    /**
     * @ORM\Column(name="linkArticle", type="string", length=255, nullable=true)
     */
    protected $linkArticle;

    public function __construct()
    {
        $this->achatList = new \Doctrine\Common\Collections\ArrayCollection();
        $this->panierList = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix()
    {
        return $this->prix;
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    public function getAchatList()
    {
        return $this->achatList;
    }

    public function setAchatList($achatList)
    {
        $this->achatList = $achatList;

        return $this;
    }

    /**
     * Set imageFile.
     *
     * @param File|null $image object image
     *
     * @return string
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get imageFile.
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set pdfFile.
     *
     * @param File|null $document object document
     *
     * @return string
     */
    public function setPdfFile(File $pdf = null)
    {
        $this->pdfFile = $pdf;
        if ($pdf) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get pdfFile.
     *
     * @return File
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function getPanierList()
    {
        return $this->panierList;
    }

    public function setPanierList($panierList)
    {
        $this->panierList = $panierList;

        return $this;
    }

    public function getCurrentAchat()
    {
        return $this->currentAchat;
    }

    public function setCurrentAchat($currentAchat)
    {
        $this->currentAchat = $currentAchat;

        return $this;
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getTvaAmount()
    {
        return round($this->prix - $this->getNetPrice(), 2);
    }

    public function getNetPrice()
    {
        return round($this->prix / (1 + (self::TVA / 100)), 2);
        //return $this->prix / (1 + (self::TVA / 100));
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Produit
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add achatList
     *
     * @param \App\Entity\Achat $achatList
     *
     * @return Produit
     */
    public function addAchatList(\App\Entity\Achat $achatList)
    {
        $this->achatList[] = $achatList;

        return $this;
    }

    /**
     * Remove achatList
     *
     * @param \App\Entity\Achat $achatList
     */
    public function removeAchatList(\App\Entity\Achat $achatList)
    {
        $this->achatList->removeElement($achatList);
    }

    /**
     * Add panierList
     *
     * @param \App\Entity\Panier $panierList
     *
     * @return Produit
     */
    public function addPanierList(\App\Entity\Panier $panierList)
    {
        $this->panierList[] = $panierList;

        return $this;
    }

    /**
     * Remove panierList
     *
     * @param \App\Entity\Panier $panierList
     */
    public function removePanierList(\App\Entity\Panier $panierList)
    {
        $this->panierList->removeElement($panierList);
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     *
     * @return Produit
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string
     */
    public function getAuteur()
    {
        return $this->auteur;
    }


    /**
     * Set rubrique.
     *
     * @param string $rubrique
     *
     * @return Produit
     */
    public function setRubrique($rubrique)
    {
        $this->rubrique = $rubrique;

        return $this;
    }

    /**
     * Get rubrique.
     *
     * @return string
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set dateParution.
     *
     * @param \DateTime|null $dateParution
     *
     * @return Produit
     */
    public function setDateParution($dateParution = null)
    {
        $this->dateParution = $dateParution;

        return $this;
    }

    /**
     * Get dateParution.
     *
     * @return \DateTime|null
     */
    public function getDateParution()
    {
        return $this->dateParution;
    }

    /**
     * Set disponible.
     *
     * @param bool|null $disponible
     *
     * @return Produit
     */
    public function setDisponible($disponible = null)
    {
        $this->disponible = $disponible;

        return $this;
    }

    /**
     * Get disponible.
     *
     * @return bool|null
     */
    public function getDisponible()
    {
        return $this->disponible;
    }

    public function __toString()
    {
        return $this->nom." - ".$this->prix."€";
    }

    /**
     * Set linkArticle.
     *
     * @param string $linkArticle
     *
     * @return Produit
     */
    public function setLinkArticle($linkArticle)
    {
        $this->linkArticle = $linkArticle;

        return $this;
    }

    /**
     * Get linkArticle.
     *
     * @return string
     */
    public function getLinkArticle()
    {
        return $this->linkArticle;
    }
}
