<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_departement", type="string", length=255, nullable=true)
     */
    private $codeDepartement;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="insee_code", type="string", length=255, nullable=true)
     */
    private $inseeCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gps_lat", type="string", length=255, nullable=true)
     */
    private $gpsLat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gps_lng", type="string", length=255, nullable=true)
     */
    private $gpsLng;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeDepartement.
     *
     * @param string|null $codeDepartement
     *
     * @return City
     */
    public function setCodeDepartement($codeDepartement = null)
    {
        $this->codeDepartement = $codeDepartement;

        return $this;
    }

    /**
     * Get codeDepartement.
     *
     * @return string|null
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return City
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set inseeCode.
     *
     * @param string|null $inseeCode
     *
     * @return City
     */
    public function setInseeCode($inseeCode = null)
    {
        $this->inseeCode = $inseeCode;

        return $this;
    }

    /**
     * Get inseeCode.
     *
     * @return string|null
     */
    public function getInseeCode()
    {
        return $this->inseeCode;
    }

    /**
     * Set zipCode.
     *
     * @param string|null $zipCode
     *
     * @return City
     */
    public function setZipCode($zipCode = null)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set gpsLat.
     *
     * @param string|null $gpsLat
     *
     * @return City
     */
    public function setGpsLat($gpsLat = null)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat.
     *
     * @return string|null
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set gpsLng.
     *
     * @param string|null $gpsLng
     *
     * @return City
     */
    public function setGpsLng($gpsLng = null)
    {
        $this->gpsLng = $gpsLng;

        return $this;
    }

    /**
     * Get gpsLng.
     *
     * @return string|null
     */
    public function getGpsLng()
    {
        return $this->gpsLng;
    }
}
