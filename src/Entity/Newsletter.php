<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table(name="newsletter")
 * @ORM\Entity(repositoryClass="App\Repository\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=55, nullable=true)
     */
    private $ip;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * Object Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit",cascade={"persist"})
     * @ORM\JoinColumn(name="produit_id")
     */
    private $produit;

    /**
     * Boolean sendblue
     *
     * @ORM\Column(name="sendblue",type="boolean",nullable=true)
     *
     * @var bool
     */
    private $sendblue = false;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Newsletter
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return Newsletter
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Newsletter
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Set produit.
     *
     * @param \App\Entity\Produit|null $produit
     *
     * @return PanierProduit
     */
    public function setProduit(\App\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit.
     *
     * @return \App\Entity\Produit|null
     */
    public function getProduit()
    {
        return $this->produit;
    }

    public function __construct()
    {
        //$this->produit = new  \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Set sendblue.
     *
     * @param bool|null $sendblue
     *
     */
    public function setSendBlue($sendblue = null)
    {
        $this->sendblue = $sendblue;

        return $this;
    }

    /**
     * Get sendblue.
     *
     * @return bool|null
     */
    public function getSendBlue()
    {
        return $this->sendblue;
    }
}
