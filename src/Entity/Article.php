<?php

/**
 * Article Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialize;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Eko\FeedBundle\Item\Writer\ItemInterface;

/**
 * Article class.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 * @ExclusionPolicy("all")
 */
class Article implements ItemInterface
{
    const STATUS_EN_COURS = 'en_cours';
    const STATUS_A_VALIDER = 'a_valider';
    const STATUS_VALIDE = 'valide';
    const STATUS_PUBLIE = 'publie';

    const TYPE_ARTICLE = 'Articles';
    const TYPE_PODCAST = 'Podcasts';
    const TYPE_VIDEO = 'Vidéos';
    const TYPE_EVENEMENT = 'Événements';
    const TYPE_EDITO = 'Edito';
    const TYPE_CALAMEO = 'Calameo';

    /**
     * Article class.
     *
     * @var int
     *
     * @ORM\Column(name="id",               type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * String titre.
     *
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     * @Expose
     */
    private $titre;

    /**
     * String contenu.
     *
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=true)
     */
    private $contenu;

    /**
     * Object categorie.
     *
     * @ORM\ManyToOne(targetEntity="Categorie",cascade={"persist"}, inversedBy="articles")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Expose
     */
    private $categorie;

    /**
     * Entity Tags.
     *
     * @ORM\ManyToMany(targetEntity="Tags",mappedBy="articles",cascade={"persist"})
     * @Expose
     */
    private $tags;

    /**
     * String tagnew.
     *
     * @var string
     */
    private $tagnew;

    /**
     * String type.
     *
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     *
     * @Expose
     */
    private $type;

    /**
     * String slug.
     *
     * @Gedmo\Slug(fields={"titre"})
     *
     * @ORM\Column(length=255, unique=true)
     *
     * @Expose
     */
    private $slug;

    /**
     * String image.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     *
     * @Expose
     */
    private $image;

    /**
     * String imageFile.
     *
     * * @Assert\Image(
     *      maxSize="20M",
     *      mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/jpg"}
     *     )
     *
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * Date mise à jour.
     *
     * @ORM\Column(name="updatedAt",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * String auteur article.
     *
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", nullable=true, length=255)
     * @Expose
     */
    private $auteur;

    /**
     * Date creation article.
     *
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation",type="datetime",nullable=true)
     */
    private $dateCreation;

    /**
     * Text introduction article.
     *
     * @var string
     *
     * @ORM\Column(name="resume", type="text", nullable=true)
     *
     * @Expose
     */
    private $resume;

    /**
     * Premium article payant.
     *
     * @var bool
     *
     * @ORM\Column(name="premium", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $premium;

    /**
     * String status article.
     *
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true, length=55)
     *
     * @Expose
     */
    private $status;

    /**
     * Date publication article.
     *
     * @var \DateTime
     *
     * @ORM\Column(name="datePublication",type="datetime",nullable=true)
     *
     * @Expose
     */
    private $datePublication;

    /**
     * Entity Commentaire.
     *
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="article")
     *
     * @Expose
     */
    private $comments;

    protected $fullContent;

    /**
     * Entity UneArticle.
     *
     * @ORM\OneToMany(targetEntity="UneArticle", mappedBy="article", cascade={"remove"})
     */
    private $uneArticle;

    /**
     * Date debut evenement
     *
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutEvent",type="datetime",nullable=true)
     *
     * @Expose
     */
    private $dateDebutEvent;

    /**
     * Date fin evenement
     *
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinEvent",type="datetime",nullable=true)
     *
     * @Expose
     */
    private $dateFinEvent;

    /**
     * String lieu evenement.
     *
     * @var string
     *
     * @ORM\Column(name="lieuEvent", type="string", nullable=true, length=255)
     * @Expose
     */
    private $lieuEvent;

    /**
     * Entity VideoArticle.
     *
     * @ORM\OneToOne(targetEntity="VideoArticle", mappedBy="article", cascade={"persist","remove"})
     * @Expose
     */
    private $videoArticle;

    /**
     * @Expose
     */
    private $urlExist = false;

    /**
     * Entity Revue.
     *
     * @ORM\ManyToMany(targetEntity="Revue",mappedBy="article",cascade={"persist","remove"})
     */
    private $revue;

    /**
     * String image minuature video.
     *
     * @var string
     *
     * @ORM\Column(name="thumbnails", type="string", nullable=true, length=255)
     * @Expose
     */
    private $thumbnails;

    /**
     * Boolean homePage article.
     *
     * @var bool
     *
     * @ORM\Column(name="homePage", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $homePage = false;

    /**
     * Boolean revuePage article.
     *
     * @var bool
     *
     * @ORM\Column(name="revuePage", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $revuePage = false;

    /**
     * Entity PositionArticleRevue.
     *
     * @ORM\OneToMany(targetEntity="PositionArticleRevue", mappedBy="article")
     */
    private $positionArticle;

    /**
     * String podcast.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     *
     * @Serialize\Expose
     */
    private $podcast;

    /**
     * String podcastFile.
     *
     * @Assert\File(
     *     maxSize = "20M",
     *     mimeTypes = {"audio/mpeg3", "audio/x-mpeg-3", "video/mpeg", "video/x-mpeg", "audio/aiff"},
     *     mimeTypesMessage = "Please upload a valid Audio"
     * )
     *
     * @Vich\UploadableField(mapping="podcast_file", fileNameProperty="podcast")
     *
     * @var File
     */
    private $podcastFile;

    /**
     * Entity User to add Article
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     */
    private $user;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Archive", cascade={"remove"}, mappedBy="article")
     */
    private $archive;

    /**
     * @var
     * @Expose
     */
    private $contenuCache;

    /**
     * @var string
     * @ORM\Column(name="link_calameo", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $linkCalameo;

    /**
     * @var \DateTime
     */
    private $timePublication;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre.
     *
     * @param string $titre titre article
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu.
     *
     * @param string $contenu contenu article
     *
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set categorie.
     *
     * @param \App\Entity\Categorie $categorie categorie article
     *
     * @return Article
     */
    public function setCategorie(\App\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie.
     *
     * @return \App\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->revue = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateCreation = new \DateTime('now');

    }

    /**
     * Add tag.
     *
     * @param \App\Entity\Tags $tag tag article
     *
     * @return Article
     */
    public function addTag(\App\Entity\Tags $tag)
    {
        $this->tags[] = $tag;
        if (!$tag->getArticles()->contains($this)) {
            $tag->addArticle($this);
        }

        return $this;
    }

    /**
     * Remove tag.
     *
     * @param \App\Entity\Tags $tag tag article
     *
     * @return bool TRUE if this collection contained the specified element
     */
    public function removeTag(\App\Entity\Tags $tag)
    {
        $this->tags->removeElement($tag);

        $tag->removeArticle($this);
    }

    /**
     * Get tags.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get all tags.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAllTags()
    {
        return $this->tags;
    }

    /**
     * Get All new tags.
     *
     * @return tagnew
     */
    public function getTagNew()
    {
        return $this->tagnew;
    }

    /**
     * Set tagnew.
     *
     * @param string $tagnew nouveau tag article
     *
     * @return $this
     */
    public function setTagNew($tagnew)
    {
        $this->tagnew = $tagnew;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type.
     *
     * @param string $type name of type article
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set slug.
     *
     * @param string $slug slug article
     *
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set image.
     *
     * @param string $image image article
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imageFile.
     *
     * @param File|null $image object image
     *
     * @return string
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get imageFile.
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt date mise à jour article
     *
     * @return Article
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set auteur.
     *
     * @param string|null $auteur name of author
     *
     * @return Article
     */
    public function setAuteur($auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur.
     *
     * @return string|null
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set dateCreation.
     *
     * @param \DateTime|null $dateCreation date creation article
     *
     * @return Article
     */
    public function setDateCreation($dateCreation = null)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime|null
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set resume.
     *
     * @param string|null $resume introduction article
     *
     * @return Article
     */
    public function setResume($resume = null)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume.
     *
     * @return string|null
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set premium.
     *
     * @param bool $premium true si article payant
     *
     * @return Article
     */
    public function setPremium($premium)
    {
        $this->premium = $premium;

        return $this;
    }

    /**
     * Get premium.
     *
     * @return bool
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * Set status.
     *
     * @param string|null $status status article
     *
     * @return Article
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status lib
     *
     * @return string|null
     */
    public function getStatusLib()
    {
        if (!$this->status) {
            return '';
        }

        return [
            self::STATUS_EN_COURS => 'En cours',
            self::STATUS_A_VALIDER => 'À valider',
            self::STATUS_VALIDE => 'Validé',
            self::STATUS_PUBLIE => 'Publié',
        ][$this->status];
    }

    /**
     * Set datePublication.
     *
     * @param \DateTime|null $datePublication date publication article
     *
     * @return Article
     */
    public function setDatePublication($datePublication = null)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication.
     *
     * @return \DateTime|null
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * Add comment.
     *
     * @param \App\Entity\Commentaire $comment Object commentaire
     *
     * @return Article
     */
    public function addComment(\App\Entity\Commentaire $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment.
     *
     * @param \App\Entity\Commentaire $comment Entity comment
     *
     * @return bool TRUE if this collection contained the specified
     */
    public function removeComment(\App\Entity\Commentaire $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * Get comments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set titre.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->titre;
    }

    /**
     * Get fullContent
     *
     * @return string
     *
     * @Serialize\VirtualProperty
     * @Serialize\SerializedName("fullContent")
     */
    public function getFullContent()
    {
        if (!$this->fullContent) {
            return false;
        }

        return true;
    }

    /**
     * Set fullContent
     *
     * @param boolean $fullContent fullContent
     *
     * @return Article
     */
    public function setFullContent($fullContent = true)
    {
        $this->fullContent = $fullContent;

        return $this;
    }

    /**
     * Ajoute la classe "twitter-tweet" à tous les blockquote
     * Fix parce que ck vire les classes à la sauvegarde
     *
     * @return null
     */
    public function restoreTwitterBlockquote()
    {
        $this->setContenu(
            str_replace(
                '<blockquote>',
                '<blockquote class="twitter-tweet">',
                $this->getContenu()
            )
        );
    }

    /**
     * Add uneArticle.
     *
     * @param \App\Entity\UneArticle $uneArticle
     *
     * @return Article
     */
    public function addUneArticle(\App\Entity\UneArticle $uneArticle)
    {
        $this->uneArticle[] = $uneArticle;

        return $this;
    }

    /**
     * Remove uneArticle.
     *
     * @param \App\Entity\UneArticle $uneArticle
     */
    public function removeUneArticle(\App\Entity\UneArticle $uneArticle)
    {
        $this->uneArticle->removeElement($uneArticle);
    }

    /**
     * Get uneArticle.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUneArticle()
    {
        return $this->uneArticle;
    }

    /**
     * Set dateDebutEvent
     *
     * @param \DateTime $dateDebutEvent
     *
     * @return Article
     */
    public function setDateDebutEvent($dateDebutEvent)
    {
        $this->dateDebutEvent = $dateDebutEvent;

        return $this;
    }

    /**
     * Get dateDebutEvent
     *
     * @return \DateTime
     */
    public function getDateDebutEvent()
    {
        return $this->dateDebutEvent;
    }

    /**
     * Set dateFinEvent
     *
     * @param \DateTime $dateFinEvent
     *
     * @return Article
     */
    public function setDateFinEvent($dateFinEvent)
    {
        $this->dateFinEvent = $dateFinEvent;

        return $this;
    }

    /**
     * Get dateFinEvent
     *
     * @return \DateTime
     */
    public function getDateFinEvent()
    {
        return $this->dateFinEvent;
    }

    /**
     * Set lieuEvent
     *
     * @param string $lieuEvent
     *
     * @return Article
     */
    public function setLieuEvent($lieuEvent)
    {
        $this->lieuEvent = $lieuEvent;

        return $this;
    }

    /**
     * Get lieuEvent
     *
     * @return string
     */
    public function getLieuEvent()
    {
        return $this->lieuEvent;
    }



    /**
     * Set videoArticle
     *
     * @param \App\Entity\VideoArticle $videoArticle
     *
     * @return Article
     */
    public function setVideoArticle(\App\Entity\VideoArticle $videoArticle = null)
    {
        if($videoArticle != null) {
            $this->videoArticle = $videoArticle;
            return $this;
        }

    }

    /**
     * Get videoArticle
     *
     * @return \App\Entity\VideoArticle
     */
    public function getVideoArticle()
    {
        return $this->videoArticle;
    }

    public function setUrlExist(bool $val)
    {
        $this->urlExist = $val;

        return $this;
    }

    /**
     * Add revue
     *
     * @param \App\Entity\Revue $revue
     *
     * @return Article
     */
    public function addRevue(\App\Entity\Revue $revue)
    {
        $this->revue[] = $revue;
        $revue->addArticle($this);

        return $this;
    }

    /**
     * Remove revue
     *
     * @param \App\Entity\Revue $revue
     */
    public function removeRevue(\App\Entity\Revue $revue)
    {
        $this->revue->removeElement($revue);
        $revue->removeArticle($this);

    }

    /**
     * Get revue
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRevue()
    {
        return $this->revue;
    }

    /**
     * @return string
     */
    public function getFeedItemTitle()
    {
        return $this->titre;
    }

    /**
     * @return string
     */
    public function getFeedItemDescription()
    {
        return $this->resume;
    }

    /**
     * @return \DateTime
     */
    public function getFeedItemPubDate()
    {
        return $this->datePublication;
    }

    /**
     * @return string
     */
    public function getFeedItemLink()
    {
        return "http://www.laprofessioncomptable.com/article/".$this->getCategorie()->getSlug()."/".$this->slug;
    }

    /**
     * Set thumbnails.
     *
     * @param string|null $thumbnails
     *
     * @return Article
     */
    public function setThumbnails($thumbnails = null)
    {
        $this->thumbnails = $thumbnails;

        return $this;
    }

    /**
     * Get thumbnails.
     *
     * @return string|null
     */
    public function getThumbnails()
    {
        return $this->thumbnails;
    }

    /**
     * Set homePage.
     *
     * @param bool|null $homePage
     *
     * @return Article
     */
    public function setHomePage($homePage = null)
    {
        $this->homePage = $homePage;

        return $this;
    }

    /**
     * Get homePage.
     *
     * @return bool|null
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * Set revuePage.
     *
     * @param bool|null $revuePage
     *
     * @return Article
     */
    public function setRevuePage($revuePage = null)
    {
        $this->revuePage = $revuePage;

        return $this;
    }

    /**
     * Get revuePage.
     *
     * @return bool|null
     */
    public function getRevuePage()
    {
        return $this->revuePage;
    }

    /**
     * Add positionArticle.
     *
     * @param \App\Entity\PositionArticleRevue $positionArticle
     *
     * @return Article
     */
    public function addPositionArticle(\App\Entity\PositionArticleRevue $positionArticle)
    {
        $this->positionArticle[] = $positionArticle;

        return $this;
    }

    /**
     * Remove positionArticle.
     *
     * @param \App\Entity\PositionArticleRevue $positionArticle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePositionArticle(\App\Entity\PositionArticleRevue $positionArticle)
    {
        return $this->positionArticle->removeElement($positionArticle);
    }

    /**
     * Get positionArticle.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositionArticle()
    {
        return $this->positionArticle;
    }

    /**
     * Set podcast.
     *
     * @param string|null $podcast
     *
     * @return Article
     */
    public function setPodcast($podcast = null)
    {
        $this->podcast = $podcast;

        return $this;
    }

    /**
     * Get podcast.
     *
     * @return string|null
     */
    public function getPodcast()
    {
        return $this->podcast;
    }

    /**
     * Set podcastFile.
     *
     * @param File|null $podcast object poscast
     *
     * @return string
     */
    public function setPodcastFile(File $podcast = null)
    {
        $this->podcastFile = $podcast;
        if ($podcast) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get podcastFile.
     *
     * @return File
     */
    public function getPodcastFile()
    {
        return $this->podcastFile;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\User|null $user
     *
     * @return Article
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function addCategorie(\App\Entity\Categorie $categorie)
    {
        $this->categorie[] = $categorie;

        return $this;
    }

    public function addType($type)
    {
        $this->type[] = $type;

        return $this;
    }

    public function addStatus($status)
    {
        $this->status[] = $status;

        return $this;
    }

    public function addAuteur($auteur)
    {
        $this->auteur[] = $auteur;

        return $this;
    }



    /**
     * Set archive.
     *
     * @param \App\Entity\Archive|null $archive
     *
     * @return Article
     */
    public function setArchive(\App\Entity\Archive $archive = null)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive.
     *
     * @return \App\Entity\Archive|null
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set contenuCache.
     *
     * @param string $contenuCache contenu article
     *
     */
    public function setContenuCache($contenuCache)
    {
        $this->contenuCache = $contenuCache;

        return $this;
    }

    /**
     * Get contenuCache.
     *
     * @return string
     */
    public function getContenuCache()
    {
        return $this->contenuCache;
    }

    /**
     * Set linkCalameo.
     *
     * @param string|null $linkCalameo
     *
     * @return Article
     */
    public function setLinkCalameo($linkCalameo = null)
    {
        $this->linkCalameo = $linkCalameo;

        return $this;
    }

    /**
     * Get linkCalameo.
     *
     * @return string|null
     */
    public function getLinkCalameo()
    {
        return $this->linkCalameo;
    }

    /**
     * @return \DateTime|null
     */
    public function getTimePublication()
    {
        return $this->timePublication;
    }

    /**
     * @param null $timePublication
     * @return $this
     */
    public function setTimePublication($timePublication = null)
    {
        $this->timePublication = $timePublication;

        return $this;
    }
}
