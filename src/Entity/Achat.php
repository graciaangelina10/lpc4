<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serialize;

/**
 * @ORM\Entity
 * @ORM\Table(name="achat")
 */
class Achat
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="achatList", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Produit", inversedBy="achatList", cascade={"persist"})
     * @ORM\JoinColumn(name="produit_id", referencedColumnName="id")
     */
    protected $produit;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateAchat;

    /**
     * @ORM\ManyToOne(targetEntity="Paiement", inversedBy="achatList", cascade={"persist"})
     * @ORM\JoinColumn(name="paiement_id", referencedColumnName="id")
     */
    protected $paiement;

    /**
     * @var int
     *
     * @ORM\Column(name="qtt", type="integer", nullable=true)
     */
    private $qtt;

    /**
     * @var float
     *
     * @ORM\Column(name="prixUnitaire", type="float", nullable=true)
     */
    private $prixUnitaire;

    /**
     * @var float
     *
     * @ORM\Column(name="prixTotale", type="float", nullable=true)
     */
    private $prixTotale;

    /**
     * @var float
     *
     * @ORM\Column(name="tva", type="float", nullable=true)
     */
    private $tva;

    /**
     * @var string
     *
     * @ORM\Column(name="typePaiement", type="string", nullable=true, length=55)
     */
    private $typePaiement;



    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getProduit()
    {
        return $this->produit;
    }

    public function setProduit($produit)
    {
        $this->produit = $produit;

        return $this;
    }

    public function getDateAchat()
    {
        return $this->dateAchat;
    }

    public function setDateAchat($dateAchat)
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getPaiement()
    {
        return $this->paiement;
    }

    public function setPaiement($paiement)
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * Set qtt.
     *
     * @param int $qtt
     *
     * @return Achat
     */
    public function setQtt($qtt)
    {
        $this->qtt = $qtt;

        return $this;
    }

    /**
     * Get qtt.
     *
     * @return int
     */
    public function getQtt()
    {
        return $this->qtt;
    }

    /**
     * Set prixUnitaire.
     *
     * @param float $prixUnitaire
     *
     * @return Achat
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    /**
     * Get prixUnitaire.
     *
     * @return float
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }

    /**
     * Set prixTotale.
     *
     * @param float $prixTotale
     *
     * @return Achat
     */
    public function setPrixTotale($prixTotale)
    {
        $this->prixTotale = $prixTotale;

        return $this;
    }

    /**
     * Get prixTotale.
     *
     * @return float
     */
    public function getPrixTotale()
    {
        return $this->prixTotale;
    }

    /**
     * Set tva.
     *
     * @param float $tva
     *
     * @return Achat
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva.
     *
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set typePaiement.
     *
     * @param string|null $typePaiement
     *
     * @return Achat
     */
    public function setTypePaiement($typePaiement = null)
    {
        $this->typePaiement = $typePaiement;

        return $this;
    }

    /**
     * Get typePaiement.
     *
     * @return string|null
     */
    public function getTypePaiement()
    {
        return $this->typePaiement;
    }
}
