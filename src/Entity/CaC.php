<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaC
 *
 * @ORM\Table(name="ca_c")
 * @ORM\Entity(repositoryClass="App\Repository\CaCRepository")
 */
class CaC
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=10)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="caLettre", type="string", length=255, nullable=true)
     */
    private $caLettre;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="cas")
     */
    private $cabinet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return CaC
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set caLettre.
     *
     * @param string|null $caLettre
     *
     * @return CaC
     */
    public function setCaLettre($caLettre = null)
    {
        $this->caLettre = $caLettre;

        return $this;
    }

    /**
     * Get caLettre.
     *
     * @return string|null
     */
    public function getCaLettre()
    {
        return $this->caLettre;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }
}
