<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cabinet
 *
 * @ORM\Table(name="cabinet")
 * @ORM\Entity(repositoryClass="App\Repository\CabinetRepository")
 */
class Cabinet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codePostal", type="string", length=255, nullable=true)
     */
    private $codePostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="departement", type="string", length=255, nullable=true)
     */
    private $departement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formeJuridique", type="string", length=255, nullable=true)
     */
    private $formeJuridique;

    /**
     * @var string|null
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @var string|null
     *
     * @ORM\Column(name="siren", type="string", length=255, nullable=true)
     */
    private $siren;

    /**
     * @ORM\OneToMany(targetEntity=Ca::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $cas;

    /**
     * @ORM\OneToMany(targetEntity=CaC::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $caCs;

    /**
     * @ORM\OneToMany(targetEntity=CaExpertise::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $caExpertises;

    /**
     * @ORM\OneToMany(targetEntity=TailleCabinet::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $tailleCabinets;

    /**
     * @ORM\OneToMany(targetEntity=InformationFiliale::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $informationFiliales;

    /**
     * @ORM\OneToMany(targetEntity=Questionnaire::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $questionnaires;

    /**
     * @ORM\ManyToOne(targetEntity=EtudeNational::class, inversedBy="cabinets")
     */
    private $etude;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_president", type="string", length=255, nullable=true)
     */
    private $nomPresident;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_president", type="string", length=255, nullable=true)
     */
    private $prenomPresident;

    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=10, nullable=true)
     */
    private $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="destinataire", type="string", length=255, nullable=true)
     */
    private $destinataire;

    /**
     * @var string
     *
     * @ORM\Column(name="email_destinataire", type="string", length=255, nullable=true)
     */
    private $emailDestinataire;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_pluridiscplinaire", type="boolean", nullable=true)
     */
    private $isPluridiscplinaire;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_pluridiscplinaire", type="text", nullable=true)
     */
    private $txtPluridiscplinaire;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_reseau", type="boolean", nullable=true)
     */
    private $isReseau;

    /**
     * @ORM\OneToMany(targetEntity=Personne::class, mappedBy="cabinet", cascade={"persist", "remove"})
     */
    private $personnes;

    public function __construct()
    {
        $this->cas = new ArrayCollection();
        $this->caCs = new ArrayCollection();
        $this->caExpertises = new ArrayCollection();
        $this->tailleCabinets = new ArrayCollection();
        $this->informationFiliales = new ArrayCollection();
        $this->questionnaires = new ArrayCollection();
        $this->personnes = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Cabinet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse.
     *
     * @param string|null $adresse
     *
     * @return Cabinet
     */
    public function setAdresse($adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string|null
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville.
     *
     * @param string|null $ville
     *
     * @return Cabinet
     */
    public function setVille($ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string|null
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set codePostal.
     *
     * @param string|null $codePostal
     *
     * @return Cabinet
     */
    public function setCodePostal($codePostal = null)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal.
     *
     * @return string|null
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set departement.
     *
     * @param string|null $departement
     *
     * @return Cabinet
     */
    public function setDepartement($departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement.
     *
     * @return string|null
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set region.
     *
     * @param string|null $region
     *
     * @return Cabinet
     */
    public function setRegion($region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set telephone.
     *
     * @param string|null $telephone
     *
     * @return Cabinet
     */
    public function setTelephone($telephone = null)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Cabinet
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set site.
     *
     * @param string|null $site
     *
     * @return Cabinet
     */
    public function setSite($site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site.
     *
     * @return string|null
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set formeJuridique.
     *
     * @param string|null $formeJuridique
     *
     * @return Cabinet
     */
    public function setFormeJuridique($formeJuridique = null)
    {
        $this->formeJuridique = $formeJuridique;

        return $this;
    }

    /**
     * Get formeJuridique.
     *
     * @return string|null
     */
    public function getFormeJuridique()
    {
        return $this->formeJuridique;
    }

    /**
     * Set siret.
     *
     * @param string|null $siret
     *
     * @return Cabinet
     */
    public function setSiret($siret = null)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret.
     *
     * @return string|null
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set siren.
     *
     * @param string|null $siren
     *
     * @return Cabinet
     */
    public function setSiren($siren = null)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren.
     *
     * @return string|null
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @return Collection|Ca[]
     */
    public function getCas(): Collection
    {
        return $this->cas;
    }

    public function addCa(Ca $ca): self
    {
        if (!$this->cas->contains($ca)) {
            $this->cas[] = $ca;
            $ca->setCabinet($this);
        }

        return $this;
    }

    public function removeCa(Ca $ca): self
    {
        if ($this->cas->contains($ca)) {
            $this->cas->removeElement($ca);
            // set the owning side to null (unless already changed)
            if ($ca->getCabinet() === $this) {
                $ca->setCabinet(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }

    /**
     * @return Collection|CaC[]
     */
    public function getCaCs(): Collection
    {
        return $this->caCs;
    }

    public function addCaC(CaC $caC): self
    {
        if (!$this->caCs->contains($caC)) {
            $this->caCs[] = $caC;
            $caC->setCabinet($this);
        }

        return $this;
    }

    public function removeCaC(CaC $caC): self
    {
        if ($this->caCs->contains($caC)) {
            $this->caCs->removeElement($caC);
            // set the owning side to null (unless already changed)
            if ($caC->getCabinet() === $this) {
                $caC->setCabinet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CaExpertise[]
     */
    public function getCaExpertises(): Collection
    {
        return $this->caExpertises;
    }

    public function addCaExpertise(CaExpertise $caExpertise): self
    {
        if (!$this->caExpertises->contains($caExpertise)) {
            $this->caExpertises[] = $caExpertise;
            $caExpertise->setCabinet($this);
        }

        return $this;
    }

    public function removeCaExpertise(CaExpertise $caExpertise): self
    {
        if ($this->caExpertises->contains($caExpertise)) {
            $this->caExpertises->removeElement($caExpertise);
            // set the owning side to null (unless already changed)
            if ($caExpertise->getCabinet() === $this) {
                $caExpertise->setCabinet(null);

            }
        }

        return $this;
    }

    /**
     * @return Collection|TailleCabinet[]
     */
    public function getTailleCabinets(): Collection
    {
        return $this->tailleCabinets;
    }

    public function addTailleCabinet(TailleCabinet $tailleCabinet): self
    {
        if (!$this->tailleCabinets->contains($tailleCabinet)) {
            $this->tailleCabinets[] = $tailleCabinet;
            $tailleCabinet->setCabinet($this);
        }

        return $this;
    }

    public function removeTailleCabinet(TailleCabinet $tailleCabinet): self
    {
        if ($this->tailleCabinets->contains($tailleCabinet)) {
            $this->tailleCabinets->removeElement($tailleCabinet);
            // set the owning side to null (unless already changed)
            if ($tailleCabinet->getCabinet() === $this) {
                $tailleCabinet->setCabinet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InformationFiliale[]
     */
    public function getInformationFiliales(): Collection
    {
        return $this->informationFiliales;
    }

    public function addInformationFiliale(InformationFiliale $informationFiliale): self
    {
        if (!$this->informationFiliales->contains($informationFiliale)) {
            $this->informationFiliales[] = $informationFiliale;
            $informationFiliale->setCabinet($this);
        }

        return $this;
    }

    public function removeInformationFiliale(InformationFiliale $informationFiliale): self
    {
        if ($this->informationFiliales->contains($informationFiliale)) {
            $this->informationFiliales->removeElement($informationFiliale);
            // set the owning side to null (unless already changed)
            if ($informationFiliale->getCabinet() === $this) {
                $informationFiliale->setCabinet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Questionnaire[]
     */
    public function getQuestionnaires(): Collection
    {
        return $this->questionnaires;
    }

    public function addQuestionnaire(Questionnaire $questionnaire): self
    {
        if (!$this->questionnaires->contains($questionnaire)) {
            $this->questionnaires[] = $questionnaire;
            $questionnaire->setCabinet($this);
        }

        return $this;
    }

    public function removeQuestionnaire(Questionnaire $questionnaire): self
    {
        if ($this->questionnaires->contains($questionnaire)) {
            $this->questionnaires->removeElement($questionnaire);
            // set the owning side to null (unless already changed)
            if ($questionnaire->getCabinet() === $this) {
                $questionnaire->setCabinet(null);
            }
        }

        return $this;
    }

    public function getEtude(): ?EtudeNational
    {
        return $this->etude;
    }

    public function setEtude(?EtudeNational $etude): self
    {
        $this->etude = $etude;

        return $this;
    }

    /**
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param string $civilite
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;
    }

    /**
     * @return string
     */
    public function getNomPresident()
    {
        return $this->nomPresident;
    }

    /**
     * @param string $nomPresident
     */
    public function setNomPresident($nomPresident)
    {
        $this->nomPresident = $nomPresident;
    }

    /**
     * @return string
     */
    public function getPrenomPresident()
    {
        return $this->prenomPresident;
    }

    /**
     * @param string $prenomPresident
     */
    public function setPrenomPresident($prenomPresident)
    {
        $this->prenomPresident = $prenomPresident;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }

    /**
     * @param string $destinataire
     */
    public function setDestinataire($destinataire)
    {
        $this->destinataire = $destinataire;
    }

    /**
     * @return string
     */
    public function getEmailDestinataire()
    {
        return $this->emailDestinataire;
    }

    /**
     * @param string $emailDestinataire
     */
    public function setEmailDestinataire($emailDestinataire)
    {
        $this->emailDestinataire = $emailDestinataire;
    }

    /**
     * @param bool $isPluridiscplinaire
     */
    public function setIsPluridiscplinaire($isPluridiscplinaire)
    {
        $this->isPluridiscplinaire = $isPluridiscplinaire;
    }

    /**
     * @return bool
     */
    public function isPluridiscplinaire()
    {
        return $this->isPluridiscplinaire;
    }

    /**
     * @return string
     */
    public function getTxtPluridiscplinaire()
    {
        return $this->txtPluridiscplinaire;
    }

    /**
     * @param string $txtPluridiscplinaire
     */
    public function setTxtPluridiscplinaire($txtPluridiscplinaire)
    {
        $this->txtPluridiscplinaire = $txtPluridiscplinaire;
    }

    /**
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return bool
     */
    public function isReseau()
    {
        return $this->isReseau;
    }

    /**
     * @param bool $isReseau
     */
    public function setIsReseau($isReseau)
    {
        $this->isReseau = $isReseau;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getPersonnes(): Collection
    {
        return $this->personnes;
    }

    public function addPersonne(Personne $personne): self
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes[] = $personne;
            $personne->setCabinet($this);
        }

        return $this;
    }

    public function removePersonne(Personne $personne): self
    {
        if ($this->personnes->contains($personne)) {
            $this->personnes->removeElement($personne);
            // set the owning side to null (unless already changed)
            if ($personne->getCabinet() === $this) {
                $personne->setCabinet(null);
            }
        }

        return $this;
    }
}
