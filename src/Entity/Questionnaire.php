<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionnaire
 *
 * @ORM\Table(name="questionnaire")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireRepository")
 */
class Questionnaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="questionnaires")
     */
    private $cabinet;

    /**
     * @ORM\ManyToOne(targetEntity=EtudeNational::class, inversedBy="questionnaires")
     */
    private $etude;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasResponse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbSend;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $responseAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $destinataire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbSendRelance;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Questionnaire
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set token.
     *
     * @param string|null $token
     *
     * @return Questionnaire
     */
    public function setToken($token = null)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string|null
     */
    public function getToken()
    {
        return $this->token;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }

    public function getEtude(): ?EtudeNational
    {
        return $this->etude;
    }

    public function setEtude(?EtudeNational $etude): self
    {
        $this->etude = $etude;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getHasResponse(): ?bool
    {
        return $this->hasResponse;
    }

    public function setHasResponse(?bool $hasResponse): self
    {
        $this->hasResponse = $hasResponse;

        return $this;
    }

    public function getNbSend(): ?int
    {
        return $this->nbSend;
    }

    public function setNbSend(?int $nbSend): self
    {
        $this->nbSend = $nbSend;

        return $this;
    }

    public function getResponseAt(): ?\DateTimeInterface
    {
        return $this->responseAt;
    }

    public function setResponseAt(?\DateTimeInterface $responseAt): self
    {
        $this->responseAt = $responseAt;

        return $this;
    }

    public function getDestinataire(): ?string
    {
        return $this->destinataire;
    }

    public function setDestinataire(?string $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbSendRelance()
    {
        return $this->nbSendRelance;
    }

    /**
     * @param mixed $nbSendRelance
     */
    public function setNbSendRelance($nbSendRelance)
    {
        $this->nbSendRelance = $nbSendRelance;
    }
}
