<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serialize;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="panier")
 */
class Panier
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Produit", inversedBy="panierList")
     * @ORM\JoinTable(name="panier_produit")
     */
    protected $produitList;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="panier")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="float")
     */
    protected $prixTotal;

    /**
     * @ORM\Column(type="float")
     */
    protected $tvaTotal;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $paid;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $formerUserId;

    public function __construct()
    {
        $this->produitList = new ArrayCollection();
        $this->prixTotal = 0;
        $this->tvaTotal = 0;
        $this->paid = false;
        $this->qtt = 1;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getProduitList()
    {
        return $this->produitList;
    }

    public function setProduitList($produitList)
    {
        $this->produitList = $produitList;

        return $this;
    }

    public function contains(Produit $produit)
    {
        return $this->produitList->contains($produit);
    }

    public function addProduit(Produit $produit)
    {
        if (!$this->produitList->contains($produit)) {
            $produitPrix = $produit->getPrix();
            $this->produitList[] = $produit;
            $this->prixTotal += $produitPrix;
            $this->tvaTotal += $produit->getTvaAmount();
        }

        return $this;
    }

    public function removeProduit(Produit $produit)
    {
        $this->produitList->removeElement($produit);
        $this->prixTotal -= $produit->getPrix();
        $this->tvaTotal -= $produit->getTvaAmount();

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

    public function getNetTotal()
    {
        $total = 0;

        foreach ($this->produitList as $produit) {
            $total += $produit->getNetPrice();
        }

        return $total;
    }

    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }

    public function getTvaTotal()
    {
        return $this->tvaTotal;
    }

    public function setTvaTotal($tvaTotal)
    {
        $this->tvaTotal = $tvaTotal;

        return $this;
    }

    public function isPaid()
    {
        return $this->paid;
    }

    public function setPaid($paid = true)
    {
        $this->paid = $paid;
    }

    /**
     * Add produitList
     *
     * @param \App\Entity\Produit $produitList
     *
     * @return Panier
     */
    public function addProduitList(\App\Entity\Produit $produitList)
    {
        $this->produitList[] = $produitList;

        return $this;
    }

    public function getFormerUserId()
    {
        return $this->formerUserId;
    }

    public function setFormerUserId($formerUserId)
    {
        $this->formerUserId = $formerUserId;

        return $this;
    }

    /**
     * Remove produitList
     *
     * @param \App\Entity\Produit $produitList
     */
    public function removeProduitList(\App\Entity\Produit $produitList)
    {
        $this->produitList->removeElement($produitList);
    }

    /**
     * Get paid.
     *
     * @return bool
     */
    public function getPaid()
    {
        return $this->paid;
    }


}
