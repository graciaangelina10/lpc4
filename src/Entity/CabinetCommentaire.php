<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CabinetCommentaire
 *
 * @ORM\Table(name="cabinet_commentaire")
 * @ORM\Entity(repositoryClass="App\Repository\CabinetCommentaireRepository")
 */
class CabinetCommentaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="annee", type="string", length=10, nullable=true)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="cabinetCommentaire")
     */
    private $cabinet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string|null $annee
     *
     * @return CabinetCommentaire
     */
    public function setAnnee($annee = null)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string|null
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set commentaire.
     *
     * @param string|null $commentaire
     *
     * @return CabinetCommentaire
     */
    public function setCommentaire($commentaire = null)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire.
     *
     * @return string|null
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }
}
