<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PanierProduit
 *
 * @ORM\Table(name="panier_produit_v2")
 * @ORM\Entity(repositoryClass="App\Repository\PanierProduitRepository")
 */
class PanierProduit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qtt", type="integer")
     */
    private $qtt;

    /**
     * Object Panier
     *
     * @ORM\ManyToOne(targetEntity="Panier",cascade={"persist"})
     * @ORM\JoinColumn(name="panier_id")
     */
    private $panier;

    /**
     * Object Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit",cascade={"persist"})
     * @ORM\JoinColumn(name="produit_id")
     */
    private $produit;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qtt.
     *
     * @param int $qtt
     *
     * @return PanierProduit
     */
    public function setQtt($qtt)
    {
        $this->qtt = $qtt;

        return $this;
    }

    /**
     * Get qtt.
     *
     * @return int
     */
    public function getQtt()
    {
        return $this->qtt;
    }

    /**
     * Set panier.
     *
     * @param \App\Entity\Panier|null $panier
     *
     * @return PanierProduit
     */
    public function setPanier(\App\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier.
     *
     * @return \App\Entity\Panier|null
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Set produit.
     *
     * @param \App\Entity\Produit|null $produit
     *
     * @return PanierProduit
     */
    public function setProduit(\App\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit.
     *
     * @return \App\Entity\Produit|null
     */
    public function getProduit()
    {
        return $this->produit;
    }

    public function __construct()
    {
        $this->produit = new  \Doctrine\Common\Collections\ArrayCollection();
        $this->panier = new  \Doctrine\Common\Collections\ArrayCollection();

    }
}
