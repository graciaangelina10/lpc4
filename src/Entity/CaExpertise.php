<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaExpertise
 *
 * @ORM\Table(name="ca_expertise")
 * @ORM\Entity(repositoryClass="App\Repository\CaExpertiseRepository")
 */
class CaExpertise
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=10)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="caLettre", type="string", length=255, nullable=true)
     */
    private $caLettre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="aLettre", type="string", length=255, nullable=true)
     */
    private $aLettre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bLettre", type="string", length=255, nullable=true)
     */
    private $bLettre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cLettre", type="string", length=255, nullable=true)
     */
    private $cLettre;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="cas")
     */
    private $cabinet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return CaExpertise
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set caLettre.
     *
     * @param string|null $caLettre
     *
     * @return CaExpertise
     */
    public function setCaLettre($caLettre = null)
    {
        $this->caLettre = $caLettre;

        return $this;
    }

    /**
     * Get caLettre.
     *
     * @return string|null
     */
    public function getCaLettre()
    {
        return $this->caLettre;
    }

    /**
     * Set aLettre.
     *
     * @param string|null $aLettre
     *
     * @return CaExpertise
     */
    public function setALettre($aLettre = null)
    {
        $this->aLettre = $aLettre;

        return $this;
    }

    /**
     * Get aLettre.
     *
     * @return string|null
     */
    public function getALettre()
    {
        return $this->aLettre;
    }

    /**
     * Set bLettre.
     *
     * @param string|null $bLettre
     *
     * @return CaExpertise
     */
    public function setBLettre($bLettre = null)
    {
        $this->bLettre = $bLettre;

        return $this;
    }

    /**
     * Get bLettre.
     *
     * @return string|null
     */
    public function getBLettre()
    {
        return $this->bLettre;
    }

    /**
     * Set cLettre.
     *
     * @param string|null $cLettre
     *
     * @return CaExpertise
     */
    public function setCLettre($cLettre = null)
    {
        $this->cLettre = $cLettre;

        return $this;
    }

    /**
     * Get cLettre.
     *
     * @return string|null
     */
    public function getCLettre()
    {
        return $this->cLettre;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }
}
