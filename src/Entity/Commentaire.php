<?php
/**
 * Commentaire Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints\DateTime;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Commentaire.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @ExclusionPolicy("all")
 * 
 * @Gedmo\Tree(type="nested")
 */
class Commentaire
{
    /**
     * Integer id.
     *
     * @var int
     *
     * @ORM\Column(name="id",               type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * Entity user.
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id",     nullable=false)
     *
     * @Expose
     */
    private $user;

    /**
     * Text commentaire.
     *
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     *
     * @Expose
     */
    private $comment;

    /**
     * Boolean approuve comment.
     *
     * @var bool
     *
     * @ORM\Column(name="approuve", type="boolean")
     *
     * @Expose
     */
    private $approuve = true;

    /**
     * Entity Article.
     *
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id",     referencedColumnName="id")
     */
    private $article;

    /**
     * Date creation article.
     *
     * @var datetime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * Date mise à jour comment.
     *
     * @var datetime
     *
     * @ORM\Column(name="dateUpdate", type="datetime")
     */
    private $dateUpdate;

    /**
     * @var Commentaire
     *
     * @Gedmo\TreeParent
     *
     * @ORM\ManyToOne(targetEntity="Commentaire", inversedBy="response")
     */
    private $parent;

    /**
     * @var Commentaire
     *
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="parent")
     * @ORM\OrderBy({"treeLeft" = "ASC"})
     */
    private $response;

    /**
     * @var int
     *
     * @Gedmo\TreeLeft
     *
     * @ORM\Column(type="integer")
     */
    private $treeLeft;
    /**
     * @var int
     *
     * @Gedmo\TreeLevel
     *
     * @ORM\Column(type="integer")
     */
    private $treeLevel;
    /**
     * @var int
     *
     * @Gedmo\TreeRight
     *
     * @ORM\Column(type="integer")
     */
    private $treeRight;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment.
     *
     * @param string $comment commentaire article
     *
     * @return Commentaire
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set approuve.
     *
     * @param bool $approuve true si commentaire valide
     *
     * @return Commentaire
     */
    public function setApprouve($approuve)
    {
        $this->approuve = $approuve;

        return $this;
    }

    /**
     * Get approuve.
     *
     * @return bool
     */
    public function getApprouve()
    {
        return $this->approuve;
    }

    /**
     * Set dateCreation.
     *
     * @param \DateTime $dateCreation date creation commentaire
     *
     * @return Commentaire
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateUpdate.
     *
     * @param \DateTime $dateUpdate date mise à jour commentaire
     *
     * @return Commentaire
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate.
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\User $user user to add comment
     *
     * @return Commentaire
     */
    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set article.
     *
     * @param \App\Entity\Article|null $article article to comment
     *
     * @return Commentaire
     */
    public function setArticle(\App\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article.
     *
     * @return \App\Entity\Article|null
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Commentaire constructor.
     */
    public function __construct()
    {
        $this->setDateCreation(new \DateTime('now'));
        $this->setDateUpdate(new \DateTime('now'));
        $this->response = new ArrayCollection();
    }

    /**
     * Set auto update date.
     *
     * @ORM\PreUpdate
     *
     * @return string
     */
    public function setUpdateDateComment()
    {
        $this->setDateUpdate(new \DateTime('now'));

        return $this;
    }

    /**
     * Set parent.
     *
     * @param \App\Entity\Commentaire|null $parent
     *
     * @return Commentaire
     */
    public function setParent(\App\Entity\Commentaire $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \App\Entity\Commentaire|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add response.
     *
     * @param \App\Entity\Commentaire $response
     *
     * @return Commentaire
     */
    public function addResponse(\App\Entity\Commentaire $response)
    {
        $this->response[] = $response;

        return $this;
    }

    /**
     * Remove response.
     *
     * @param \App\Entity\Commentaire $response
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResponse(\App\Entity\Commentaire $response)
    {
        return $this->response->removeElement($response);
    }

    /**
     * Get response.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponse()
    {
        return $this->response;
    }

    public function __toString()
    {
        return (strlen(strip_tags($this->comment))>50)?substr(strip_tags($this->comment),0,50)."...":strip_tags($this->comment);
    }

    /**
     * Set treeLeft.
     *
     * @param int $treeLeft
     *
     * @return Commentaire
     */
    public function setTreeLeft($treeLeft)
    {
        $this->treeLeft = $treeLeft;

        return $this;
    }

    /**
     * Get treeLeft.
     *
     * @return int
     */
    public function getTreeLeft()
    {
        return $this->treeLeft;
    }

    /**
     * Set treeLevel.
     *
     * @param int $treeLevel
     *
     * @return Commentaire
     */
    public function setTreeLevel($treeLevel)
    {
        $this->treeLevel = $treeLevel;

        return $this;
    }

    /**
     * Get treeLevel.
     *
     * @return int
     */
    public function getTreeLevel()
    {
        return $this->treeLevel;
    }

    /**
     * Set treeRight.
     *
     * @param int $treeRight
     *
     * @return Commentaire
     */
    public function setTreeRight($treeRight)
    {
        $this->treeRight = $treeRight;

        return $this;
    }

    /**
     * Get treeRight.
     *
     * @return int
     */
    public function getTreeRight()
    {
        return $this->treeRight;
    }
}
