<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Revue
 *
 * @ORM\Table(name="revue")
 * @ORM\Entity(repositoryClass="App\Repository\RevueRepository")
 *
 * @Vich\Uploadable
 *
 * @ExclusionPolicy("all")
 */
class Revue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     * @Expose
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", nullable=true)
     * @Expose
     */
    private $descriptif;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @Expose
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=255, nullable=true)
     * @Expose
     */
    private $document;

    /**
     * @var float
     *
     * @ORM\Column(name="tarif", type="float", nullable=true)
     * @Expose
     */
    private $tarif;

    /**
     * String imageFile.
     *
     * @Assert\Image(
     *      maxSize="20M",
     *      mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/jpg"},
     *     mimeTypesMessage = "Please upload a valid Image"
     *     )
     *
     * @Vich\UploadableField(mapping="revue_images", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * Date mise à jour.
     *
     * @ORM\Column(name="updatedAt",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * String pdfFile.
     *
     * @Assert\File(
     *     maxSize = "20M",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     *
     * @Vich\UploadableField(mapping="revue_pdf", fileNameProperty="document")
     *
     * @var File
     */
    private $pdfFile;

    /**
     * Date dateCreation.
     *
     * @ORM\Column(name="dateCreation",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * Variable article.
     *
     * @ORM\ManyToMany(targetEntity="Article", inversedBy="revue",cascade={"persist"})
     * @ORM\JoinTable(name="revue_article")
     * @Expose
     */
    private $article;

    /**
     * @var int
     *
     * @ORM\Column(name="isCheck", type="boolean", nullable=true)
     * @Expose
     */
    private $isCheck = false;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=55)
     * @Expose
     */
    private $numero;

    /**
     * Object SommaireRevue
     *
     * @ORM\OneToMany(targetEntity="SommaireRevue", mappedBy="revue",cascade={"remove","persist"})
     * @Expose
     */
    private $sommaire;

    /**
     * String slug.
     *
     * @Gedmo\Slug(fields={"titre"})
     *
     * @ORM\Column(length=255, unique=true)
     * @Expose
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @Expose
     */
    private $active = false;

    /**
     * Entity PositionArticleRevue.
     *
     * @ORM\OneToMany(targetEntity="PositionArticleRevue", mappedBy="revue")
     */
    protected $positionArticleRevue;

    /**
     * @var string
     * @ORM\Column(name="link_calameo", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $linkCalameo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Revue
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set descriptif
     *
     * @param string $descriptif
     *
     * @return Revue
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;

        return $this;
    }

    /**
     * Get descriptif
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Revue
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set document
     *
     * @param string $document
     *
     * @return Revue
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set tarif
     *
     * @param float $tarif
     *
     * @return Revue
     */
    public function setTarif($tarif)
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * Get tarif
     *
     * @return float
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Revue
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set imageFile.
     *
     * @param File|null $image object image
     *
     * @return string
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get imageFile.
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set pdfFile.
     *
     * @param File|null $document object document
     *
     * @return string
     */
    public function setPdfFile(File $document = null)
    {
        $this->pdfFile = $document;
        if ($document) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get pdfFile.
     *
     * @return File
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Revue
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->article = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->sommaire = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add article
     *
     * @param \App\Entity\Article $article
     *
     * @return Revue
     */
    public function addArticle(\App\Entity\Article $article)
    {
        $this->article[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \App\Entity\Article $article
     */
    public function removeArticle(\App\Entity\Article $article)
    {
        $this->article->removeElement($article);

    }

    /**
     * Get article
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->titre;
    }

    /**
     * Set isCheck
     *
     * @param boolean $isCheck
     *
     * @return Revue
     */
    public function setIsCheck($isCheck)
    {
        $this->isCheck = $isCheck;

        return $this;
    }

    /**
     * Get isCheck
     *
     * @return boolean
     */
    public function getIsCheck()
    {
        return $this->isCheck;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Revue
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Add sommaire
     *
     * @param \App\Entity\SommaireRevue $sommaire
     *
     * @return Revue
     */
    public function addSommaire(\App\Entity\SommaireRevue $sommaire)
    {
        $this->sommaire[] = $sommaire;
        $sommaire->setRevue($this);

        return $this;
    }

    /**
     * Remove sommaire
     *
     * @param \App\Entity\SommaireRevue $sommaire
     */
    public function removeSommaire(\App\Entity\SommaireRevue $sommaire)
    {
        $this->sommaire->removeElement($sommaire);


    }

    /**
     * Get sommaire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSommaire()
    {
        return $this->sommaire;
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Revue
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Revue
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    

    /**
     * Add positionArticleRevue.
     *
     * @param \App\Entity\PositionArticleRevue $positionArticleRevue
     *
     * @return Revue
     */
    public function addPositionArticleRevue(\App\Entity\PositionArticleRevue $positionArticleRevue)
    {
        $this->positionArticleRevue[] = $positionArticleRevue;

        return $this;
    }

    /**
     * Remove positionArticleRevue.
     *
     * @param \App\Entity\PositionArticleRevue $positionArticleRevue
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePositionArticleRevue(\App\Entity\PositionArticleRevue $positionArticleRevue)
    {
        return $this->positionArticleRevue->removeElement($positionArticleRevue);
    }

    /**
     * Get positionArticleRevue.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositionArticleRevue()
    {
        return $this->positionArticleRevue;
    }

    /**
     * Set linkCalameo.
     *
     * @param string|null $linkCalameo
     *
     * @return Revue
     */
    public function setLinkCalameo($linkCalameo = null)
    {
        $this->linkCalameo = $linkCalameo;

        return $this;
    }

    /**
     * Get linkCalameo.
     *
     * @return string|null
     */
    public function getLinkCalameo()
    {
        return $this->linkCalameo;
    }
}
