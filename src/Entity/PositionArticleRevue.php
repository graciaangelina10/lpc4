<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PositionArticleRevue
 *
 * @ORM\Table(name="position_article_revue")
 * @ORM\Entity(repositoryClass="App\Repository\PositionArticleRevueRepository")
 */
class PositionArticleRevue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Entity Article
     *
     * @ORM\ManyToOne(targetEntity="Article",cascade={"persist"}, inversedBy="positionArticle")
     */
    private $article;

    /**
     * Entity Revue
     *
     * @ORM\ManyToOne(targetEntity="Revue",cascade={"persist"}, inversedBy="positionArticleRevue")
     */
    private $revue;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return PositionArticleRevue
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set article.
     *
     * @param \App\Entity\Article|null $article
     *
     * @return PositionArticleRevue
     */
    public function setArticle(\App\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article.
     *
     * @return \App\Entity\Article|null
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set revue.
     *
     * @param \App\Entity\Revue|null $revue
     *
     * @return PositionArticleRevue
     */
    public function setRevue(\App\Entity\Revue $revue = null)
    {
        $this->revue = $revue;

        return $this;
    }

    /**
     * Get revue.
     *
     * @return \App\Entity\Revue|null
     */
    public function getRevue()
    {
        return $this->revue;
    }
}
