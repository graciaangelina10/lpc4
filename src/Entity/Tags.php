<?php
/**
 * Tags Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Tags  Class Doc Comment.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity(repositoryClass="App\Repository\TagsRepository")
 *
 * @UniqueEntity(fields="nom", message="Ce tag existe déjà en base!!")
 *
 * @ExclusionPolicy("all")
 */
class Tags
{
    /**
     * Description class Tags.
     *
     * @var int
     *
     * @ORM\Column(name="id",               type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Variable nom.
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     *
     * @Expose
     */
    private $nom;

    /**
     * Variable articles.
     *
     * @ORM\ManyToMany(targetEntity="Article", inversedBy="tags",cascade={"persist"})
     * @ORM\JoinTable(name="tags_article")
     */
    private $articles;

    /**
     * @var int
     *
     * @ORM\Column(name="home", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $home = false;

    /**
     * @var int
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     *
     * @Expose
     */
    private $position;

    /**
     * String slug.
     *
     * @Gedmo\Slug(fields={"nom"})
     *
     * @ORM\Column(length=255, unique=true)
     *
     * @Expose
     */
    private $slug;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom name of tag
     *
     * @return Tags
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add article.
     *
     * @param \App\Entity\Article $article instance of Article
     *
     * @return Tags
     */
    public function addArticle(\App\Entity\Article $article)
    {
        $this->articles[] = $article;
        if (!$article->getTags()->contains($this)) {
            $article->addTag($this);
        }

        return $this;
    }

    /**
     * Remove article.
     *
     * @param \App\Entity\Article $article instance of Article
     *
     * @return bool TRUE if this collection contained the specified element
     */
    public function removeArticle(\App\Entity\Article $article)
    {
        return $this->articles->removeElement($article);
    }

    /**
     * Get articles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set __toString.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Set home.
     *
     * @param bool $home
     *
     * @return Tags
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home.
     *
     * @return bool
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Tags
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Tags
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
