<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * VideoArticle
 *
 * @ORM\Table(name="video_article")
 * @ORM\Entity(repositoryClass="App\Repository\VideoArticleRepository")
 *
 * @ExclusionPolicy("all")
 */
class VideoArticle
{
    const TYPE_VIMEO = 'Vimeo';
    const TYPE_YOU_TUBE = 'Youtube';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $lien;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime", nullable=true)
     *
     * @Expose
     */
    private $dateCreation;


    /**
     * object Article
     *
     * @ORM\OneToOne(targetEntity="Article", cascade={"persist"}, inversedBy="videoArticle")
     * @ORM\JoinColumn(onDelete="CASCADE",   nullable=false)
     *
     * @Expose
     */
    private $article;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lien
     *
     * @param string $lien
     *
     * @return VideoArticle
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return VideoArticle
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return VideoArticle
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set article
     *
     * @param \App\Entity\Article $article
     *
     * @return VideoArticle
     */
    public function setArticle(\App\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \App\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->dateCreation = new \DateTime('now');
    }
}
