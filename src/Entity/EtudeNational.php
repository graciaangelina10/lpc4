<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EtudeNational
 *
 * @ORM\Table(name="etude_national")
 * @ORM\Entity(repositoryClass="App\Repository\EtudeNationalRepository")
 */
class EtudeNational
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=10)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titre1", type="string", length=255, nullable=true)
     */
    private $titre1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titre2", type="string", length=255, nullable=true)
     */
    private $titre2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptif", type="text", nullable=true)
     */
    private $descriptif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptif1", type="text", nullable=true)
     */
    private $descriptif1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptif2", type="text", nullable=true)
     */
    private $descriptif2;

    /**
     * @ORM\OneToMany(targetEntity=Questionnaire::class, mappedBy="etude")
     */
    private $questionnaires;

    /**
     * @ORM\OneToMany(targetEntity=Cabinet::class, mappedBy="etude")
     */
    private $cabinets;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     *
     */
    private $isActive;

    /**
     *
     * @ORM\Column(name="date_cloture", type="date", nullable=true)
     *
     */
    private $dateCloture;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titre3", type="string", length=255, nullable=true)
     */
    private $titre3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptif3", type="text", nullable=true)
     */
    private $descriptif3;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_reseau", type="boolean", nullable=true)
     */
    private $isReseau;

    public function __construct()
    {
        $this->questionnaires = new ArrayCollection();
        $this->cabinets = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return EtudeNational
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set titre.
     *
     * @param string|null $titre
     *
     * @return EtudeNational
     */
    public function setTitre($titre = null)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string|null
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return Collection|Questionnaire[]
     */
    public function getQuestionnaires(): Collection
    {
        return $this->questionnaires;
    }

    public function addQuestionnaire(Questionnaire $questionnaire): self
    {
        if (!$this->questionnaires->contains($questionnaire)) {
            $this->questionnaires[] = $questionnaire;
            $questionnaire->setEtude($this);
        }

        return $this;
    }

    public function removeQuestionnaire(Questionnaire $questionnaire): self
    {
        if ($this->questionnaires->contains($questionnaire)) {
            $this->questionnaires->removeElement($questionnaire);
            // set the owning side to null (unless already changed)
            if ($questionnaire->getEtude() === $this) {
                $questionnaire->setEtude(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param string|null $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return string|null
     */
    public function getTitre1()
    {
        return $this->titre1;
    }

    /**
     * @param string|null $titre1
     */
    public function setTitre1($titre1)
    {
        $this->titre1 = $titre1;
    }

    /**
     * @return string|null
     */
    public function getDescriptif1()
    {
        return $this->descriptif1;
    }

    /**
     * @param string|null $descriptif1
     */
    public function setDescriptif1($descriptif1)
    {
        $this->descriptif1 = $descriptif1;
    }

    /**
     * @return string|null
     */
    public function getDescriptif2()
    {
        return $this->descriptif2;
    }

    /**
     * @param string|null $descriptif2
     */
    public function setDescriptif2($descriptif2)
    {
        $this->descriptif2 = $descriptif2;
    }

    /**
     * @return string|null
     */
    public function getTitre2()
    {
        return $this->titre2;
    }

    /**
     * @param string|null $titre2
     */
    public function setTitre2($titre2)
    {
        $this->titre2 = $titre2;
    }

    /**
     * @return Collection|Cabinet[]
     */
    public function getCabinets(): Collection
    {
        return $this->cabinets;
    }

    public function addCabinet(Cabinet $cabinet): self
    {
        if (!$this->cabinets->contains($cabinet)) {
            $this->cabinets[] = $cabinet;
            $cabinet->setEtude($this);
        }

        return $this;
    }

    public function removeCabinet(Cabinet $cabinet): self
    {
        if ($this->cabinets->contains($cabinet)) {
            $this->cabinets->removeElement($cabinet);
            // set the owning side to null (unless already changed)
            if ($cabinet->getEtude() === $this) {
                $cabinet->setEtude(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if ($this->isReseau) {
            return 'Etude réseau national '.$this->annee;
        }
        return 'Etude cabinet national '.$this->annee;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getDateCloture()
    {
        return $this->dateCloture;
    }

    /**
     * @param mixed $dateCloture
     */
    public function setDateCloture($dateCloture)
    {
        $this->dateCloture = $dateCloture;
    }

    /**
     * @return string|null
     */
    public function getDescriptif3()
    {
        return $this->descriptif3;
    }

    /**
     * @param string|null $descriptif3
     */
    public function setDescriptif3($descriptif3)
    {
        $this->descriptif3 = $descriptif3;
    }

    /**
     * @return string|null
     */
    public function getTitre3()
    {
        return $this->titre3;
    }

    /**
     * @param string|null $titre3
     */
    public function setTitre3($titre3)
    {
        $this->titre3 = $titre3;
    }

    /**
     * @return bool
     */
    public function isReseau()
    {
        return $this->isReseau;
    }

    /**
     * @param bool $isReseau
     */
    public function setIsReseau($isReseau)
    {
        $this->isReseau = $isReseau;
    }
}
