<?php
/**
 * UneArticle Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Entity
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * UneArticle
 *
 * @category Class
 *
 * @package App\Entity
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link                                                                    ****
 * @ORM\Table(name="uneArticle")
 * @ORM\Entity(repositoryClass="App\Repository\UneArticleRepository")
 *
 * @UniqueEntity(fields="article", message="Cet article existe déjà sur la liste !!")
 *
 * @ExclusionPolicy("all")
 */
class UneArticle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var int
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=false)
     *
     * @Expose
     */
    private $position;

    /**
     * object Article
     *
     * @ORM\ManyToOne(targetEntity="Article", cascade={"persist"}, inversedBy="uneArticle")
     *
     * @Expose
     */
    private $article;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return UneArticle
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set article
     *
     * @param \App\Entity\Article $article
     *
     * @return UneArticle
     */
    public function setArticle(\App\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \App\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }
}
