<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Table(name="abonnement")
 * @ORM\Entity
 */
class Abonnement
{
    const YEARLY_PRICE = 100;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateStart;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="abonnementList", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="PaiementAbonnement", mappedBy="abonnement", cascade={"persist"})
     */
    protected $paiementAbonnementList;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $approvalUrl;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $agreed;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $authorized;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $paid;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $agreementToken;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $agreementPaypalId;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $prix;

    /**
     * @var int
     *
     * @ORM\Column(name="qtt", type="integer", nullable=true)
     */
    private $qtt;

    public function __construct()
    {
        $this->agreed = false;
        $this->authorized = false;
        $this->paid = false;
        $this->paiementAbonnementList = new ArrayCollection();
        $this->qtt = 1;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getApprovalUrl()
    {
        return $this->approvalUrl;
    }

    public function setApprovalUrl($approvalUrl)
    {
        $this->approvalUrl = $approvalUrl;

        return $this;
    }

    public function getPaid()
    {
        return $this->paid;
    }

    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    public function getAuthorized()
    {
        return $this->authorized;
    }

    public function setAuthorized($authorized)
    {
        $this->authorized = $authorized;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getAgreementToken()
    {
        return $this->agreementToken;
    }

    public function setAgreementToken($agreementToken)
    {
        $this->agreementToken = $agreementToken;

        return $this;
    }

    public function getAgreementPaypalId()
    {
        return $this->agreementPaypalId;
    }

    public function setAgreementPaypalId($agreementPaypalId)
    {
        $this->agreementPaypalId = $agreementPaypalId;

        return $this;
    }

    public function isActive()
    {
        return $this->agreed;
    }

    public function getDateStart()
    {
        return $this->dateStart;
    }

    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getAgreed()
    {
        return $this->agreed;
    }

    public function setAgreed($agreed)
    {
        $this->agreed = $agreed;

        return $this;
    }

    /**
     * Set prix.
     *
     * @param float $prix
     *
     * @return Abonnement
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix.
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Add paiementAbonnementList.
     *
     * @param \App\Entity\PaiementAbonnement $paiementAbonnementList
     *
     * @return Abonnement
     */
    public function addPaiementAbonnementList(\App\Entity\PaiementAbonnement $paiementAbonnementList)
    {
        $this->paiementAbonnementList[] = $paiementAbonnementList;

        return $this;
    }

    /**
     * Remove paiementAbonnementList.
     *
     * @param \App\Entity\PaiementAbonnement $paiementAbonnementList
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePaiementAbonnementList(\App\Entity\PaiementAbonnement $paiementAbonnementList)
    {
        return $this->paiementAbonnementList->removeElement($paiementAbonnementList);
    }

    /**
     * Get paiementAbonnementList.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiementAbonnementList()
    {
        return $this->paiementAbonnementList;
    }

    /**
     * Set qtt.
     *
     * @param int $qtt
     *
     * @return Achat
     */
    public function setQtt($qtt)
    {
        $this->qtt = $qtt;

        return $this;
    }

    /**
     * Get qtt.
     *
     * @return int
     */
    public function getQtt()
    {
        return $this->qtt;
    }
}
