<?php


/**
 * Categorie Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Entity
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Categorie class.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 *
 * @ExclusionPolicy("all")
 *
 * @Gedmo\Tree(type="nested")
 */
class Categorie
{
    /**
     * Integer id.
     *
     * @var int
     *
     * @ORM\Column(name="id",               type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * String nom.
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     *
     * @Expose
     */
    private $nom;

    /**
     * @var int
     *
     * @Gedmo\TreeLeft
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $treeLeft;
    /**
     * @var int
     *
     * @Gedmo\TreeLevel
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $treeLevel;
    /**
     * @var int
     *
     * @Gedmo\TreeRight
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $treeRight;
    /**
     * @var Categorie
     *
     * @Gedmo\TreeParent
     *
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="children")
     */
    private $parent;
    /**
     * @var Categorie[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Categorie", mappedBy="parent")
     * @ORM\OrderBy({"treeLeft" = "ASC"})
     *
     * @Expose
     */
    private $children;

    /**
     * Entity Article.
     *
     * @ORM\OneToMany(targetEntity="Article", mappedBy="categorie", cascade={"persist"})
     */
    protected $articles;

    /**
     * String slug.
     *
     * @Gedmo\Slug(fields={"nom"})
     *
     * @ORM\Column(length=255, unique=true)
     *
     * @Expose
     */
    private $slug;

    /**
     * Boolean home
     *
     * @ORM\Column(name="home",type="boolean",nullable=true)
     *
     * @var bool
     */
    private $home = false;


    /**
     * Categorie constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom name of categorie
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get nom default value.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Get article.
     *
     * @return ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set Article.
     *
     * @param \App\Entity\Article $articles Article instance of object Article
     *
     * @return $this
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * Add article.
     *
     * @param \App\Entity\Article $article object Article
     *
     * @return Categorie
     */
    public function addArticle(\App\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article.
     *
     * @param \App\Entity\Article $article object Article
     *
     * @return bool TRUE if this collection contained the specified element
     */
    public function removeArticle(\App\Entity\Article $article)
    {
        return $this->articles->removeElement($article);
    }

    /**
     * Set treeLeft.
     *
     * @param int $treeLeft
     *
     * @return Categorie
     */
    public function setTreeLeft($treeLeft)
    {
        $this->treeLeft = $treeLeft;

        return $this;
    }

    /**
     * Get treeLeft.
     *
     * @return int
     */
    public function getTreeLeft()
    {
        return $this->treeLeft;
    }

    /**
     * Set treeLevel.
     *
     * @param int $treeLevel
     *
     * @return Categorie
     */
    public function setTreeLevel($treeLevel)
    {
        $this->treeLevel = $treeLevel;

        return $this;
    }

    /**
     * Get treeLevel.
     *
     * @return int
     */
    public function getTreeLevel()
    {
        return $this->treeLevel;
    }

    /**
     * Set treeRight.
     *
     * @param int $treeRight
     *
     * @return Categorie
     */
    public function setTreeRight($treeRight)
    {
        $this->treeRight = $treeRight;

        return $this;
    }

    /**
     * Get treeRight.
     *
     * @return int
     */
    public function getTreeRight()
    {
        return $this->treeRight;
    }

    /**
     * Set parent.
     *
     * @param \App\Entity\Categorie $parent
     *
     * @return Categorie
     */
    public function setParent(\App\Entity\Categorie $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \App\Entity\Categorie
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param \App\Entity\Categorie $child
     *
     * @return Categorie
     */
    public function addChild(\App\Entity\Categorie $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \App\Entity\Categorie $child
     */
    public function removeChild(\App\Entity\Categorie $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Categorie
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }



    /**
     * Set home.
     *
     * @param bool|null $home
     *
     * @return Categorie
     */
    public function setHome($home = null)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home.
     *
     * @return bool|null
     */
    public function getHome()
    {
        return $this->home;
    }
}
