<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serialize;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="paiement_abonnement")
 */
class PaiementAbonnement
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="achatList", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Abonnement", inversedBy="paiementAbonnementList", cascade={"persist"})
     * @ORM\JoinColumn(name="abonnement_id", referencedColumnName="id")
     */
    protected $abonnement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $datePaiement;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $paymentId;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $payerId;

    /**
     * @ORM\Column(name="nom", type="string", nullable= true )
     */
    private $nom;

    /**
     * @ORM\Column(name="prenom", type="string", nullable= true )
     */
    private $prenom;

    /*
     * @ORM\Column(name="profession", type="string", nullable= true )
     */
    private $profession;

    /**
     * @ORM\Column(name="societe", type="string", nullable= true )
     */
    private $societe;

    /**
     * @ORM\Column(name="codePostal", type="string", nullable= true )
     */
    private $codePostal;

    /**
     * @ORM\Column(name="ville", type="string", nullable= true )
     */
    private $ville;

    /**
     * @ORM\Column(name="telephone", type="string", nullable= true )
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $adresseFacturation1;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $adresseFacturation2;

    /**
     * @ORM\Column(name="adresseLivraison1", type="string", nullable= true )
     */
    private $adresseLivraison1;

    /**
     * @ORM\Column(name="adresseLivraison2", type="string", nullable= true )
     */
    private $adresseLivraison2;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $nomLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $prenomLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $societeLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $codePostalLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $villeLivraison;

    /**
     * @ORM\Column(type="float")
     */
    private $amountPaid;

    public function __construct()
    {
        $this->achatList = new ArrayCollection();
        $this->amountPaid = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getDateAchat()
    {
        return $this->dateAchat;
    }

    public function setDateAchat($dateAchat)
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getPaymentId()
    {
        return $this->paymentId;
    }

    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    public function getPayerId()
    {
        return $this->payerId;
    }

    public function setPayerId($payerId)
    {
        $this->payerId = $payerId;

        return $this;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getPanier()
    {
        return $this->panier;
    }

    public function setPanier($panier)
    {
        $this->panier = $panier;

        return $this;
    }

    public function getProfession()
    {
        return $this->profession;
    }

    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    public function getSociete()
    {
        return $this->societe;
    }

    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    public function getCodePostal()
    {
        return $this->codePostal;
    }

    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille()
    {
        return $this->ville;
    }

    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresseFacturation1()
    {
        return $this->adresseFacturation1;
    }

    public function setAdresseFacturation1($adresseFacturation1)
    {
        $this->adresseFacturation1 = $adresseFacturation1;

        return $this;
    }

    public function getAdresseFacturation2()
    {
        return $this->adresseFacturation2;
    }

    public function setAdresseFacturation2($adresseFacturation2)
    {
        $this->adresseFacturation2 = $adresseFacturation2;

        return $this;
    }

    public function getAdresseLivraison1()
    {
        return $this->adresseLivraison1;
    }

    public function setAdresseLivraison1($adresseLivraison1)
    {
        $this->adresseLivraison1 = $adresseLivraison1;

        return $this;
    }

    public function getAdresseLivraison2()
    {
        return $this->adresseLivraison2;
    }

    public function setAdresseLivraison2($adresseLivraison2)
    {
        $this->adresseLivraison2 = $adresseLivraison2;

        return $this;
    }

    public function getNomLivraison()
    {
        return $this->nomLivraison;
    }

    public function setNomLivraison($nomLivraison)
    {
        $this->nomLivraison = $nomLivraison;

        return $this;
    }

    public function getPrenomLivraison()
    {
        return $this->prenomLivraison;
    }

    public function setPrenomLivraison($prenomLivraison)
    {
        $this->prenomLivraison = $prenomLivraison;

        return $this;
    }

    public function getSocieteLivraison()
    {
        return $this->societeLivraison;
    }

    public function setSocieteLivraison($societeLivraison)
    {
        $this->societeLivraison = $societeLivraison;

        return $this;
    }

    public function getCodePostalLivraison()
    {
        return $this->codePostalLivraison;
    }

    public function setCodePostalLivraison($codePostalLivraison)
    {
        $this->codePostalLivraison = $codePostalLivraison;

        return $this;
    }

    public function getVilleLivraison()
    {
        return $this->villeLivraison;
    }

    public function setVilleLivraison($villeLivraison)
    {
        $this->villeLivraison = $villeLivraison;

        return $this;
    }

    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    public function getAbonnement()
    {
        return $this->abonnement;
    }

    public function setAbonnement($abonnement)
    {
        $this->abonnement = $abonnement;

        return $this;
    }

    /**
     * Set datePaiement.
     *
     * @param \DateTime|null $datePaiement
     *
     * @return PaiementAbonnement
     */
    public function setDatePaiement($datePaiement = null)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * Get datePaiement.
     *
     * @return \DateTime|null
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }
}
