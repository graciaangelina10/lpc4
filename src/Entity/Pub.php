<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pub
 *
 * @ORM\Table(name="pub")
 * @ORM\Entity(repositoryClass="App\Repository\PubRepository")
 *
 * @Vich\Uploadable
 */
class Pub
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255, nullable=true)
     */
    private $lien;

    /**
     * String imageFile.
     *
     * @Assert\Image(
     *      maxSize="20M",
     *      mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "image/jpg", "image/gif"},
     *     mimeTypesMessage = "Please upload a valid Image"
     *     )
     *
     * @Vich\UploadableField(mapping="pub_images", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * Date mise à jour.
     *
     * @ORM\Column(name="updatedAt",type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var
     * @ORM\Column(name="forAbonne", type="boolean", nullable=true)
     */
    private $forAbonnee;

    /**
     * @var
     * @ORM\Column(name="codeAdServer", type="text", nullable=true)
     */
    private $codeAdserver;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Pub
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Pub
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set lien
     *
     * @param string $lien
     *
     * @return Pub
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Pub
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pub
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set imageFile.
     *
     * @param File|null $image object image
     *
     * @return string
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get imageFile.
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set active.
     *
     * @param bool|null $active
     *
     * @return Pub
     */
    public function setActive($active = null)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set forAbonnee.
     *
     * @param bool|null $forAbonnee
     *
     * @return Pub
     */
    public function setForAbonnee($forAbonnee = null)
    {
        $this->forAbonnee = $forAbonnee;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool|null
     */
    public function getForAbonnee()
    {
        return $this->forAbonnee;
    }

    /**
     * @return mixed
     */
    public function getCodeAdserver()
    {
        return $this->codeAdserver;
    }

    /**
     * @param mixed $codeAdserver
     */
    public function setCodeAdserver($codeAdserver)
    {
        $this->codeAdserver = $codeAdserver;
    }
}
