<?php
/**
 * User Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User class.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 *
 * @license
 *
 * @link ****
 *
 * @ORM\Entity
 * @UniqueEntity(fields={"email"}, message="Cette adresse email est déjà utilisé.")
 * @ORM\Table(name="user")
 * @ExclusionPolicy("all")
 */
class User implements UserInterface
{
    const PROFESSION_COMMISAIRE_AUX_COMPTES = "Commissaire aux comptes";
    const PROFESSION_COLLABORATEUR_CABINET = "Collaborateur dans un cabinet EC / CAC (autre que EC / CAC)";
    const PROFESSION_EXPERT_COMPTABLE = "Expert-comptable / Commissaire aux comptes";
    const PROFESSION_STAGIAIRE = "Stagiaire EC / CAC";
    const PROFESSION_AUTRE = "Autre";
    /**
     * Integer id.
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * String nom.
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", nullable= true )
     * @Expose
     */
    private $nom;

    /**
     * String prenom.
     *
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", nullable= true )
     * @Expose
     */
    private $prenom;

    /**
     * @ORM\OneToOne(targetEntity="Panier", mappedBy="user")
     */
    protected $panier;

    /**
     * String profession
     *
     * @var string
     *
     * @ORM\Column(name="profession", type="string", nullable= true )
     * @Expose
     */
    private $profession;

    /**
     * String societe
     *
     * @var string
     *
     * @ORM\Column(name="societe", type="string", nullable= true )
     * @Expose
     */
    private $societe;

    /**
     * String codePostal
     *
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", nullable= true )
     * @Expose
     */
    private $codePostal;

    /**
     * String ville
     *
     * @var string
     *
     * @ORM\Column(name="ville", type="string", nullable= true )
     * @Expose
     */
    private $ville;

    /**
     * String telephone
     *
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", nullable= true )
     * @Expose
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $adresseFacturation1;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $adresseFacturation2;


    /**
     * @ORM\OneToMany(targetEntity="Achat", mappedBy="user", cascade={"persist"})
     */
    protected $achatList;

    /**
     * String adresseLivraison1
     *
     * @var string
     *
     * @ORM\Column(name="adresseLivraison1", type="string", nullable= true )
     * @Expose
     */
    private $adresseLivraison1;

    /**
     * String adresseLivraison2
     *
     * @var string
     *
     * @ORM\Column(name="adresseLivraison2", type="string", nullable= true )
     * @Expose
     */
    private $adresseLivraison2;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $nomLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $prenomLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $societeLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $codePostalLivraison;

    /**
     * @ORM\Column(type="string", nullable= true )
     */
    private $villeLivraison;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $conditionsAccepted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $newsletterSubscription;

    /**
     * @ORM\OneToMany(targetEntity="Abonnement", mappedBy="user", cascade={"persist"})
     */
    protected $abonnementList;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $paidSubscriber;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endSubscriptionDate;

    /**
     * Date creation article.
     *
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation",type="datetime",nullable=true)
     */
    private $dateCreation;

    /**
     * String profession autre
     *
     * @var string
     *
     * @ORM\Column(name="profession_autre", type="string", nullable= true )
     * @Expose
     */
    private $autreProfession;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Expose
     */
    protected $subscriptionRenewal;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isComment;

    /**
     * String lastCommande
     *
     * @var string
     *
     * @ORM\Column(name="lastCommande", type="string", nullable= true )
     * @Expose
     */
    private $lastCommande;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usernameCanonical;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailcanonical;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationToken;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $passwordRequestedAt;

    /**
     * @ORM\Column(type="json")
     */
    private $roles;

    /**
     * User constructor.
     */
    public function __construct()
    {
//        parent::__construct();
        $this->achatList = new ArrayCollection();
        $this->abonnementList = new ArrayCollection();
        $this->paidSubscriber = false;
        $this->endSubscriptionDate = null;
        $this->dateCreation = new \DateTime('now');
    }

    /**
     * Set nom.
     *
     * @param string|null $nom name of user
     *
     * @return User
     */
    public function setNom($nom = null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string|null $prenom name of prenom
     *
     * @return User
     */
    public function setPrenom($prenom = null)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string|null
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return User
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return User
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return User
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return User
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    public function getPanier()
    {
        return $this->panier;
    }

    public function setPanier($panier)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getAchatList()
    {
        return $this->achatList;
    }

    public function setAchatList($achatList)
    {
        $this->achatList = $achatList;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }



    /**
     * Set adresseLivraison1
     *
     * @param string $adresseLivraison1
     *
     * @return User
     */
    public function setAdresseLivraison1($adresseLivraison1)
    {
        $this->adresseLivraison1 = $adresseLivraison1;

        return $this;
    }

    public function getAdresseLivraison2()
    {
        return $this->adresseLivraison2;
    }

    /**
     * Get adresseLivraison1
     *
     * @return string
     */
    public function getAdresseLivraison1()
    {
        return $this->adresseLivraison1;
    }

    /**
     * Set adresseLivraison2
     *
     * @param string $adresseLivraison2
     *
     * @return User
     */
    public function setAdresseLivraison2($adresseLivraison2)
    {
        $this->adresseLivraison2 = $adresseLivraison2;

        return $this;
    }

    public function getFullName()
    {
        return $this->prenom . ' ' . $this->nom;
    }

    /**
     * Set adresseFacturation1
     *
     * @param string $adresseFacturation1
     *
     * @return User
     */
    public function setAdresseFacturation1($adresseFacturation1)
    {
        $this->adresseFacturation1 = $adresseFacturation1;

        return $this;
    }

    /**
     * Get adresseFacturation1
     *
     * @return string
     */
    public function getAdresseFacturation1()
    {
        return $this->adresseFacturation1;
    }

    /**
     * Set adresseFacturation2
     *
     * @param string $adresseFacturation2
     *
     * @return User
     */
    public function setAdresseFacturation2($adresseFacturation2)
    {
        $this->adresseFacturation2 = $adresseFacturation2;

        return $this;
    }

    /**
     * Get adresseFacturation2
     *
     * @return string
     */
    public function getAdresseFacturation2()
    {
        return $this->adresseFacturation2;
    }

    /**
     * Add achatList
     *
     * @param \App\Entity\Achat $achatList
     *
     * @return User
     */
    public function addAchatList(\App\Entity\Achat $achatList)
    {
        $this->achatList[] = $achatList;

        return $this;
    }

    /**
     * Remove achatList
     *
     * @param \App\Entity\Achat $achatList
     */
    public function removeAchatList(\App\Entity\Achat $achatList)
    {
        $this->achatList->removeElement($achatList);
    }

    public function getNomLivraison()
    {
        return $this->nomLivraison;
    }

    public function setNomLivraison($nomLivraison)
    {
        $this->nomLivraison = $nomLivraison;

        return $this;
    }

    public function getPrenomLivraison()
    {
        return $this->prenomLivraison;
    }

    public function setPrenomLivraison($prenomLivraison)
    {
        $this->prenomLivraison = $prenomLivraison;

        return $this;
    }

    public function getSocieteLivraison()
    {
        return $this->societeLivraison;
    }

    public function setSocieteLivraison($societeLivraison)
    {
        $this->societeLivraison = $societeLivraison;

        return $this;
    }

    public function getCodePostalLivraison()
    {
        return $this->codePostalLivraison;
    }

    public function setCodePostalLivraison($codePostalLivraison)
    {
        $this->codePostalLivraison = $codePostalLivraison;

        return $this;
    }

    public function getVilleLivraison()
    {
        return $this->villeLivraison;
    }

    public function setVilleLivraison($villeLivraison)
    {
        $this->villeLivraison = $villeLivraison;

        return $this;
    }

    public function getConditionsAccepted()
    {
        return $this->conditionsAccepted;
    }

    public function setConditionsAccepted($conditionsAccepted)
    {
        $this->conditionsAccepted = $conditionsAccepted;

        return $this;
    }

    public function getNewsletterSubscription()
    {
        return $this->newsletterSubscription;
    }

    public function setNewsletterSubscription($newsletterSubscription)
    {
        $this->newsletterSubscription = $newsletterSubscription;

        return $this;
    }

    public function isPaidSubscriber()
    {
        return $this->paidSubscriber;
    }

    public function getEndSubscriptionDate()
    {
        return $this->endSubscriptionDate;
    }

    public function setEndSubscriptionDate($endSubscriptionDate)
    {
        $this->endSubscriptionDate = $endSubscriptionDate;
        return $this;
    }

    public function addAbonnement(Abonnement $abonnement)
    {
        $this->abonnementList[] = $abonnement;
        return $this;
    }

    public function getPaidSubscriber()
    {
        return $this->paidSubscriber;
    }

    public function setPaidSubscriber($paidSubscriber)
    {
        $this->paidSubscriber = $paidSubscriber;

        return $this;
    }

    /**
     * Set dateCreation.
     *
     * @param \DateTime|null $dateCreation
     *
     * @return User
     */
    public function setDateCreation($dateCreation = null)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime|null
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Add abonnementList.
     *
     * @param \App\Entity\Abonnement $abonnementList
     *
     * @return User
     */
    public function addAbonnementList(\App\Entity\Abonnement $abonnementList)
    {
        $this->abonnementList[] = $abonnementList;

        return $this;
    }

    /**
     * Remove abonnementList.
     *
     * @param \App\Entity\Abonnement $abonnementList
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAbonnementList(\App\Entity\Abonnement $abonnementList)
    {
        return $this->abonnementList->removeElement($abonnementList);
    }

    /**
     * Get abonnementList.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbonnementList()
    {
        return $this->abonnementList;
    }

    /**
     * Set autreProfession.
     *
     * @param string|null $autreProfession
     *
     * @return User
     */
    public function setAutreProfession($autreProfession = null)
    {
        $this->autreProfession = $autreProfession;

        return $this;
    }

    /**
     * Get autreProfession.
     *
     * @return string|null
     */
    public function getAutreProfession()
    {
        return $this->autreProfession;
    }

    /**
     * Set subscriptionRenewal.
     *
     * @param bool|null $subscriptionRenewal
     *
     * @return User
     */
    public function setSubscriptionRenewal($subscriptionRenewal = null)
    {
        $this->subscriptionRenewal = $subscriptionRenewal;

        return $this;
    }

    /**
     * Get subscriptionRenewal.
     *
     * @return bool|null
     */
    public function getSubscriptionRenewal()
    {
        return $this->subscriptionRenewal;
    }

    /**
     * Set isComment.
     *
     * @param bool|null $isComment
     *
     * @return User
     */
    public function setIsComment($isComment = null)
    {
        $this->isComment = $isComment;

        return $this;
    }

    /**
     * Get isComment.
     *
     * @return bool|null
     */
    public function getIsComment()
    {
        return $this->isComment;
    }

    /**
     * @return string
     */
    public function getLastCommande()
    {
        return $this->lastCommande;
    }

    /**
     * @param string $lastCommande
     */
    public function setLastCommande($lastCommande)
    {
        $this->lastCommande = $lastCommande;
    }

    public function eraseCredentials()
    {

    }

    public function getUsername()
    {
        $this->username;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

public function setUsername(?string $username): self
{
    $this->username = $username;

    return $this;
}

public function getUsernameCanonical(): ?string
{
    return $this->usernameCanonical;
}

public function setUsernameCanonical(?string $usernameCanonical): self
{
    $this->usernameCanonical = $usernameCanonical;

    return $this;
}

public function getEmail(): ?string
{
    return $this->email;
}

public function setEmail(string $email): self
{
    $this->email = $email;

    return $this;
}

public function getEmailcanonical(): ?string
{
    return $this->emailcanonical;
}

public function setEmailcanonical(?string $emailcanonical): self
{
    $this->emailcanonical = $emailcanonical;

    return $this;
}

public function getEnabled(): ?bool
{
    return $this->enabled;
}

public function setEnabled(bool $enabled): self
{
    $this->enabled = $enabled;

    return $this;
}

public function setSalt(?string $salt): self
{
    $this->salt = $salt;

    return $this;
}

public function setPassword(string $password): self
{
    $this->password = $password;

    return $this;
}

public function getPlainPassword(): ?string
{
    return $this->plainPassword;
}

public function setPlainPassword(string $plainPassword): self
{
    $this->plainPassword = $plainPassword;

    return $this;
}

public function getLastLogin(): ?\DateTimeInterface
{
    return $this->lastLogin;
}

public function setLastLogin(?\DateTimeInterface $lastLogin): self
{
    $this->lastLogin = $lastLogin;

    return $this;
}

public function getConfirmationToken(): ?string
{
    return $this->confirmationToken;
}

public function setConfirmationToken(?string $confirmationToken): self
{
    $this->confirmationToken = $confirmationToken;

    return $this;
}

public function getPasswordRequestedAt(): ?\DateTimeImmutable
{
    return $this->passwordRequestedAt;
}

public function setPasswordRequestedAt(?\DateTimeImmutable $passwordRequestedAt): self
{
    $this->passwordRequestedAt = $passwordRequestedAt;

    return $this;
}

public function setRoles(array $roles): self
{
    $this->roles = $roles;

    return $this;
}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
}
