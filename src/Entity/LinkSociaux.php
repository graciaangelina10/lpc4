<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkSociaux
 *
 * @ORM\Table(name="link_sociaux")
 * @ORM\Entity(repositoryClass="App\Repository\LinkSociauxRepository")
 */
class LinkSociaux
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reseaux", type="string", length=255)
     */
    private $reseaux;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255)
     */
    private $lien;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reseaux
     *
     * @param string $reseaux
     *
     * @return LinkSociaux
     */
    public function setReseaux($reseaux)
    {
        $this->reseaux = $reseaux;

        return $this;
    }

    /**
     * Get reseaux
     *
     * @return string
     */
    public function getReseaux()
    {
        return $this->reseaux;
    }

    /**
     * Set lien
     *
     * @param string $lien
     *
     * @return LinkSociaux
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }
}
