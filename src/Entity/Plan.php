<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="plan")
 */
class Plan
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $paypalPlanId;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPaypalPlanId()
    {
        return $this->paypalPlanId;
    }

    public function setPaypalPlanId($paypalPlanId)
    {
        $this->paypalPlanId = $paypalPlanId;

        return $this;
    }

    /**
     * Set prix.
     *
     * @param float|null $prix
     *
     * @return Plan
     */
    public function setPrix($prix = null)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix.
     *
     * @return float|null
     */
    public function getPrix()
    {
        return $this->prix;
    }
}
