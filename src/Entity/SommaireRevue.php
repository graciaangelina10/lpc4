<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SommaireRevue
 *
 * @ORM\Table(name="sommaire_revue")
 * @ORM\Entity(repositoryClass="App\Repository\SommaireRevueRepository")
 */
class SommaireRevue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="text", nullable=true)
     */
    private $titre;

    /**
     * Object Revue
     *
     * @ORM\ManyToOne(targetEntity="Revue", cascade={"persist"}, inversedBy="sommaire")
     */
    private $revue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return SommaireRevue
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set revue
     *
     * @param \App\Entity\Revue $revue
     *
     * @return SommaireRevue
     */
    public function setRevue(\App\Entity\Revue $revue = null)
    {
        $this->revue = $revue;

        return $this;
    }

    /**
     * Get revue
     *
     * @return \App\Entity\Revue
     */
    public function getRevue()
    {
        return $this->revue;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->titre;
    }
}
