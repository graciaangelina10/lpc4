<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InformationFiliale
 *
 * @ORM\Table(name="information_filiale")
 * @ORM\Entity(repositoryClass="App\Repository\InformationFilialeRepository")
 */
class InformationFiliale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=10)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="juridique", type="string", length=255, nullable=true)
     */
    private $juridique;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fiscal", type="string", length=255, nullable=true)
     */
    private $fiscal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conseil", type="string", length=255, nullable=true)
     */
    private $conseil;

    /**
     * @var string|null
     *
     * @ORM\Column(name="autre", type="string", length=255, nullable=true)
     */
    private $autre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total", type="string", length=255, nullable=true)
     */
    private $total;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="cas")
     */
    private $cabinet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return InformationFiliale
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set juridique.
     *
     * @param string|null $juridique
     *
     * @return InformationFiliale
     */
    public function setJuridique($juridique = null)
    {
        $this->juridique = $juridique;

        return $this;
    }

    /**
     * Get juridique.
     *
     * @return string|null
     */
    public function getJuridique()
    {
        return $this->juridique;
    }

    /**
     * Set fiscal.
     *
     * @param string|null $fiscal
     *
     * @return InformationFiliale
     */
    public function setFiscal($fiscal = null)
    {
        $this->fiscal = $fiscal;

        return $this;
    }

    /**
     * Get fiscal.
     *
     * @return string|null
     */
    public function getFiscal()
    {
        return $this->fiscal;
    }

    /**
     * Set conseil.
     *
     * @param string|null $conseil
     *
     * @return InformationFiliale
     */
    public function setConseil($conseil = null)
    {
        $this->conseil = $conseil;

        return $this;
    }

    /**
     * Get conseil.
     *
     * @return string|null
     */
    public function getConseil()
    {
        return $this->conseil;
    }

    /**
     * Set autre.
     *
     * @param string|null $autre
     *
     * @return InformationFiliale
     */
    public function setAutre($autre = null)
    {
        $this->autre = $autre;

        return $this;
    }

    /**
     * Get autre.
     *
     * @return string|null
     */
    public function getAutre()
    {
        return $this->autre;
    }

    /**
     * Set total.
     *
     * @param string|null $total
     *
     * @return InformationFiliale
     */
    public function setTotal($total = null)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return string|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set commentaire.
     *
     * @param string|null $commentaire
     *
     * @return InformationFiliale
     */
    public function setCommentaire($commentaire = null)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire.
     *
     * @return string|null
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }
}
