<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 10/11/18
 * Time: 3:47 PM
 */

namespace App\Model;


class ImportUser
{
    private $file;

    /**
     * @param null $file
     * @return $this
     */
    public function setFile($file = null)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

}