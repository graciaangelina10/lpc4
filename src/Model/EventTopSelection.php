<?php
namespace App\Model;

use App\Entity\EventTop;
use App\Entity\Article;

class EventTopSelection
{
    protected $article1;
    protected $article2;
    protected $article3;
    protected $article4;

    /**
     * Get article1
     *
     * @return Article
     */
    public function getArticle1()
    {
        return $this->article1;
    }

    /**
     * Set article1.
     *
     * @param Article $article1 article1
     *
     * @return EventTopSelection
     */
    public function setArticle1($article1)
    {
        $this->article1 = $article1;

        return $this;
    }

    /**
     * Get article2
     *
     * @return Article
     */
    public function getArticle2()
    {
        return $this->article2;
    }

    /**
     * Set article2.
     *
     * @param Article $article2 article2
     *
     * @return EventTopSelection
     */
    public function setArticle2($article2)
    {
        $this->article2 = $article2;

        return $this;
    }

    /**
     * Get article3
     *
     * @return Article
     */
    public function getArticle3()
    {
        return $this->article3;
    }

    /**
     * Set article3.
     *
     * @param Article $article3 article3
     *
     * @return EventTopSelection
     */
    public function setArticle3($article3)
    {
        $this->article3 = $article3;

        return $this;
    }

    /**
     * Get article4
     *
     * @return Article
     */
    public function getArticle4()
    {
        return $this->article4;
    }

    /**
     * Set article4.
     *
     * @param Article $article4 article4
     *
     * @return EventTopSelection
     */
    public function setArticle4($article4)
    {
        $this->article4 = $article4;

        return $this;
    }
}
