<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * Class AfterLoginRedirection
 *
 * @package App\EventListener
 */
class AfterLogoutRedirection implements LogoutSuccessHandlerInterface
{
    private $router;

    /**
     * AfterLoginRedirection constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request        $request
     *
     * @param TokenInterface $token
     *
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        $uri = $request->get('uri');
        $redirection = new RedirectResponse($uri);

        $user = $this->tokenStorage->getToken()->getUser();

        $myfile = fopen(__DIR__."/../../../web/uploads/login.txt", "a+");
        fwrite($myfile, date("Y-m-d H:i:s").' - logout succesfully : '.$user->getEmail()."\n");
        fclose($myfile);

        return $redirection;
    }
}