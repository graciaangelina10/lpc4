<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 9/10/18
<<<<<<< HEAD
 * Time: 9:57 PM
=======
 * Time: 10:37 AM
>>>>>>> feat.abonnement.form
 */

namespace App\EventListener;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * @Service()
 */
class RequestListener
{
    private $em;
    private $token_storage;
    private $session;
    private $dispatcher;
    protected $security;
    protected $authorizationChecker;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $token_storage, SessionInterface $session, EventDispatcherInterface $dispatcher, Security $security, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->token_storage = $token_storage;
        $this->session = $session;
        $this->dispatcher = $dispatcher;
        $this->security = $security;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $ipFrom = $event->getRequest()->getClientIp();

        $ipAllowedArray = array("217.108.187.199", "217.108.187.200", "185.22.198.1"); //Old IP FUDICIAL

        $ipAllowedArray = [];

        $user = $this->token_storage->getToken()->getUser();

        $uri = $event->getRequest()->getUri();

        if ($user && $user != "anon." && !preg_match("/admin/", $uri)) {
            if (in_array('ROLE_ABONNE', $user->getRoles())) {
                $endSubscriptionDate = $user->getEndSubscriptionDate();
                if ($endSubscriptionDate < new \DateTime('now')) {

                    $roles = $user->getRoles();
                    $tabRoles = [];
                    if ($roles) {
                        foreach ($roles as $role) {
                            if ($role != "ROLE_ABONNE") {
                                $tabRoles [] = $role;
                            }
                        }
                    }

                    $user->setRoles($tabRoles);
                    $user->setPaidSubscriber(false);
                }



            }
        }

        if (in_array($ipFrom, $ipAllowedArray)) {

            $fiducial = $this->session->get('fiducial');

            if (null == $fiducial) {
                $credentials = array(
                    'username' => 'sso-fiducial@ogmyos.com',
                    'password' => 'hTa5nYKL');


                $userFind = $this->em->getRepository("App:User")->findByEmail($credentials['username']);

                if ($userFind) {
                    $this->session->set('fiducial', 1);

                    $user = $userFind[0];

                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->token_storage->setToken($token);

                    $this->session->set('_security_main', serialize($token));

                    // Fire the login event manually
                    $request = $event->getRequest();
                    $event = new InteractiveLoginEvent($request, $token);
                    $this->dispatcher->dispatch("security.interactive_login", $event);
                }
            }


        }

    }
}