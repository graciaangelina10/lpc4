<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 10/31/18
 * Time: 3:39 PM
 */

namespace App\EventListener;


use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener {

    protected $userManager;

        public function __construct(UserProviderInterface $userManager){
        $this->userManager = $userManager;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        // Enregistrer les users connectés
        $myfile = fopen(__DIR__."/../../../web/uploads/login.txt", "a+");
        fwrite($myfile, date("Y-m-d H:i:s").' - Login succesfully : '.$user->getEmail()."\n");
        fclose($myfile);
        // do something else
        // return new Response();


    }


}