<?php

/**
 * ArticlesController Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Controller\Api
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */


namespace App\Controller\Dev\Api;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ArticlesController.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 *
 * @Route("/api-dev")
 */
class ArticlesDevController extends AbstractController
{
    /**
     * Get Article.
     *
     * @param Request $request request
     *
     * @Route("/articles",name="dev_articles_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesAction(Request $request)
    {
        $limit = $request->query->get('limit');
        $page = $request->query->get('page');
        $category = $request->query->get('category');
        $types = $request->query->get('types');

        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository(Article::class)
            ->getArticles($limit, $page, $category, $types);

        if (!$articles) {
            return new JsonResponse(['status' => '404'], Response::HTTP_NOT_FOUND);
        }

        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Get Article par categorie.
     *
     * @param Request $request request
     *
     * @Route("/articles/categories",name="dev_articles_categorie_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesCategorieAction(Request $request)
    {
        $categoryID = $request->query->get('categoryID');

        $em = $this->getDoctrine()->getManager();

        $categorie = $em->getRepository('App:Categorie')->find($categoryID);
        if (!$categorie) {
            return new JsonResponse(
                ['message' => 'Categorie not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $articles = $em->getRepository('App:Article')
            ->findBy(array('categorie' => $categorie, 'status' => 'publie'));

        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Show article.
     *
     * @param int $id id_article
     *
     * @Route("/article/{id}",name="dev_fiche_article",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App:Article')->find($id);
        if (!$article) {
            return new JsonResponse(
                ['message' => 'Article not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            $this->get('jms_serializer')
                ->serialize($article, 'json')
        );
    }

    /**
     * Get categorie.
     *
     * @Route("/categories/",name="dev_categorie_list", methods={"GET"})
     *
     * @return Response
     */
    public function getCategoriesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('App:Categorie')->findAll();

        if (!$categories) {
            return new JsonResponse(
                ['message' => 'Categorie not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            $this->get('jms_serializer')
                ->serialize($categories, 'json')
        );
    }

    /**
     * Get Article par Tag.
     *
     * @param int $id id_tag
     *
     * @Route("/articles/tag/{id}",name="dev_articles_tag_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesTagAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $tag = $em->getRepository('App:Tags')->find($id);

        if (!$tag) {
            return new JsonResponse(
                ['message' => 'Tag not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $articles = $tag->getArticles();

        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Add commentaire article.
     *
     * @param Request $request request
     * @param int     $id      id_article
     *
     * @Route("/commentaire/article/{id}", name="dev_add_commentaire_article",
     *     methods={"GET","POST"})
     *
     * @return Response
     */
    public function addCommentaireArticle(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        if (null == $user) {
            return new JsonResponse(
                ['user' => 'not connect'],
                Response::HTTP_NOT_FOUND
            );
        }
        //find article
        $article = $em->getRepository('App:Article')->find($id);
        $comment = $request->get('comment');
        //Add commentaire
        $this->get('app.article.service')
            ->addComment($user, $article, $comment);

        return $this->redirectToRoute('dev_home');
    }
}
