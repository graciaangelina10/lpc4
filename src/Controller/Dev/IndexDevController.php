<?php
/**
 * IndexController File Doc Comment.
 *
 * @category IndexController
 *
 * @author  Bocasay
 * @license
 *
 * @see
 */

namespace App\Controller\Dev;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;

/**
 * @Route("/dev")
 */
class IndexDevController extends AbstractController
{
    /**
     * Route de test équivalent page d'accueil
     *
     * @return Response
     *
     * @Route("/", name="dev_home")
     */
    public function indexDevAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository(Article::class)->getArticles(10, 1);

        $categories = $em->getRepository('App:Categorie')->findAll();

        $articlesUne = $em->getRepository('App:UneArticle')->findBy(array(), array('position' => 'asc'));

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'firstArticle' => false,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
            ]
        );
    }

    /**
     * Route de test pour
     *
     * @param  string $slug slug article
     * @return Response
     *
     * @Route("/articles/{slug}", name="dev_handle_article")
     */
    public function handleArticleDevAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $articleFullContent = $em->getRepository('App:Article')->findOneBy(array('slug' => $slug));

        if (!$articleFullContent) {
            $articleFullContent = array();
        } else {
            // On clone pour bypasser le proxy doctrine.
            // Si on ne clone pas, la version de l'articel qui est dans la liste
            //  est aussi mit en mode fullContent
            $articleFullContent = clone $articleFullContent;
            $articleFullContent->setFullContent(true);
            $articleFullContent = [$articleFullContent];
        }

        $articlesAll = $em->getRepository(Article::class)->getArticles(10, 1);

        if (!$articlesAll) {
            $articlesAll = array();
        }

        $articles = @array_merge($articleFullContent, $articlesAll);

        $categories = $em->getRepository('App:Categorie')->findAll();

        $articlesUne = $em->getRepository('App:UneArticle')->findBy(array(), array('position' => 'asc'));

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
            ]
        );
    }
}
