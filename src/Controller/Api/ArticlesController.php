<?php

namespace App\Controller\Api;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ArticlesController.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @Route("/api")
 */
class ArticlesController extends AbstractController
{
    /**
     * Get Article.
     *
     * @param Request $request request
     *
     * @Route("/articles",name="articles_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesAction(Request $request)
    {
        $limit = $request->query->get('limit');
        $page = $request->query->get('page');
        $category = $request->query->get('category');
        $types = $request->query->get('types');
        $revue = $request->query->get('revue');
        $tag = $request->query->get('tag');
        $search = $request->query->get('search');
        $home = $request->query->get('home');

        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository(Article::class)
            ->getArticles($limit, $page, $category, $types,$revue, $tag, $search, $home);

        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Get Article par categorie.
     *
     * @param Request $request request
     *
     * @Route("/articles/categories",name="articles_categorie_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesCategorieAction(Request $request)
    {
        $categoryID = $request->query->get('categoryID');

        $em = $this->getDoctrine()->getManager();

        $categorie = $em->getRepository('App:Categorie')->find($categoryID);
        if (!$categorie) {
            return new JsonResponse(
                ['message' => 'Categorie not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $articles = $em->getRepository('App:Article')
            ->findBy(array('categorie' => $categorie, 'status' => 'publie'));

        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Show article.
     *
     * @param int $id id_article
     *
     * @Route("/article/{id}",name="fiche_article",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App:Article')->find($id);
        if (!$article) {
            return new JsonResponse(
                ['message' => 'Article not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            $this->get('jms_serializer')
                ->serialize($article, 'json')
        );
    }

    /**
     * Get categorie.
     *
     * @Route("/categories/",name="categorie_list", methods={"GET"})
     *
     * @return Response
     */
    public function getCategoriesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('App:Categorie')->findAll();

        if (!$categories) {
            return new JsonResponse(
                ['message' => 'Categorie not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            $this->get('jms_serializer')
                ->serialize($categories, 'json')
        );
    }

    /**
     * Get Article par Tag.
     *
     * @param int $slug slug tag
     *
     * @Route("/tag/{slug}",name="articles_tag_list", methods={"GET"})
     *
     * @return Response
     */
    public function getArticlesTagAction(Request $request, $slug)
    {
        $limit = $request->query->get('limit');
        $page = $request->query->get('page');
        $category = $request->query->get('category');
        $types = $request->query->get('types');
        $revue = $request->query->get('revue');
        $tag = $request->query->get('tag');

        $em = $this->getDoctrine()->getManager();
        $tag = $em->getRepository('App:Tags')->findBy(array("slug"=>$slug));

        if (!$tag) {
            return new JsonResponse(
                ['message' => 'Tag not found'],
                Response::HTTP_NOT_FOUND
            );
        }



        $articles = $em->getRepository(Article::class)
            ->getArticles($limit, $page, $category, $types,$revue, $slug);


        return new Response(
            $this->get('jms_serializer')
                ->serialize($articles, 'json')
        );
    }

    /**
     * Add commentaire article.
     *
     * @param Request $request request
     * @param int     $id      id_article
     *
     * @Route("/commentaire/article/{id}", name="add_commentaire_article",
     *     methods={"GET","POST"})
     *
     * @return Response
     */
    public function addCommentaireArticle(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        if (null == $user) {
            return new JsonResponse(
                ['user' => 'not connect'],
                Response::HTTP_NOT_FOUND
            );
        }
        //find article
        $article = $em->getRepository('App:Article')->find($id);
        $comment = $request->get('comment');
        //Add commentaire
        $this->get('app.article.service')
            ->addComment($user, $article, $comment);

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/articleContenu", name="article_contenu")
     * @param Request $request
     * @return string
     */
    public function getContenuArticle(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $id = $request->get("id");

        $contenu = "";

        $article = $em->getRepository("App:Article")->find($id);

        if ($article) {
            if ($article->getPremium() && (!$user || !in_array('ROLE_ABONNE', $user->getRoles()))) {
                $contenuLimite = $article->getContenu();
                $contenu = $this->truncate($contenuLimite, 1300, '.....', true, true);

            } else {
                $contenu = $article->getContenu();

            }

        }

        return new Response($contenu);

    }

    public function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false) {
        if (is_array($ending)) {
            extract($ending);
        }
        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0 , $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }

        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</'.$tag.'>';
            }
        }

        return $truncate;
    }
}
