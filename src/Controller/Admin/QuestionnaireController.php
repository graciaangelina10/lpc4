<?php

namespace App\Controller\Admin;

use App\Entity\Ca;
use App\Entity\Cabinet;
use App\Entity\CabinetCommentaire;
use App\Entity\EtudeNational;
use App\Entity\Questionnaire;
use App\Entity\TailleCabinet;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Questionnaire controller.
 *
 * @Route("admin/questionnaire")
 */
class QuestionnaireController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all questionnaire entities.
     *
     * @Route("/", name="admin_questionnaire_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etude = $em->getRepository(EtudeNational::class)->findOneBy(['isActive' => 1]);

        $statistiques = $em->getRepository(Questionnaire::class)->findBy(['etude' => $etude]);

        if (!$statistiques) {
            $cabinets = $em->getRepository(Cabinet::class)->findAll();

            foreach ($cabinets as $cabinet) {

                $length = 3;
                $token = bin2hex(random_bytes($length));

                $questionnaire = new Questionnaire();
                $questionnaire->setIsActive(true)
                    ->setCabinet($cabinet)
                    ->setEmail($cabinet->getEmailDestinataire())
                    ->setCreatedAt(new \DateTime('now'))
                    ->setCode($token)
                    ->setDestinataire($cabinet->getDestinataire())
                    ->setEtude($etude)
                    ;

                $em->persist($questionnaire);
            }
            

            $em->flush();

            $statistiques = $em->getRepository('App:Questionnaire')->findBy(['etude' => $etude]);
        }

        $annee = "";

        $annee1 = "";

        $annee2 = "";

        $tabResult = [];

        if ($etude) {
            $annee = $etude->getAnnee();

            $annee1 = $annee -1;

            $annee2 = $annee - 2;

            foreach ($statistiques as $questionnaire) {

                $cabinet = $questionnaire->getCabinet();

                $item = $annee1;

                $ca = $em->getRepository(Ca::class)->findCa($cabinet, $item);

                if ($ca) {
                    $tabResult[$questionnaire->getId()]['ca_'.$item] = $ca->getCaLettre();
                }else {
                    $tabResult[$questionnaire->getId()]['ca_'.$item] = "";
                }

                /*$cac = $em->getRepository("App:CaC")->findCaC($cabinet, $item);
                if ($cac) {
                    $tabResult[$questionnaire->getId()]['cac_'.$item] = $cac->getCaLettre();
                }else {
                    $tabResult[$questionnaire->getId()]['cac_'.$item] = "";
                }*/

                /*$caExpertise = $em->getRepository("App:CaExpertise")->findCaExpertise($cabinet, $item);
                if ($caExpertise) {
                    $tabResult[$questionnaire->getId()]['caexpertise_'.$item] = $caExpertise->getCaLettre();
                    $tabResult[$questionnaire->getId()]['caexpertiseA_'.$item] = $caExpertise->getALettre();
                    $tabResult[$questionnaire->getId()]['caexpertiseB_'.$item] = $caExpertise->getBLettre();
                    $tabResult[$questionnaire->getId()]['caexpertiseC_'.$item] = $caExpertise->getCLettre();
                }else {
                    $tabResult[$questionnaire->getId()]['caexpertise_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['caexpertiseA_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['caexpertiseB_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['caexpertiseC_'.$item] = "";
                }*/

                //Nb client
                $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);

                if ($tailleCabinet) {
                    $tabResult[$questionnaire->getId()]['nbClient_'.$item] = $tailleCabinet->getNbClient();
                    $tabResult[$questionnaire->getId()]['dateCloture_'.$item] = $tailleCabinet->getDateCloture();
                    $tabResult[$questionnaire->getId()]['effectif_'.$item] = $tailleCabinet->getEffectif();
                    $tabResult[$questionnaire->getId()]['hommeEc_'.$item] = $tailleCabinet->getNbHommeEc();
                    $tabResult[$questionnaire->getId()]['femmeEc_'.$item] = $tailleCabinet->getNbFemmeEc();
                    $tabResult[$questionnaire->getId()]['hommeAs_'.$item] = $tailleCabinet->getNbHommeAs();
                    $tabResult[$questionnaire->getId()]['femmeAs_'.$item] = $tailleCabinet->getNbFemmeAs();
                    $tabResult[$questionnaire->getId()]['nbBureau_'.$item] = $tailleCabinet->getNbBureau();
                    $tabResult[$questionnaire->getId()]['localisation_'.$item] = explode("-", $tailleCabinet->getLocalisation());
                    $tabResult[$questionnaire->getId()]['groupement_'.$item] = explode(",", $tailleCabinet->getGroupeNational());
                    $tabResult[$questionnaire->getId()]['reseau_'.$item] = explode(",", $tailleCabinet->getReseauNational());
                }else {
                    $tabResult[$questionnaire->getId()]['nbClient_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['dateCloture_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['effectif_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['hommeEc_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['femmeEc_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['hommeAs_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['femmeAs_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['nbBureau_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['localisation_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['groupement_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['reseau_'.$item] = "";
                }

                //information filiale
                /*$informationFiliale = $em->getRepository("App:InformationFiliale")->findInformation($cabinet, $item);
                if ($informationFiliale) {
                    $tabResult[$questionnaire->getId()]['juridique_'.$item] = $informationFiliale->getJuridique();
                    $tabResult[$questionnaire->getId()]['fiscal_'.$item] = $informationFiliale->getFiscal();
                    $tabResult[$questionnaire->getId()]['conseil_'.$item] = $informationFiliale->getConseil();
                    $tabResult[$questionnaire->getId()]['autre_'.$item] = $informationFiliale->getAutre();
                    $tabResult[$questionnaire->getId()]['total_'.$item] = $informationFiliale->getTotal();

                }else{
                    $tabResult[$questionnaire->getId()]['juridique_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['fiscal_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['conseil_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['autre_'.$item] = "";
                    $tabResult[$questionnaire->getId()]['total_'.$item] = "";
                }*/
            }
        }

        return $this->render('admin/questionnaire/index.html.twig', [
            'statistiques' => $statistiques,
            'etude' => $etude,
            'annee' => $annee,
            'annee1' => $annee1,
            'annee2' => $annee2,
            'tabResult' => $tabResult,
        ]);
    }

    /**
     * Creates a new questionnaire entity.
     *
     * @Route("/new", name="admin_questionnaire_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $questionnaire = new Questionnaire();
        $form = $this->createForm('App\Form\QuestionnaireType', $questionnaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($questionnaire);
            $em->flush();

            return $this->redirectToRoute('admin_questionnaire_show', array('id' => $questionnaire->getId()));
        }

        return $this->render('admin/questionnaire/new.html.twig', array(
            'questionnaire' => $questionnaire,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a questionnaire entity.
     *
     * @Route("/{id}", name="admin_questionnaire_show")
     * @Method("GET")
     */
    public function showAction(Questionnaire $questionnaire)
    {
        $deleteForm = $this->createDeleteForm($questionnaire);

        return $this->render('admin/questionnaire/show.html.twig', array(
            'questionnaire' => $questionnaire,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing questionnaire entity.
     *
     * @Route("/{id}/edit", name="admin_questionnaire_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Questionnaire $questionnaire)
    {
        $deleteForm = $this->createDeleteForm($questionnaire);
        $editForm = $this->createForm('App\Form\QuestionnaireType', $questionnaire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_questionnaire_edit', array('id' => $questionnaire->getId()));
        }

        return $this->render('admin/questionnaire/edit.html.twig', array(
            'questionnaire' => $questionnaire,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a questionnaire entity.
     *
     * @Route("/delete/{id}", name="admin_questionnaire_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Questionnaire $questionnaire)
    {
        $form = $this->createDeleteForm($questionnaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($questionnaire);
            $em->flush();
        }

        return $this->redirectToRoute('admin_questionnaire_index');
    }

    /**
     * Creates a form to delete a questionnaire entity.
     *
     * @param Questionnaire $questionnaire The questionnaire entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Questionnaire $questionnaire)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_questionnaire_delete', array('id' => $questionnaire->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @param Questionnaire $questionnaire
     * @param UserService $userService
     * @Route("/send-questionnaire/{id}", name="admin_questionnaire_send_mail")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function sendQuestionnaire(Questionnaire $questionnaire, UserService $userService, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $relance = $request->get('relance');

        //$to [] = "marcel.razafimandimby@gmail.com";

        $to = [];

        $destinataire = [];

        //$destinataire [] = "Marcel";

        $tabTo = explode(";", $questionnaire->getEmail());

        $tabDestinataire = explode(";", $questionnaire->getDestinataire());

        foreach ($tabTo as $item) {
            $to [] = trim($item);
        }

        foreach ($tabDestinataire as $item) {
            $destinataire [] = trim($item);
        }

        $userService->sendEmailQuestionnaire($to, $questionnaire, $destinataire, $relance);

        $nbSend = $questionnaire->getNbSend();

        $nbSendRelance = $questionnaire->getNbSendRelance();

        $questionnaire->setCreatedAt(new \DateTime('now'))
            ->setIsActive(true)

            ;
        if ($relance == 1) {
            $questionnaire->setNbSendRelance($nbSendRelance+1);
        }else {
            $questionnaire->setNbSend($nbSend+1);
        }

        $entityManager->persist($questionnaire);

        $entityManager->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            "Le message a été bien envoyé"
        );

        return $this->redirectToRoute('admin_questionnaire_index');

    }


    /**
     * @param Questionnaire $questionnaire
     * @param UserService $userService
     * @Route("/send-questionnaire/{id}/from-ajax", name="admin_questionnaire_send_mail_from_ajax")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function sendAllQuestionnaire(Questionnaire $questionnaire, UserService $userService)
    {
        $entityManager = $this->getDoctrine()->getManager();

        //$to [] = "marcel.razafimandimby@gmail.com";

        $to = [];

        $destinataire = [];

        //$destinataire [] = "Marcel";

        $tabTo = explode(";", $questionnaire->getEmail());

        $tabDestinataire = explode(";", $questionnaire->getDestinataire());

        foreach ($tabTo as $item) {
            $to [] = trim($item);
        }

        foreach ($tabDestinataire as $item) {
            $destinataire [] = trim($item);
        }

        $userService->sendEmailQuestionnaire($to, $questionnaire, $destinataire);

        $nbSend = $questionnaire->getNbSend();

        $questionnaire->setCreatedAt(new \DateTime('now'))
            ->setIsActive(true)
            ->setNbSend($nbSend+1)
        ;

        $entityManager->persist($questionnaire);

        $entityManager->flush();

        /*$this->get('session')->getFlashBag()->add(
            'success',
            "Le message a été bien envoyé"
        );*/

        return new Response("OK") ;

    }


    /**
     * @param Questionnaire $questionnaire
     * @param UserService $userService
     * @Route("/send-questionnaire-all/from-ajax", name="admin_questionnaire_all_send_mail_from_ajax")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function sendAllQuestionnaireCabinet(UserService $userService)
    {
        $entityManager = $this->getDoctrine()->getManager();

        //$to [] = "marcel.razafimandimby@gmail.com";

        $etude = $entityManager->getRepository("App:EtudeNational")->findOneBy(['isActive' => 1]);



        $questionnaires = $entityManager->getRepository("App:Questionnaire")->findBy(['etude' => $etude]);

        if ($questionnaires) {

            foreach ($questionnaires as $questionnaire) {

                $to = [];

                $destinataire = [];

                //$destinataire [] = "Marcel";

                if (null == $questionnaire->getEmail() || $questionnaire->getEmail() == "") {
                    continue;
                }

                $tabTo = explode(";", $questionnaire->getEmail());

                $tabDestinataire = explode(";", $questionnaire->getDestinataire());

                foreach ($tabTo as $item) {
                    $to [] = trim($item);
                }

                foreach ($tabDestinataire as $item) {
                    $destinataire [] = trim($item);
                }

                $userService->sendEmailQuestionnaire($to, $questionnaire, $destinataire);

                $nbSend = $questionnaire->getNbSend();

                $questionnaire->setCreatedAt(new \DateTime('now'))
                    ->setIsActive(true)
                    ->setNbSend($nbSend+1)
                ;

                $entityManager->persist($questionnaire);
            }

        }

        $entityManager->flush();

        /*$this->get('session')->getFlashBag()->add(
            'success',
            "Le message a été bien envoyé"
        );*/

        return new Response("OK") ;

    }

    /**
     * @Route("/export-questionnaire/from-bo", name="admin_questionnaire_export", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportQuestionnaire(UserService $userService, Request $request)
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $modele = $request->get('modele');

        $fileName = 'LPC_questionnaire_'.date("Y-m-d").'.xlsx';
        $temp_file = $userService->exportQuestionnaire($fileName, $modele);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);

    }

    /**
     * @Route("/cabinet-update-commentaire/from-bo", name="admin_cabinet_update_commentaire", methods={"GET", "POST"})
     */
    public function updateCommentaireCabinet()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $annee = "2022";

        $cabinets = $entityManager->getRepository('App:Cabinet')->findAll();

        foreach ($cabinets as $cabinet) {
            if (null != $cabinet->getCommentaire()) {
                $cabinetCommentaire = new CabinetCommentaire();
                $cabinetCommentaire->setAnnee($annee);
                $cabinetCommentaire->setCommentaire($cabinet->getCommentaire());
                $cabinetCommentaire->setCabinet($cabinet);

                $cabinet->setCommentaire(null);

                $entityManager->persist($cabinetCommentaire);

                $entityManager->persist($cabinet);
            }
        }

        $entityManager->flush();

        return new Response("OK");


    }

}
