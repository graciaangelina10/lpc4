<?php

/**
 * IndexController Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Controller\Admin
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 */
class IndexController extends AbstractController
{
    /**
     * Index Action.
     *
     * @return Response
     *
     * @Route("/admin",name="admin_accueil")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR', 'ROLE_CONTRIBUTEUR']);

        return $this->render('admin/Index/index.html.twig');
    }
}
