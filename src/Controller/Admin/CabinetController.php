<?php

namespace App\Controller\Admin;

use App\Entity\Ca;
use App\Entity\Cabinet;
use App\Entity\CaC;
use App\Entity\CaExpertise;
use App\Entity\Departement;
use App\Entity\EtudeNational;
use App\Entity\Groupe;
use App\Entity\InformationFiliale;
use App\Entity\Personne;
use App\Entity\Questionnaire;
use App\Entity\Region;
use App\Entity\Regroupement;
use App\Entity\TailleCabinet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Cabinet controller.
 *
 * @Route("admin/cabinet")
 */
class CabinetController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all cabinet entities.
     *
     * @Route("/", name="admin_cabinet_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cabinets = $em->getRepository(Cabinet::class)->findAll();

        return $this->render('admin/cabinet/index.html.twig', array(
            'cabinets' => $cabinets,
        ));
    }

    /**
     * Creates a new cabinet entity.
     *
     * @Route("/new", name="admin_cabinet_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cabinet = new Cabinet();
        $form = $this->createForm('App\Form\CabinetType', $cabinet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cabinet);
            $em->flush();

            return $this->redirectToRoute('admin_cabinet_show', array('id' => $cabinet->getId()));
        }

        return $this->render('admin/cabinet/new.html.twig', array(
            'cabinet' => $cabinet,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cabinet entity.
     *
     * @Route("/{id}", name="admin_cabinet_show")
     * @Method("GET")
     */
    public function showAction(Cabinet $cabinet)
    {
        $deleteForm = $this->createDeleteForm($cabinet);

        return $this->render('admin/cabinet/show.html.twig', array(
            'cabinet' => $cabinet,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cabinet entity.
     *
     * @Route("/{id}/edit", name="admin_cabinet_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cabinet $cabinet)
    {
        $deleteForm = $this->createDeleteForm($cabinet);
        $editForm = $this->createForm('App\Form\CabinetType', $cabinet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_cabinet_edit', array('id' => $cabinet->getId()));
        }

        return $this->render('admin/cabinet/edit.html.twig', array(
            'cabinet' => $cabinet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cabinet entity.
     *
     * @Route("/delete/{id}", name="admin_cabinet_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cabinet $cabinet)
    {

        $form = $this->createDeleteForm($cabinet);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cabinet);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'error',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_cabinet_etude_index');
    }

    /**
     * Creates a form to delete a cabinet entity.
     *
     * @param Cabinet $cabinet The cabinet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cabinet $cabinet)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_cabinet_delete', array('id' => $cabinet->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Lists all cabinet entities.
     *
     * @Route("/etude/from-lpc", name="admin_cabinet_etude_index")
     * @Method("GET")
     */
    public function indexCabinetAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cabinets = $em->getRepository(Cabinet::class)->findAll();

        return $this->render('admin/cabinet/indexCabinet.html.twig', array(
            'cabinets' => $cabinets,
        ));
    }

    /**
     * Displays a form to edit an existing cabinet entity.
     *
     * @Route("/{id}/edit/from-lpc", name="admin_cabinet_etude_edit")
     * @Method({"GET", "POST"})
     */
    public function editCabinetAction(Request $request, Cabinet $cabinet)
    {
        $deleteForm = $this->createDeleteForm($cabinet);
        $editForm = $this->createForm('App\Form\CabinetQuestionnaireType', $cabinet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->flush();

            $questionnaire = $em->getRepository(Questionnaire::class)->findOneBy(['cabinet' => $cabinet]);

            if (!$questionnaire) {
                $questionnaire = new Questionnaire();
                $questionnaire->setIsActive(true)
                    ->setCabinet($cabinet)
                    ->setEmail($cabinet->getEmailDestinataire())
                    ->setCreatedAt(new \DateTime('now'))
                    ->setCode($cabinet->getCode())
                    ->setDestinataire($cabinet->getDestinataire())
                    ->setEtude($cabinet->getEtude())
                    ;

                $em->persist($questionnaire);

                $em->flush();
            }

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_cabinet_etude_index');
        }

        return $this->render('admin/cabinet/editCabinet.html.twig', array(
            'cabinet' => $cabinet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a new cabinet entity.
     *
     * @Route("/new/from-lpc", name="admin_cabinet_etude_new")
     * @Method({"GET", "POST"})
     */
    public function newCabinetAction(Request $request)
    {
        $cabinet = new Cabinet();

        //add token
        $length = 3;
        $token = bin2hex(random_bytes($length));

        $form = $this->createForm('App\Form\CabinetQuestionnaireType', $cabinet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $cabinet->setCode($token);

            $em->persist($cabinet);
            $em->flush();

            $questionnaire = new Questionnaire();
            $questionnaire->setIsActive(true)
                ->setCabinet($cabinet)
                ->setEmail($cabinet->getEmailDestinataire())
                ->setCreatedAt(new \DateTime('now'))
                ->setCode($cabinet->getCode())
                ->setDestinataire($cabinet->getDestinataire())
                ->setEtude($cabinet->getEtude())
            ;

            $em->persist($questionnaire);

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_cabinet_etude_index');
        }

        return $this->render('admin/cabinet/newCabinet.html.twig', array(
            'cabinet' => $cabinet,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @Route("/import/from-file", name="admin_cabinet_import")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importCabinet(Request $request)
    {
        $token = $request->get('_token');

        if (null != $token) {


            $target_dir = $this->getParameter('kernel.project_dir'). '/public/uploads/imports/';

            $basename = uniqid()."_".basename($_FILES['import_cabinet']['name']);
            $target_file = $target_dir .$basename;

            if (move_uploaded_file($_FILES["import_cabinet"]["tmp_name"], $target_file)) {
                $em = $this->getDoctrine()->getManager();

                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

                $reader->setReadDataOnly(true);

                $spreadsheet = $reader->load($target_file);

                $sheetCount = $spreadsheet->getSheetCount();

                $sheetCount = 1;

                $etudeNational = $em->getRepository(EtudeNational::class)->findOneBy(['isActive' => 1]);

                $tabNameSheet = [];

                $tabAnnee = [];

                $anneeEnCours = date("Y");

                for ($i=1;$i<=10;$i++) {
                    $tabAnnee [] = $anneeEnCours;
                    $anneeEnCours = $anneeEnCours -1;
                }

                for ($i = 0; $i < $sheetCount; $i++) {
                    $sheet = $spreadsheet->getSheet($i);

                    $item = str_replace("classement_","", $sheet->getTitle());

                    if (!in_array($item, $tabAnnee)) {
                        continue;
                    }

                    $sheetData = $sheet->toArray();

                    $sheetData = $spreadsheet->getActiveSheet()->toArray();

                    unset($sheetData[0]);

                    $ligne = 1;

                    foreach ($sheetData as $t) {

                        // process element here;
                        //$t[0] = Annee
                        //$t[1] = Nom cabinet
                        //$t[2] = Nom president
                        //$t[3] = CA
                        //$t[4] = CAC
                        //$t[5] = EC
                        //$t[6] = A
                        //$t[7] = B
                        //$t[8] = C
                        //$t[9] = EC Hommes
                        //$t[10] = EC Femmes
                        //$t[11] = Associé Hommes
                        //$t[12] = Associé Femmes
                        //$t[13] = Nb de client
                        //$t[14] = Date cloture
                        //$t[15] = Effectif
                        //$t[16] = Nb de bureau
                        //$t[17] = Localisation
                        //$t[18] = Réseau, association technique ou groupement français
                        //$t[19] = Réseau international
                        //$t[20] = Destinateur Nom
                        //$t[21] = Destinateur Email
                        //$t[22] = Juridique (K€)
                        //$t[23] = Fiscal (K€)
                        //$t[24] = Conseil en organisation (K€)
                        //$t[25] = Autres (K€)
                        //$t[26] = TOTAL (K€)
                        //$t[27] = code
                        //$t[28] = isPluridiscplinaire
                        //$t[29] = commentaire
                        //$t[30] = Texte plutidisciplinaire
                        //$t[31] = Forme juridique
                        //$t[32] = Numéro SIRET
                        //$t[33] = Numéro SIREN
                        //$t[34] = Adresse
                        //$t[35] = Code postal
                        //$t[36] = Ville
                        //$t[37] = Département
                        //$t[38] = Région
                        //$t[39] = Tél
                        //$t[40] = Email
                        //$t[41] = Site internet


                        $dateCloture = "";
                        if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $t[14])) {
                            $dateCloture = $t[14];
                        }elseif ($t[14] && $t[14] != "") {
                            $dateCloture = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($t[14]);
                            $dateCloture = $dateCloture->format('d/m/Y');
                        }

                        $item = $t[0];

                        if (!$t[1]) {
                            continue;
                        }

                        //add code
                        $length = 3;
                        $code = ($t[27])?$t[27]: bin2hex(random_bytes($length));

                        $cabinet = $em->getRepository('App:Cabinet')->findOneByCode($code);

                        $newCabinet = false;

                        if (!$cabinet) {
                            $cabinet = new Cabinet();
                            $newCabinet = true;
                        }

                        $cabinet->setNom($t[1])
                            ->setDestinataire($t[20])
                        ;

                        $emailDestinataire = $t[21];

                        $emailDestinataire = "pbaey+".$ligne."@cpc-doc.com";

                        $cabinet->setEmailDestinataire($emailDestinataire);
                        $cabinet->setCode($code);
                        $cabinet->setEtude($etudeNational);
                        $cabinet->setIsPluridiscplinaire($t[28]);
                        $cabinet->setCommentaire($t[29]);
                        /*$cabinet->setTxtPluridiscplinaire(isset($t[30])?$t[30]:null);
                        $cabinet->setFormeJuridique(isset($t[31])?$t[31]:null);
                        $cabinet->setSiret(isset($t[32])?$t[32]:null);
                        $cabinet->setSiren(isset($t[33])?$t[33]:null);
                        $cabinet->setAdresse(isset($t[34])?$t[34]:null);
                        $cabinet->setCodePostal(isset($t[35])?$t[35]:null);
                        $cabinet->setVille(isset($t[36])?$t[36]:null);
                        $cabinet->setDepartement(isset($t[37])?$t[37]:null);
                        $cabinet->setRegion(isset($t[38])?$t[38]:null);
                        $cabinet->setTelephone(isset($t[39])?$t[39]:null);
                        $cabinet->setEmail(isset($t[40])?$t[40]:null);
                        $cabinet->setSite(isset($t[41])?$t[41]:null);*/

                        $ligne++;

                        $nomPresidents = explode(";", $t[2]);
                        $kk = 0;
                        if (sizeof($nomPresidents) >0) {
                            foreach ($nomPresidents as $nomPresident) {

                                if ($kk == 0) {
                                    $cabinet->setNomPresident($nomPresident);
                                }else {
                                    $personne = new Personne();
                                    $personne->setNom($nomPresident)
                                        ->setCabinet($cabinet)
                                        ;
                                    $em->persist($personne);
                                }
                                $kk++;
                            }
                        }



                        $em->persist($cabinet);

                        if ($newCabinet) {

                            //$cabinet = $em->getRepository("App:Cabinet")->find($cabinet->getId());

                            $em->flush();
                        }

                        //Ca
                        $ca = $em->getRepository(Ca::class)->findCa($cabinet, $item);
                        if (!$ca) {
                            $ca = new Ca();
                        }

                        $ca->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setCaLettre($t[3])
                        ;

                        $em->persist($ca);
                        //CAC
                        $cac = $em->getRepository(CaC::class)->findCaC($cabinet, $item);
                        if (!$cac) {
                            $cac = new CaC();
                        }

                        $cac->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setCaLettre($t[4])
                        ;

                        $em->persist($cac);

                        //CA expertise
                        $caExpertise = $em->getRepository(CaExpertise::class)->findCaExpertise($cabinet, $item);
                        if (!$caExpertise) {
                            $caExpertise = new CaExpertise();
                        }

                        $caExpertise->setAnnee($item)
                            ->setCabinet($cabinet)
                            ->setCaLettre($t[5])
                            ->setALettre($t[6])
                            ->setBLettre($t[7])
                            ->setCLettre($t[8])
                        ;

                        $em->persist($caExpertise);

                        //Taille cabinet
                        $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);
                        if (!$tailleCabinet) {
                            $tailleCabinet = new TailleCabinet();
                        }


                        $localisations = explode("-", $t[17]);

                        $listLoc = "";

                        foreach ($localisations as $localisation) {
                            if ($localisation) {
                                $checkDep = $em->getRepository(Departement::class)->findByCode(trim($localisation));
                                if (!$checkDep) {
                                    $checkReg = $em->getRepository(Region::class)->findByNom(trim($localisation));

                                    if (!$checkReg) {
                                        $regionNew = new Region();
                                        $regionNew->setNom($localisation);

                                        $em->persist($regionNew);
                                    }
                                }

                                $listLoc .= $listLoc.trim($localisation)."-";
                            }
                        }

                        $groupes = explode("-", $t[18]);
                        $listGroupe = "";

                        foreach ($groupes as $groupe) {
                            if ($groupe) {
                                $checkGroupe = $em->getRepository(Groupe::class)->findByNom(trim($groupe));
                                if (!$checkGroupe) {
                                    $groupeNew = new Groupe();
                                    $groupeNew->setNom($groupe);

                                    $em->persist($groupeNew);
                                    $em->flush();

                                    $listGroupe .= $listGroupe.$groupeNew->getId().",";
                                }else {
                                    $groupe = $checkGroupe[0];
                                    $listGroupe .= $listGroupe.$groupe->getId().",";
                                }
                            }
                        }

                        $reseaux = explode("-", $t[19]);
                        $listReseau = "";

                        foreach ($reseaux as $reseau) {
                            if ($reseau) {
                                $checkReseau = $em->getRepository(Regroupement::class)->findByNom(trim($reseau));
                                if (!$checkReseau) {
                                    $reseauNew = new Regroupement();
                                    $reseauNew->setNom($reseau);

                                    $em->persist($reseauNew);
                                    $em->flush();

                                    $listReseau .= $listReseau.$reseauNew->getId().",";
                                }else {
                                    $reseau = $checkReseau[0];
                                    $listReseau .= $listReseau.$reseau->getId().",";
                                }
                            }
                        }

                        $tailleCabinet->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setNbClient($t[13])
                            ->setDateCloture($dateCloture)
                            ->setEffectif($t[15])
                            ->setGroupeNational($listGroupe)
                            ->setLocalisation($listLoc)
                            ->setNbBureau($t[16])
                            ->setNbFemmeAs($t[12])
                            ->setNbFemmeEc($t[10])
                            ->setNbHommeAs($t[11])
                            ->setNbHommeEc($t[9])
                            ->setReseauNational($listReseau)
                        ;

                        $em->persist($tailleCabinet);

                        //Information filiale
                        $informationFiliale = $em->getRepository(InformationFiliale::class)->findInformation($cabinet, $item);

                        if (!$informationFiliale) {
                            $informationFiliale = new InformationFiliale();
                        }

                        $informationFiliale->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setAutre($t[25])
                            ->setConseil($t[24])
                            ->setFiscal($t[23])
                            ->setJuridique($t[22])
                            ->setTotal($t[26])
                        ;

                        $em->persist($informationFiliale);

                        $questionnaire = $em->getRepository(Questionnaire::class)->findOneBy(['cabinet' => $cabinet]);

                        if (!$questionnaire) {
                            $questionnaire = new Questionnaire();
                        }

                        $questionnaire->setIsActive(true)
                            ->setCabinet($cabinet)
                            ->setEmail($cabinet->getEmailDestinataire())
                            ->setCreatedAt(new \DateTime('now'))
                            ->setCode($cabinet->getCode())
                            ->setDestinataire($cabinet->getDestinataire())
                            ->setEtude($cabinet->getEtude())
                        ;

                        $em->persist($questionnaire);



                    }

                    $em->flush();
                }





                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->translator->trans('enregistrement.effectuee')
                );

                return $this->redirectToRoute('admin_cabinet_etude_index');


            }


        }

        return $this->render('admin/cabinet/import.html.twig', [

        ]);
    }


    /**
     * Deletes a cabinet entity.
     *
     * @Route("/delete/delete-all/from-marcel", name="admin_cabinet_delete_from_marcel")
     * @Method("GET")
     */
    public function deleteAllAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $cabinets = $em->getRepository(Cabinet::class)->findAll();

        foreach ($cabinets as $cabinet) {
            $em->remove($cabinet);

        }
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'error',
            $this->translator->trans('suppression.effectuee')
        );

        return $this->redirectToRoute('admin_cabinet_etude_index');
    }
}
