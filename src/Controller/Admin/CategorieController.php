<?php

/**
 * CategorieController Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Controller\Admin
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Controller\Admin;

use App\Entity\Categorie;
use App\Service\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\CategorieType;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Categorie controller.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @Route("admin/categorie")
 */
class CategorieController extends AbstractController
{
    /**
     * @var ArticleService
     */
    private $articleService;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(ArticleService $articleService, TranslatorInterface $translator)
    {
        $this->articleService = $articleService;
        $this->translator = $translator;
    }

    /**
     * Lists all categorie entities.
     *
     * @return Response
     *
     * @Route("/", name="admin_categorie_index",methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        //initialisé la categorie pour la hierarchie
        $this->articleService->initCategorie();

        $categories = $em->getRepository(Categorie::class)->findBy(
            array(),
            array('treeLeft' => 'ASC')
        );

        return $this->render(
            'admin/categorie/index.html.twig',
            [
                'categories' => $categories,
            ]
        );
    }

    /**
     * Creates a new categorie entity.
     *
     * @param Request $request request
     *
     * @return Response
     *
     * @Route("/new", name="admin_categorie_new",methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $categorie = new Categorie();
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_categorie_index',
                array('id' => $categorie->getId())
            );
        }

        return $this->render(
            'admin/categorie/new.html.twig',
            [
                'categorie' => $categorie,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a categorie entity.
     *
     * @param  Categorie $categorie categorie
     * @return Response
     *
     * @Route("/{id}", name="admin_categorie_show",methods={"GET"})
     */
    public function showAction(Categorie $categorie)
    {
        $deleteForm = $this->createDeleteForm($categorie);

        return $this->render(
            'admin/categorie/show.html.twig',
            [
                'categorie' => $categorie,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing categorie entity.
     *
     * @param  Request   $request   request
     * @param  Categorie $categorie categorie
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_categorie_edit",methods={"GET","POST"})
     */
    public function editAction(Request $request, Categorie $categorie)
    {
        $deleteForm = $this->createDeleteForm($categorie);
        $editForm = $this->createForm(CategorieType::class, $categorie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_categorie_edit',
                array('id' => $categorie->getId())
            );
        }

        return $this->render(
            'admin/categorie/edit.html.twig',
            [
                'categorie' => $categorie,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a categorie entity.
     *
     * @param  Categorie $categorie categorie
     * @return RedirectResponse
     *
     * @Route("/delete/{id}", name="admin_categorie_delete",methods={"GET","POST"})
     */
    public function deleteElementAction(Categorie $categorie)
    {
        if (!$categorie) {
            throw new \Exception('Unable to find categorie entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_categorie_index');
    }

    /**
     * Function deleteAction
     * A utiliser si la suppression se fait via un submit.
     *
     * @param Request   $request   request
     * @param Categorie $categorie categorie
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Categorie $categorie)
    {
        $form = $this->createDeleteForm($categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_categorie_index');
    }

    /**
     * Creates a form to delete a categorie entity.
     *
     * @param  Categorie $categorie The categorie entity
     * @return Form
     */
    public function createDeleteForm(Categorie $categorie)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'admin_categorie_delete',
                    array('id' => $categorie->getId())
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
