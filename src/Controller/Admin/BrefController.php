<?php

namespace App\Controller\Admin;

use App\Entity\Bref;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bref controller.
 *
 * @Route("admin/bref")
 */
class BrefController extends AbstractController
{
    /**
     * Lists all bref entities.
     *
     * @Route("/", name="admin_bref_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $brefs = $em->getRepository('App:Bref')->findBy(array(), array("datePublication"=>"DESC"));

        return $this->render(
            'admin/bref/index.html.twig', array(
            'brefs' => $brefs,
            )
        );
    }

    /**
     * Creates a new bref entity.
     *
     * @Route("/new", name="admin_bref_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $bref = new Bref();
        $form = $this->createForm('App\Form\BrefType', $bref);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bref);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_bref_index', array('id' => $bref->getId()));
        }

        return $this->render(
            'admin/bref/new.html.twig', array(
            'bref' => $bref,
            'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a bref entity.
     *
     * @Route("/{id}", name="admin_bref_show", methods={"GET"})
     */
    public function showAction(Bref $bref)
    {
        $deleteForm = $this->createDeleteForm($bref);

        return $this->render(
            'admin/bref/show.html.twig', array(
            'bref' => $bref,
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing bref entity.
     *
     * @Route("/{id}/edit", name="admin_bref_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Bref $bref)
    {
        $deleteForm = $this->createDeleteForm($bref);
        $editForm = $this->createForm('App\Form\BrefType', $bref);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_bref_edit', array('id' => $bref->getId()));
        }

        return $this->render(
            'admin/bref/edit.html.twig', array(
            'bref' => $bref,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a bref entity.
     *
     * @param Bref $bref bref
     *
     * @Route("/delete/{id}", name="admin_bref_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Bref $bref)
    {
        if (!$bref) {
            throw new \Exception('Unable to find bref entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bref);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_bref_index');
    }

    /**
     * Deletes a bref entity.
     *
     * @Route("/{id}", name="admin_bref_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Bref $bref)
    {
        $form = $this->createDeleteForm($bref);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bref);
            $em->flush();
        }

        return $this->redirectToRoute('admin_bref_index');
    }

    /**
     * Creates a form to delete a bref entity.
     *
     * @param Bref $bref The bref entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bref $bref)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_bref_delete', array('id' => $bref->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
