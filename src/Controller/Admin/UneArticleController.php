<?php

namespace App\Controller\Admin;

use App\Entity\UneArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Service\UneArticleService;
use App\Form\UneSelectionType;
use App\Model\UneSelection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Unearticle controller.
 *
 * @Route("admin/unearticle")
 */
class UneArticleController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all uneArticle entities.
     *
     * @return Response
     *
     * @param UneArticleService $uneService manager des unes
     *
     * @Route("/", name="admin_unearticle_index", methods={"GET"})
     */
    public function indexAction(UneArticleService $uneService)
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $uneService->checkDefaultUnesExistence();
        $unes = $uneService->getAll();

        $uneSelection = new UneSelection();

        foreach ($unes as $une) {
            $article = $une->getArticle();

            if ($article) {
                $setter = 'setArticle' . $une->getPosition();
                if (method_exists($uneSelection, $setter)) {
                    $uneSelection->$setter($article);
                }
            }
        }

        $form = $this->createForm(
            UneSelectionType::class,
            $uneSelection,
            [
                'action' => $this->generateUrl('admin_unearticle_update_selection'),
            ]
        );

        return $this->render(
            'admin/unearticle/index.html.twig',
            [
                'unes' => $unes,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Mise à jour des unes à partir d'un form UneSelection
     *
     * @param  Request           $request    request
     * @param  UneArticleService $uneService service de management des unes
     * @return Response
     *
     * @Route("/update", name="admin_unearticle_update_selection")
     * @Method({"POST"})
     */
    public function updateSelectionAction(Request $request, UneArticleService $uneService)
    {
        $uneSelection = new UneSelection();

        $form = $this->createForm(
            UneSelectionType::class,
            $uneSelection,
            [
                'action' => $this->generateUrl('admin_unearticle_update_selection'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $unes = $uneService->getAll();

            foreach ($unes as $une) {
                $article = null;
                $getter = 'getArticle' . $une->getPosition();

                if (method_exists($uneSelection, $getter)) {
                    $article = $uneSelection->$getter();
                }

                $une->setArticle($article);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );
        }

        return $this->redirectToRoute('admin_unearticle_index');
    }
}
