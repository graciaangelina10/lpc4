<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 2/21/19
 * Time: 3:57 PM
 */

namespace App\Controller\Admin;

use App\Entity\Achat;
use App\Entity\User;
use App\Form\AchatType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AchatController
 * @package App\Controller\Admin
 *
 * @Route("/admin/achat")
 */
class AchatController extends AbstractController
{

    /**
     * @param Request $request
     * @param User $user
     *
     * @Route("/produit/{id}", name="add_produit_user", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, User $user)
    {
        if (!$user) {
            throw new \Exception('Unable to find user entity');
        }

        $achat = new Achat();
        $achat->setUser($user);
        $form = $this->createForm(AchatType::class, $achat);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $achat->setUser($user);
            $em->persist($achat);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute("admin_user_edit", ["id" => $achat->getUser()->getId()]);
        }

        return $this->render("admin/achat/new.html.twig", [
            "user" => $user,
            "form" => $form->createView(),
        ]);
    }


}