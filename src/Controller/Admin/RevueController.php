<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\PositionArticleRevue;
use App\Entity\Revue;
use App\Form\RevueType;
use App\Service\ArticleService;
use App\Service\RevueService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Revue controller.
 *
 * @Route("admin/revue")
 */
class RevueController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all revue entities.
     *
     * @Route("/", name="admin_revue_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        $revues = $em->getRepository(Revue::class)->findAll();

        return $this->render(
            'admin/revue/index.html.twig', array(
            'revues' => $revues,
            )
        );
    }

    /**
     * Creates a new revue entity.
     *
     * @Route("/new", name="admin_revue_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $revue = new Revue();
        $form = $this->createForm(RevueType::class, $revue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($revue);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_revue_index', array('id' => $revue->getId()));
        }

        return $this->render(
            'admin/revue/new.html.twig', array(
            'revue' => $revue,
            'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a revue entity.
     *
     * @Route("/{id}", name="admin_revue_show", methods={"GET"})
     */
    public function showAction(Revue $revue)
    {
        $deleteForm = $this->createDeleteForm($revue);

        return $this->render(
            'admin/revue/show.html.twig', array(
            'revue' => $revue,
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing revue entity.
     *
     * @Route("/{id}/edit", name="admin_revue_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Revue $revue)
    {
        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($revue);
        $editForm = $this->createForm('App\Form\RevueType', $revue);

        $originalSommaire = new ArrayCollection();

        foreach ($revue->getSommaire() as $sommaire){
            $originalSommaire->add($sommaire);
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {


            // remove the relationship between the tag and the Task
            foreach ($originalSommaire as $sommaire) {
                if(false === $revue->getSommaire()->contains($sommaire)) {
                    $revue->removeSommaire($sommaire);
                    $em->remove($sommaire);
                }
            }

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_revue_edit', array('id' => $revue->getId()));
        }

        $articles = $em->getRepository("App:PositionArticleRevue")->findBy(array("revue"=>$revue),array("position"=>"ASC"));

        $changePositionArticle = $this->get('session')->get('change_position_article');
        if ($changePositionArticle) {
            $this->get('session')->remove('change_position_article');
        }

        return $this->render(
            'admin/revue/edit.html.twig', array(
            'revue' => $revue,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
                'articles'=> $articles,
                'changePositionArticle'=> $changePositionArticle
            )
        );
    }

    /**
     * Deletes a Revue entity.
     *
     * @param Revue $revue revue
     *
     * @Route("/delete/{id}", name="admin_revue_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Revue $revue)
    {
        if (!$revue) {
            throw new \Exception('Unable to find revue entity');
        } else {
            $em = $this->getDoctrine()->getManager();

            $articles = $revue->getArticle();
            if ($articles) {
                foreach ($articles as $unArticle) {
                    $positionArticleRevue = $em->getRepository("App:PositionArticleRevue")->findBy(array("revue"=>$revue, "article"=>$unArticle));
                    if ($positionArticleRevue) {
                        $em->remove($positionArticleRevue[0]);
                    }
                }
            }
            $em->remove($revue);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_revue_index');
    }

    /**
     * Deletes a revue entity.
     *
     * @Route("/{id}", name="admin_revue_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Revue $revue)
    {
        $form = $this->createDeleteForm($revue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($revue);
            $em->flush();
        }

        return $this->redirectToRoute('admin_revue_index');
    }

    /**
     * Creates a form to delete a revue entity.
     *
     * @param Revue $revue The revue entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Revue $revue)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_revue_delete', array('id' => $revue->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param integer $id
     * @param integer $position
     *
     * @Route("/sort/{id}/{position}/{idRevue}/{direction}", name="admin_revue_article_sort", methods={"GET"})
     * @return                         \Symfony\Component\HttpFoundation\Response
     */
    public function sortAction($id, $position, $direction, $idRevue, RevueService $revueService)
    {
        $em = $this->getDoctrine()->getManager();

        $revue = $em->getRepository('App:Revue')->find($idRevue);

        $revueService->majPositionArticle($direction, $position, $revue);
        $PositionArticleRevue = $em->getRepository('App:PositionArticleRevue')->find($id);
        $PositionArticleRevue->setPosition($position);
        $em->persist($PositionArticleRevue);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('enregistrement.effectuee')
        );
        $this->get('session')->set('change_position_article', '1');

        return $this->redirectToRoute('admin_revue_edit',array('id' => $revue->getId()));
    }

    /**
     * Deletes a Article Revue entity.
     *
     * @param Revue $revue revue
     *
     * @Route("/delete-article/{id}/{idArticle}", name="admin_article_delete_revue",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteArticleRevueAction(Revue $revue, $idArticle, RevueService $revueService)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository("App:Article")->find($idArticle);

        $positionArticleRevue = $em->getRepository("App:PositionArticleRevue")->findBy(array("revue"=>$revue, "article"=>$article));

        if ($positionArticleRevue) {
            $position = $positionArticleRevue[0]->getPosition();
            $revueService->majPositionArticle('down', ($position +1), $revue);

            $em->remove($positionArticleRevue[0]);
            $article->removeRevue($revue);

            $em->flush();
        }

        return $this->redirectToRoute('admin_revue_edit',array('id' => $revue->getId()));

    }

    /**
     * @param Revue $revue, ArticleService $articleService
     *
     * @Route("/duplicate/{id}", name="admin_revue_duplicate", methods={"POST", "GET"})
     *
     * @return RedirectResponse
     */
    public function duplicateAction(Revue $revue, ArticleService $articleService)
    {
        $articleService->duplicateContent($revue, "revue");

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('enregistrement.effectuee')
        );

        return $this->redirectToRoute('admin_revue_index');

    }
}
