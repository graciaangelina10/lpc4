<?php

namespace App\Controller\Admin;

use App\Entity\EtudeNational;
use App\Form\EtudeNationalType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Etudenational controller.
 *
 * @Route("admin/etudenational")
 */
class EtudeNationalController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all etudeNational entities.
     *
     * @Route("/", name="admin_etudenational_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etudeNationals = $em->getRepository(EtudeNational::class)->findAll();

        return $this->render('admin/etudenational/index.html.twig', array(
            'etudeNationals' => $etudeNationals,
        ));
    }

    /**
     * Creates a new etudeNational entity.
     *
     * @Route("/new", name="admin_etudenational_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $etudeNational = new Etudenational();
        $form = $this->createForm(EtudeNationalType::class, $etudeNational);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etudeNational);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_etudenational_index');
        }

        return $this->render('admin/etudenational/new.html.twig', array(
            'etudeNational' => $etudeNational,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a etudeNational entity.
     *
     * @Route("/{id}", name="admin_etudenational_show")
     * @Method("GET")
     */
    public function showAction(EtudeNational $etudeNational)
    {
        $deleteForm = $this->createDeleteForm($etudeNational);

        return $this->render('admin/etudenational/show.html.twig', array(
            'etudeNational' => $etudeNational,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing etudeNational entity.
     *
     * @Route("/{id}/edit", name="admin_etudenational_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EtudeNational $etudeNational)
    {
        $deleteForm = $this->createDeleteForm($etudeNational);
        $editForm = $this->createForm('App\Form\EtudeNationalType', $etudeNational);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_etudenational_edit', array('id' => $etudeNational->getId()));
        }

        return $this->render('admin/etudenational/edit.html.twig', array(
            'etudeNational' => $etudeNational,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a etudeNational entity.
     *
     * @Route("/delete/{id}", name="admin_etudenational_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, EtudeNational $etudeNational)
    {
        $form = $this->createDeleteForm($etudeNational);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etudeNational);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'error',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_etudenational_index');
    }

    /**
     * Creates a form to delete a etudeNational entity.
     *
     * @param EtudeNational $etudeNational The etudeNational entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EtudeNational $etudeNational)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_etudenational_delete', array('id' => $etudeNational->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @param EtudeNational $etudeNational
     * @Route("/duplique/{id}", name="admin_etudenational_duplique")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dupliqueAction(EtudeNational $etudeNational)
    {
        $em = $this->getDoctrine()->getManager();

        $etudeNationalNew = clone $etudeNational;
        $etudeNationalNew->setTitre($etudeNational->getTitre().'[duplication]');

        $etudeNationalNew->setIsActive(false);
        $em->persist($etudeNationalNew);

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->translator->trans('enregistrement.effectuee')
        );

        $em->flush();


        return $this->redirectToRoute('admin_etudenational_index');

    }
}
