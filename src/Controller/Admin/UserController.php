<?php

namespace App\Controller\Admin;

use App\Entity\Abonnement;
use App\Entity\User;
use App\Form\ImportUserType;
use App\Form\UserType;
use App\Model\ImportUser;
use App\Security\CustomPasswordEncoder;
use App\Service\AbonnementService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\Form\FormFactoryInterface;
use FOS\UserBundle\FOSUserEvents;
//use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * User controller.
 *
 * @Route("admin/user")
 */
class UserController extends AbstractController
{
    private $eventDispatcher;
    private $formFactory;
    private $userManager;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var CustomPasswordEncoder
     */
    private $passwordEncoder;

    public function __construct(EventDispatcherInterface $eventDispatcher,
                                FormFactoryInterface $formFactory,
                                UserProviderInterface $userManager,
                                TranslatorInterface $translator,
                                CustomPasswordEncoder $passwordEncoder)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
        $this->translator = $translator;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Lists all user entities.
     *
     * @Route("/", name="admin_user_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findBy(array(), array("id"=>"desc"));

        return $this->render('admin/user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="admin_user_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($user->getPlainPassword());
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_user_index', array('id' => $user->getId()));
        }

        return $this->render('admin/user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="admin_user_show", methods={"GET"})
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('admin/user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, User $user, AbonnementService $abonnementService)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('App\Form\UserType', $user);
        $editForm->handleRequest($request);

        $achats = $user->getAchatList();

        $abonnement = $em->getRepository(Abonnement::class)->findByUser($user);

        $prixAbonnement = $abonnementService->getPrixAbonnement();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_user_edit', array('id' => $user->getId()));

        }

        return $this->render('admin/user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'achats'=>$achats,
            'abonnement'=>$abonnement,
            'prixAbonnement'=>$prixAbonnement,
        ));
    }

    /**
     * Deletes a User entity.
     *
     * @param User $user user
     *
     * @Route("/delete/{id}", name="admin_user_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(User $user, UserService $userService)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        if (!$user) {
            throw new \Exception('Unable to find user entity');
        } else {
            $userService->deleteUser($user);
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="admin_user_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Change password an existing user entity.
     *
     * @Route("/profile/{id}", name="admin_user_profile", methods={"GET", "POST"})
     */
    public function changePassword(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('App\Form\ResetType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $this->userManager->updateUser($user);


            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_user_edit', array('id' => $user->getId()));
        }

        return $this->render('admin/user/reset.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/add/csv", name="admin_user_import", methods={"GET", "POST"})
     * @return Response
     */
    public function importAction(Request $request, UserService $userService)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $importFile = new ImportUser();

        $form = $this->createForm(ImportUserType::class, $importFile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $importFile->getFile();
            $fileName = 'user.csv';

            $file->move($this->getParameter('app.path.import_csv_user'),$fileName);

//            $factory = $this->container->get('security.password_encoder');
            $userService->addUserImport($this->passwordEncoder);

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
            return $this->redirectToRoute("admin_user_index");

        } else {
            return $this->render('admin/user/import.html.twig',array(
                'form'=>$form->createView(),
            ))  ;
        }



    }

    /**
     * @Route("/add/export", name="admin_user_export", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportUser(UserService $userService)
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $fileName = 'LPC_user_'.date("Y-m-d").'.xlsx';
        $temp_file = $userService->exportUserDataBase($fileName);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);

    }
}
