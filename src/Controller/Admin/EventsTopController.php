<?php

namespace App\Controller\Admin;

use App\Entity\EventTop;
use App\Service\EventTopService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Form\EventTopSelectionType;
use App\Model\EventTopSelection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("admin/events")
 */
class EventsTopController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * @Route("/", name="admin_events_top", methods={"GET"})
     */
    public function indexAction(EventTopService $eventTopService)
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $eventTopService->checkDefaultEventsExistence();
        
        $events = $eventTopService->getAll();
        
        $eventTopSelection = new EventTopSelection();
        
        foreach ($events as $event) {
            $eventTop = $event->getArticle();

            if ($eventTop) {
                $setter = 'setArticle' . $event->getPosition();
                if (method_exists($eventTopSelection, $setter)) {
                    $eventTopSelection->$setter($eventTop);
                }
            }
        }

        $form = $this->createForm(
            EventTopSelectionType::class,
            $eventTopSelection,
            [
                'action' => $this->generateUrl('admin_events_update_selection'),
            ]
        );

        return $this->render(
            'admin/events/index.html.twig',
            [
                'events' => $events,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/update", name="admin_events_update_selection")
     * @Method({"POST"})
     */
    public function updateSelectionAction(Request $request, EventTopService $eventTopService)
    {
        $eventTopSelection = new EventTopSelection();

        $form = $this->createForm(
            EventTopSelectionType::class,
            $eventTopSelection,
            [
                'action' => $this->generateUrl('admin_events_update_selection'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $events = $eventTopService->getAll();
            
            foreach ($events as $event) {
                $eventTop = null;
                $getter = 'getArticle' . $event->getPosition();

                if (method_exists($eventTopSelection, $getter)) {
                    $eventTop = $eventTopSelection->$getter();
                }
                
                $event->setArticle($eventTop);
            }
            
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );
        }

        return $this->redirectToRoute('admin_events_top');
    }

}
