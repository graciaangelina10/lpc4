<?php
/**
 * TagsController Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Controller\Admin
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
namespace App\Controller\Admin;

use App\Entity\Tags;
use App\Service\TagsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Tag controller.
 *
 * @category Class
 *
 * @package App\Controller\Admin
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 *
 * @Route("admin/tags")
 */
class TagsController extends AbstractController
{
    /**
     * @var TagsService
     */
    private $tagsService;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TagsService $tagsService, TranslatorInterface $translator)
    {
        $this->tagsService = $tagsService;
        $this->translator = $translator;
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/", name="admin_tags_index",methods={"GET"})
     *
     * @return null
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        //reinitialiser la position des tags
        $this->tagsService->initPosition();

        $tags = $em->getRepository(Tags::class)->findBy(array(), array("position" => "asc"));

        return $this->render(
            'admin/tags/index.html.twig',
            [
                'tags' => $tags,
            ]
        );
    }

    /**
     * Creates a new tag entity.
     *
     * @param Request $request request
     *
     * @Route("/new", name="admin_tags_new",methods={"GET", "POST"})
     *
     * @return null
     */
    public function newAction(Request $request)
    {
        $tag = new Tags();
        $form = $this->createForm('App\Form\TagsType', $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_tags_index',
                array('id' => $tag->getId())
            );
        }

        return $this->render(
            'admin/tags/new.html.twig',
            [
                'tag' => $tag,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a tag entity.
     *
     * @param Tags $tag tag
     *
     * @Route("/{id}", name="admin_tags_show",methods={"GET"})
     *
     * @return null
     */
    public function showAction(Tags $tag)
    {
        $deleteForm = $this->createDeleteForm($tag);

        return $this->render(
            'admin/tags/show.html.twig',
            [
                'tag' => $tag,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing tag entity.
     *
     * @param Request $request request
     * @param Tags    $tag     tag
     *
     * @Route("/{id}/edit", name="admin_tags_edit",methods={"GET", "POST"})
     *
     * @return null
     */
    public function editAction(Request $request, Tags $tag)
    {
        $deleteForm = $this->createDeleteForm($tag);
        $editForm = $this->createForm('App\Form\TagsType', $tag);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_tags_edit',
                array('id' => $tag->getId())
            );
        }

        return $this->render(
            'admin/tags/edit.html.twig',
            [
                'tag' => $tag,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a tag entity.
     *
     * @param Tags $tag tag
     *
     * @Route("/delete/{id}", name="admin_tags_delete",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Tags $tag)
    {
        if (!$tag) {
            throw new \Exception("Unable to find tag entity");
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tag);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_tags_index');
    }

    /**
     * Delete element
     *
     * @param Request $request request
     * @param Tags    $tag     tag
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Tags $tag)
    {
        $form = $this->createDeleteForm($tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tag);
            $em->flush();
        }

        return $this->redirectToRoute('admin_tags_index');
    }

    /**
     * Creates a form to delete a tag entity.
     *
     * @param Tags $tag The tag entity
     *
     * @return null
     */
    public function createDeleteForm(Tags $tag)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'admin_tags_delete',
                    array('id' => $tag->getId())
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Resorts an item using it's doctrine sortable property
     *
     * @param integer $id
     * @param integer $position
     *
     * @Route("/sort/{id}/{position}", name="admin_tags_sort", methods={"GET"})
     * @return                         \Symfony\Component\HttpFoundation\Response
     */
    public function sortAction($id, $position)
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository(Tags::class)->find($id);
        $tags->setPosition($position);
        $em->persist($tags);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->translator->trans('enregistrement.effectuee')
        );

        return $this->redirectToRoute('admin_tags_index');
    }
}
