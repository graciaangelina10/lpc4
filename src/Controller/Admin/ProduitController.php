<?php

namespace App\Controller\Admin;

use App\Entity\Abonnement;
use App\Entity\Achat;
use App\Entity\Paiement;
use App\Entity\Plan;
use App\Entity\Produit;
use App\Form\ProduitType;
use App\Service\ArticleService;
use App\Service\PaiementService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\PaypalService;
use App\Service\AbonnementService;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Produit controller.
 *
 * @Route("admin/produit")
 */
class ProduitController extends AbstractController
{
    protected $paypalService;
    protected $abonnementService;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(PaypalService $paypalService, AbonnementService $abonnementService, TranslatorInterface $translator)
    {
        $this->paypalService = $paypalService;
        $this->prixAbonnement = $abonnementService->getPrixAbonnement();
        $this->translator = $translator;
    }
    /**
     * Lists all produit entities.
     *
     * @Route("/", name="admin_produit_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository(Produit::class)->findAll();

        return $this->render(
            'admin/produit/index.html.twig', array(
            'produits' => $produits,
            )
        );
    }

    /**
     * Creates a new produit entity.
     *
     * @Route("/new", name="admin_produit_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_produit_index', array('id' => $produit->getId()));
        }

        return $this->render(
            'admin/produit/new.html.twig', array(
            'produit' => $produit,
            'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a produit entity.
     *
     * @Route("/{id}", name="admin_produit_show", methods={"GET"})
     */
    public function showAction(Produit $produit)
    {
        $deleteForm = $this->createDeleteForm($produit);

        return $this->render(
            'admin/produit/show.html.twig', array(
            'produit' => $produit,
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing produit entity.
     *
     * @Route("/{id}/edit", name="admin_produit_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Produit $produit, PaypalService $paypalService)
    {

        $deleteForm = $this->createDeleteForm($produit);
        $editForm = $this->createForm('App\Form\ProduitType', $produit);
        $prixInitial = $produit->getPrix();

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $paypalService->majTarifPlan($_POST, $prixInitial);

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_produit_edit', array('id' => $produit->getId()));
        }

        return $this->render(
            'admin/produit/edit.html.twig', array(
            'produit' => $produit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a Produit entity.
     *
     * @param Produit $produit produit
     *
     * @Route("/delete/{id}", name="admin_produit_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Produit $produit)
    {
        if (!$produit) {
            throw new \Exception('Unable to find Produit entity');
        } else {
            $em = $this->getDoctrine()->getManager();

            $panierProduit = $em->getRepository("App:PanierProduit")->findBy(array("produit"=>$produit));

            if ($panierProduit) {
                foreach ($panierProduit as $panier) {
                    $em->remove($panier);
                }
            }

            $achats = $em->getRepository("App:Achat")->findBy(array("produit"=>$produit));
            if ($achats) {
                foreach ($achats as $achat) {
                    $em->remove($achat);
                }
            }

            $em->remove($produit);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_produit_index');
    }

    /**
     * Deletes a produit entity.
     *
     * @Route("/{id}", name="admin_produit_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Produit $produit)
    {
        $form = $this->createDeleteForm($produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
        }

        return $this->redirectToRoute('admin_produit_index');
    }

    /**
     * Creates a form to delete a produit entity.
     *
     * @param Produit $produit The produit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produit $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_produit_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Paiement $paiement
     * @Route("/achat/delete/{id}", name="admin_achat_delete",methods={"GET","POST"})
     * @return RedirectResponse
     */
    public function deleteAchatProduitAction(Achat $achat, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $achat->getUser();

        $em->remove($achat);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('suppression.effectuee')
        );

        return $this->redirectToRoute('admin_user_edit', array('id' => $user->getId()));

    }

    /**
     * @param Abonnement $abonnement
     * @Route("/abonnement/delete/{id}", name="admin_abonnement_delete",methods={"GET","POST"})
     * @return RedirectResponse
     */
    public function deleteAbonnementUserAction(Abonnement $abonnement, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $abonnement->getUser();

        $em->remove($abonnement);
        $user->setPaidSubscriber(null);
        $user->setEndSubscriptionDate(null);
        $em->persist($user);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('suppression.effectuee')
        );

        return $this->redirectToRoute('admin_user_edit', array('id' => $user->getId()));

    }

    /**
     * @param Produit $produit, ArticleService $articleService
     *
     * @Route("/duplicate/{id}", name="admin_produit_duplicate", methods={"POST", "GET"})
     *
     * @return RedirectResponse
     */
    public function duplicateAction(Produit $produit, ArticleService $articleService)
    {
        $articleService->duplicateContent($produit, "produit");

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('enregistrement.effectuee')
        );

        return $this->redirectToRoute('admin_produit_index');

    }
}
