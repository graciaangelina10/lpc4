<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 2/4/19
 * Time: 10:01 AM
 */

namespace App\Controller\Admin;
use App\Entity\Archive;
use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * Class ArchiveController
 * @package App\Controller\Admin
 *
 * @Route("/admin/archive")
 */
class ArchiveController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * @param Article $article
     *
     * @Route("/add/{id}", name="admin_archive_add_article", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addArchiveAction(Article $article)
    {
        $em = $this->getDoctrine()->getManager();

        $archive = new Archive();
        $archive->setArticle($article);

        $em->persist($archive);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'warning',
            "L'article a été bien archivé"
        );

        return $this->redirectToRoute('admin_article_index');
    }

    /**
     * @Route("/", name="admin_archive")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $archives = $em->getRepository(Archive::class)->findAll();

        return $this->render("admin/archive/index.html.twig", [
            'archives' => $archives,
        ]);
    }

    /**
     * @param Archive $archive
     *
     * @Route("/restaurer/{id}", name="admin_archive_restaurer", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function restaurerAction(Archive $archive)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($archive);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->translator->trans('enregistrement.effectuee')
        );

        return $this->redirectToRoute('admin_article_index');

    }

}