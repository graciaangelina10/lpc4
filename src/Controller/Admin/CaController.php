<?php

namespace App\Controller\Admin;

use App\Entity\Ca;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Ca controller.
 *
 * @Route("admin/ca")
 */
class CaController extends AbstractController
{
    /**
     * Lists all ca entities.
     *
     * @Route("/", name="admin_ca_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cas = $em->getRepository('App:Ca')->findAll();

        return $this->render('admin/ca/index.html.twig', array(
            'cas' => $cas,
        ));
    }

    /**
     * Creates a new ca entity.
     *
     * @Route("/new", name="admin_ca_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ca = new Ca();
        $form = $this->createForm('App\Form\CaType', $ca);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ca);
            $em->flush();

            return $this->redirectToRoute('admin_ca_show', array('id' => $ca->getId()));
        }

        return $this->render('admin/ca/new.html.twig', array(
            'ca' => $ca,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ca entity.
     *
     * @Route("/{id}", name="admin_ca_show")
     * @Method("GET")
     */
    public function showAction(Ca $ca)
    {
        $deleteForm = $this->createDeleteForm($ca);

        return $this->render('admin/ca/show.html.twig', array(
            'ca' => $ca,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ca entity.
     *
     * @Route("/{id}/edit", name="admin_ca_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ca $ca)
    {
        $deleteForm = $this->createDeleteForm($ca);
        $editForm = $this->createForm('App\Form\CaType', $ca);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ca_edit', array('id' => $ca->getId()));
        }

        return $this->render('admin/ca/edit.html.twig', array(
            'ca' => $ca,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ca entity.
     *
     * @Route("/delete/{id}", name="admin_ca_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Ca $ca)
    {
        $form = $this->createDeleteForm($ca);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ca);
            $em->flush();
        }

        return $this->redirectToRoute('admin_ca_index');
    }

    /**
     * Creates a form to delete a ca entity.
     *
     * @param Ca $ca The ca entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ca $ca)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_ca_delete', array('id' => $ca->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
