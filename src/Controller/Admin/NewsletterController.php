<?php

namespace App\Controller\Admin;

use App\Entity\Newsletter;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Newsletter controller.
 *
 * @Route("admin/newsletter")
 */
class NewsletterController extends AbstractController
{

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Lists all newsletter entities.
     *
     * @Route("/", name="admin_newsletter_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $newsletters = $em->getRepository(Newsletter::class)->findNewsletter();

        return $this->render('admin/newsletter/index.html.twig', array(
            'newsletters' => $newsletters,
        ));
    }

    /**
     * Creates a new newsletter entity.
     *
     * @Route("/new", name="admin_newsletter_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $newsletter = new Newsletter();
        $form = $this->createForm('App\Form\NewsletterType', $newsletter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newsletter);
            $em->flush();

            return $this->redirectToRoute('admin_newsletter_show', array('id' => $newsletter->getId()));
        }

        return $this->render('admin/newsletter/new.html.twig', array(
            'newsletter' => $newsletter,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a newsletter entity.
     *
     * @Route("/{id}", name="admin_newsletter_show")
     * @Method("GET")
     */
    public function showAction(Newsletter $newsletter)
    {
        $deleteForm = $this->createDeleteForm($newsletter);

        return $this->render('admin/newsletter/show.html.twig', array(
            'newsletter' => $newsletter,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing newsletter entity.
     *
     * @Route("/{id}/edit", name="admin_newsletter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Newsletter $newsletter)
    {
        $deleteForm = $this->createDeleteForm($newsletter);
        $editForm = $this->createForm('App\Form\NewsletterType', $newsletter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_newsletter_index');
        }

        return $this->render('admin/newsletter/edit.html.twig', array(
            'newsletter' => $newsletter,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a newsletter entity.
     *
     * @Route("/{id}", name="admin_newsletter_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Newsletter $newsletter)
    {
        $form = $this->createDeleteForm($newsletter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($newsletter);
            $em->flush();
        }

        return $this->redirectToRoute('admin_newsletter_index');
    }

    /**
     * Creates a form to delete a newsletter entity.
     *
     * @param Newsletter $newsletter The newsletter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Newsletter $newsletter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_newsletter_delete', array('id' => $newsletter->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Deletes a pub entity.
     *
     * @param Pub $pub pub
     *
     * @Route("/delete/{id}", name="admin_newsletter_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Newsletter $newsletter)
    {
        if (!$newsletter) {
            throw new \Exception('Unable to find newsletter entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($newsletter);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_newsletter_index');
    }

    /**
     * Lists all newsletter entities.
     *
     * @Route("/barometre/index", name="admin_barometre_index")
     * @Method("GET")
     */
    public function indexBarometreAction()
    {
        $em = $this->getDoctrine()->getManager();

        $newsletters = $em->getRepository(Newsletter::class)->findBarometre();

        return $this->render('admin/newsletter/barometre.html.twig', array(
            'newsletters' => $newsletters,
        ));
    }

    /**
     * Deletes a pub entity.
     *
     *
     * @Route("/delete-barometre/{id}", name="admin_barometre_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementBarometreAction(Newsletter $newsletter)
    {
        if (!$newsletter) {
            throw new \Exception('Unable to find Barometre entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($newsletter);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_barometre_index');
    }

    /**
     * Displays a form to edit an existing newsletter entity.
     *
     * @Route("/{id}/edit-barometre", name="admin_barometre_edit")
     * @Method({"GET", "POST"})
     */
    public function editBarometreAction(Request $request, Newsletter $newsletter)
    {
        $deleteForm = $this->createDeleteForm($newsletter);
        $editForm = $this->createForm('App\Form\NewsletterDownloadType', $newsletter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_barometre_index');
        }

        return $this->render('admin/newsletter/editBarometre.html.twig', array(
            'newsletter' => $newsletter,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/add/export-barometre", name="admin_barometre_export", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportUser(UserService $userService)
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $fileName = 'LPC_barometre_'.date("Y-m-d").'.xlsx';
        $temp_file = $userService->exportBarometreDataBase($fileName);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);

    }

    /**
     * Deletes a newsletter entity.
     *
     * @Route("/sendinblue/{id}/from-ajax", name="admin_newsletter_sendiblue_update")
     * @Method("DELETE")
     */
    public function updateSendinBlueAction(Request $request, Newsletter $newsletter)
    {
        $em = $this->getDoctrine()->getManager();

        $isSendiblue = $request->get('isSendinblue');

        $newsletter->setSendBlue($isSendiblue);

        $em->persist($newsletter);

        $em->flush();

        return new Response("OK");
    }
}
