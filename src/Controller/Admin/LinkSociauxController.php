<?php

namespace App\Controller\Admin;

use App\Entity\LinkSociaux;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Linksociaux controller.
 *
 * @Route("admin/linksociaux")
 */
class LinkSociauxController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all linkSociaux entities.
     *
     * @Route("/", name="admin_linksociaux_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        $linkSociauxes = $em->getRepository(LinkSociaux::class)->findAll();

        return $this->render(
            'admin/linksociaux/index.html.twig', array(
            'linkSociauxes' => $linkSociauxes,
            )
        );
    }

    /**
     * Creates a new linkSociaux entity.
     *
     * @Route("/new", name="admin_linksociaux_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $linkSociaux = new Linksociaux();
        $form = $this->createForm('App\Form\LinkSociauxType', $linkSociaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($linkSociaux);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_linksociaux_index', array('id' => $linkSociaux->getId()));
        }

        return $this->render(
            'admin/linksociaux/new.html.twig', array(
            'linkSociaux' => $linkSociaux,
            'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a linkSociaux entity.
     *
     * @Route("/{id}", name="admin_linksociaux_show", methods={"GET"})
     */
    public function showAction(LinkSociaux $linkSociaux)
    {
        $deleteForm = $this->createDeleteForm($linkSociaux);

        return $this->render(
            'admin/linksociaux/show.html.twig', array(
            'linkSociaux' => $linkSociaux,
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing linkSociaux entity.
     *
     * @Route("/{id}/edit", name="admin_linksociaux_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, LinkSociaux $linkSociaux)
    {
        $deleteForm = $this->createDeleteForm($linkSociaux);
        $editForm = $this->createForm('App\Form\LinkSociauxType', $linkSociaux);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_linksociaux_edit', array('id' => $linkSociaux->getId()));
        }

        return $this->render(
            'admin/linksociaux/edit.html.twig', array(
            'linkSociaux' => $linkSociaux,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a linksociaux entity.
     *
     * @param LinkSociaux $linkSociaux bref
     *
     * @Route("/delete/{id}", name="admin_linksociaux_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(LinkSociaux $linkSociaux)
    {
        if (!$linkSociaux) {
            throw new \Exception('Unable to find linkSociaux entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($linkSociaux);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_linksociaux_index');
    }

    /**
     * Deletes a linkSociaux entity.
     *
     * @Route("/{id}", name="admin_linksociaux_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, LinkSociaux $linkSociaux)
    {
        $form = $this->createDeleteForm($linkSociaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($linkSociaux);
            $em->flush();
        }

        return $this->redirectToRoute('admin_linksociaux_index');
    }

    /**
     * Creates a form to delete a linkSociaux entity.
     *
     * @param LinkSociaux $linkSociaux The linkSociaux entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LinkSociaux $linkSociaux)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_linksociaux_delete', array('id' => $linkSociaux->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
