<?php
/**
 * DefaultController Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */
class DefaultController extends AbstractController
{
    /**
     * Index Action.
     *
     * @return Response
     *
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render(
            'default/index.html.twig',
            [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) .
                DIRECTORY_SEPARATOR,
            ]
        );
    }
}
