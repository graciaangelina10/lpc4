<?php

namespace App\Controller\Admin;

use App\Entity\Pub;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Pub controller.
 *
 * @Route("admin/pub")
 */
class PubController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all pub entities.
     *
     * @Route("/", name="admin_pub_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        $pubs = $em->getRepository(Pub::class)->findAll();

        return $this->render(
            'admin/pub/index.html.twig', array(
            'pubs' => $pubs,
            )
        );
    }

    /**
     * Creates a new pub entity.
     *
     * @Route("/new", name="admin_pub_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $pub = new Pub();
        $form = $this->createForm('App\Form\PubType', $pub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($pub->getForAbonnee()) {
                $pubActive = $em->getRepository('App:Pub')->findBy(['active'=>true, 'forAbonnee' => true, 'position'=>$pub->getPosition()]);
                if ($pubActive) {
                    $pubActive = $pubActive[0];
                    $pubActive->setActive(false);
                    $pubActive->setForAbonnee(false);
                    $em->persist($pubActive);
                }
            }else {
                $pubActive = $em->getRepository(Pub::class)->findBy(['active'=>true, 'forAbonnee' => false, 'position'=>$pub->getPosition()]);
                if ($pubActive) {
                    $pubActive = $pubActive[0];
                    $pubActive->setActive(false);
                    $em->persist($pubActive);
                }
            }


            $em->persist($pub);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_pub_index', array('id' => $pub->getId()));
        }

        return $this->render(
            'admin/pub/new.html.twig', array(
            'pub' => $pub,
            'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a pub entity.
     *
     * @Route("/{id}", name="admin_pub_show", methods={"GET"})
     */
    public function showAction(Pub $pub)
    {
        $deleteForm = $this->createDeleteForm($pub);

        return $this->render(
            'admin/pub/show.html.twig', array(
            'pub' => $pub,
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing pub entity.
     *
     * @Route("/{id}/edit", name="admin_pub_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Pub $pub)
    {
        $deleteForm = $this->createDeleteForm($pub);
        $editForm = $this->createForm('App\Form\PubType', $pub);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($pub->getActive()) {
                //desactiver d'abord s'il y a d'autre active

                if ($pub->getForAbonnee()) {
                    $pubActive = $em->getRepository('App:Pub')->findBy(['active'=>true, 'forAbonnee' => true, 'position'=>$pub->getPosition()]);
                    if ($pubActive) {
                        $pubActive = $pubActive[0];
                        $pubActive->setActive(false);
                        $pubActive->setForAbonnee(false);
                        $em->persist($pubActive);
                    }
                }else {
                    $pubActive = $em->getRepository('App:Pub')->findBy(['active'=>true, 'forAbonnee' => false, 'position'=>$pub->getPosition()]);
                    if ($pubActive) {
                        $pubActive = $pubActive[0];
                        $pubActive->setActive(false);
                        $em->persist($pubActive);
                    }
                }



            }
            $em->flush();


            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute('admin_pub_edit', array('id' => $pub->getId()));
        }

        return $this->render(
            'admin/pub/edit.html.twig', array(
            'pub' => $pub,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a pub entity.
     *
     * @param Pub $pub pub
     *
     * @Route("/delete/{id}", name="admin_pub_delete_element",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Pub $pub)
    {
        if (!$pub) {
            throw new \Exception('Unable to find pub entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pub);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_pub_index');
    }

    /**
     * Deletes a pub entity.
     *
     * @Route("/{id}", name="admin_pub_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Pub $pub)
    {
        $form = $this->createDeleteForm($pub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pub);
            $em->flush();
        }

        return $this->redirectToRoute('admin_pub_index');
    }

    /**
     * Creates a form to delete a pub entity.
     *
     * @param Pub $pub The pub entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pub $pub)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_pub_delete', array('id' => $pub->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function getPubAction($position, $page = null)
    {
        $em = $this->getDoctrine()->getManager();

        //recuperer la derniere pub insérer
        if (null != $page && ($page == "liste_revues" || $page == "liste_revues_annee" || $page == "revue_fiche")) {
            $pubs = $em->getRepository(Pub::class)->findBy(array("position"=>$position, 'active'=>true, 'forAbonnee' => true), array("id"=>"desc"));
            $pub = array();
            if ($pubs) {
                $pub = $pubs[0];
            }else {
                $pubs = $em->getRepository(Pub::class)->findBy(array("position"=>$position, 'active'=>true, 'forAbonnee' => false), array("id"=>"desc"));
                if ($pubs) {
                    $pub = $pubs[0];
                }else {
                    $pubs = $em->getRepository(Pub::class)->findBy(array("position"=>$position), array("id"=>"desc"));
                    if ($pubs) {
                        $pub = $pubs[0];
                    }
                }
            }
        }else {
            $pubs = $em->getRepository(Pub::class)->findBy(array("position"=>$position, 'active'=>true), array("id"=>"desc"));
            $pub = array();
            if ($pubs) {
                $pub = $pubs[0];
            }else {
                $pubs = $em->getRepository(Pub::class)->findBy(array("position"=>$position), array("id"=>"desc"));
                if ($pubs) {
                    $pub = $pubs[0];
                }
            }
        }


        return $this->render(
            'admin/pub/pub.html.twig', array(
            'pub' => $pub,
            )
        );
    }
}
