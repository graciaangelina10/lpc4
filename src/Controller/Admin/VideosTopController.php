<?php

namespace App\Controller\Admin;

use App\Entity\VideoTop;
use App\Service\VideoTopService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Form\VideoTopSelectionType;
use App\Model\VideoTopSelection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("admin/videos")
 */
class VideosTopController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * @Route("/", name="admin_videos_top", methods={"GET"})
     */
    public function indexAction(VideoTopService $videoTopService)
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $videoTopService->checkDefaultVideosExistence();
        $videos = $videoTopService->getAll();

        $videoTopSelection = new VideoTopSelection();

        foreach ($videos as $video) {
            $article = $video->getArticle();

            if ($article) {
                $setter = 'setArticle' . $video->getPosition();
                if (method_exists($videoTopSelection, $setter)) {
                    $videoTopSelection->$setter($article);
                }
            }
        }

        $form = $this->createForm(
            VideoTopSelectionType::class,
            $videoTopSelection,
            [
                'action' => $this->generateUrl('admin_videos_update_selection'),
            ]
        );

        return $this->render(
            'admin/videos/index.html.twig',
            [
                'videos' => $videos,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/update", name="admin_videos_update_selection")
     * @Method({"POST"})
     */
    public function updateSelectionAction(Request $request, VideoTopService $videoTopService)
    {
        $videoTopSelection = new VideoTopSelection();

        $form = $this->createForm(
            VideoTopSelectionType::class,
            $videoTopSelection,
            [
                'action' => $this->generateUrl('admin_videos_update_selection'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $videos = $videoTopService->getAll();

            foreach ($videos as $video) {
                $article = null;
                $getter = 'getArticle' . $video->getPosition();

                if (method_exists($videoTopSelection, $getter)) {
                    $article = $videoTopSelection->$getter();
                }

                $video->setArticle($article);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );
        }

        return $this->redirectToRoute('admin_videos_top');
    }

}
