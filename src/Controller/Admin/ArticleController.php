<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\EventTop;
use App\Entity\PositionArticleRevue;
use App\Entity\VideoTop;
use App\Form\ArticleContributeurType;
use App\Form\ArticleFilterType;
use App\Service\ArticleService;
use App\Service\RevueService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Article controller.
 *
 * @category Class
 * @see      ****
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @Route("admin/article")
 */
class ArticleController extends AbstractController
{
    private $articleService;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(ArticleService $articleService, TranslatorInterface $translator)
    {
        $this->articleService = $articleService;
        $this->translator = $translator;
    }

    /**
     * Lists all article entities.
     *
     * @return Response
     *
     * @Route("/", name="admin_article_index",methods={"GET", "POST"})
     */
    public function indexAction(Request $request, ArticleService $articleService)
    {
        $this->denyAccessUnlessGranted(['ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_VALIDATOR', 'ROLE_CONTRIBUTEUR']);

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();

        if (in_array('ROLE_CONTRIBUTEUR', $user->getRoles())){
            $andUser = ['user'=>$user];
        } else {
            $andUser = [];
        }

        $articleForm = new Article();

        //créer le filtre
        $form = $this->createForm(
            ArticleFilterType::class,
            $articleForm
        );

        //$form->handleRequest($request); //Pb avec categorie collection
        //if ($form->isSubmitted() && $form->isValid()) {
        if (isset($_POST['appliquer']) && null!=$_POST['appliquer']){
            $data = $_POST;
            $session->set('dataFilter',$data);
            $articles = $em->getRepository(Article::class)->getDataByFilter($data, $andUser, $session);

            $articleForm = $articleService->setDataFormList($articleForm, $data);

            $form = $this->createForm(
                ArticleFilterType::class,
                $articleForm
            );

        } else {
            if (null!=$request->get('init')) {
                $data = [];
                $session->set('dataFilter',$data);
                $articles = $em->getRepository(Article::class)->getDataByFilter($data, $andUser, $session);
            } elseif (null!=$session->get('dataFilter')){
                $data = $session->get('dataFilter');


                $articles = $em->getRepository(Article::class)->getDataByFilter($data, $andUser, $session);

                $articleForm = $articleService->setDataFormList($articleForm, $data);

                $form = $this->createForm(
                    ArticleFilterType::class,
                    $articleForm
                );
            } else {
                $data = [];
                $session->set('dataFilter',$data);
                $articles = $em->getRepository(Article::class)->getDataByFilter($data, $andUser, $session);
            }

        }

        return $this->render(
            'admin/article/index.html.twig',
            [
                'articles' => $articles,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Creates a new article entity.
     *
     * @param  Request $request request
     * @return Response
     *
     * @Route("/new", name="admin_article_new",methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();

        $validator = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || $this->get('security.authorization_checker')->isGranted('ROLE_EDITOR');

        $articleType = $this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')?ArticleContributeurType::class:ArticleType::class;

        $form = $this->createForm(
            $articleType,
            $article,
            [
                'validation_groups' => $validator,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();

            $data = $form->getData();
            $titre = $data->getTitre();
            //enlever la balise sur le titre
            $article->setTitre(strip_tags(html_entity_decode($titre, ENT_QUOTES | ENT_HTML5)));


            if (null!= $article->getDatePublication()) {
                $datePublication = $article->getDatePublication();
                $timePublication = $article->getTimePublication();

                $datePub = $datePublication->format('Y-m-d');
                $timePub = $timePublication->format('H:i');

                $datePubTime = new \DateTime($datePub." ".$timePub);

                $article->setDatePublication($datePubTime);
            }

            // update or add new tag
            $videoArticle = null;
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')) {
                $article = $this->articleService
                    ->setTagArticle($_POST, $article);

                $article->restoreTwitterBlockquote();
            }
            $videoArticle = $article->getVideoArticle();
            $videoArticle->setArticle($article);


            if ($article->getType() =="Vidéos") {
                //save thumbnails video
                $this->articleService
                    ->saveThumbnails($article);
            }

            $article->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            if($article->getType() !="Vidéos" && null!=$videoArticle) {
                $em->remove($videoArticle);
                $em->flush();
            }

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_article_edit',
                array('id' => $article->getId())
            );
        }

        $extFile = $articleType = $this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')?'_contributeur':'';

        return $this->render(
            'admin/article/new'.$extFile.'.html.twig',
            [
                'article' => $article,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a article entity.
     *
     * @param  Article $article article
     * @return Response
     *
     * @Route("/{id}", name="admin_article_show",methods={"GET"})
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render(
            'admin/article/show.html.twig',
            [
                'article' => $article,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @param  Request $request request
     * @param  Article $article article
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_article_edit",methods={"GET","POST"})
     */
    public function editAction(Request $request, Article $article, RevueService $revueService)
    {
        $deleteForm = $this->createDeleteForm($article);

        $validator = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || $this->get('security.authorization_checker')->isGranted('ROLE_EDITOR');

        $articleType = $this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')?ArticleContributeurType::class:ArticleType::class;

        $datePublication = $article->getDatePublication();
        $article->setTimePublication($datePublication);
        $editForm = $this->createForm(
            $articleType,
            $article,
            [
                'validation_groups' => $validator,
            ]
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $data = $editForm->getData();
            $titre = $data->getTitre();
            //enlever la balise sur le titre
            $article->setTitre(strip_tags(html_entity_decode($titre, ENT_QUOTES | ENT_HTML5)));

            if (null!= $article->getDatePublication()) {
                $datePublication = $article->getDatePublication();
                $timePublication = $article->getTimePublication();

                $datePub = $datePublication->format('Y-m-d');
                $timePub = $timePublication->format('H:i');

                $datePubTime = new \DateTime($datePub." ".$timePub);

                $article->setDatePublication($datePubTime);
            }


            $videoArticle = null;
            // update or add new tag
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')) {
                $article = $this->articleService
                    ->setTagArticle($_POST, $article);

                $article->restoreTwitterBlockquote();
            }
            $videoArticle = $article->getVideoArticle();
            $videoArticle->setArticle($article);


            if ($article->getType() =="Vidéos") {
                //save thumbnails video
                $imageThumbnails = $this->articleService
                    ->saveThumbnails($article);
            }

            //maj position article Revue
            $revueService->addPositionArticle($_POST, $article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            if($article->getType() !="Vidéos" && null!=$videoArticle) {
                $em->remove($videoArticle);
                $em->flush();
            }


            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_article_edit',
                array('id' => $article->getId())
            );
        }

        $extFile = $articleType = $this->get('security.authorization_checker')->isGranted('ROLE_CONTRIBUTEUR')?'_contributeur':'';

        return $this->render(
            'admin/article/edit'.$extFile.'.html.twig',
            [
                'article' => $article,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a article entity.
     *
     * @param Article $article article
     *
     * @Route("/delete/{id}", name="admin_article_delete",methods={"GET","POST"})
     *
     * @return RedirectResponse
     */
    public function deleteElementAction(Article $article)
    {
        if (!$article) {
            throw new \Exception('Unable to find article entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $eventTop = $em->getRepository(EventTop::class)->findByArticle($article);
            if ($eventTop) {
                foreach ($eventTop as $event) {
                    $em->remove($event);
                }
            }
            $videoTop = $em->getRepository(VideoTop::class)->findByArticle($article);
            if ($videoTop) {
                foreach ($videoTop as $eventVideo) {
                    $em->remove($eventVideo);
                }
            }

            $revueArticle = $article->getRevue();
            if ($revueArticle) {
                foreach ($revueArticle as $unRevue) {
                    $article->removeRevue($unRevue);
                }
            }

            //suppression position article revue
            $positionArticleRevue = $em->getRepository(PositionArticleRevue::class)->findBy(array("article"=>$article));
            if ($positionArticleRevue) {
                foreach ($positionArticleRevue as $articleRevue) {
                    $em->remove($articleRevue);
                }
            }



            $em->remove($article);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'error',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_archive');
    }

    /**
     * Delete element.
     *
     * @param Request $request request
     * @param Article $article article
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('admin_article_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param  Article $article The article entity
     * @return Form
     */
    public function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'admin_article_delete',
                    array('id' => $article->getId())
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Article $article
     *
     * @Route("/duplicate/{id}", name="admin_article_duplicate", methods={"POST", "GET"})
     *
     * @return RedirectResponse
     */
    public function duplicateAction(Article $article, ArticleService $articleService)
    {
        $articleService->duplicateContent($article, "article");

        $this->get('session')->getFlashBag()->add(
            'success',
            $this->translator->trans('enregistrement.effectuee')
        );

        return $this->redirectToRoute('admin_article_index');

    }
}
