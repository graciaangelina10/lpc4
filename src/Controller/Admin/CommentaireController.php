<?php

/**
 * CommentaireController Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Controller\Admin
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Commentaire controller.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @Route("admin/commentaire")
 */
class CommentaireController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * Lists all commentaire entities.
     *
     * @return Response
     *
     * @Route("/", name="admin_commentaire_index", methods={"GET"})
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $em = $this->getDoctrine()->getManager();

        $commentaires = $em->getRepository(Commentaire::class)->findBy(
            array(),
            ['treeLeft' => 'ASC', "dateCreation"=>"DESC"]
        );

        $articleWithComment = $em->getRepository(Article::class)->getArticleWithComment();

        return $this->render(
            'admin/commentaire/index.html.twig',
            [
                'commentaires' => $commentaires,
                'articles' => $articleWithComment,
            ]
        );
    }

    /**
     * Creates a new commentaire entity.
     *
     * @param  Request $request request
     * @return Response
     *
     * @Route("/new", name="admin_commentaire_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (null!=$commentaire->getParent()) {
                $article = $commentaire->getParent()->getArticle();
                $commentaire->setArticle($article);
            }

            $em->persist($commentaire);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_commentaire_index',
                array(
                    'id' => $commentaire->getId(),
                )
            );
        }

        return $this->render(
            'admin/commentaire/new.html.twig',
            [
                'commentaire' => $commentaire,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a commentaire entity.
     *
     * @param  Commentaire $commentaire commentaire
     * @return Response
     *
     * @Route("/{id}", name="admin_commentaire_show", methods={"GET"})
     */
    public function showAction(Commentaire $commentaire)
    {
        $deleteForm = $this->createDeleteForm($commentaire);

        return $this->render(
            'commentaire/show.html.twig',
            [
                'commentaire' => $commentaire,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing commentaire entity.
     *
     * @param  Request     $request     request
     * @param  Commentaire $commentaire commentaire
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_commentaire_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Commentaire $commentaire)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $deleteForm = $this->createDeleteForm($commentaire);
        $editForm = $this->createForm(
            CommentaireType::class,
            $commentaire
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('enregistrement.effectuee')
            );

            return $this->redirectToRoute(
                'admin_commentaire_edit',
                array('id' => $commentaire->getId())
            );
        }

        return $this->render(
            'admin/commentaire/edit.html.twig',
            [
                'commentaire' => $commentaire,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a commentire entity.
     *
     * @param  Commentaire $commentaire commentaire
     * @return RedirectResponse
     *
     * @Route("/delete/{id}", name="admin_commentaire_delete",methods={"GET","POST"})
     */
    public function deleteElementAction(Commentaire $commentaire)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_VALIDATOR']);

        if (!$commentaire) {
            throw new \Exception('Unable to find commentaire entity');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commentaire);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->translator->trans('suppression.effectuee')
            );
        }

        return $this->redirectToRoute('admin_commentaire_index');
    }

    /**
     * Delete Action.
     *
     * @param Request     $request     request
     * @param Commentaire $commentaire commentaire
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Commentaire $commentaire)
    {
        $this->denyAccessUnlessGranted(['ROLE_ADMIN', 'ROLE_VALIDATOR']);

        $form = $this->createDeleteForm($commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commentaire);
            $em->flush();
        }

        return $this->redirectToRoute('admin_commentaire_index');
    }

    /**
     * Creates a form to delete a commentaire entity.
     *
     * @param  Commentaire $commentaire The commentaire entity
     * @return Form
     */
    public function createDeleteForm(Commentaire $commentaire)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'admin_commentaire_delete',
                    array('id' => $commentaire->getId())
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
