<?php
namespace App\Controller\Main;

use App\Entity\PanierProduit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;
use Psr\Log\LoggerInterface;

use App\Entity\Produit;
use App\Service\AchatService;
use App\Service\PanierService;
use App\Service\PaypalService;
use App\Service\PaiementService;
use App\Service\AbonnementService;
use App\Form\Boutique\LoginType;
use App\Form\AdresseType;

class AbonnementPaiementController extends AbstractController
{
    /**
     * @Route("/boutique/paiement-abonnement-ok", name="payment_abonnement_execute")
     * @Method({"POST"})
     */
    public function executeAction(
        PaypalService $paypalService,
        LoggerInterface $logger,
        AbonnementService $abonnementService,
        Request $request,
        PanierService $panierService,
        \Swift_Mailer $mailer
    ) {
        $user = $this->getUser();
        $token = $request->get('token');

        $abonnement = $abonnementService->getNotExecutedYetAbonnement($user);
        $abonnement->setAgreementToken($token);
        $abonnement->setAgreed(true);
        $abonnement->setPrix($abonnementService->getPrixAbonnement());
        $user->setPaidSubscriber(true);
        $user->setEndSubscriptionDate($abonnement->getDateEnd());
        $user->setSubscriptionRenewal(true);
        $user->setNewsletterSubscription(true);
        $paypalService->executeAgreement($token);

        $panier = $panierService->getPanierForUser($user);
        $em = $this->getDoctrine()->getManager();

        if ($panier) {
            $produitList = $panier->getProduitList();
            foreach ($produitList as $produitPanier) {
                $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("produit"=>$produitPanier, "panier"=>$panier));

                if ($panierProduit) {
                    $em->remove($panierProduit[0]);
                }
                $panier->removeProduit($produitPanier);

            }
        }
        $em->flush();
        //send email
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement);
        //send à cpc
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement, "c.demarco@bba.fr");
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement, "pbaey@cpc-doc.com");
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement, "mrazafimandimby@bocasay.com");
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement, "camyot@bba.fr");
        $paypalService->sendMail($user, "Confirmation abonnement sur LPC", $mailer, $abonnement, "comptabilite@bba.fr");

        return $this->redirectToRoute('front_boutique_abonnement_ok');
    }

    /**
     * @Route("/api/payment-abonnement/cancel", name="payment_abonnement_cancel")
     * @Method({"POST"})
     */
    public function cancelAction(
        PaypalService $paypalService,
        Request $request,
        LoggerInterface $logger,
        AchatService $achatService,
        PaiementService $paiementService,
        PanierService $panierService
    ) {
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);
        $em = $this->getDoctrine()->getManager();

        if ($panier) {
            $produitList = $panier->getProduitList();
            foreach ($produitList as $produitPanier) {
                $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("produit"=>$produitPanier, "panier"=>$panier));

                if ($panierProduit) {
                    $em->remove($panierProduit[0]);
                }
                $panier->removeProduit($produitPanier);

            }
        }
        $em->flush();
        return $this->render('main/Boutique/Failed/cancel.html.twig');
    }

    /**
     * @Route("/boutique/echec_paiement", name="payment_failed")
     * @Method({"GET"})
     */
    public function failedAction(PanierService $panierService)
    {

        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);
        $em = $this->getDoctrine()->getManager();

        if ($panier) {
            $produitList = $panier->getProduitList();
            foreach ($produitList as $produitPanier) {
                $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("produit"=>$produitPanier, "panier"=>$panier));

                if ($panierProduit) {
                    $em->remove($panierProduit[0]);
                }
                $panier->removeProduit($produitPanier);

            }
        }
        $em->flush();

        return $this->render('main/Boutique/Failed/index.html.twig');
    }

    /**
     * @Route("/boutique/abonnement-paypal", name="front_boutique_agreement_creation")
     * @Method({"GET"})
     */
    public function paypalAgreementCreation(
        PaypalService $paypalService,
        AbonnementService $abonnementService
    ) {
        $user = $this->getUser();

        $agreement = $paypalService->createAgreement($user);
        $abonnement = $abonnementService->createAbonnementFromAgreement($agreement, $user);
        $approvalUrl = $agreement->getApprovalLink();

        return $this->redirect($approvalUrl);
    }

    /**
     * @Route("/boutique/abonnement-paypal/execute", name="front_boutique_agreement_execute")
     * @Method({"GET"})
     */
    public function paypalExcutePaymentAbonnement(
        Request $request,
        AbonnementService $abonnementService,
        PaypalService $paypalService
    ) {

    }
}
