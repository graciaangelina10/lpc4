<?php
namespace App\Controller\Main;

use App\Entity\Abonnement;
use App\Entity\Paiement;
use App\Entity\PanierProduit;
use App\Entity\User;
use App\Service\PaiementService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use App\Entity\Produit;
use App\Service\AchatService;
use App\Service\PaypalService;
use App\Service\PanierService;
use App\Form\Boutique\LoginType;
use App\Form\OrderRegistrationType;
use App\Form\AdresseType;

class BoutiqueController extends AbstractController
{
    private $userManager;
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher, UserProviderInterface $userManager)
    {
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/boutique", name="front_boutique", methods={"GET"})
     */
    public function indexAction(AchatService $achatService, PanierService $panierService)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $produits = $achatService->getProduitsForUser($user);
        $panier = $panierService->getPanierForUser($user);

        $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');

        $horsSerie = $achatService->getProduit($produits, 'hors-serie');

        $professionComptable = $achatService->getProduit($produits, 'profession-comptable');

        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        return $this->render(
            'main/Boutique/index.html.twig',
            [
                'produits' => $professionComptable,
                'abonnementPrime' => $abonnementPrime,
                'horsSerie' => $horsSerie,
                'panier' => $panier,
                'panierProduit' => $panierProduit,
            ]
        );
    }

    /**
     * Post pour ajouter un produit au panier
     * Si le produit est déjà dans le panier, ça ne fait rien
     *
     * @Route("/boutique/add_to cart/{produit}", name="front_boutique_add_to_cart", methods={"POST", "GET"})
     */
    public function addToCartAction(Produit $produit, PanierService $panierService, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);
        $panier->addProduit($produit);
        //create new
        $panierProduit = new PanierProduit();
        $panierProduit->setProduit($produit);
        $panierProduit->setPanier($panier);
        $panierProduit->setQtt(1);
        $em->persist($panierProduit);

        $em->flush();

        //$ipAdresse = "212.85.152.125"; //Ip french test
        //$ipAdresse = "23.221.76.66"; //Ip United States
        $ipAdresse = $request->getClientIp();

        //check frais de transport
        if ($produit->getRubrique() == "abonnement-prime") {
            $panierService->setFraisTransport($panier, $ipAdresse);
        }


        $panierProduits = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        //check panier produit
        $hasAbonnement = false;
        foreach ($panierProduits as $panierProduit) {
            if ($panierProduit->getProduit()->getRubrique() == "abonnement-prime") {
                $hasAbonnement = true;
            }
        }

        if ($hasAbonnement && $produit->getRubrique() != "abonnement-prime") {
            $this->get('session')->getFlashBag()->add(
                'error',
                'Vous avez déjà dans votre panier un abonnement d\'un an au magazine LPC'
            );
        }


        if (null != $request->get('for') && $request->get('for') == "confirmation") {

            if ($hasAbonnement) {
                foreach ($panierProduits as $panierProduit) {
                    if ($panierProduit->getProduit()->getRubrique() != "abonnement-prime" && $panierProduit->getProduit()->getRubrique() != "frais-transport") {
                        $em->remove($panierProduit);
                        $panier->removeProduit($panierProduit->getProduit());
                    }
                }
                $em->flush();
            }


            return $this->redirectToRoute('front_boutique_order_confirmation');
        }

        return $this->redirectToRoute('front_boutique');
    }

    /**
     * Post pour ajouter un produit au panier
     * Si le produit est déjà dans le panier, ça ne fait rien
     *
     * @Route("/boutique/add_to cart_v2/{produit}", name="front_boutique_add_to_cart_v2", methods={"POST"})
     */
    public function addToCartV2Action(Produit $produit, PanierService $panierService, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);
        $panier->addProduit($produit);
        //create new
        $panierProduit = new PanierProduit();
        $panierProduit->setProduit($produit);
        $panierProduit->setPanier($panier);
        $panierProduit->setQtt(1);
        $em->persist($panierProduit);

        $em->flush();

        //$ipAdresse = "212.85.152.125"; //Ip french test
        //$ipAdresse = "23.221.76.66"; //Ip United States
        $ipAdresse = $request->getClientIp();

        //check frais de transport
        if ($produit->getRubrique() == "abonnement-prime") {
            $panierService->setFraisTransport($panier, $ipAdresse);
        }


        /*$panierProduits = $em->getRepository("App:PanierProduit")->findBy(array("panier"=>$panier));
        //check panier produit
        $hasAbonnement = false;
        foreach ($panierProduits as $panierProduit) {
            if ($panierProduit->getProduit()->getRubrique() == "abonnement-prime") {
                $hasAbonnement = true;
            }
        }

        if ($hasAbonnement) {
            foreach ($panierProduits as $panierProduit) {
                if ($panierProduit->getProduit()->getRubrique() != "abonnement-prime") {
                    $em->remove($panierProduit);
                }
            }
            $em->flush();
        }*/

        if (null != $request->get('for') && $request->get('for') == "confirmation") {
            return $this->redirectToRoute('front_boutique_order_confirmation');
        }

        //Modif marcel le 04/05/2021: pop up proposition abonnement

        $currentRoute = "front_boutique";

        return $this->panierAjaxProduit($panierService, $currentRoute);

        //return $this->redirectToRoute('front_boutique');
    }

    /**
     * Post pour retirer un article du panier
     *
     * @Route("/boutique/remove_fromq_cart/{produit}", name="front_boutique_remove_from_cart", methods={"POST"})
     */
    public function removeFromCartAction(Request $request, Produit $produit, PanierService $panierService)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);

        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("produit"=>$produit, "panier"=>$panier));

        if ($panierProduit) {
            $em->remove($panierProduit[0]);
        }

        $panier->removeProduit($produit);

        $em->flush();

        $panierService->removeFraisTransport($panier);

        $lastRoute = $request->get('last-route');

        if (!$lastRoute) {
            $lastRoute = 'front_boutique';
        }

        return $this->redirectToRoute($lastRoute);
    }

    /**
     * Page qui vérifie qu'on a bien un utilisateur de loggé
     * Si il n'y a pas d'user de log, on affiche 2 forms :
     *  - un pour s'inscrire
     *  - un pour se log
     *
     * Une fois qu'on a bien un utilisateur qui est log, on redirige vers la confirmation
     * de commande
     *
     * @Route(
     *    "/boutique/identification/{nextRoute}",
     *    name="front_boutique_identification",
     *    defaults={"nextRoute"="front_boutique_order_confirmation"},
     *     methods={"GET"}
     *    )
     */
    public function identificationAction(Request $request, $nextRoute)
    {
        $user = $this->getUser();
        $session = $request->getSession();
        $session->set('app.route_after_identification', $nextRoute);

        if ($user) {
            return $this->redirectToRoute($nextRoute);
        }

        return $this->renderIdentification(null, null, $nextRoute);
    }

    public function renderIdentification(
        $loginForm = null,
        $registrationForm = null
    ) {
        if (!$loginForm) {
            $loginForm = $this->createForm(
                LoginType::class,
                null,
                ['action' => $this->generateUrl('front_boutique_login')]
            );
        }

        if (!$registrationForm) {
            $registrationForm = $this->createForm(
                OrderRegistrationType::class,
                null,
                ['action' => $this->generateUrl('front_boutique_registration')]
            );
        }

        return $this->render(
            'main/Boutique/Identification/index.html.twig',
            [
                'loginForm' => $loginForm->createView(),
                'registrationForm' => $registrationForm->createView(),
            ]
        );
    }

    /**
     * Route pour se log à partir de la page d'identification
     *
     * @Route("/boutique/login", name="front_boutique_login", methods={"POST"})
     */
    public function loginAction(PanierService $panierService, Request $request)
    {
        $loginForm = $this->createForm(LoginType::class);
        $loginForm->handleRequest($request);

        $registrationForm = $this->createForm(
            OrderRegistrationType::class,
            null,
            ['action' => $this->generateUrl('front_boutique_registration')]
        );

        if (!$loginForm->isValid()) {

            return $this->render(
                'main/Boutique/Identification/index.html.twig',
                [
                    'loginForm' => $loginForm->createView(),
                    'registrationForm' => $registrationForm->createView(),
                ]
            );
        }

        $em = $this->getDoctrine()->getManager();
        $username = $loginForm['username']->getData();
        $enteredPassword = $loginForm['password']->getData();

        $repoUser = $em->getRepository(User::class);
        $user = $repoUser->findOneByUsername($username);
        if (!$user) {
            $user = $repoUser->findOneByEmail($username);
        }


        if (!$user) {
            $loginForm->get('username')
                      ->addError(new FormError('Email ou mot de passe incorrect'));

            return $this->render(
                'main/Boutique/Identification/index.html.twig',
                [
                    'loginForm' => $loginForm->createView(),
                    'registrationForm' => $registrationForm->createView(),
                ]
            );
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($enteredPassword, $user->getSalt());

        $encoderService = $this->get('security.password_encoder');
        $isPasswordValid = $encoderService->isPasswordValid($user, $enteredPassword);

        if (!$isPasswordValid) {
            $loginForm->get('username')
                      ->addError(new FormError('Email ou mot de passe incorrect'));

            return $this->render(
                'main/Boutique/Identification/index.html.twig',
                [
                    'loginForm' => $loginForm->createView(),
                    'registrationForm' => $registrationForm->createView(),
                ]
            );
        }

        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
        $this->get("security.token_storage")->setToken($token);
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        // On transfère le panier session vers le panier user
        $panier = $panierService->transferSessionToUser($user);

        //create new Panier produit
        if ($panier) {
            foreach ($panier->getProduitList() as $produit) {
                $checkProduit = $em->getRepository(PanierProduit::class)->findBy(array("produit"=>$produit, "panier"=>$panier));
                if (!$checkProduit) {
                    $panierProduit = new PanierProduit();
                    $panierProduit->setProduit($produit);
                    $panierProduit->setPanier($panier);
                    $panierProduit->setQtt(1);
                    $em->persist($panierProduit);
                }

            }
        }

        if ($panier) {
            $panierService->cleanPanier($panier);
        }

        $em->flush();

        $session = $request->getSession();
        $nextRoute = $session->get('app.route_after_identification');

        if (!$nextRoute) {
            $nextRoute = 'front_boutique_order_confirmation';
        }

        return $this->redirectToRoute($nextRoute);
    }

    /**
     * Route pour s'inscrire à partir de la page d'identification
     *
     * @Route("/boutique/inscription", name="front_boutique_registration", methods={"POST"})
     */
    public function registerAction(Request $request, PanierService $panierService)
    {
        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(OrderRegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $event = new FormEvent($form, $request);
                $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $this->userManager->updateUser($user);

                // On transfère le panier session vers le panier user
                $panier = $panierService->transferSessionToUser($user);

                //create new Panier produit
                if ($panier) {
                    foreach ($panier->getProduitList() as $produit) {
                        $panierProduit = new PanierProduit();
                        $panierProduit->setProduit($produit);
                        $panierProduit->setPanier($panier);
                        $panierProduit->setQtt(1);
                        $em->persist($panierProduit);
                    }
                }


                if ($panier) {
                    $panierService->cleanPanier($panier);
                }

                $em->flush();

                $session = $request->getSession();
                $nextRoute = $session->get('app.route_after_identification');

                if (!$nextRoute) {
                    $nextRoute = 'front_boutique_order_confirmation';
                }

                $response = $this->redirectToRoute($nextRoute);

                $this->eventDispatcher->dispatch(
                    FOSUserEvents::REGISTRATION_COMPLETED,
                    new FilterUserResponseEvent($user, $request, $response)
                );

                return $response;
            }

            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->renderIdentification(null, $form);
    }

    /**
     * Page de récap de commande, avec un bouton pour valider et un pour retourner à la
     * boutique pour changer son panier
     *
     * @Route("/boutique/confirmation", name="front_boutique_order_confirmation", methods={"GET", "POST"})
     */
    public function orderConfirmationAction(PanierService $panierService, AchatService $achatService)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $isAbonne = false;

        if (!$user) {
            return $this->redirectToRoute('front_boutique_identification');
        }

        if ($user->getPaidSubscriber() || in_array("ROLE_ABONNE", $user->getRoles())) {
            $isAbonne = true;
        }

        $panier = $panierService->getPanierForUser($user);

        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        //update tarif Total panier
        $panierService->updatePanier($panier, $panierProduit);

        $produits = $achatService->getProduitsForUser($user);

        $abonnementPrime = $em->getRepository(Produit::class)->findOneBy(array("rubrique"=>"abonnement-prime"));

        return $this->render(
            'main/Boutique/Confirmation/index.html.twig',
            [
                'panier' => $panierService->getPanierForUser($user),
                'panierProduit'=>$panierProduit,
                'isAbonne' => $isAbonne,
                'abonnementPrime' => $abonnementPrime,

            ]
        );
    }

    /**
     * Page de paiement
     *
     * @Route("/boutique/paiement", name="front_boutique_checkout", methods={"POST","GET"})
     */
    public function checkoutAction(
        Request $request,
        PanierService $panierService,
        PaiementService $paiementService,
        AchatService $achatService, PaypalService $paypalService, \Swift_Mailer $mailer)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $panier = $panierService->getPanierForUser($user);

        $type = "Confirmation de votre commande";

        //Transaction
        $paymentNumCommande = $request->get('vads_order_id'); // numero commande

        $vads_trans_status = $request->get('vads_trans_status'); //AUTHORISED - paiement authorisé
        //ABANDONED: Paiement abandonné par l’acheteur.
        //ACCEPTED: Accepté
        //AUTHORISED: En attente de remise - La transaction est acceptée et sera remise en banque automatiquement à la date prévue.
        //AUTHORISED_TO_VALIDATE : À valider
        //CANCELLED: La transaction est annulée par le marchand
        //CAPTURED: Présenté - La transaction est remise en banque.
        //CAPTURE_FAILED: La remise de la transaction a échoué. Contactez le Support.
        //EXPIRED: Expiré. La date d'expiration de la demande d'autorisation est atteinte et le marchand n’a pas validé la transaction. Le porteur ne sera donc pas débité
        //INITIAL : En attente
        //NOT_CREATED : Transaction non créée
        //REFUSED : La transaction est refusée.
        //SUSPENDED : Suspendu
        //UNDER_VERIFICATION
        //WAITING_AUTHORISATION : En attente d'autorisation
        //WAITING_AUTHORISATION_TO_VALIDATE : A valider et autoriser

        if (null != $vads_trans_status) {

            $payerID = $request->get('vads_trans_uuid');

            $montantPayer = (null!=$request->get('vads_effective_amount'))?$request->get('vads_effective_amount')/100:0;

            $vads_payment_cards = $request->get('vads_payment_cards');

            $paiement = $em->getRepository(Paiement::class)->findOneBy(['paymentId' => $paymentNumCommande]);

            if ($paiement && null != $vads_trans_status) {
                $paiement->setState($vads_trans_status);
                $em->persist($paiement);
                $em->flush();

            }else {

                if (!$user) {

                    //Check user
                    $user = $em->getRepository(User::class)->findOneBy(['lastCommande' => $paymentNumCommande]);

                    if (!$user) {
                        return $this->redirectToRoute('front_boutique_identification');
                    }

                    //Connexion automatique
                    $factory = $this->get('security.encoder_factory');


                    $encoder = $factory->getEncoder($user);

                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);

                    // If the firewall name is not main, then the set value would be instead:
                    // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
                    $this->get('session')->set('_security_main', serialize($token));

                    // Fire the login event manually
                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);


                }


                if ($vads_trans_status == "ACCEPTED" || $vads_trans_status == "AUTHORISED") {
                    $panier = $panierService->getPanierForUser($user);


                    //Est-ce-abonnement
                    if (preg_match("/LPC_ABO/", $paymentNumCommande)) {

                        $produitList = $panier->getProduitList();

                        $type = (sizeof($produitList) > 1)?"Confirmation commande + abonnement sur LPC":"Confirmation abonnement sur LPC";

                        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

                        if (sizeof($produitList) > 1) {
                            $produit = $em->getRepository(Produit::class)->findByRubrique("abonnement-prime");
                            if ($produit) {
                                $produit = $produit[0];
                                $idProduit = $produit->getId();
                            }

                            foreach ($produitList as $produitPanier) {
                                if ($produitPanier->getId() != $idProduit) {

                                } else {
                                    //$checkAbonnement = true;

                                    $qtt = 1;

                                    $now = new \DateTime();
                                    $oneYearLater = new \DateTime();
                                    $oneYearLater->modify('+'.$qtt.' year');

                                    $abonnement = new Abonnement();
                                    $abonnement->setUser($user);
                                    $abonnement->setDateStart($now);
                                    $abonnement->setDateEnd($oneYearLater);
                                    $abonnement->setAgreementPaypalId($paymentNumCommande);

                                    $user->addAbonnement($abonnement);

                                    $em->persist($abonnement);

                                    $abonnement->setAgreed(true);
                                    $abonnement->setPrix($produitPanier->getPrix());
                                    $abonnement->setQtt($qtt);
                                    $user->setPaidSubscriber(true);
                                    $user->setEndSubscriptionDate($abonnement->getDateEnd());
                                    $user->setSubscriptionRenewal(true);
                                    $user->setNewsletterSubscription(true);
                                    $user->setLastCommande(null);

                                    $em->persist($user);
                                }
                            }
                        }else {

                            $produitPanier = $panierProduit[0];

                            $qtt = $produitPanier->getQtt();

                            $now = new \DateTime();
                            $oneYearLater = new \DateTime();
                            $oneYearLater->modify('+'.$qtt.' year');

                            $abonnement = new Abonnement();
                            $abonnement->setUser($user);
                            $abonnement->setDateStart($now);
                            $abonnement->setDateEnd($oneYearLater);
                            $abonnement->setAgreementPaypalId($paymentNumCommande);

                            $user->addAbonnement($abonnement);

                            $em->persist($abonnement);

                            $abonnement->setAgreed(true);
                            $abonnement->setPrix($montantPayer);
                            $abonnement->setQtt($qtt);
                            $user->setPaidSubscriber(true);
                            $user->setEndSubscriptionDate($abonnement->getDateEnd());
                            $user->setSubscriptionRenewal(true);
                            $user->setNewsletterSubscription(true);
                            $user->setLastCommande(null);

                            $em->persist($user);
                        }







                        if (sizeof($produitList) > 1) {
                            if (null == $montantPayer) {
                                $montantPayer = $panier->getPrixTotal();
                            }
                            $paiement = $paiementService->createPaiement($user, $paymentNumCommande, $payerID, $montantPayer, $vads_trans_status);
                            $achatService->buyPanier($paiement, $panier, "CB", 'forProduct');
                        }else {
                            $paiement = $abonnement;
                        }

                        if ($panierProduit) {
                            foreach ($panierProduit as $produit) {
                                $em->remove($produit);
                            }

                        }

                        $em->flush();

                    }else {

                        if (null == $montantPayer) {
                            $montantPayer = $panier->getPrixTotal();
                        }
                        $paiement = $paiementService->createPaiement($user, $paymentNumCommande, $payerID, $montantPayer, $vads_trans_status);
                        $achatService->buyPanier($paiement, $panier, "CB");

                        $user->setLastCommande(null);

                        $em->persist($user);

                        $em->flush();
                    }



                    $panierService->removeSessionPanier();
                }

            }

            $to = null;
            if (null != $user->getEmail()) {
                $to = $user->getEmail();
            }
            //send email
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, $to, $vads_trans_status, $montantPayer);
            //send à cpc
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, "c.demarco@bba.fr", $vads_trans_status, $montantPayer);
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, "pbaey@cpc-doc.com", $vads_trans_status, $montantPayer);
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, "mrazafimandimby@bocasay.com", $vads_trans_status, $montantPayer);
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, "camyot@bba.fr", $vads_trans_status, $montantPayer);
            $paypalService->sendMailViaCB($user, $type, $mailer, $paiement, "comptabilite@bba.fr", $vads_trans_status, $montantPayer);


            //Faire la redirection vers mes commandes

            switch ($vads_trans_status) {
                case "ACCEPTED":
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        "Votre paiement a été effectué avec succès "
                    );
                    break;

                case "AUTHORISED":
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        "Votre paiement a été effectué avec succès "
                    );

                    break;

                case "ABANDONED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        'Votre paiement a été abandonné'
                    );
                    break;

                case "AUTHORISED_TO_VALIDATE":
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        "Votre paiement a été effectué mais encore à valider "
                    );
                    break;

                case "CANCELLED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        'Votre paiement a été annulé'
                    );
                    break;

                case "CAPTURED":
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        "Présenté - La transaction est remise en banque."
                    );
                    break;

                case "CAPTURE_FAILED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "La remise de la transaction a échoué. Contactez le Support"
                    );
                    break;

                case "EXPIRED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Merci de réessayer avec un autre mode de paiement (EXPIRED)."
                    );
                    break;

                case "INITIAL":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Merci de réessayer avec un autre mode de paiement (INITIAL)."
                    );
                    break;

                case "NOT_CREATED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Merci de réessayer avec un autre mode de paiement (NOT_CREATED)."
                    );
                    break;

                case "REFUSED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Votre paiement de ".$montantPayer."€ TTC en date du ".date("d/m/Y")." a été refusé "
                    );
                    break;

                case "SUSPENDED":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Votre paiement de ".$montantPayer."€ TTC en date du ".date("d/m/Y")." a été suspendu "
                    );
                    break;

                case "WAITING_AUTHORISATION":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Merci de réessayer avec un autre mode de paiement (WAITING_AUTHORISATION)."
                    );
                    break;

                case "WAITING_AUTHORISATION_TO_VALIDATE":
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        "Merci de réessayer avec un autre mode de paiement (WAITING_AUTHORISATION_TO_VALIDATE)."
                    );
                    break;
            }

            return $this->redirectToRoute('front_mes_commandes');

            /*return $this->render('main/Boutique/Success/indexCB.html.twig', [
                'status' => $vads_trans_status,
                'paiement' => $paiement,
                'montantPayer' => $montantPayer,
            ]);*/
        }

        if (null == $panier->getPrixTotal()) {
            return $this->redirectToRoute('front_boutique');
        }


        if (!$user) {
            return $this->redirectToRoute('front_boutique_identification');
        }



        $panierService->cleanPanier($panier);
        $adresseForm = $this->createForm(AdresseType::class, $user);
        $adresseForm->handleRequest($request);

        if ($adresseForm->isValid()) {
            $em->flush();
        }

        //generer un numero de commande automatqiue
        $numCommande = "LPC_";

        $chaine = "1234567890";

        srand((double)microtime() * 1000000);

        for($i = 0; $i < 6; $i++)
        {
            $numCommande .= $chaine[rand()%strlen($chaine)];
        }

        $numCommande = $numCommande."_".$user->getId();

        $user->setLastCommande($numCommande);

        $em->persist($user);

        $em->flush();


        return $this->render(
            'main/Boutique/Checkout/index.html.twig',
            [
                'adresseForm' => $adresseForm->createView(),
                'panier' => $panier,
                'numCommande' => $numCommande,
            ]
        );
    }

    /**
     * Page de paiement d'abonnement
     *
     * @Route("/boutique/abonnement", name="front_boutique_abonnement", methods={"GET", "POST"})
     */
    public function abonnementAction(Request $request, PanierService $panierService)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $idProduit = "";
        $checkAbonnement = false;
        $produit = $em->getRepository(Produit::class)->findByRubrique("abonnement-prime");
        if ($produit) {
            $produit = $produit[0];
            $idProduit = $produit->getId();
        }

        $panier = $panierService->getPanierForUser($user);

        if (null == $panier->getPrixTotal()) {
            return $this->redirectToRoute('front_boutique');
        }

        if ($panier) {
            $produitList = $panier->getProduitList();
            foreach ($produitList as $produitPanier) {
                if ($produitPanier->getId() != $idProduit) {
                    /*$panierProduit = $em->getRepository("App:PanierProduit")->findBy(array("produit"=>$produitPanier, "panier"=>$panier));

                    if ($panierProduit) {
                        $em->remove($panierProduit[0]);
                    }
                    $panier->removeProduit($produitPanier);

                    $em->flush();*/
                } else {
                    $checkAbonnement = true;
                }
            }
        }



        if (!$checkAbonnement && (!$user || $user->getPaidSubscriber() ==0 )) {
            $panier = $panierService->getPanierForUser($user);
            $panier->addProduit($produit);
            //create new
            $panierProduit = new PanierProduit();
            $panierProduit->setProduit($produit);
            $panierProduit->setPanier($panier);
            $panierProduit->setQtt(1);
            $em->persist($panierProduit);

            $em->flush();
        }


        if (!$user) {
            return $this->redirectToRoute('front_boutique_identification',
                [
                    'nextRoute' => 'front_boutique_abonnement',
                    'idProduit' => $idProduit,
                ]
            );
        }

        $adresseForm = $this->createForm(AdresseType::class, $user);
        $adresseForm->handleRequest($request);

        if ($adresseForm->isSubmitted() && $adresseForm->isValid()) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );
            $em->flush();
        }

        //generer un numero de commande automatqiue
        $numCommande = "LPC_ABO_";

        $chaine = "1234567890";

        srand((double)microtime() * 1000000);

        for($i = 0; $i < 6; $i++)
        {
            $numCommande .= $chaine[rand()%strlen($chaine)];
        }

        $numCommande = $numCommande."_".$user->getId();

        $user->setLastCommande($numCommande);

        $em->persist($user);

        $em->flush();

        return $this->render(
            'main/Boutique/Abonnement/index.html.twig',
            [
                'adresseForm' => $adresseForm->createView(),
                'numCommande' => $numCommande,
            ]
        );
    }

    /**
     * Page de paiement d'abonnement OK
     *
     * @Route("/boutique/abonnement-ok", name="front_boutique_abonnement_ok", methods={"GET"})
     */
    public function abonnementOkAction(Request $request)
    {
        return $this->render( 'main/Boutique/Abonnement/ok.html.twig');
    }

    public function panierBlocAction(PanierService $panierService)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);

        $panierProduits = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        //check panier produit
        $hasAbonnement = false;
        foreach ($panierProduits as $panierProduit) {
            if ($panierProduit->getProduit()->getRubrique() == "abonnement-prime") {
                $hasAbonnement = true;
            }
        }

        if ($hasAbonnement) {
            foreach ($panierProduits as $panierProduit) {
                if ($panierProduit->getProduit()->getRubrique() != "abonnement-prime" && $panierProduit->getProduit()->getRubrique() != "frais-transport") {
                    $em->remove($panierProduit);
                    $panier->removeProduit($panierProduit->getProduit());
                }
            }
            $em->flush();
        }

        //Detection doublon

        $panierProduits = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));
        $tabProduit = [];
        foreach ($panierProduits as $panierProduit) {
            if (!in_array($panierProduit->getProduit()->getId(), $tabProduit)) {
                $tabProduit[] = $panierProduit->getProduit()->getId();
            }else {
                $em->remove($panierProduit);
                $panier->removeProduit($panierProduit->getProduit());
            }

        }
        $em->flush();

        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        $currentRoute = $this->container->get('request_stack')->getMasterRequest()->get('_route');

        return $this->render(
            'main/Boutique/_panier.html.twig',
            [
                'panier' => $panier,
                'currentRoute' => $currentRoute,
                'panierProduit'=>$panierProduit,
                'tva'=>'2.1',
            ]
        );
    }

    /**
     * jquery maj qtt panier
     *
     * @Route("/boutique/qtt/panier", name="front_boutique_qtt_panier", methods={"GET", "POST"})
     */
    public function majQttPanierAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idPanier = $request->get('id');
        $qtt = $request->get('qtt');

        $panierProduit = $em->getRepository(PanierProduit::class)->find($idPanier);
        if ($panierProduit) {
            $panierProduit->setQtt($qtt);
            $em->persist($panierProduit);
            $em->flush();
        }
        return new Response("ok");
    }

    public function panierAjaxProduit($panierService, $currentRoute)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);

        $panierProduit = $em->getRepository(PanierProduit::class)->findBy(array("panier"=>$panier));

        return $this->render(
            'main/Boutique/_panier.html.twig',
            [
                'panier' => $panier,
                'currentRoute' => $currentRoute,
                'panierProduit'=>$panierProduit,
                'tva'=>'2.1',
            ]
        );
    }



}
