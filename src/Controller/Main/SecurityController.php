<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Main;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller managing security.
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class SecurityController extends AbstractController
{
    private $tokenManager;
    protected $requestStack;

    public function __construct(CsrfTokenManagerInterface $tokenManager = null, RequestStack $requestStack)
    {
        $this->tokenManager = $tokenManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @param Request $request
     *
     * @Route("/login", name="fos_main_user_security_login")
     * @return Response
     */
    public function loginAction(Request $request)
    {
        /**
         * @var $session Session
        */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        $uri = $request->get('urltoredirect');
        if ($uri == "" || empty($uri)) {
            $uriSession = $session->get('uri');
            if(isset($uriSession) && null != $uriSession) {
                $uri = $uriSession;
            }
        }

        return $this->renderLogin(
            array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
                'uri'=> $uri
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function loginAbonneAction(Request $request, $uri)
    {
        /**
 * @var $session Session 
*/
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->renderLoginAbonne(
            array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
                'uri'=> $uri
            )
        );
    }

    /**
     * @param Request $request
     *
     * @Route("/login_check", name="fos_user_security_check", methods="POST")
     * @return Response
     */
    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * @param Request $request
     *
     * @Route("/logout", name="fos_user_security_logout", methods="POST")
     * @return Response
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return Response
     */
    protected function renderLogin(array $data)
    {
        $request = $this->requestStack->getCurrentRequest();
        $currentRoute = $request->attributes->get('_route');
        if($currentRoute == "fos_main_user_security_login") {
            return $this->render('Security/login-main.html.twig', $data);
        }else {
            return $this->render('Security/login.html.twig', $data);
        }

    }

    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return Response
     */
    protected function renderLoginAbonne(array $data)
    {
        return $this->render('Security/login-abonne.html.twig', $data);
    }
}
