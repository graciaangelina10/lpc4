<?php

namespace App\Controller\Main;

use App\Entity\Ca;
use App\Entity\Cabinet;
use App\Entity\CaC;
use App\Entity\CaExpertise;
use App\Entity\City;
use App\Entity\Departement;
use App\Entity\EtudeNational;
use App\Entity\Groupe;
use App\Entity\InformationFiliale;
use App\Entity\Questionnaire;
use App\Entity\Region;
use App\Entity\Regroupement;
use App\Entity\TailleCabinet;
use App\Form\CabinetResponseType;
use App\Repository\EtudeNationalRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class QuestionnaireController extends AbstractController
{
    /**
     * @Route("/questionnaire")
     */
    public function questionnaireAction()
    {
        return $this->render('admin/questionnaire/questionnaire.html.twig', array(

        ));


    }

    /**
     * @Route("/questionnaire/{id}", name="questionnaire_etude")
     */
    public function questionnaireEtudeAction(EtudeNational $etude, Request $request)
    {

        //$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous ne pouvez pas acceder à cette page');

        $em = $this->getDoctrine()->getManager();

        //$etude = $em->getRepository('App:EtudeNational')->findOneBy(['annee' => $annee]);

        if (!$etude) {
            return $this->redirectToRoute('home');
        }

        $pluridiscplinaire = $request->get('isPluri');

        $cabinet = new Cabinet();

        $form = $this->createForm('App\Form\CabinetResponseType', $cabinet );

        $groupes = $em->getRepository(Groupe::class)->findBy([], ['nom' => 'ASC']);

        $regroupements = $em->getRepository(Regroupement::class)->findBy([], ['nom' => 'ASC']);

        $questionnaire = new Questionnaire();
        $questionnaire->setEtude($etude)
            ->setCabinet($cabinet)
            ;

        $regions = $em->getRepository(Region::class)->findAll();

        $departements = $em->getRepository(Departement::class)->findAll();

        $localisations = [];

        $tabLoc = [];

        $i=0;
        foreach ($departements as $departement) {
            if (!in_array($departement->getNom(), $tabLoc)) {
                $localisations[$i]['nom'] = trim($departement->getCode()." ".$departement->getNom());
                $localisations[$i]['value'] = trim($departement->getCode());
                $i++;
            }

        }

        foreach ($regions as $region) {
            if (!in_array($region->getNom(), $tabLoc)) {
                $localisations[$i]['nom'] = trim($region->getNom());
                $localisations[$i]['value'] = trim($region->getNom());

                $tabLoc [] = $region->getNom();
                $i++;
            }

        }

        $tabGroupes = [];

        $tabNonApplicable = [];

        foreach ($groupes as $groupe) {
            if ($groupe->getNom() == "Non Applicable" || $groupe->getNom() == "Non applicable") {
                $tabNonApplicable [] = $groupe;
            }else {
                $tabGroupes [] = $groupe;
            }

        }

        $tabReGroupements = [];

        $tabNonApplicable2 = [];

        foreach ($regroupements as $regroupement) {
            if ($regroupement->getNom() == "Non Applicable" || $regroupement->getNom() == "Non applicable") {
                $tabNonApplicable2 [] = $regroupement;
            }else {
                $tabReGroupements [] = $regroupement;
            }

        }

        $regroupements = @array_merge($tabNonApplicable2, $tabReGroupements);

        $groupes = @array_merge($tabNonApplicable, $tabGroupes);

        $template = ($etude->isReseau())?"questionnaireEtude_reseau.html.twig":"questionnaireEtude.html.twig";

        return $this->render('admin/questionnaire/'.$template, array(
            'etude' => $etude,
            'questionnaire' => $questionnaire,
            'questionnaireRequest' => '',
            'code' => '',
            'form' => $form->createView(),
            'saveYes' => false,
            'tabResult' => [],
            'groupes' => $groupes,
            'regroupements' => $regroupements,
            'localisations' => $localisations,
            'pluridiscplinaire' => $pluridiscplinaire,
            'user' => $this->getUser(),
        ));


    }

    /**
     * @Route("/questionnaire/{id}/{annee}", name="questionnaire_send_etude")
     */
    public function questionnaireValideAction(
        Questionnaire $questionnaire, 
        $annee, 
        Request $request,
        UserService $userService
        )
    {

        $em = $this->getDoctrine()->getManager();


        $etude = $em->getRepository(EtudeNational::class)->findOneBy(['annee' => $annee]);

        if (!$etude) {
            return $this->redirectToRoute('home');
        }

        $code = $request->get('code');

        if (null != $code) {
            $this->get('session')->set('code', $code);
        }elseif (null != $this->get('session')->get('code')) {
            $code = $this->get('session')->get('code');
        }

        $questionnaireRequest = $em->getRepository(Questionnaire::class)->findOneBy(['code' => $code, 'id'=> $questionnaire->getId()]);

        if ($code != $questionnaire->getCode()) {
            $cabinet = new Cabinet();
        }else {
            $cabinet = $questionnaire->getCabinet();
        }

        $download = $request->get('download');

        if (null != $download && $download == 1) {
            $filename = ($cabinet->isPluridiscplinaire())?$this->getParameter('kernel.project_dir') . '/web/uploads/etude_national_2022_v1.pdf':$this->getParameter('kernel.project_dir') . '/web/uploads/etude_national_2022_v2.pdf';
            if(!file_exists($filename)){
                return new Response('File not found', 404);
            }

            $response = new BinaryFileResponse($filename);
            $response ->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_INLINE,
                "Etude national 2022"
            );

            return $response;
        }


        $form = $this->createForm(CabinetResponseType::class, $cabinet );

        $form->handleRequest($request);

        $saveYes = false;

        $annee = $questionnaire->getEtude()->getAnnee();

        //save autre champ
        $tabAnnee = [
            $annee -2,
            $annee -1
        ];

        if ($form->isSubmitted()) {
            
            $em->persist($cabinet);

            $questionnaire->setHasResponse(true);

            $em->persist($questionnaire);


            $em->flush();

            $code = $questionnaire->getCode();

            $questionnaireRequest = $questionnaire;

            foreach ($tabAnnee as $item) {
                //Ca
                $ca = $em->getRepository(Ca::class)->findCa($cabinet, $item);
                if (!$ca) {
                    $ca = new Ca();
                    $ca->setCabinet($questionnaire->getCabinet())
                        ->setAnnee($item)
                        ->setCaLettre(str_replace(" ", "", $_POST['ca_'.$item]))
                        ;

                    $em->persist($ca);
                }else {
                    $ca->setCaLettre(str_replace(" ", "", $_POST['ca_'.$item]))
                    ;

                    $em->persist($ca);
                }

                //Cac
                $cac = $em->getRepository(CaC::class)->findCaC($cabinet, $item);
                if (!$cac) {
                    $cac = new CaC();
                    $cac->setCabinet($cabinet)
                        ->setAnnee($item)
                        ->setCaLettre(str_replace(" ", "", $_POST['cac_'.$item]))
                        ;

                    $em->persist($cac);
                }else {
                    $cac->setCaLettre(str_replace(" ", "", $_POST['cac_'.$item]))
                    ;

                    $em->persist($cac);
                }

                //CaExpertise
                $caExpertise = $em->getRepository(CaExpertise::class)->findCaExpertise($cabinet, $item);
                if (!$caExpertise) {
                    $caExpertise = new CaExpertise();
                    $caExpertise->setAnnee($item)
                        ->setCabinet($cabinet)
                        ->setCaLettre(str_replace(" ", "", $_POST['caexpertise_'.$item]))
                        ->setALettre(str_replace(" ", "", $_POST['caexpertiseA_'.$item]))
                        ->setBLettre(str_replace(" ", "", $_POST['caexpertiseB_'.$item]))
                        ->setCLettre(str_replace(" ", "", $_POST['caexpertiseC_'.$item]))
                        ;

                    $em->persist($caExpertise);
                }else {
                    $caExpertise->setCaLettre(str_replace(" ", "", $_POST['caexpertise_'.$item]))
                        ->setALettre(str_replace(" ", "", $_POST['caexpertiseA_'.$item]))
                        ->setBLettre(str_replace(" ", "", $_POST['caexpertiseB_'.$item]))
                        ->setCLettre(str_replace(" ", "", $_POST['caexpertiseC_'.$item]))
                    ;

                    $em->persist($caExpertise);
                }

                //Nb client
                $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);
                if (!$tailleCabinet) {
                    $tailleCabinet = new TailleCabinet();


                    $groupement = (isset($_POST['groupement_'.$item]))?implode(",", $_POST['groupement_'.$item]): "";

                    $regroupement = (isset($_POST['reseau_'.$item]))?implode(",", $_POST['reseau_'.$item] ): "";

                    $localisation = (isset($_POST['localisation_'.$item]))?implode("-", $_POST['localisation_'.$item] ): "";

                    $groupement_new = (isset($_POST['groupement_new_'.$item]))?explode(",", $_POST['groupement_new_'.$item]): "";

                    $regroupement_new = (isset($_POST['reseau_new_'.$item]))?explode(",", $_POST['reseau_new_'.$item] ): "";

                    $localisation_new = (isset($_POST['localisation_new_'.$item]))?explode("-", $_POST['localisation_new_'.$item] ): "";

                    if ($localisation_new && sizeof($localisation_new) > 0) {
                        $tabLocId = [];
                        foreach ($localisation_new as $unLocalisation) {
                            if ($unLocalisation) {
                                $regionNew = $em->getRepository(Region::class)->findOneByNom($unLocalisation);
                                if ($regionNew) {

                                }else {
                                    $regionNew = new Region();
                                    $regionNew->setNom($unLocalisation)
                                    ;

                                    $em->persist($regionNew);
                                }

                                $tabLocId [] = $unLocalisation;
                            }

                        }

                        if ($tabLocId) {
                            $localisation .= $localisation.'-'.implode("-", $tabLocId);
                        }
                    }

                    if ($groupement_new && sizeof($groupement_new) > 0) {
                        $tabGroupeId = [];
                        foreach ($groupement_new as $unGroupement) {
                            if ($unGroupement) {
                                $groupeNew = $em->getRepository(Groupe::class)->findOneByNom($unGroupement);
                                if (!$groupeNew) {
                                    $groupeNew = new Groupe();
                                    $groupeNew->setNom($unGroupement);

                                    $em->persist($groupeNew);
                                    $em->flush();

                                    $groupeNew = $em->getRepository(Groupe::class)->findOneByNom($unGroupement);
                                }

                                if ($groupeNew) {
                                    $tabGroupeId [] = $groupeNew->getId();
                                }
                            }

                        }

                        if ($tabGroupeId) {
                            $groupement .= $groupement.','.implode(",", $tabGroupeId);
                        }
                    }

                    if ($regroupement_new && sizeof($regroupement_new) > 0) {
                        $tabRegroupeId = [];
                        foreach ($regroupement_new as $unRegroupement) {
                            if ($unRegroupement) {
                                $regroupementNew = $em->getRepository(Regroupement::class)->findOneByNom($unRegroupement);

                                if (!$regroupementNew) {
                                    $regroupementNew = new Regroupement();
                                    $regroupementNew->setNom($unRegroupement)
                                    ;

                                    $em->persist($regroupementNew);
                                    $em->flush();

                                    $regroupementNew = $em->getRepository(Regroupement::class)->findOneByNom($unRegroupement);
                                }

                                if ($regroupement_new) {
                                    $tabRegroupeId [] = $regroupementNew->getId();
                                }

                            }

                        }

                        if ($tabRegroupeId) {
                            $regroupement .= $regroupement.','.implode(",", $tabRegroupeId);
                        }
                    }

                    $tailleCabinet->setCabinet($cabinet)
                        ->setAnnee($item)
                        ->setNbClient($_POST['nbClient_'.$item])
                        ->setDateCloture($_POST['dateCloture_'.$item])
                        ->setEffectif($_POST['effectif_'.$item])
                        ->setGroupeNational($groupement)
                        ->setLocalisation($localisation)
                        ->setNbBureau($_POST['nbBureau_'.$item])
                        ->setNbFemmeAs($_POST['femmeAs_'.$item])
                        ->setNbFemmeEc($_POST['femmeEc_'.$item])
                        ->setNbHommeAs($_POST['hommeAs_'.$item])
                        ->setNbHommeEc($_POST['hommeEc_'.$item])
                        ->setReseauNational($regroupement)
                        ;

                    $em->persist($tailleCabinet);
                }else {


                    $groupement = (isset($_POST['groupement_'.$item]))?implode(",", $_POST['groupement_'.$item]): "";

                    $regroupement = (isset($_POST['reseau_'.$item]))?implode(",", $_POST['reseau_'.$item] ): "";

                    $localisation = (isset($_POST['localisation_'.$item]))?implode("-", $_POST['localisation_'.$item] ): "";

                    $groupement_new = (isset($_POST['groupement_new_'.$item]))?explode(",", $_POST['groupement_new_'.$item]): "";

                    $regroupement_new = (isset($_POST['reseau_new_'.$item]))?explode(",", $_POST['reseau_new_'.$item] ): "";

                    $localisation_new = (isset($_POST['localisation_new_'.$item]))?explode("-", $_POST['localisation_new_'.$item] ): "";

                    if ($localisation_new && sizeof($localisation_new) > 0) {
                        $tabLocId = [];
                        foreach ($localisation_new as $unLocalisation) {
                            if ($unLocalisation) {

                                $regionNew = $em->getRepository(Region::class)->findOneByNom($unLocalisation);
                                if ($regionNew) {

                                }else {
                                    $regionNew = new Region();
                                    $regionNew->setNom($unLocalisation)
                                    ;

                                    $em->persist($regionNew);
                                }


                                $tabLocId [] = $unLocalisation;
                            }

                        }

                        if ($tabLocId) {
                            $localisation .= $localisation.'-'.implode("-", $tabLocId);
                        }
                    }

                    if ($groupement_new && sizeof($groupement_new) > 0) {
                        $tabGroupeId = [];
                        foreach ($groupement_new as $unGroupement) {
                            if ($unGroupement) {
                                $groupeNew = $em->getRepository(Groupe::class)->findOneByNom($unGroupement);
                                if (!$groupeNew) {
                                    $groupeNew = new Groupe();
                                    $groupeNew->setNom($unGroupement)
                                    ;

                                    $em->persist($groupeNew);
                                    $em->flush();

                                    $groupeNew = $em->getRepository(Groupe::class)->findOneByNom($unGroupement);
                                }

                                if ($groupeNew) {
                                    $tabGroupeId [] = $groupeNew->getId();
                                }

                            }

                        }

                        if ($tabGroupeId) {
                            $groupement .= $groupement.','.implode(",", $tabGroupeId);
                        }
                    }

                    if ($regroupement_new && sizeof($regroupement_new) > 0) {

                        $tabRegroupeId = [];
                        foreach ($regroupement_new as $unRegroupement) {
                            if ($unRegroupement) {

                                $regroupementNew = $em->getRepository(Regroupement::class)->findOneByNom($unRegroupement);

                                if (!$regroupementNew) {
                                    $regroupementNew = new Regroupement();
                                    $regroupementNew->setNom($unRegroupement)
                                    ;

                                    $em->persist($regroupementNew);
                                    $em->flush();

                                    $regroupementNew = $em->getRepository(Regroupement::class)->findOneByNom($unRegroupement);
                                }

                                if ($regroupement_new) {
                                    $tabRegroupeId [] = $regroupementNew->getId();
                                }

                            }

                        }

                        if ($tabRegroupeId) {
                            $regroupement .= $regroupement.','.implode(",", $tabRegroupeId);
                        }
                    }

                    $tailleCabinet->setNbClient($_POST['nbClient_'.$item])
                        ->setDateCloture($_POST['dateCloture_'.$item])
                        ->setEffectif($_POST['effectif_'.$item])
                        ->setGroupeNational($groupement)
                        ->setLocalisation($localisation)
                        ->setNbBureau($_POST['nbBureau_'.$item])
                        ->setNbFemmeAs($_POST['femmeAs_'.$item])
                        ->setNbFemmeEc($_POST['femmeEc_'.$item])
                        ->setNbHommeAs($_POST['hommeAs_'.$item])
                        ->setNbHommeEc($_POST['hommeEc_'.$item])
                        ->setReseauNational($regroupement)
                    ;

                    $em->persist($tailleCabinet);
                }

                //information filiale
                $informationFiliale = $em->getRepository(InformationFiliale::class)->findInformation($cabinet, $item);
                if (!$informationFiliale) {
                    $informationFiliale = new InformationFiliale();
                    $informationFiliale->setCabinet($cabinet)
                        ->setAnnee($item)
                        ->setAutre(str_replace(" ", "", $_POST['autre_'.$item]))
                        ->setConseil(str_replace(" ", "", $_POST['conseil_'.$item]))
                        ->setFiscal(str_replace(" ", "", $_POST['fiscal_'.$item]))
                        ->setJuridique(str_replace(" ", "", $_POST['juridique_'.$item]))
                        ->setTotal(str_replace(" ", "", $_POST['total_'.$item]))
                        ;

                    $em->persist($informationFiliale);
                }else{
                    $informationFiliale->setAutre(str_replace(" ", "", $_POST['autre_'.$item]))
                        ->setConseil(str_replace(" ", "", $_POST['conseil_'.$item]))
                        ->setFiscal(str_replace(" ", "", $_POST['fiscal_'.$item]))
                        ->setJuridique(str_replace(" ", "", $_POST['juridique_'.$item]))
                        ->setTotal(str_replace(" ", "", $_POST['total_'.$item]))
                    ;

                    $em->persist($informationFiliale);
                }
            }

            $em->flush();

            $saveYes = true;

            //Send mail à christine quand le client clique sur Je valide
            if (isset($_POST['checkJevalide']) && $_POST['checkJevalide'] == 1) {
                $to [] = "cdemarco@laprofessioncomptable.com";
                $to [] = "mrazafimandimby@bocasay.com";
                $destinataire [] = "cdemarco@laprofessioncomptable.com";
                $destinataire [] = "mrazafimandimby@bocasay.com";
                $userService->sendEmailQuestionnaire($to, $questionnaire, $destinataire, "jeValide");

            }

        }


        $tabResult = [];

        if ($code == $questionnaire->getCode()) {

            foreach ($tabAnnee as $item) {
                $ca = $em->getRepository(Ca::class)->findCa($cabinet, $item);

                if ($ca) {
                    $tabResult['ca_'.$item] = $ca->getCaLettre();
                }else {
                    $tabResult['ca_'.$item] = "";
                }

                $cac = $em->getRepository(CaC::class)->findCaC($cabinet, $item);
                if ($cac) {
                    $tabResult['cac_'.$item] = $cac->getCaLettre();
                }else {
                    $tabResult['cac_'.$item] = "";
                }

                $caExpertise = $em->getRepository(CaExpertise::class)->findCaExpertise($cabinet, $item);
                if ($caExpertise) {
                    $tabResult['caexpertise_'.$item] = $caExpertise->getCaLettre();
                    $tabResult['caexpertiseA_'.$item] = $caExpertise->getALettre();
                    $tabResult['caexpertiseB_'.$item] = $caExpertise->getBLettre();
                    $tabResult['caexpertiseC_'.$item] = $caExpertise->getCLettre();
                }else {
                    $tabResult['caexpertise_'.$item] = "";
                    $tabResult['caexpertiseA_'.$item] = "";
                    $tabResult['caexpertiseB_'.$item] = "";
                    $tabResult['caexpertiseC_'.$item] = "";
                }

                //Nb client
                $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);

                if ($tailleCabinet) {
                    $tabResult['nbClient_'.$item] = $tailleCabinet->getNbClient();
                    $tabResult['dateCloture_'.$item] = $tailleCabinet->getDateCloture();
                    $tabResult['effectif_'.$item] = $tailleCabinet->getEffectif();
                    $tabResult['hommeEc_'.$item] = $tailleCabinet->getNbHommeEc();
                    $tabResult['femmeEc_'.$item] = $tailleCabinet->getNbFemmeEc();
                    $tabResult['hommeAs_'.$item] = $tailleCabinet->getNbHommeAs();
                    $tabResult['femmeAs_'.$item] = $tailleCabinet->getNbFemmeAs();
                    $tabResult['nbBureau_'.$item] = $tailleCabinet->getNbBureau();
                    $tabResult['localisation_'.$item] = explode("-", $tailleCabinet->getLocalisation());
                    $tabResult['groupement_'.$item] = explode(",", $tailleCabinet->getGroupeNational());
                    $tabResult['reseau_'.$item] = explode(",", $tailleCabinet->getReseauNational());
                }else {
                    $tabResult['nbClient_'.$item] = "";
                    $tabResult['dateCloture_'.$item] = "";
                    $tabResult['effectif_'.$item] = "";
                    $tabResult['hommeEc_'.$item] = "";
                    $tabResult['femmeEc_'.$item] = "";
                    $tabResult['hommeAs_'.$item] = "";
                    $tabResult['femmeAs_'.$item] = "";
                    $tabResult['nbBureau_'.$item] = "";
                    $tabResult['localisation_'.$item] = "";
                    $tabResult['groupement_'.$item] = "";
                    $tabResult['reseau_'.$item] = "";
                }

                //information filiale
                $informationFiliale = $em->getRepository(InformationFiliale::class)->findInformation($cabinet, $item);
                if ($informationFiliale) {
                    $tabResult['juridique_'.$item] = $informationFiliale->getJuridique();
                    $tabResult['fiscal_'.$item] = $informationFiliale->getFiscal();
                    $tabResult['conseil_'.$item] = $informationFiliale->getConseil();
                    $tabResult['autre_'.$item] = $informationFiliale->getAutre();
                    $tabResult['total_'.$item] = $informationFiliale->getTotal();

                }else{
                    $tabResult['juridique_'.$item] = "";
                    $tabResult['fiscal_'.$item] = "";
                    $tabResult['conseil_'.$item] = "";
                    $tabResult['autre_'.$item] = "";
                    $tabResult['total_'.$item] = "";
                }
            }


        }

        $groupes = $em->getRepository(Groupe::class)->findBy([], ['nom' => 'ASC']);

        $regroupements = $em->getRepository(Regroupement::class)->findBy([], ['nom' => 'ASC']);

        $regions = $em->getRepository(Region::class)->findAll();

        $departements = $em->getRepository(Departement::class)->findAll();

        $localisations = [];

        $tabLoc = [];

        $i=0;

        foreach ($departements as $departement) {
            if (!in_array($departement->getNom(), $tabLoc)) {
                $localisations[$i]['nom'] = trim($departement->getCode()." ".$departement->getNom());
                $localisations[$i]['value'] = trim($departement->getCode());
                $i++;
            }

        }

        foreach ($regions as $region) {
            if (!in_array($region->getNom(), $tabLoc)) {
                $localisations[$i]['nom'] = trim($region->getNom());
                $localisations[$i]['value'] = trim($region->getNom());

                $tabLoc [] = $region->getNom();
                $i++;
            }

        }

        $tabGroupes = [];

        $tabNonApplicable = [];

        foreach ($groupes as $groupe) {
            if ($groupe->getNom() == "Non Applicable" || $groupe->getNom() == "Non applicable") {
                $tabNonApplicable [] = $groupe;
            }else {
                $tabGroupes [] = $groupe;
            }

        }

        $tabReGroupements = [];

        $tabNonApplicable2 = [];

        foreach ($regroupements as $regroupement) {
            if ($regroupement->getNom() == "Non Applicable" || $regroupement->getNom() == "Non applicable") {
                $tabNonApplicable2 [] = $regroupement;
            }else {
                $tabReGroupements [] = $regroupement;
            }

        }

        $regroupements = @array_merge($tabNonApplicable2, $tabReGroupements);

        $groupes = @array_merge($tabNonApplicable, $tabGroupes);


        $template = ($etude->isReseau())?"questionnaireEtude_reseau.html.twig":"questionnaireEtude.html.twig";

        return $this->render('admin/questionnaire/'.$template, array(
            'etude' => $etude,
            'questionnaire' => $questionnaire,
            'questionnaireRequest' => $questionnaireRequest,
            'code' => $code,
            'form' => $form->createView(),
            'saveYes' => $saveYes,
            'tabResult' => $tabResult,
            'groupes' => $groupes,
            'regroupements' => $regroupements,
            'localisations' => $localisations,
            'user' => $this->getUser(),
        ));


    }

    /**
     * @Route("/home/code-postal/from-ajax", name="get_code_postal")
     * @return JsonResponse
     */
    public function getCodePostal()
    {
        $dataJson = file_get_contents("uploads/codePostal.json");

        $tabCodePostal = json_decode($dataJson);

        $tabData = $tabCodePostal;

        return new JsonResponse($tabData);
    }

    /**
     * @Route("/home/departement/from-ajax", name="get_departement")
     * @return JsonResponse
     */
    public function getdepartement()
    {
        $dataJson = file_get_contents("uploads/departement.json");

        $tabDepartements = json_decode($dataJson);

        $dataJson = file_get_contents("uploads/region.json");

        $tabRegion = json_decode($dataJson);

        $tabData = array_merge($tabDepartements, $tabRegion);


        return new JsonResponse($tabData);
    }

    /**
     * @Route("/home/ville/from-ajax", name="get_ville")
     * @return JsonResponse
     */
    public function getVille()
    {
        $dataJson = file_get_contents("uploads/city.json");

        $tabCity = json_decode($dataJson);

        $tabData = $tabCity;

        return new JsonResponse($tabData);
    }

    /**
     * @Route("/home/region/from-ajax", name="get_region")
     * @return JsonResponse
     */
    public function getRegion()
    {
        $dataJson = file_get_contents("uploads/region.json");

        $tabDepartements = json_decode($dataJson);

        $tabData = $tabDepartements;

        return new JsonResponse($tabData);
    }

    /**
     * @Route("/home/set-ville/from-ajax", name="get_set_ville")
     * @return JsonResponse
     */
    public function villeFromCodePostal(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $codePostal = $request->get('codePostal');

        $cities = $em->getRepository(City::class)->findCityVille($codePostal);

        $tabCity = [];

        $nameCity = "";

        if ($cities) {
            foreach ($cities as $city) {
                $tabCity[] = $city->getNom();

                $nameCity = $city->getNom();

                break;
            }
        }

        return new Response($nameCity);


    }

    /**
     * @param Request $request
     * @Route("/questionnaire-lpc/addGroupe/from-ajax", name="questionnaire_add_groupe_from_ajax", methods={"POST"})
     * @return Response
     */
    public function addGroupe(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idQuestionnaire = $request->get('idQuestionnaire');

        $questionnaire = $em->getRepository(Questionnaire::class)->find($idQuestionnaire);

        $type = $request->get('type');

        $annee = $request->get('annee');

        $annee1 = $request->get('annee1');

        $annee2 = $request->get('annee2');

        $nom = $request->get('nom');

        $regroupements = [];

        $groupes = [];

        $cabinet = $questionnaire->getCabinet();

        //save autre champ
        $tabAnnee = [
            $annee -2,
            $annee -1
        ];

        switch ($type) {
            case "reseau":

                $regroupement = new Regroupement();
                $regroupement->setNom($nom);

                $em->persist($regroupement);

                $em->flush();


                foreach ($tabAnnee as $item) {

                    $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);

                    if ($tailleCabinet) {
                        $reseauNational = $tailleCabinet->getReseauNational();
                        if ($reseauNational) {
                            $reseauNational = $reseauNational.','.$regroupement->getId();
                        }else {
                            $reseauNational = $regroupement->getId();
                        }
                        $tailleCabinet->setReseauNational($reseauNational);

                        $em->persist($tailleCabinet);

                    }else {

                        $reseauNational = $regroupement->getId();

                        $tailleCabinet = new TailleCabinet();

                        $tailleCabinet->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setReseauNational($reseauNational)
                        ;

                        $em->persist($tailleCabinet);

                    }
                }

                $em->flush();



                $regroupements = $em->getRepository(Regroupement::class)->findBy([], ['nom' => 'ASC']);

                break;

            case "groupe":

                $groupe = new Groupe();
                $groupe->setNom($nom);

                $em->persist($groupe);
                $em->flush();

                foreach ($tabAnnee as $item) {

                    $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);

                    if ($tailleCabinet) {
                        $groupeNational = $tailleCabinet->getGroupeNational();
                        if ($groupeNational) {
                            $groupeNational = $groupeNational.','.$groupe->getId();
                        }else {
                            $groupeNational = $groupe->getId();
                        }
                        $tailleCabinet->setGroupeNational($groupeNational);

                        $em->persist($tailleCabinet);

                    }else {

                        $groupeNational = $groupe->getId();

                        $tailleCabinet = new TailleCabinet();

                        $tailleCabinet->setCabinet($cabinet)
                            ->setAnnee($item)
                            ->setGroupeNational($groupeNational)
                        ;

                        $em->persist($tailleCabinet);

                    }
                }

                $em->flush();

                $groupes = $em->getRepository(Groupe::class)->findBy([], ['nom' => 'ASC']);

                $questionnaire = $em->getRepository(Questionnaire::class)->find($idQuestionnaire);


                break;

            default:

                break;
        }

        $tabResult = [];



            foreach ($tabAnnee as $item) {

                //Nb client
                $tailleCabinet = $em->getRepository(TailleCabinet::class)->findTaille($cabinet, $item);

                if ($tailleCabinet) {
                    $tabResult['nbClient_'.$item] = $tailleCabinet->getNbClient();
                    $tabResult['dateCloture_'.$item] = $tailleCabinet->getDateCloture();
                    $tabResult['effectif_'.$item] = $tailleCabinet->getEffectif();
                    $tabResult['hommeEc_'.$item] = $tailleCabinet->getNbHommeEc();
                    $tabResult['femmeEc_'.$item] = $tailleCabinet->getNbFemmeEc();
                    $tabResult['hommeAs_'.$item] = $tailleCabinet->getNbHommeAs();
                    $tabResult['femmeAs_'.$item] = $tailleCabinet->getNbFemmeAs();
                    $tabResult['nbBureau_'.$item] = $tailleCabinet->getNbBureau();
                    $tabResult['localisation_'.$item] = explode(",", $tailleCabinet->getLocalisation());
                    $tabResult['groupement_'.$item] = explode(",", $tailleCabinet->getGroupeNational());
                    $tabResult['reseau_'.$item] = explode(",", $tailleCabinet->getReseauNational());
                }else {
                    $tabResult['nbClient_'.$item] = "";
                    $tabResult['dateCloture_'.$item] = "";
                    $tabResult['effectif_'.$item] = "";
                    $tabResult['hommeEc_'.$item] = "";
                    $tabResult['femmeEc_'.$item] = "";
                    $tabResult['hommeAs_'.$item] = "";
                    $tabResult['femmeAs_'.$item] = "";
                    $tabResult['nbBureau_'.$item] = "";
                    $tabResult['localisation_'.$item] = "";
                    $tabResult['groupement_'.$item] = "";
                    $tabResult['reseau_'.$item] = "";
                }

            }





        return $this->render('admin/questionnaire/questionnaire_tr_'.$type.'.html.twig', [
            'questionnaire' => $questionnaire,
            'groupes' => $groupes,
            'regroupements' => $regroupements,
            'annee' => $annee,
            'annee1' => $annee1,
            'annee2' => $annee2,
            'tabResult' => $tabResult,
        ]);

    }



}
