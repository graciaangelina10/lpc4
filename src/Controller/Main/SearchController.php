<?php
/**
 * IndexController File Doc Comment.
 *
 * @category SearchController
 *
 * @author  Bocasay
 * @license
 *
 * @see
 */

namespace App\Controller\Main;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Article;
use App\Service\EventTopService;
use App\Service\VideoTopService;
use App\Service\BrefService;
use App\Service\ArticleService;
use App\Service\UneArticleService;

class SearchController extends AbstractController
{

    /**
     * @Route("/search", name="front_search")
     *
     * @return Response
     */
    public function indexAction(EventTopService $eventService, VideoTopService $videoService, ArticleService $articleService, VideoTopService $videoTopService, Request $request, BrefService $brefService)
    {
        $em = $this->getDoctrine()->getManager();

        $search = $request->get('q');
        $articles = $articleService->getArticles(10, 1,null, null,null, null, $search);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $videosTop = $videoService->getAll();

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosLatest = $videoTopService->getAll();

        $tagHome = array();

        $agenda = $eventService->getAll();

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Search/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'firstArticle' => false,
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'search' => $search,
            ]
        );
    }


}
