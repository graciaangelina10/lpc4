<?php
/**
 * IndexController File Doc Comment.
 *
 * @category IndexController
 *
 * @author  Bocasay
 * @license
 *
 * @see
 */

namespace App\Controller\Main;

use App\Entity\Categorie;
use App\Entity\LinkSociaux;
use App\Entity\Page;
use App\Entity\Produit;
use App\Entity\Tags;
use App\Repository\ProduitRepository;
use App\Service\AchatService;
use App\Service\MetaService;
use App\Service\SiteMapService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Service\EventTopService;
use App\Service\VideoTopService;
use App\Service\BrefService;
use App\Service\ArticleService;
use App\Service\UneArticleService;

class IndexController extends AbstractController
{
    const RSS_FEED =  'https://cpc.kentikaas.com/RSS/Fil_216_KEY470016480304470024479441469026438137431702.xml?title=LPCBReVeS';
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="home")
     *
     * @return Response
     */
    public function indexAction(EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, MetaService $metaService)
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $articleService->getArticles(10, 1, null, null, null, null, null, 1);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = $uneArticleService->getAll();

        $videosTop = $videoService->getAll();

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosLatest = $videoTopService->getAll();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $agenda = $eventService->getAll();

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'articlesUne' => $this->serializer->serialize($articlesUne, 'json'),
                'videosTop' => $this->serializer->serialize($videosTop, 'json'),
                'eventsTop' => $this->serializer->serialize($eventsTop, 'json'),
                'firstArticle' => false,
                'tagHome' => $tagHome,
                'bref' => $this->serializer->serialize($bref, 'json'),
                'videosLatest' => $this->serializer->serialize($videosLatest, 'json'),
                'agenda' => $this->serializer->serialize($agenda, 'json'),
                'meta'=> $metaService->generateMeta("home"),
                'accueil'=>1,
            ]
        );
    }

    /**
     * Show article.
     *
     * @param string $slug slug
     *
     * @Route("/article/{categorie}/{slug}",name="show_article",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleAction(
        $categorie,
        $slug,
        EventTopService $eventService,
        VideoTopService $videoService,
        BrefService $brefService,
        ArticleService $articleService,
        UneArticleService $uneArticleService,
        VideoTopService $videoTopService,
        MetaService $metaService,
        AchatService $achatService,
        Request $request
    )
    {
        $em = $this->getDoctrine()->getManager();

        $articleFullContent = $articleService->getArticleFullContent($slug);

        $produitRepository = $em->getRepository(Produit::class);

        $userNotAbonne = false;

        if (!$articleFullContent) {
            $articleFullContent = array();
        } else {
            // On clone pour bypasser le proxy doctrine.
            // Si on ne clone pas, la version de l'articel qui est dans la liste
            //  est aussi mit en mode fullContent
            //Desactiver pour modif avec lecture 1300mots pour les non abonnées du 10/03/2020 par Marcel

            if ($articleFullContent->getPremium() == 1 ) {
                $user = $this->getUser();

                if (!$user || !in_array('ROLE_ABONNE', $user->getRoles())) {
                    //Check if produit acheté par le client
                    $url = "https://www.laprofessioncomptable.com".$request->getRequestUri();

                    $produit = $produitRepository->findOneBy(['linkArticle' => $url]);

                    if ($achatService->checkProduitUser($user, $produit)) {
                        $user->setPaidSubscriber(true);
                        $userNotAbonne = true;
                    }else {

                        $contenuLimite = $articleFullContent->getContenu();
                        $contenu = $articleService->truncate($contenuLimite, 1300, '.....', true, true);

                        $articleFullContent->setContenu($contenu);
                    }

                } else {




                }

                /*if (!$user) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Index/_article_forbidden.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                        'article' => $articleFullContent,
                    ]);
                }
                if(!in_array('ROLE_ABONNE', $user->getRoles())){

                    $achats = $em->getRepository("App:Achat")->findBy(array("user"=>$user),array("id"=>"DESC"));
                    $tabUrlProduit = [];
                    $uri = $request->getSchemeAndHttpHost()."".$this->generateUrl('show_article', array('slug'=>$slug, 'categorie'=>$categorie));

                    if ($achats) {
                        foreach ($achats as $un_achat) {
                            $produit = $un_achat->getProduit();
                            if ($produit->getLinkArticle()) {
                                $tabUrlProduit[] = $produit->getLinkArticle();
                            }
                        }
                    }

                    if ($user->getPaidSubscriber() != 1 ) {
                        if (!in_array($uri, $tabUrlProduit)) {
                            $produits = $achatService->getProduitsForUser($user);
                            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');

                            return $this->render('main/Index/_article_forbidden.html.twig', [
                                'abonnementPrime' => $abonnementPrime,
                                'article' => $articleFullContent,
                            ]);
                        }


                    }

                }*/
            }

            if ($categorie == "undefined") { //Pour un article sans categorie
                $categorie = new Categorie();
                $categorie->setNom("undefined");
                $categorie->setSlug("undefined");
                $articleFullContent->setCategorie($categorie);
            }

            if ($articleFullContent->getType() == "Calameo") {
                $articleFullContent->setType("Articles");
            }

            $title = $articleFullContent->getTitre();

            $description = $articleFullContent->getResume();

            $articleFullContent = clone $articleFullContent;
            $articleFullContent->setFullContent(true);
            $articleFullContent->setContenuCache($articleFullContent->getContenu());
            $articleFullContent = [$articleFullContent];

        }

        if ($userNotAbonne) {
            $articlesAll = [];
        }else {
            $articlesAll = $articleService->getArticles(10, 1);
        }


        if (!$articlesAll) {
            $articlesAll = array();
        }

        $articles = @array_merge($articleFullContent, $articlesAll);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = $uneArticleService->getAll();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'tagHome' => $tagHome,
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'categorie'=>$categorie,
                'meta'=> $metaService->generateMeta("show_article", $title, strip_tags($description)),
                'accueil'=>1,
                'userNotAbonne' => $userNotAbonne,
            ]
        );
    }

    /**
     * Show article aperçu admin.
     *
     * @param string $slug slug
     *
     * @Route("/admin/apercu/{slug}",name="show_article_apercu",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticlePreviewAction($slug, EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, MetaService $metaService)
    {
        $em = $this->getDoctrine()->getManager();

        $articleFullContent = $em->getRepository(Article::class)->findBySlug($slug);

        if (!$articleFullContent) {
            $articleFullContent = array();
        } else {

            $articleFullContent = $articleFullContent[0];
            $title = $articleFullContent->getTitre();
            $description = $articleFullContent->getResume();
            if (method_exists($articleFullContent->getCategorie(), "getSlug") ) {
                $categorie = $articleFullContent->getCategorie()->getSlug();
            }else {
                //Pour un article sans categorie
                $categorie = new Categorie();
                $categorie->setNom("undefined");
                $categorie->setSlug("undefined");
                $articleFullContent->setCategorie($categorie);
            }

            if ($articleFullContent->getType() == "Calameo") {
                $articleFullContent->setType("Articles");
            }

            $articleFullContent = clone $articleFullContent;

            $articleFullContent->setFullContent(true);

            $articleFullContent->setContenuCache($articleFullContent->getContenu());

            $articleFullContent = [$articleFullContent];
        }

        $articlesAll = $articleService->getArticles(10, 1);

        if (!$articlesAll) {
            $articlesAll = array();
        }

        $articles = @array_merge($articleFullContent, $articlesAll);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = $uneArticleService->getAll();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->findBy(array("type"=>"Événements","status"=>"publie"), array("datePublication"=>"DESC"), 4);

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        $title = "Admin - check article";
        $description = "Admin - check article";

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'tagHome' => $tagHome,
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'meta'=> $metaService->generateMeta("show_article", $title, strip_tags($description)),
                'accueil'=>1,
                'disableRedirections'=> true
            ]
        );
    }

    /**
     * Show article par categorie.
     *
     * @param string $slug slug
     *
     * @Route("/categorie/{slug}",name="show_article_categorie",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleCategorieAction($slug, EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, MetaService $metaService)
    {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository(Categorie::class)->findBySlug($slug);
        if ($categorie) {
            $articles = $articleService->getArticles(10, 1, $categorie[0]->getId());
        } else {
            $articles = array();
        }

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $currentCategorie = $em->getRepository(Categorie::class)->findBy(array('slug' => $slug));

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        $idParent = "";
        if (method_exists($currentCategorie[0]->getParent(),'getId')) {
            $idParent = $currentCategorie[0]->getParent()->getId();
        }

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $this->serializer->serialize($articlesUne, 'json'),
                'currentCategorie' => $currentCategorie,
                'tagHome' => $tagHome,
                'videosTop' => $this->serializer->serialize($videosTop, 'json'),
                'eventsTop' => $this->serializer->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->serializer->serialize($bref, 'json'),
                'videosLatest' => $this->serializer->serialize($videosLatest, 'json'),
                'agenda' => $this->serializer->serialize($agenda, 'json'),
                'idParent'=>$idParent,
                'meta'=> $metaService->generateMeta("show_article_categorie", $categorie[0]->getNom())
            ]
        );
    }

    /**
     * Show article par tag.
     *
     * @param string $slug slug
     *
     * @Route("/tag/{slug}",name="show_article_tag",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleTagAction($slug, EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, VideoTopService $videoTopService, MetaService $metaService)
    {
        $em = $this->getDoctrine()->getManager();

        $tag = $em->getRepository(Tags::class)->findBySlug($slug);
        if (!$tag){
            throw new \Exception('Unable to find Tag');
        }
        $articles = $articleService->getArticles(10, 1, null, null, null, $slug);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->findBy(array("type"=>"Événements","status"=>"publie"), array("datePublication"=>"DESC"), 4);

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Index/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $articlesUne,
                'videosTop' => $this->serializer->serialize($videosTop, 'json'),
                'eventsTop' => $this->serializer->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->serializer->serialize($bref, 'json'),
                'videosLatest' => $this->serializer->serialize($videosLatest, 'json'),
                'agenda' => $this->serializer->serialize($agenda, 'json'),
                'tagSlug' =>$slug,
                'meta'=> $metaService->generateMeta("show_article_tag", $tag[0]->getNom())
            ]
        );
    }

    public function shareButtonAction($position)
    {
        $em = $this->getDoctrine()->getManager();

        //recuperer la derniere pub insérer
        $linkReseaux = $em->getRepository(LinkSociaux::class)->findAll();

        return $this->render(
            'main/Index/_share_buttons.html.twig', array(
                'linkReseaux' => $linkReseaux,
            )
        );
    }

    /**
     * @Route("/breves_rss", name="front_breves_rss", methods={"GET","POST"})
     *
     * @return Response
     */
    public function rssBrevesAction()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::RSS_FEED);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        //$response = new Response($data, $httpCode);
        //$response->headers->set('Content-Type', 'text/xml');
        $xmlData = simplexml_load_string($data);
        $tabData = array();
        if ($xmlData) {
            $i = 0;
            foreach ($xmlData->channel->item as $data) {

                if ($i > 2) break; //afficher seulement les 3 dernières
                $tabData[$i]['id'] = trim($data->Record_num);
                $tabData[$i]['titre'] = trim($data->title);
                $pubDate = str_replace("GMT","",trim($data->pubDate));
                $datePublication = new \DateTime($pubDate);
                $tabData[$i]['datePublication'] = $datePublication->format("d.m.y");
                //$tabData[$i]['link'] = trim($data->Lien->URL);
                $tabData[$i]['link'] = trim($data->link);
                $i++;
            }
        }

        return $this->render(
            'main/Commun/_breves.html.twig', array(
                'tabData' => $tabData,
            )
        );

    }

    /**
     * Generate the article feed
     *
     * @Route("/feed",name="feed_rss",methods={"GET"})
     *
     * @return Response XML Feed
     */
    public function feedAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findBy(array("status"=>"publie"),array("datePublication"=>"DESC"));

        $feed = $this->get('eko_feed.feed.manager')->get('article');
        $feed->addFromArray($articles);

        return new Response($feed->render('rss')); // or 'atom'
    }

    public function menuFooterAction()
    {
        $em = $this->getDoctrine()->getManager();

        //recuperer la derniere pub insérer
        $pageFooter = $em->getRepository(Page::class)->findBy(array("footer"=>1),array("position"=>"asc"));

        return $this->render(
            'main/Index/_menuFooter.html.twig', array(
                'pageFooter' => $pageFooter,
            )
        );
    }

    /**
     * Generate sitemap xml
     *
     * @Route("/sitemap",name="sitemap",methods={"GET"})
     *
     * @return Response XML
     */
    public function siteMapAction(SiteMapService $siteMapService){

        $container = $this->container;
        $urls=$siteMapService->genererURL($container);

        $today = date("Y-m-d");
        $response = new Response($this->renderView('main/Index/sitemap.xml.twig',array('urls' => $urls, 'date' => $today )));
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
        return $response;

    }

    /**
     * @Route("/articleData/jsonData", name="articlesDataJson")
     * @return Response
     */
    public function articlesJsAction()
    {
        $articles = $this->get('session')->get('articlesData');

        $response = new Response("var articlesFirstBatch =".$articles);
        $response->headers->set('Content-Type', 'text/javascript');
        return $response;

    }

    /**
     * @Route("/articlesUneData/jsonData", name="articlesUneDataJson")
     * @return Response
     */
    public function articlesUneJsAction()
    {
        $articles = $this->get('session')->get('articlesUneData');

        $response = new Response("var frontPageArticle =".$articles);
        $response->headers->set('Content-Type', 'text/javascript');
        return $response;

    }

    /**
     * @param EventTopService $eventService
     * @param VideoTopService $videoService
     * @param ArticleService $articleService
     * @param VideoTopService $videoTopService
     * @param Request $request
     * @param BrefService $brefService
     * @Route("/calameo/article/load/calameo",name="index_load_page_calameo",methods={"GET"})
     * @return Response
     */
    public function loadPageCalameo(EventTopService $eventService, VideoTopService $videoService, ArticleService $articleService, VideoTopService $videoTopService, Request $request, BrefService $brefService)
    {
        $em = $this->getDoctrine()->getManager();

        $search = $request->get('url');
        //$articles = $articleService->getArticles(10, 1,null, null,null, null, $search);

        $urlCalameo = "https://v.calameo.com/?bkcode=005853105eacf1028c146&mode=viewer&authid=ggwcUENgrZIc&page=1&clicktourl=https://www.laprofessioncomptable.com/article/actualites/classement-2020-de-la-profession-comptable-en-france&clickto=url&allowminiskin=1&wmode=opaque";

        $contenuCalameo = file_get_contents($urlCalameo);


        //return new Response($contenuCalameo);

        // Return a response with a specific content
        $response = new Response($contenuCalameo);

        $response->headers->set('Content-Type', 'text/html');
        // Dispatch request
        return $response;

        $articles = [];

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $videosTop = $videoService->getAll();

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosLatest = $videoTopService->getAll();

        $tagHome = array();

        $agenda = $eventService->getAll();

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Search/index.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'firstArticle' => false,
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'search' => $search,
            ]
        );
    }






}
