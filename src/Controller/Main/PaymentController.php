<?php

namespace App\Controller\Main;

use App\Entity\Paiement;
use App\Service\Webhook;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;
use Psr\Log\LoggerInterface;

use App\Entity\Produit;
use App\Service\AchatService;
use App\Service\PanierService;
use App\Service\PaypalService;
use App\Service\PaiementService;
use App\Form\Boutique\LoginType;
use App\Form\AdresseType;

class PaymentController extends AbstractController
{
    /**
     * @Route("/api/payment/create", name="payment_create", methods={"GET", "POST"})
     */
    public function createAction(PaypalService $paypalService, LoggerInterface $logger) {
        $user = $this->getUser();
        $payment = null;

        try {
            $payment = $paypalService->createPaymentForCurrentPanier($user);

        } catch (\Exception $e) {
            $logger->critical($e);
            return $this->redirectToRoute('payment_failed');
        }

        $jsonPayment = new \stdclass();
        $jsonPayment->id = $payment->id;

        return new JsonResponse($jsonPayment);
    }

    /**
     * @Route("/api/payment/execute", name="payment_execute", methods={"POST"})
     */
    public function executeAction(
        PaypalService $paypalService,
        Request $request,
        LoggerInterface $logger,
        AchatService $achatService,
        PaiementService $paiementService,
        PanierService $panierService,
        \Swift_Mailer $mailer
    ) {
        $paymentID = $request->get('paymentID');

        if (!$paymentID) {
            return new Response('No paymentID specified', 400);
        }

        $payerID = $request->get('payerID');

        if (!$payerID) {
            return new Response('No payerID specified', 400);
        }

        try {
            $paypalService->executePayment($paymentID, $payerID);
        } catch (\Exception $e) {
            $logger->critical($e);
            return $this->redirectToRoute('payment_failed');
        }

        $user = $this->getUser();
        $panier = $panierService->getPanierForUser($user);
        $paiement = $paiementService->createPaiement($user, $paymentID, $payerID, $panier->getPrixTotal());
        $achatService->buyPanier($paiement, $panier);
        $panierService->removeSessionPanier();

        $em = $this->getDoctrine()->getManager();

        $paiement = $em->getRepository(Paiement::class)->find($paiement->getId());

        //send email
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement);
        //send à cpc
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement, "c.demarco@bba.fr");
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement, "pbaey@cpc-doc.com");
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement, "mrazafimandimby@bocasay.com");
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement, "camyot@bba.fr");
        $paypalService->sendMail($user, "Confirmation de votre commande", $mailer, $paiement, "comptabilite@bba.fr");


        return $this->render('main/Boutique/Success/index.html.twig');
    }

    /**
     * @Route("/boutique/echec_paiement", name="payment_failed", methods={"GET"})
     */
    public function failedAction()
    {
        return $this->render('main/Boutique/Failed/index.html.twig');
    }


    /**
     * @param PaypalService $paypalService
     * @param Webhook $webhook
     * @Route("boutique/webhook", name="payment_webhook_list", methods={"GET", "POST"})
     * @return \PayPal\Api\Webhook
     */
    public function getWebhookNotification(PaypalService $paypalService, \Swift_Mailer $mailer)
    {
        $apiContext = $paypalService->getApiContext();

        $params = array(
            // 'start_time'=>'2014-12-06T11:00:00Z',
            // 'end_time'=>'2014-12-12T11:00:00Z'
        );

        try {
            $output = \PayPal\Api\WebhookEvent::all($params, $apiContext);
        } catch (Exception $ex) {
            die($ex);
        }

        $resultEvents = json_decode($output, true);
        $paypalService->setNotification($resultEvents, $mailer);

        return new Response("OK");
    }

}
