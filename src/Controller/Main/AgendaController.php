<?php

namespace App\Controller\Main;

use App\Entity\Categorie;
use App\Entity\Tags;
use App\Service\AchatService;
use App\Service\AgendaService;
use App\Service\MetaService;
use App\Service\SiteMapService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Service\EventTopService;
use App\Service\VideoTopService;
use App\Service\BrefService;
use App\Service\ArticleService;
use App\Service\UneArticleService;

class AgendaController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDateEventAction(AgendaService $agendaService)
    {
        $listeDate = $agendaService->listDate();
        $this->get('session')->set('listeDateEvent', $listeDate);

        return $this->render('main/Agenda/date_event.html.twig', array(
            'listeDate'=> $listeDate,
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDateEventBoutomAction(AgendaService $agendaService)
    {
        if (null != $this->get('session')->get('listeDateEvent')) {
            $listeDate =   $this->get('session')->get('listeDateEvent');
        } else {
            $listeDate = $agendaService->listDate();
        }

        return $this->render('main/Agenda/date_event_boutom.html.twig', array(
            'listeDate'=> $listeDate,
        ));
    }

    /**
     * Show article agenda.
     *
     * @param string $slug slug
     *
     * @Route("/article-agenda/{categorie}/{slug}",name="show_article_agenda",methods={"GET"})
     *
     * @return JsonResponse|Response
     */
    public function showArticleAgendaAction($categorie, $slug, EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, MetaService $metaService, AchatService $achatService)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();




        $articleFullContent = $articleService->getArticleFullContent($slug);

        if ($categorie == "undefined") { //Pour un article sans categorie
            $categorie = new Categorie();
            $categorie->setNom("undefined");
            $categorie->setSlug("undefined");
            $articleFullContent->setCategorie($categorie);
        }

        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Index/_article_forbidden.html.twig', [
                'abonnementPrime' => $abonnementPrime,
                'article' => $articleFullContent,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Index/_article_forbidden.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                        'article' => $articleFullContent,
                    ]);

                }

            }
        }



        if (!$articleFullContent) {
            $articleFullContent = array();
        } else {
            // On clone pour bypasser le proxy doctrine.
            // Si on ne clone pas, la version de l'articel qui est dans la liste
            //  est aussi mit en mode fullContent
            if ($articleFullContent->getPremium() == 1 ) {
                $user = $this->getUser();
                if (!$user) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);
                }
                if(!in_array('ROLE_ABONNE', $user->getRoles())){
                    if ($user->getPaidSubscriber() != 1) {
                        $produits = $achatService->getProduitsForUser($user);
                        $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                        return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                            'abonnementPrime' => $abonnementPrime,
                        ]);

                    }

                }
            }

            $title = "Agenda | ".$articleFullContent->getTitre();
            $description = $articleFullContent->getResume();
            $articleFullContent = clone $articleFullContent;
            $articleFullContent->setFullContent(true);
            $articleFullContent->setContenuCache($articleFullContent->getContenu());
            $articleFullContent = [$articleFullContent];


        }

        $dateAgendaEnCours = date("Ym");
        $articlesAll = $articleService->getArticles(100, 1, null, ['Événements'], null, null, null, null, $dateAgendaEnCours);
        $this->get('session')->set('dateCurrentEvent', $dateAgendaEnCours);
        $dateAgendaNext = date('Ym', strtotime("+1 months", strtotime($dateAgendaEnCours)));
        $this->get('session')->set('dateCurrentEventNext', $dateAgendaNext);

        $dateAgendaPrevious = date('Ym', strtotime("-1 months",strtotime($dateAgendaEnCours)));
        $this->get('session')->set('dateCurrentEventPrevious', $dateAgendaPrevious);


        if (!$articlesAll) {
            $articlesAll = array();
        }

        $articles = @array_merge($articleFullContent, $articlesAll);

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        return $this->render(
            'main/Agenda/agenda.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => true,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'tagHome' => $tagHome,
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'categorie'=>$categorie,
                'meta'=> $metaService->generateMeta("show_article", $title, strip_tags($description)),
                'accueil'=>1,
            ]
        );
    }

    /**
     * @Route("agenda/{dateAgenda}", name="article_agenda_date", methods={"POST", "GET"})
     */
    public function agendaPageAction($dateAgenda, EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, MetaService $metaService, AchatService $achatService)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                'abonnementPrime' => $abonnementPrime,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);

                }

            }
        }

        $articlesAll = $articleService->getArticles(100, 1, null, ['Événements'], null, null, null, null, $dateAgenda);

        if (!$articlesAll) {
            $articlesAll = array();
        }

        $articles = $articlesAll;

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->findBy(array("type"=>"Événements","status"=>"publie"), array("datePublication"=>"DESC"), 4);

        $bref = $brefService->getAll();

        $videosTop = $videoService->getAll();

        $videosLatest = $videoTopService->getAll();

        $agenda = $eventService->getAll();

        $title = "Agenda ".$dateAgenda." | La profession comptable";

        $description = "Agenda ".$dateAgenda." | La profession comptable";

        $this->get('session')->set('dateCurrentEvent', $dateAgenda);

        $dateConvert = date("Ymd",strtotime($dateAgenda."01"));

        $dateAgendaNext = date('Ym', strtotime("+1 months",strtotime($dateConvert)));
        $dateAgendaPrevious = date('Ym', strtotime("-1 months",strtotime($dateConvert)));

        $this->get('session')->set('dateCurrentEventNext', $dateAgendaNext);
        $this->get('session')->set('dateCurrentEventPrevious', $dateAgendaPrevious);

        return $this->render(
            'main/Agenda/agenda.html.twig',
            [
                'articles' => $articles,
                'categories' => $categories,
                'fullContent' => true,
                'firstArticle' => false,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'tagHome' => $tagHome,
                'videosTop' => $this->get('jms_serializer')->serialize($videosTop, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
                'categorie'=>array(),
                'meta'=> $metaService->generateMeta("show_article", $title, strip_tags($description)),
                'accueil'=>0,
            ]
        );
    }

}
