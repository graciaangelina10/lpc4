<?php

namespace App\Controller\Main;

use App\Entity\Page;
use App\Service\MetaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/information/{slug}", name="page_information")
     */
    public function indexAction($slug, MetaService $metaService)
    {
        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository(Page::class)->findBySlug($slug);
        if ($page) {
            $page = $page[0];
        }else {
            $page = array();
        }
        return $this->render('main/Page/index.html.twig', array(
            'page'=>$page,
            'meta'=> $metaService->generateMeta("page_information", $page->getTitre(), html_entity_decode(substr(strip_tags($page->getContenu()),0, 250)))
        ));
    }

}
