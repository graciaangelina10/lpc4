<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 3/13/19
 * Time: 9:51 AM
 */

namespace App\Controller\Main;


use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\User;
use App\Form\CommentaireFrontType;
use App\Form\LoginCommentType;
use App\Form\OrderRegistrationCommentType;
use App\Service\UserService;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CommentController extends AbstractController
{
    private $userManager;
    private $eventDispatcher;
    private $userPasswordEncoder;

    public function __construct(EventDispatcherInterface $eventDispatcher, UserProviderInterface $userManager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("/comment/add/{id}", name="commentaire_add", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction($id, Request $request, UserService $userService)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            return $this->redirectToRoute("home");
        }

        $updateComment = $request->get("update_comment");
        if ($updateComment == 1) {
            return $this->commentUpdate($request, $article, $user, $userService);
        }

        $comment = new Commentaire();
        $form = $this->createForm("App\Form\CommentaireFrontType", $comment);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            if (null == $user) {
                return $this->redirect("/login?urltoredirect=".$request->get('routeToArticle'));
            }
            //if comment is the response another comment, you will check the variable commentaire_id not empty
            $commentaire_id = $request->get("commentaire_id");
            $commentParent = $this->checkCommentParent($commentaire_id);

            if (null == $article->getCategorie()) {
                $categorie = new Categorie();
                $categorie->setNom("undefined");
                $categorie->setSlug("undefined");
                $article->setCategorie($categorie);
            }

            if ($commentParent) {
                $comment->setParent($commentParent);
            }
            $comment->setArticle($article);
            $comment->setUser($user);
            $em->persist($comment);
            $em->flush();

            $this->get('session')->set('showComment', 1);
            $this->get('session')->set('txtComment', "");

            $this->get('session')->getFlashBag()->add(
                'success',
                'Enregistrement effectuée avec succès!'
            );

            //Send email to administrateur
            $to = [
                'mrazafimandimby@bocasay.com',
                'pbaey@laprofessioncomptable.com',
                'camyot@laprofessioncomptable.com',
                'pbaey@cpc-doc.com'
            ];

            $userService->sendEmailNewComment($to, $user, $comment);

            //redirection vers la page fiche article avec show comment
            return $this->redirect($request->get('routeToArticle'));

        }

        $comments = $em->getRepository(Commentaire::class)->findBy(["article"=>$article, 'approuve'=> 1],['treeLeft' => 'ASC', "dateCreation"=>"DESC"]);

        return $this->render("main/Commentaire/list.html.twig", [
            "comments" => $comments,
            "form" => $form->createView(),
            "article"=>$article,
            'routeToArticle' => $request->get('routeToArticle')
        ]);
    }

    /**
     * @param $id
     * @return array|object|null
     */
    public function checkCommentParent($id= null)
    {
        if (null != $id) {
            $em = $this->getDoctrine()->getManager();
            $comment = $em->getRepository(Commentaire::class)->find($id);

            return $comment;
        }

        return [];

    }

    /**
     * Update comment via btn Modifier
     *
     * @param $request
     * @param $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function commentUpdate($request, $article, $userComment, $userService)
    {
        $em = $this->getDoctrine()->getManager();


        $comment = $em->getRepository(Commentaire::class)->findBy([
            "id"=>$request->get('commentaire_id'),
            "user"=>$userComment,
            "article"=> $article
        ]);

        if ($comment) {
            $comment = $comment[0];
            if (null == $userComment) {
                return $this->redirect("/login?urltoredirect=".$request->get('routeToArticle'));
            }

            $message = $request->get('commentaire_front');

            $comment->setComment($message['comment']);
            $comment->setArticle($article);
            $comment->setUser($userComment);
            $em->persist($comment);
            $em->flush();

            $this->get('session')->set('showComment', 1);
            $this->get('session')->set('txtComment', "");

            $this->get('session')->getFlashBag()->add(
                'success',
                'Modification effectuée avec succès!'
            );

            //Send email to administrateur
            $to = [
                'marcel.razafimandimby@gmail.com',
                'mrazafimandimby@bocasay.com'
            ];

            $userService->sendEmailNewComment($to, $userComment, $comment);

            //redirection vers la page fiche article avec show comment
            return $this->redirect($request->get('routeToArticle'));
        }

        return $this->redirectToRoute("home");

    }

    /**
     * @param Article $article
     * @param Request $request
     *
     * @Route("/comment/list/{id}", name="commentaire_list", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            return new Response("null");
        }

        $comment = new Commentaire();
        $form = $this->createForm("App\Form\CommentaireFrontType", $comment);

        $comments = $em->getRepository(Commentaire::class)->findBy(["article"=>$article, 'approuve'=> 1],['treeLeft' => 'ASC', "dateCreation"=>"DESC"]);

        return $this->render("main/Commentaire/list.html.twig", [
            "comments" => $comments,
            "form" => $form->createView(),
            "article"=>$article,
            'routeToArticle' => $request->get('routeToArticle')
        ]);
    }

    /**
     * @param Commentaire $commentaire
     * @param Request $request
     *
     * @Route("comment/delete/{id}", name="comment_delete", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Commentaire $commentaire, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if ($commentaire && $user == $commentaire->getUser()) {
            //check if comment had the response comment
            $responseComment = $em->getRepository(Commentaire::class)->findBy([
                "parent"=> $commentaire
            ]);

            if ($responseComment) {
                foreach ($responseComment as $un_response) {
                    $un_response->setParent(null);
                    $em->persist($un_response);
                    $em->flush();
                }
            }
            $em->remove($commentaire);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'error',
                'Suppression effectuée avec succès!'
            );

            $this->get('session')->set('showComment', 1);

            return $this->redirect($request->get('routeToArticle'));

        }

        return $this->redirectToRoute('home');

    }

    /**
     * @param Request $request
     *
     * @Route("comment/authentic", name="comment_authentic", methods={"GET", "POST"})
     * @return Response
     */
    public function authentificationAction(Request $request)
    {
        $loginForm = $this->createForm(
            LoginCommentType::class,
            null,
            ['action' => $this->generateUrl('comment_login')]
        );

        $registrationForm = $this->createForm(
            OrderRegistrationCommentType::class,
            null,
            ['action' => $this->generateUrl('comment_registration')]
        );

        return $this->render("main/Commentaire/authentification.html.twig", [
            'loginForm' => $loginForm->createView(),
            'registrationForm' => $registrationForm->createView(),
            'routeToArticle'=> $request->get('urltoredirect'),
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("comment/registration", name="comment_registration", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response|null
     */
    public function registrationAction(Request $request)
    {

        $urlRedirect = $request->get('routeToArticle');

        if (null == $urlRedirect) {
            return $this->redirectToRoute('home');
        }

        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null!= $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(OrderRegistrationCommentType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user->setIsComment(true);
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
            $this->userManager->updateUser($user);

            $em->flush();


            $response = $this->redirect($urlRedirect);

            $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response
                ));

            $this->get('session')->set('showComment', 1);

            return $response;
        }

        $event = new FormEvent($form, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

        if (null!== $response = $event->getResponse()) {
            return $response;
        }

        $loginForm = $this->createForm(
            LoginCommentType::class,
            null,
            ['action' => $this->generateUrl('comment_login')]
        );

        return $this->render("main/Commentaire/authentification.html.twig", [
            'loginForm' => $loginForm->createView(),
            'registrationForm' => $form->createView(),
            'routeToArticle'=> $urlRedirect,
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("comment/login", name="comment_login", methods={"POST", "GET"})
     * @return Response
     */
    public function loginCommentAction(Request $request)
    {

        $urlRedirect = $request->get('routeToArticle');

        if (null == $urlRedirect) {
            return $this->redirectToRoute('home');
        }

        $loginForm = $this->createForm(
            LoginCommentType::class,
            null,
            ['action' => $this->generateUrl('comment_login')]
        );

        $registrationForm = $this->createForm(
            OrderRegistrationCommentType::class,
            null,
            ['action' => $this->generateUrl('comment_registration')]
        );

        $loginForm->handleRequest($request);

        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $username = $loginForm['username']->getData();
            $enteredPassword = $loginForm['password']->getData();

            $repoUser = $em->getRepository(User::class);
            $user = $repoUser->findOneByUsername($username);
            if (!$user) {
                $user = $repoUser->findOneByEmail($username);
            }

            if (!$user) {
                $loginForm->get("username")
                    ->addError(new FormError("Email ou mot de passe incorrect"));

                return $this->render("main/Commentaire/authentification.html.twig", [
                    'loginForm' => $loginForm->createView(),
                    'registrationForm' => $registrationForm->createView(),
                    'routeToArticle'=> $urlRedirect,
                ]);
            }


            $isPasswordValid = $this->userPasswordEncoder->isPasswordValid($user, $enteredPassword);

            if (!$isPasswordValid) {
                $loginForm->get('username')
                    ->addError(new FormError("Email ou mot de passe incorrect"));

                return $this->render("main/Commentaire/authentification.html.twig", [
                    'loginForm' => $loginForm->createView(),
                    'registrationForm' => $registrationForm->createView(),
                    'routeToArticle'=> $urlRedirect,
                ]);
            }

            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $event = new InteractiveLoginEvent($request, $token);
            $this->eventDispatcher->dispatch("security.interactive_login", $event);

            $this->get('session')->set('showComment', 1);

            $em->flush();

            return $this->redirect($urlRedirect);

        }



        return $this->render("main/Commentaire/authentification.html.twig", [
            'loginForm' => $loginForm->createView(),
            'registrationForm' => $registrationForm->createView(),
            'routeToArticle'=> $urlRedirect,
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("comment/saveTxtComment", name="comment_saveTxt", methods={"POST"})
     *
     * @return Response
     */
    public function saveCommentAction(Request $request)
    {
        $txtComment = $request->get('txtComment');
        //add txtComment in session
        $this->get('session')->set('txtComment', $txtComment);

        return new Response($txtComment);
    }



}