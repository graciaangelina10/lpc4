<?php

namespace App\Controller\Main;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\Revue;
use App\Entity\Tags;
use App\Service\AchatService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\EventTopService;
use App\Service\VideoTopService;
use App\Service\BrefService;
use App\Service\ArticleService;
use App\Service\UneArticleService;
use Symfony\Component\Routing\Annotation\Route;

class RevueController extends AbstractController
{
    /**
     * @Route("/mes-revues", name="liste_revues")
     */
    public function mesRevuesAction(AchatService $achatService)
    {
        $user = $this->getUser();
        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                'abonnementPrime' => $abonnementPrime,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);

                }

            }
        }

        $em = $this->getDoctrine()->getManager();
        $revues = $em->getRepository(Revue::class)->findBy(array("active"=>1), array("dateCreation"=>"DESC"));

        $anneeActive = date("Y");
        $tabListDateRevue = $this->get('app.revue.service')->getAnneRevue($revues, $anneeActive);

        if (!in_array(date("Y"),$tabListDateRevue['tabAnneeRevue'])){
            $anneeActive = $tabListDateRevue['tabAnneeRevue'][0];
        } else {
            $anneeActive = date("Y");
        }

        $revuesDate = $this->get('app.revue.service')->getRevueDate($revues, $anneeActive);

        return $this->render(
            'main/Revue/mes_revues.html.twig', array(
            'revues'=>$revuesDate,
            'dateRevue'=>$tabListDateRevue['tabAnneeRevue'],
            'anneeActive'=>$anneeActive,
                'anneePrevious'=>$tabListDateRevue['anneePrevious'],
                'anneeNext'=> $tabListDateRevue['anneeNext'],
            )
        );
    }

    /**
     * @Route("/mes-revues/{annee}", name="liste_revues_annee")
     */
    public function mesRevuesDateAction($annee, AchatService $achatService)
    {
        $user = $this->getUser();
        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                'abonnementPrime' => $abonnementPrime,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);

                }

            }
        }

        $em = $this->getDoctrine()->getManager();
        $revues = $em->getRepository(Revue::class)->findBy(array("active"=>1), array("dateCreation"=>"DESC"));

        $tabListDateRevue = $this->get('app.revue.service')->getAnneRevue($revues, $annee);

        $revuesDate = $this->get('app.revue.service')->getRevueDate($revues, $annee);
        return $this->render(
            'main/Revue/mes_revues.html.twig', array(
            'revues'=>$revuesDate,
                'dateRevue'=>$tabListDateRevue['tabAnneeRevue'],
                'anneeActive'=>$annee,
                'anneePrevious'=>$tabListDateRevue['anneePrevious'],
                'anneeNext'=> $tabListDateRevue['anneeNext'],
            )
        );
    }

    /**
     * @param $slug
     *
     * @Route("/revue/{slug}", name="revue_fiche")
     */
    public function revueAction($slug,EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, AchatService $achatService)
    {
        $user = $this->getUser();
        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                'abonnementPrime' => $abonnementPrime,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);

                }

            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $revue = $em->getRepository(Revue::class)->findBy(array("slug"=>$slug, "active"=>1));
        if ($revue) {
            $revue = $revue[0];
            //add numero revue session
            $this->get('session')->set('numero',$revue->getNumero());
        }else {
            return $this->redirectToRoute("home");
        }

        $articles = $articleService->getArticles(10, 1,null,null,$revue->getId());

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $articlesUne = array();

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $videosLatest = $videoTopService->getAll();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $agenda = $eventService->getAll();

        $bref = $brefService->getAll();

        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Revue/revue.html.twig', array(
                'revue'=>$revue,
                'articles' => $articles,
                'categories' => $categories,
                'articlesUne' => $this->get('jms_serializer')->serialize($articlesUne, 'json'),
                'eventsTop' => $this->get('jms_serializer')->serialize($eventsTop, 'json'),
                'firstArticle' => false,
                'tagHome' => $tagHome,
                'bref' => $this->get('jms_serializer')->serialize($bref, 'json'),
                'videosLatest' => $this->get('jms_serializer')->serialize($videosLatest, 'json'),
                'agenda' => $this->get('jms_serializer')->serialize($agenda, 'json'),
            )
        );
    }


    /**
     * @param $slug
     *
     * @Route("/lire-revue/{slug}", name="revue_fiche_lecture")
     */
    public function revueDetailAction($slug,EventTopService $eventService, VideoTopService $videoService, BrefService $brefService, ArticleService $articleService, UneArticleService $uneArticleService, VideoTopService $videoTopService, AchatService $achatService)
    {
        $user = $this->getUser();
        if (!$user) {
            $produits = $achatService->getProduitsForUser($user);
            $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
            return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                'abonnementPrime' => $abonnementPrime,
            ]);
        }else{
            if(!in_array('ROLE_ABONNE', $user->getRoles())){
                if ($user->getPaidSubscriber() != 1) {
                    $produits = $achatService->getProduitsForUser($user);
                    $abonnementPrime = $achatService->getProduit($produits, 'abonnement-prime');
                    return $this->render('main/Boutique/Failed/non_authorize.html.twig', [
                        'abonnementPrime' => $abonnementPrime,
                    ]);

                }

            }
        }

        $em = $this->getDoctrine()->getManager();

        $revue = $em->getRepository(Revue::class)->findBy(array("slug"=>$slug));

        if ($revue) {
            $revue = $revue[0];
            //add numero revue session
            $this->get('session')->set('numero',$revue->getNumero());
        }else {
            return $this->redirectToRoute("home");
        }



        //dont't show comment
        $this->get('session')->set('showComment', 0);

        return $this->render(
            'main/Revue/revue_calameo.html.twig', array(
                'revue'=>$revue,

            )
        );
    }

}
