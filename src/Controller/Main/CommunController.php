<?php

namespace App\Controller\Main;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\Newsletter;
use App\Entity\Produit;
use App\Entity\Revue;
use App\Entity\Tags;
use App\Form\NewsletterDownloadType;
use App\Form\NewsletterType;
use App\Service\BrefService;
use App\Service\EventTopService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CommunController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param EventTopService $eventService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function jsSiderBarAction(EventTopService $eventService, BrefService $brefService)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Categorie::class)->getCategorieParentWithArticle();

        $tagHome = $em->getRepository(Tags::class)->findBy(array("home"=>1), array("position"=>"ASC"));

        $eventsTop = $em->getRepository(Article::class)->getEventCenter();

        $agenda = $eventService->getAll();

        $bref = $brefService->getAll();

        return $this->render(
            'main/Commun/sider_bar.html.twig', array(
            'categories' => $categories,
            'tagHome' => $tagHome,
            'eventsTop' => $this->serializer->serialize($eventsTop, 'json'),
            'agenda' => $this->serializer->serialize($agenda, 'json'),
                'bref' => $this->serializer->serialize($bref, 'json'),
            )
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getLastRevueAction()
    {
        $em = $this->getDoctrine()->getManager();

        $revue = $em->getRepository(Revue::class)->findOneBy(array("isCheck"=>"1", "active"=>1));


        if(!$revue) {
            $revue = $em->getRepository(Revue::class)->findOneBy(array("active"=>1),array("id"=>"DESC"));
            if (!$revue) {
                return $this->render(
                    'main/Commun/revue-sliderbar.html.twig', array(
                        'revue' => array(),
                        'revuePrecedent'=>array(),

                    )
                );
            }
        }


        $revuePrecedent = $em->getRepository(Revue::class)->findOneBy(array("id"=>($revue->getId()-1)));
        if(!$revuePrecedent){
            $revuePrecedent = array();
        }

        return $this->render(
            'main/Commun/revue-sliderbar.html.twig', array(
            'revue' => $revue,
                'revuePrecedent'=>$revuePrecedent,

            )
        );
    }

    public function newsletterAction($uri, $desktop)
    {
        $newsletter = new Newsletter();

        $form = $this->createForm(NewsletterType::class, $newsletter);

        return $this->render(
            'main/Commun/newsletter.html.twig', array(
                'form' => $form->createView(),
                'uri' => $uri,
                'desktop' => $desktop,
            )
        );
    }

    public function downloadPdfAction($uri, $desktop)
    {
        $newsletter = new Newsletter();

        $form = $this->createForm(NewsletterDownloadType::class, $newsletter);

        $em = $this->getDoctrine()->getManager();

        $pdfGratuit = $em->getRepository(Produit::class)->findOneBy(array('rubrique'=>'pdf-gratuit', 'disponible' => 1));

        return $this->render(
            'main/Commun/downloadPdf.html.twig', array(
                'form' => $form->createView(),
                'uri' => $uri,
                'desktop' => $desktop,
                'pdfGratuit' => $pdfGratuit,
            )
        );
    }

    /**
     * @param Request $request
     * @Route("/newsletter/add", name="commun_add_newsletter", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addNewsLetter(Request $request)
    {
        $uri = $request->get('uri');

        $newsletter = new Newsletter();

        $form = $this->createForm(NewsletterType::class, $newsletter);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $accepteNews = $request->get('accepteNews');

            if ($accepteNews != "on" || null == $accepteNews) {
                return (null!= $uri)?$this->redirect($uri) :$this->redirectToRoute('home');
            }

            $entityManger = $this->getDoctrine()->getManager();
            $newsletter->setIp($request->getClientIp())
                ->setCreatedAt(new \DateTime('now'))
                ;
            $entityManger->persist($newsletter);
            $entityManger->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('enregistrement.effectuee')
            );

            return (null!= $uri)?$this->redirect($uri) :$this->redirectToRoute('home');
        }

        return $this->render(
            'main/Commun/newsletter.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request $request
     * @Route("/newsletter/add/downloadFile", name="commun_add_newsletter_download", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addNewsLetterDownload(Request $request)
    {

        $this->get('session')->set('hasPdfGrauit', 1);

        $em = $this->getDoctrine()->getManager();

        $pdfGratuit = $em->getRepository(Produit::class)->findOneBy(array('rubrique'=>'pdf-gratuit', 'disponible' => 1));

        $newsletter = new Newsletter();

        $ip = $request->getClientIp();

        $nom = $request->get('nom');

        $prenom = $request->get('prenom');

        $email = $request->get('email');

        $newsletter->setNom($nom);
        $newsletter->setPrenom($prenom);
        $newsletter->setEmail($email);
        $newsletter->setIp($ip);
        $newsletter->setCreatedAt(new \DateTime('now'));
        $newsletter->setProduit($pdfGratuit);

        $entityManger = $this->getDoctrine()->getManager();
        $entityManger->persist($newsletter);
        $entityManger->flush();


        return new Response("OK");

    }

    /**
     * @param Request $request
     * @Route("/uploads-pdf/produit/{filename}", name="commun_download_pdf", methods={"POST", "GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function downloadDocument($filename, Request $request)
    {
        $documentFolder = $this->getParameter('kernel.project_dir'). '/web/uploads/pdf/produit/';

        $em = $this->getDoctrine()->getManager();

        $hasPdfGrauit = $this->get('session')->get('hasPdfGrauit');

        //$hasPdfGrauit = 1;

        $pdfGratuit = $em->getRepository(Produit::class)->findOneBy(array('pdf'=>$filename, 'disponible' => 1));

        if ($pdfGratuit) {

            if ($hasPdfGrauit == 1) {

                $response = new BinaryFileResponse($documentFolder.$filename);

                $response ->setContentDisposition(
                    ResponseHeaderBag::DISPOSITION_INLINE,
                    $filename
                );
                
                return $response;

            }

            return $this->redirect("/#downloadPdf");

        }
        
        $response = new BinaryFileResponse($documentFolder.$filename);

        $response ->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filename
        );
        
        return $response;
        
    }

}
