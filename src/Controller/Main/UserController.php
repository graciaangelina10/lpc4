<?php

namespace App\Controller\Main;

use App\Entity\Abonnement;
use App\Entity\Achat;
use App\Service\AbonnementService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use App\Form\ProfilType;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserController extends AbstractController
{
    private $tokenStorage;
    private $tokenGenerator;
    private $tokenManager;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        TokenGeneratorInterface $tokenGenerator,
        CsrfTokenManagerInterface $tokenManager = null
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->tokenGenerator = $tokenGenerator;
        $this->tokenManager = $tokenManager;
    }

    public function userBlocAction(Request $request, $uri)
    {
        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->render(
            'main/User/blocUser.html.twig',
            [
                'error' => $error,
                'uri' => $uri
            ]
        );
    }

    /**
     * @Route("/mes-commandes", name="front_mes_commandes")
     * @Method({"GET"})
     */
    public function mesCommandesAction(Request $request, AbonnementService $abonnementService)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $abonnement = $em->getRepository(Abonnement::class)->findByUser($user);

        $prixAbonnement = $abonnementService->getPrixAbonnement();

        return $this->render('main/User/MesCommandes/index.html.twig', array(
            'abonnement'=>$abonnement,
            'prixAbonnement'=>$prixAbonnement,
            'achats'=>$em->getRepository(Achat::class)->findBy(array("user"=>$user),array("id"=>"DESC")),
        ));
    }

    /**
     * @Route("/mon-profil", name="front_mon_profil")
     * @Method({"GET", "POST"})
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute("home");
        }
        $form = $this->createForm(ProfilType::class, $user);


        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                //$em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('enregistrement.effectuee')
                );
            }
        }

        return $this->render('main/User/MonProfil/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/profile/", name="fos_user_profiler_abonne")
     * @Method({"GET", "POST"})
     */
    public function profileRedirectAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfilType::class, $user);


        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                //$em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('enregistrement.effectuee')
                );
            }
        }

        return $this->render('main/User/MonProfil/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
