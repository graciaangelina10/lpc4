<?php
namespace App\Command;

use App\Entity\Plan;
use App\Service\AbonnementService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Entity\Abonnement;
use App\Service\PaypalService;
use App\Entity\Plan as LocalPlan;

class CreatePaypalPlanCommand extends ContainerAwareCommand
{
    protected $paypalService;
    protected $abonnementService;

    public function __construct($name = null, PaypalService $paypalService, AbonnementService $abonnementService)
    {
        parent::__construct($name);
        $this->paypalService = $paypalService;
        $this->prixAbonnement = $abonnementService->getPrixAbonnement();
    }

    protected function configure()
    {
        $this
            ->setName('app:create:plan')
            ->setDescription('Créé le plan d\'abonnement paypal')
            ->setHelp('Créé le plan d\'abonnement paypal')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $apiContext = $this->paypalService->getApiContext();
        $router = $this->getContainer()->get('router');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repoPlan = $em->getRepository(Plan::class);
        $plansToCreate = ['abonnement-LPC'];

        foreach ($plansToCreate as $planToCreate) {
            $existingPlan = $repoPlan->findOneByName($planToCreate);
            if (!$existingPlan) {
                $output->writeln("Le plan {$planToCreate} n'existe pas localement. Création");
                $plan = new LocalPlan();
                $plan->setName($planToCreate);
                $em->persist($plan);
            }
        }

        $em->flush();
        $localPlans = $repoPlan->findAll();

        foreach ($localPlans as $planToCreate) {
            if (!$planToCreate->getPaypalPlanId()) {
                try {
                    $result = $this->paypalService->createPlan($planToCreate->getName(), $this->prixAbonnement);
                } catch (\Exception $e) {
                    $output->writeln('Erreur à la création du plan paypal ' . $planToCreate->getName());
                    $output->writeln($e->getMessage());
                    $output->writeln($e->getTraceAsString());

                    continue;
                }

                $output->writeln("Plan paypal [{$planToCreate->getName()}] ({$result->getId()}) créé");
                $planToCreate->setPaypalPlanId($result->getId());
                $em->flush();
            }
        }

        $output->writeln('Oki');
    }
}
