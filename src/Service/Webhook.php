<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 10/17/18
 * Time: 10:31 AM
 */

namespace App\Service;


class Webhook
{
    public function initWebhook()
    {
        $webhook = new \PayPal\Api\Webhook();

        // Set webhook notification URL
        $webhook->setUrl("https://8d420fc0.ngrok.io/boutique/webhook");

        // Set webhooks to subscribe to
        $webhookEventTypes = array();
        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                    '{
                                "name":"PAYMENT.SALE.COMPLETED"
                             }'
        );

        $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                    '{
                                "name":"PAYMENT.SALE.DENIED"
                        }'
        );

        $webhook->setEventTypes($webhookEventTypes);

        return $webhook;
    }

}