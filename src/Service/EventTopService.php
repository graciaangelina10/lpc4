<?php
namespace App\Service;

use App\Entity\Tags;
use App\Entity\EventTop;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commentaire;
use Doctrine\ORM\Mapping\Entity;

class EventTopService
{
    /**
     * EntityManagerInterface
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * EventTopService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(EventTop::class);
    }

    /**
     * Add tag article
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function checkDefaultEventsExistence()
    {
        $events = $this->repo->findAll();
        $requiredPositions = [1, 2, 3, 4];

        foreach ($events as $event) {
            if (($key = array_search($event->getPosition(), $requiredPositions)) !== false) {
                unset($requiredPositions[$key]);
            }
        }

        foreach ($requiredPositions as $position) {
            $event = new EventTop();
            $event->setPosition($position);
            $this->em->persist($event);
        }

        $this->em->flush();
    }

    /**
     * @return ArrayCollection
     */
    public function getAll()
    {
        return $this->repo->getAll();
    }
}
