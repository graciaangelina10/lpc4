<?php
namespace App\Service;

use App\Entity\Bref;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;

class BrefService
{
    const RSS_FEED =  'https://cpc.kentikaas.com/RSS/Fil_216_KEY470016480304470024479441469026438137431702.xml?title=LPCBReVeS';
    /**
     * EntityManagerInterface
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * BrefService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Bref::class);
    }

    /**
     * @return ArrayCollection
     */
    public function getAll()
    {
        //desactiver cette element
        return array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::RSS_FEED);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $xmlData = simplexml_load_string($data);
        $tabData = array();
        if ($xmlData) {
            $i = 0;
            foreach ($xmlData->channel->item as $data) {

                if ($i > 2) break; //afficher seulement les 3 dernières
                $tabData[$i]['id'] = trim($data->Record_num);
                $tabData[$i]['titre'] = trim($data->title);
                $tabData[$i]['datePublication'] = str_replace("GMT","",trim($data->pubDate));
                //$tabData[$i]['link'] = trim($data->Lien->URL);
                $tabData[$i]['link'] = trim($data->link);
                $i++;
            }
        }

        return $tabData;

    }

}
