<?php
namespace App\Service;

use App\Entity\VideoTop;
use App\Entity\Tags;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commentaire;
use Doctrine\ORM\Mapping\Entity;

class VideoTopService
{
    /**
     * EntityManagerInterface
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * VideoTopService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(VideoTop::class);
    }

    /**
     * Add tag article
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function checkDefaultVideosExistence()
    {
        $videos = $this->repo->findAll();
        $requiredPositions = [1, 2, 3];

        foreach ($videos as $video) {
            if (($key = array_search($video->getPosition(), $requiredPositions)) !== false) {
                unset($requiredPositions[$key]);
            }
        }

        foreach ($requiredPositions as $position) {
            $video = new VideoTop();
            $video->setPosition($position);
            $this->em->persist($video);
        }

        $this->em->flush();
    }

    /**
     * @return ArrayCollection
     */
    public function getAll()
    {
        return $this->repo->getAll();
    }
}
