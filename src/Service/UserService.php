<?php
namespace App\Service;

use App\Entity\Abonnement;
use App\Entity\Achat;
use App\Entity\Article;
use App\Entity\Bref;
use App\Entity\Commentaire;
use App\Entity\Newsletter;
use App\Entity\Paiement;
use App\Entity\Panier;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\User;
// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;


class UserService
{
    /**
     * EntityManagerInterface
     *
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Environment
     */
    private $twig;

    /**
     * BrefService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, ContainerInterface $container, Environment $twig)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(User::class);
        $this->mailer = $mailer;
        $this->container = $container;
        $this->twig = $twig;
    }

    public function deleteUser($user)
    {
        //check panier
        $panier = $this->em->getRepository(Panier::class)->findBy(array("user"=>$user));

        if ($panier) {
            foreach ($panier as $un_panier) {
                $panierProduit = $this->em->getRepository("App:PanierProduit")->findByPanier($un_panier);

                if ($panierProduit) {
                    foreach ($panierProduit as $un_panierProduit){
                        $this->em->remove($un_panierProduit);
                    }
                }
                $this->em->remove($un_panier);
            }
        }

        $abonnement =  $this->em->getRepository(Abonnement::class)->findBy(array("user"=>$user));

        if ($abonnement) {
            $this->em->remove($abonnement[0]);
        }

        //delete achat
        $achat = $this->em->getRepository(Achat::class)->findByUser($user);
        if ($achat) {
            foreach ($achat as $un_achat) {
                $this->em->remove($un_achat);
            }
        }

        //Paiement
        $paiement = $this->em->getRepository(Paiement::class)->findByUser($user);
        if ($paiement) {
            foreach ($paiement as $un_paiement) {
                $this->em->remove($un_paiement);
            }
        }

        //delete commentaire
        $commentaires = $this->em->getRepository(Commentaire::class)->findByUser($user);
        if ($commentaires) {
            foreach ($commentaires as $un_commentaire) {
                $this->em->remove($un_commentaire);
            }
        }

        //Set article auteur name
        $articles = $this->em->getRepository(Article::class)->findByUser($user);
        if ($articles) {
            foreach ($articles as $article) {
                $article->setUser(null);
                $this->em->persist($article);
            }
        }

        $this->em->remove($user);
        $this->em->flush();

        return null;
    }

    public function addUserImport($factory)
    {
        $utilisateurs = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen(__DIR__ . "/../../../web/uploads/user/user.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    if ($data[0] != "Nom"){

                        if (!isset($data[0])) {
                            $data[0] = "";
                        }
                        if (!isset($data[1])) {
                            $data[1] = "";
                        }
                        if (!isset($data[2])) {
                            $data[2] = "";
                        }
                        if (!isset($data[3]) || $data[3]=="") {
                            $data[3] = $data[2];
                        }
                        if (!isset($data[4])) {
                            $data[4] = "";
                        }
                        if (!isset($data[5])) {
                            $data[5] = "";
                        }
                        if (!isset($data[6])) {
                            $data[6] = "";
                        }
                        if (!isset($data[7])) {
                            $data[7] = "";
                        }
                        if (!isset($data[8])) {
                            $data[8] = "";
                        }
                        if (!isset($data[9])) {
                            $data[9] = "";
                        }
                        if (!isset($data[10])) {
                            $data[10] = "";
                        }
                        if (!isset($data[11])) {
                            $data[11] = "";
                        }
                        if (!isset($data[12])) {
                            $data[12] = "";
                        }
                        if (!isset($data[13])) {
                            $data[13] = "";
                        }
                        if (!isset($data[14])) {
                            $data[14] = "";
                        }
                        if (!isset($data[15])) {
                            $data[15] = "";
                        }
                        if (!isset($data[16])) {
                            $data[16] = "";
                        }
                        if (!isset($data[17])) {
                            $data[17] = "";
                        }
                        if (!isset($data[18])) {
                            $data[18] = "";
                        }
                        if (!isset($data[19])) {
                            $data[19] = "";
                        }
                        $utilisateurs[$row] = array(
                            "nom" => $data[0],
                            "prenom" => $data[1],
                            "mail" => $data[2],
                            "password" => $data[2],
                            "profession" => $data[4],
                            "societe" => $data[5],
                            "adresse" => $data[6],
                            "adresse2" => $data[7],
                            "codePostal" => $data[8],
                            "ville" => $data[9],
                            "telephone" => $data[10],
                            "abonne" => $data[11],
                            "newsletter" => $data[12],
                            "nom_livraison"=>$data[13],
                            "prenom_livraison" =>$data[14],
                            "societe_livraison" =>$data[15],
                            "adresse_livraison_1" =>$data[16],
                            "adresse_livraison_2" =>$data[17],
                            "code_postale_livraison" =>$data[18],
                            "ville_livraison" =>$data[19]
                        );
                    }
                }
            }
            fclose($handle);

        }

        $allUser = $this->repo->findAll();

        $tabUserExistant = array();
        if ($allUser) {
            foreach ($allUser as $un_user) {
                $tabUserExistant[] = $un_user->getEmail();
            }
        }

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $tabUserExistantNew = array();
        foreach ($utilisateurs as $utilisateur) {

            if (!in_array($utilisateur["mail"], $tabUserExistant) && !in_array($utilisateur["mail"], $tabUserExistantNew)) {
                // On crée un objet utilisateur
                $user = new User();

                // Encode le mot de passe
                $encoder = $factory->getEncoder($user);
                $plainpassword = $utilisateur["password"];
                $password = $encoder->encodePassword($plainpassword, $user->getSalt());

                // Hydrate l'objet avec les informations provenants du fichier CSV
                $user->setPassword($password);
                $user->setNom($utilisateur["nom"]);
                $user->setPrenom($utilisateur["prenom"]);
                $user->setEmail($utilisateur["mail"]);
                $user->setAutreProfession($utilisateur["profession"]);
                $user->setProfession("Autre");
                $user->setSociete($utilisateur["societe"]);
                $user->setAdresseFacturation1($utilisateur["adresse"]);
                $user->setAdresseFacturation2($utilisateur["adresse2"]);
                $user->setCodePostal($utilisateur["codePostal"]);
                $user->setVille($utilisateur["ville"]);
                $user->setTelephone($utilisateur["telephone"]);
                $user->setPaidSubscriber($utilisateur["abonne"]);
                $user->setEnabled(1);
                $user->setConditionsAccepted(1);

                $user->setAdresseFacturation1($utilisateur["adresse"]);
                $user->setAdresseLivraison1($utilisateur["adresse"]);
                $user->setNomLivraison($utilisateur["nom_livraison"]);
                $user->setPrenomLivraison($utilisateur["prenom_livraison"]);
                $user->setCodePostalLivraison($utilisateur["code_postale_livraison"]);
                $user->setSocieteLivraison($utilisateur["societe_livraison"]);
                $user->setVilleLivraison($utilisateur["ville_livraison"]);
                $user->setAdresseLivraison1($utilisateur["adresse_livraison_1"]);
                $user->setAdresseLivraison2($utilisateur["adresse_livraison_2"]);
                $user->setNewsletterSubscription($utilisateur["newsletter"]);
                
                if ($utilisateur["abonne"] == 1) {
                    $user->setRoles(array("ROLE_ABONNE"));
                }

                $tabUserExistantNew[] = $utilisateur["mail"];

                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $this->em->persist($user);
            }

        }

        // Ecriture dans la base de données
        $this->em->flush();

        return $utilisateurs;
    }

    public function exportUserDataBase($fileName)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nom');
        $sheet->setCellValue('B1', 'Prenom');
        $sheet->setCellValue('C1', 'Email');
        $sheet->setCellValue('D1', 'Profession');
        $sheet->setCellValue('E1', 'Societe');
        $sheet->setCellValue('F1', 'Adresse');
        $sheet->setCellValue('G1', 'Code Postal');
        $sheet->setCellValue('H1', 'Ville');
        $sheet->setCellValue('I1', 'Telephone');
        $sheet->setCellValue('J1', 'Date d\'inscription');
        $sheet->setCellValue('K1', 'Rôle');
        $sheet->setCellValue('L1', 'Abonne');
        $sheet->setCellValue('M1', 'Date fin abonnement');
        $sheet->setCellValue('N1', 'Renouvellement automatique');
        $sheet->setCellValue('O1', 'Newsletter Premium');
        $sheet->setCellValue('P1', 'Nom livraison');
        $sheet->setCellValue('Q1', 'Prenom livraison');
        $sheet->setCellValue('R1', 'Société livraison');
        $sheet->setCellValue('S1', 'Adresse livraison');
        $sheet->setCellValue('T1', 'Code postale livraison');
        $sheet->setCellValue('U1', 'Ville postale livraison');
        $sheet->setCellValue('V1', 'Produits - titres');
        $sheet->setCellValue('W1', 'Produits - quantités');
        $sheet->setCellValue('X1', 'Produits - Prix');

        $userAll = $this->em->getRepository("App:User")->findBy(array(),array("dateCreation"=>"DESC"));

        if ($userAll) {
            $k=2;
            foreach ($userAll as $user) {

                $achatUser = $this->em->getRepository("App:Achat")->findByUser($user);

                $titreProduit = [];
                $qttProduit = [];
                $priceProduit = [];

                $endSubscriptionDate = $user->getEndSubscriptionDate();

                $abonne = "FAUX";
                if ($user->getPaidSubscriber() == 1 || in_array('ROLE_ABONNE', $user->getRoles())) {
                    if ($endSubscriptionDate < new \DateTime('now')) {

                    }else {
                        $abonne = "VRAI";
                        $titreProduit [] = "Abonnement prime";
                    }

                }
                $newsletter = "FAUX";
                if ($user->getNewsletterSubscription() == 1) {
                    $newsletter = "VRAI";
                }
                $subscriptionRenewal = "FAUX";
                if ($user->getSubscriptionRenewal() == 1) {
                    $subscriptionRenewal = "VRAI";
                }


                if ($achatUser) {
                    foreach ($achatUser as $achat) {
                        $titreProduit[] = $achat->getProduit()->getNom();
                        $qttProduit[] = $achat->getQtt();
                        if (null!=$achat->getPrixUnitaire()) {
                            $priceProduit[] = ($achat->getPrixUnitaire() * $achat->getQtt())."€";
                        } else {
                            $priceProduit[] = ($achat->getProduit()->getPrix() * $achat->getQtt())."€";
                        }

                    }
                }

                $roles = "";
                if ($user->getRoles()) {
                    $roles = str_replace("ROLE_ADMIN","Administrateur",
                        str_replace("ROLE_VALIDATOR","Validateur",
                        str_replace("ROLE_EDITOR","Editeur",
                        str_replace("ROLE_ABONNE","Abonné",
                        str_replace("ROLE_USER","Utilisateur",
                        implode(",",$user->getRoles())
                    )))));
                }
                $sheet->setCellValue('A'.$k, $user->getNom());
                $sheet->setCellValue('B'.$k, $user->getPrenom());
                $sheet->setCellValue('C'.$k, $user->getEmail());
                if ($user->getProfession() && $user->getProfession()!="Autre") {
                    $sheet->setCellValue('D'.$k, $user->getProfession());
                } else if($user->getAutreProfession()) {
                    $sheet->setCellValue('D'.$k, $user->getAutreProfession());
                }else {
                    $sheet->setCellValue('D'.$k, '');
                }

                $sheet->setCellValue('E'.$k, $user->getSociete());
                $sheet->setCellValue('F'.$k, $user->getAdresseFacturation1());
                $sheet->setCellValue('G'.$k, $user->getCodePostal());
                $sheet->setCellValue('H'.$k, $user->getVille());
                $sheet->setCellValue('I'.$k, $user->getTelephone());
                $sheet->setCellValue('J'.$k, (null != $user->getDateCreation())?$user->getDateCreation()->format("d/m/Y"):"");
                $sheet->setCellValue('K'.$k, $roles);
                $sheet->setCellValue('L'.$k, $abonne);
                $sheet->setCellValue('M'.$k, (null != $user->getEndSubscriptionDate())?$user->getEndSubscriptionDate()->format("d/m/Y"):"");
                $sheet->setCellValue('N'.$k, $subscriptionRenewal);
                $sheet->setCellValue('O'.$k, $newsletter);
                $sheet->setCellValue('P'.$k, $user->getNomLivraison());
                $sheet->setCellValue('Q'.$k, $user->getPrenomLivraison());
                $sheet->setCellValue('R'.$k, $user->getSocieteLivraison());
                $sheet->setCellValue('S'.$k, $user->getAdresseLivraison1());
                $sheet->setCellValue('T'.$k, $user->getCodePostalLivraison());
                $sheet->setCellValue('U'.$k, $user->getVilleLivraison());
                $sheet->setCellValue('V'.$k, implode(", ",$titreProduit));
                $sheet->setCellValue('W'.$k, implode(", ", $qttProduit));
                $sheet->setCellValue('X'.$k, implode(", ",$priceProduit));

                $k++;
            }
        }

        $sheet->setTitle("Utilisateur LPC");

        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => array('argb' => '1ab394'),
                )
            )
        );
        $styleAlignArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' =>[
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ]
            ],
        ];

        $sheet->getStyle('A2:X'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:X1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:X1")->getFont()->setBold(true);
        $sheet->getStyle('A1:X'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:X'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        return $temp_file;


    }

    public function sendEmailNewComment($to=[], $userComment, $commentaire)
    {
        if ($to) {
            foreach ($to as $un_to){
                $message = (new \Swift_Message("LPC - Nouveau commentaire"))
                    ->setFrom($userComment->getEmail())
                    ->setTo($un_to)
                    ->setBody(
                        $this->container->get('templating')->render(
                           'emails/nouveau_commentaire.html.twig',
                            array('user' => $userComment, 'commentaire'=> $commentaire)
                        ),
                        'text/html'
                    )
                ;

                $this->mailer->send($message);


            }
        }

        return new Response("Ok");
    }

    public function sendEmailQuestionnaire($to=[], $questionnaire, $destinataire, $relance = null)
    {
        if ($to) {
            $k=0;

            $template = ($relance == 1)?"questionnaire_cabinet_relance.html.twig":"questionnaire_cabinet.html.twig";

            if ($relance == "jeValide") {
                $template = "questionnaire_valide.html.twig";
            }

            //foreach ($to as $un_to){
                if ($relance == "jeValide") {
                    $message = (new \Swift_Message($questionnaire->getCabinet()->getNom()." : VALIDATION FORMULAIRE CLASSEMENT NATIONAL 2023"))
                    ->setFrom("contact@laprofessioncomptable.com")
                    ->setTo($to)
                    ->setBody(
                        $this->container->get('templating')->render(
                            'emails/'.$template,
                            array('questionnaire' => $questionnaire, 'destinataire' => $destinataire[$k])
                        ),
                        'text/html'
                    )
                ;
                }else {
                    $message = (new \Swift_Message("CLASSEMENT NATIONAL LA PROFESSION COMPTABLE "))
                    ->setFrom("cdemarco@laprofessioncomptable.com")
                    ->setTo($to)
                    ->setBody(
                        $this->twig->render(
                            'emails/'.$template,
                            array('questionnaire' => $questionnaire, 'destinataire' => $destinataire[$k])
                        ),
                        'text/html'
                    )
                ;
                }
                

                $this->mailer->send($message);

                $k++;


            //}
        }

        return new Response("Ok");
    }

    public function exportQuestionnaire($fileName, $modele = null)
    {
        $etude = $this->em->getRepository("App:EtudeNational")->findOneBy(['isActive' => true]);

        $annee = $etude->getAnnee();

        $tabAnnee = [
            $annee -2,
            $annee -1,
            ($annee -2)."_".($annee -1),
        ];

        $questionnaires = $this->em->getRepository("App:Questionnaire")->findBy(['etude' => $etude]);

        $spreadsheet = new Spreadsheet();

        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => array('argb' => '1ab394'),
                )
            )
        );
        $styleAlignArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' =>[
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ]
            ],
        ];

        $inc = 0;

        $groupes = $this->em->getRepository('App:Groupe')->findBy([], ['nom' => 'ASC']);

        $tabGroupe = [];
        foreach ($groupes as $groupe) {
            $tabGroupe[$groupe->getId()] = $groupe->getNom();
        }

        $reseaux = $this->em->getRepository('App:Regroupement')->findBy([], ['nom' => 'ASC']);

        $tabReseaux = [];
        foreach ($reseaux as $reseau) {
            $tabReseaux[$reseau->getId()] = $reseau->getNom();
        }

        $tabDataGlobal = [];

        foreach ($tabAnnee as $item) {

            if ($inc != 0) {
                $spreadsheet->createSheet();
            }


            $spreadsheet->setActiveSheetIndex($inc);


            /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
            $sheet = $spreadsheet->getActiveSheet();

            //Fin annee

            if ($item === ($annee -2)."_".($annee -1)) {
                //Debut titre 1ere ligne
                $sheet->setCellValue('A1', 'Annee');
                $sheet->setCellValue('B1', 'Désignation');
                $sheet->setCellValue('C1', 'Nom du Président');

                $sheet->mergeCells("D1:E1");

                $sheet->setCellValue('D1', 'CA (K€)');
                //$sheet->setCellValue('E1', 'CA (K€)');

                $sheet->mergeCells("F1:G1");
                $sheet->setCellValue('F1', 'CAC (K€)');
                //$sheet->setCellValue('G1', 'CAC (K€)');

                $sheet->mergeCells("H1:I1");
                $sheet->setCellValue('H1', 'EC (K€)');
                //$sheet->setCellValue('I1', 'EC (K€)');

                $sheet->mergeCells("J1:K1");
                $sheet->setCellValue('J1', 'A (K€)');
                //$sheet->setCellValue('K1', 'A (K€)');

                $sheet->mergeCells("L1:M1");
                $sheet->setCellValue('L1', 'B (K€)');
                //$sheet->setCellValue('M1', 'B (K€)');

                $sheet->mergeCells("N1:O1");
                $sheet->setCellValue('N1', 'C (K€)');
                //$sheet->setCellValue('O1', 'C (K€)');

                $sheet->mergeCells("P1:Q1");
                $sheet->setCellValue('P1', 'EC Hommes');
                //$sheet->setCellValue('Q1', 'EC Hommes');

                $sheet->mergeCells("R1:S1");
                $sheet->setCellValue('R1', 'EC Femmes');
                //$sheet->setCellValue('S1', 'EC Femmes');

                $sheet->mergeCells("T1:U1");
                $sheet->setCellValue('T1', 'Associés Hommes');
                //$sheet->setCellValue('U1', 'Associés Hommes');

                $sheet->mergeCells("V1:W1");
                $sheet->setCellValue('V1', 'Associés Femmes');
                //$sheet->setCellValue('W1', 'Associés Femmes');

                $sheet->mergeCells("X1:Y1");
                $sheet->setCellValue('X1', 'Nb de client');
                //$sheet->setCellValue('Y1', 'Nb de client');

                $sheet->mergeCells("Z1:AA1");
                $sheet->setCellValue('Z1', 'Date clôture');
                //$sheet->setCellValue('AA', 'Date clôture');

                $sheet->mergeCells("AB1:AC1");
                $sheet->setCellValue('AB1', 'Effectif');
                //$sheet->setCellValue('AC1', 'Effectif');

                $sheet->mergeCells("AD1:AE1");
                $sheet->setCellValue('AD1', 'Nb de bureau');
                //$sheet->setCellValue('AE1', 'Nb de bureau');

                $sheet->setCellValue('AF1', 'Localisation');
                $sheet->setCellValue('AG1', 'Réseau, association technique ou groupement français');
                $sheet->setCellValue('AH1', 'Réseau international');
                $sheet->setCellValue('AI1', 'Destinateur Nom');
                $sheet->setCellValue('AJ1', 'Destinateur Email');

                $sheet->mergeCells("AK1:AL1");
                $sheet->setCellValue('AK1', 'Juridique (K€)');
                //$sheet->setCellValue('AL1', 'Juridique (K€)');

                $sheet->mergeCells("AM1:AN1");
                $sheet->setCellValue('AM1', 'Fiscal (K€)');
                //$sheet->setCellValue('AN1', 'Fiscal (K€)');

                $sheet->mergeCells("AO1:AP1");
                $sheet->setCellValue('AO1', 'Conseil en organisation (K€)');
                //$sheet->setCellValue('AP1', 'Conseil en organisation (K€)');

                $sheet->mergeCells("AQ1:AR1");
                $sheet->setCellValue('AQ1', 'Autres (K€)');
                //$sheet->setCellValue('AR1', 'Autres (K€)');

                $sheet->mergeCells("AS1:AT1");
                $sheet->setCellValue('AS1', 'TOTAL (K€)');
                //$sheet->setCellValue('AT1', 'TOTAL (K€)');

                $sheet->setCellValue('AU1', 'reference');
                $sheet->setCellValue('AV1', 'isPluridisciplinaire');
                $sheet->setCellValue('AW1', 'Texte');
                $sheet->setCellValue('AX1', 'Texte Pluridisciplinaire');
                $sheet->setCellValue('BA1', 'Forme juridique');
                $sheet->setCellValue('BB1', 'Numéro SIRET');
                $sheet->setCellValue('BC1', 'Numéro SIREN');
                $sheet->setCellValue('BD1', 'Adresse');
                $sheet->setCellValue('BE1', 'Code postal');
                $sheet->setCellValue('BF1', 'Ville');
                $sheet->setCellValue('BG1', 'Département');
                $sheet->setCellValue('BH1', 'Région');
                $sheet->setCellValue('BI1', 'Tél');
                $sheet->setCellValue('BJ1', 'Email');
                $sheet->setCellValue('BK1', 'Site internet');
                //Fin titre 1ere ligne
                //Debut titre 2em ligne
                $sheet->setCellValue('A2', '');
                $sheet->setCellValue('B2', '');
                $sheet->setCellValue('C2', '');

                $sheet->setCellValue('D2', $annee -2);
                $sheet->setCellValue('E2', $annee -1);

                $sheet->setCellValue('F2', $annee -2);
                $sheet->setCellValue('G2', $annee -1);

                $sheet->setCellValue('H2', $annee -2);
                $sheet->setCellValue('I2', $annee -1);

                $sheet->setCellValue('J2', $annee -2);
                $sheet->setCellValue('K2', $annee -1);

                $sheet->setCellValue('L2', $annee -2);
                $sheet->setCellValue('M2', $annee -1);

                $sheet->setCellValue('N2', $annee -2);
                $sheet->setCellValue('O2', $annee -1);

                $sheet->setCellValue('P2', $annee -2);
                $sheet->setCellValue('Q2', $annee -1);

                $sheet->setCellValue('R2', $annee -2);
                $sheet->setCellValue('S2', $annee -1);

                $sheet->setCellValue('T2', $annee -2);
                $sheet->setCellValue('U2', $annee -1);

                $sheet->setCellValue('V2', $annee -2);
                $sheet->setCellValue('W2', $annee -1);

                $sheet->setCellValue('X2', $annee -2);
                $sheet->setCellValue('Y2', $annee -1);

                $sheet->setCellValue('Z2', $annee -2);
                $sheet->setCellValue('AA2', $annee -1);

                $sheet->setCellValue('AB2', $annee -2);
                $sheet->setCellValue('AC2', $annee -1);

                $sheet->setCellValue('AD2', $annee -2);
                $sheet->setCellValue('AE2', $annee -1);

                $sheet->setCellValue('AF2', '');
                $sheet->setCellValue('AG2', '');
                $sheet->setCellValue('AH2', '');
                $sheet->setCellValue('AI2', '');
                $sheet->setCellValue('AJ2', '');

                $sheet->setCellValue('AK2', $annee -2);
                $sheet->setCellValue('AL2', $annee -1);

                $sheet->setCellValue('AM2', $annee -2);
                $sheet->setCellValue('AN2', $annee -1);

                $sheet->setCellValue('AO2', $annee -2);
                $sheet->setCellValue('AP1', $annee -1);

                $sheet->setCellValue('AQ2', $annee -2);
                $sheet->setCellValue('AR2', $annee -1);

                $sheet->setCellValue('AS2', $annee -2);
                $sheet->setCellValue('AT2', $annee -1);

                $sheet->setCellValue('AU2', '');
                $sheet->setCellValue('AV2', '');
                $sheet->setCellValue('AW2', '');
                $sheet->setCellValue('AX2', '');
                $sheet->setCellValue('BA2', '');
                $sheet->setCellValue('BB2', '');
                $sheet->setCellValue('BC2', '');
                $sheet->setCellValue('BD2', '');
                $sheet->setCellValue('BE2', '');
                $sheet->setCellValue('BF2', '');
                $sheet->setCellValue('BG2', '');
                $sheet->setCellValue('BH2', '');
                $sheet->setCellValue('BI2', '');
                $sheet->setCellValue('BJ2', '');
                $sheet->setCellValue('BK2', '');
                //Fin titre 2em ligne
            }else {
                $sheet->setCellValue('A1', 'Annee');
                $sheet->setCellValue('B1', 'Désignation');
                $sheet->setCellValue('C1', 'Nom du Président');
                $sheet->setCellValue('D1', 'CA (K€)');
                $sheet->setCellValue('E1', 'CAC (K€)');
                $sheet->setCellValue('F1', 'EC (K€)');
                $sheet->setCellValue('G1', 'A (K€)');
                $sheet->setCellValue('H1', 'B (K€)');
                $sheet->setCellValue('I1', 'C (K€)');
                $sheet->setCellValue('J1', 'EC Hommes');
                $sheet->setCellValue('K1', 'EC Femmes');
                $sheet->setCellValue('L1', 'Associés Hommes');
                $sheet->setCellValue('M1', 'Associés Femmes');
                $sheet->setCellValue('N1', 'Nb de client');
                $sheet->setCellValue('O1', 'Date clôture');
                $sheet->setCellValue('P1', 'Effectif');
                $sheet->setCellValue('Q1', 'Nb de bureau');
                $sheet->setCellValue('R1', 'Localisation');
                $sheet->setCellValue('S1', 'Réseau, association technique ou groupement français');
                $sheet->setCellValue('T1', 'Réseau international');
                $sheet->setCellValue('U1', 'Destinateur Nom');
                $sheet->setCellValue('V1', 'Destinateur Email');
                $sheet->setCellValue('W1', 'Juridique (K€)');
                $sheet->setCellValue('X1', 'Fiscal (K€)');
                $sheet->setCellValue('Y1', 'Conseil en organisation (K€)');
                $sheet->setCellValue('Z1', 'Autres (K€)');
                $sheet->setCellValue('AA1', 'TOTAL (K€)');
                $sheet->setCellValue('AB1', 'reference');
                $sheet->setCellValue('AC1', 'isPluridisciplinaire');
                $sheet->setCellValue('AD1', 'Texte');
                $sheet->setCellValue('AE1', 'Texte Pluridisciplinaire');
                $sheet->setCellValue('AF1', 'Forme juridique');
                $sheet->setCellValue('AG1', 'Numéro SIRET');
                $sheet->setCellValue('AH1', 'Numéro SIREN');
                $sheet->setCellValue('AI1', 'Adresse');
                $sheet->setCellValue('AJ1', 'Code postal');
                $sheet->setCellValue('AK1', 'Ville');
                $sheet->setCellValue('AL1', 'Département');
                $sheet->setCellValue('AM1', 'Région');
                $sheet->setCellValue('AN1', 'Tél');
                $sheet->setCellValue('AO1', 'Email');
                $sheet->setCellValue('AP1', 'Site internet');
            }



            if ($item === ($annee -2)."_".($annee -1)) {
                $k=3;

            }else {
                $k=2;
            }



            if ($questionnaires && null == $modele) {


                foreach ($questionnaires as $questionnaire) {

                    $cabinet = $questionnaire->getCabinet();

                    if ($item === ($annee -2)."_".($annee -1)) {



                        $itemNew = $annee -1;

                        //Ca
                        $ca = $this->em->getRepository("App:Ca")->findCa($cabinet, $itemNew);

                        //CAC
                        $cac = $this->em->getRepository("App:CaC")->findCaC($cabinet, $itemNew);

                        //CA expertise
                        $caExpertise = $this->em->getRepository("App:CaExpertise")->findCaExpertise($cabinet, $itemNew);

                        //Taille cabinet
                        $tailleCabinet = $this->em->getRepository("App:TailleCabinet")->findTaille($cabinet, $itemNew);

                        //information filiale
                        $informationFiliale = $this->em->getRepository("App:InformationFiliale")->findInformation($cabinet, $itemNew);

                    }else {
                        //Ca
                        $ca = $this->em->getRepository("App:Ca")->findCa($cabinet, $item);

                        //CAC
                        $cac = $this->em->getRepository("App:CaC")->findCaC($cabinet, $item);

                        //CA expertise
                        $caExpertise = $this->em->getRepository("App:CaExpertise")->findCaExpertise($cabinet, $item);

                        //Taille cabinet
                        $tailleCabinet = $this->em->getRepository("App:TailleCabinet")->findTaille($cabinet, $item);

                        //information filiale
                        $informationFiliale = $this->em->getRepository("App:InformationFiliale")->findInformation($cabinet, $item);
                    }



                    //groupe national
                    $listGroupe = "";
                    $tabListGroupe = [];
                    $groupeNationals = ($tailleCabinet)?$tailleCabinet->getGroupeNational():false;
                    if ($groupeNationals) {
                        $groupeNationals = explode(",", $tailleCabinet->getGroupeNational());
                        foreach ($groupeNationals as $groupeNational) {
                            if (isset($tabGroupe[trim($groupeNational)]) && !in_array(trim($groupeNational), $tabListGroupe)) {
                                $listGroupe .= $listGroupe.$tabGroupe[trim($groupeNational)].",";

                                $tabListGroupe [] = trim($groupeNational);
                            }

                        }
                    }

                    //Reseau national
                    $listeReseau = "";
                    $tabListReseau = [];
                    $resauxNationals = ($tailleCabinet)?$tailleCabinet->getReseauNational():false;
                    if ($resauxNationals) {
                        $resauxNationals = explode(",", $tailleCabinet->getReseauNational());
                        foreach ($resauxNationals as $resauxNational) {
                            if (isset($tabReseaux[trim($resauxNational)]) && !in_array(trim($resauxNational), $tabListReseau)) {
                                $listeReseau .= $listeReseau.$tabReseaux[trim($resauxNational)].",";

                                $tabListReseau [] = trim($resauxNational);
                            }

                        }
                    }




                    $personnes = $this->em->getRepository("App:Personne")->findBy(['cabinet' => $cabinet]);
                    $otherPresident = "";
                    if ($personnes) {
                        foreach ($personnes as $personne) {
                            $otherPresident .= ";".$personne->getNom();
                        }
                    }

                    if ($item === ($annee -2)."_".($annee -1)) {

                        $sheet->setCellValue('A'.$k, ($annee -2)."-".($annee -1));
                        $sheet->setCellValue('B'.$k, $cabinet->getNom());
                        $sheet->setCellValue('C'.$k, $cabinet->getNomPresident().$otherPresident);

                        $sheet->setCellValue('D'.$k, $tabDataGlobal['ca'][$cabinet->getId()][0]);
                        $sheet->setCellValue('E'.$k, $tabDataGlobal['ca'][$cabinet->getId()][1]);

                        $sheet->setCellValue('F'.$k, $tabDataGlobal['cac'][$cabinet->getId()][0]);
                        $sheet->setCellValue('G'.$k, $tabDataGlobal['cac'][$cabinet->getId()][1]);

                        $sheet->setCellValue('H'.$k, $tabDataGlobal['ec'][$cabinet->getId()][0]);
                        $sheet->setCellValue('I'.$k, $tabDataGlobal['ec'][$cabinet->getId()][1]);

                        $sheet->setCellValue('J'.$k, $tabDataGlobal['cacA'][$cabinet->getId()][0]);
                        $sheet->setCellValue('K'.$k, $tabDataGlobal['cacA'][$cabinet->getId()][1]);

                        $sheet->setCellValue('L'.$k, $tabDataGlobal['cacB'][$cabinet->getId()][0]);
                        $sheet->setCellValue('M'.$k, $tabDataGlobal['cacB'][$cabinet->getId()][1]);

                        $sheet->setCellValue('N'.$k, $tabDataGlobal['cacC'][$cabinet->getId()][0]);
                        $sheet->setCellValue('O'.$k, $tabDataGlobal['cacC'][$cabinet->getId()][1]);

                        $sheet->setCellValue('P'.$k, $tabDataGlobal['ecHomme'][$cabinet->getId()][0]);
                        $sheet->setCellValue('Q'.$k, $tabDataGlobal['ecHomme'][$cabinet->getId()][1]);

                        $sheet->setCellValue('R'.$k, $tabDataGlobal['ecFemme'][$cabinet->getId()][0]);
                        $sheet->setCellValue('S'.$k, $tabDataGlobal['ecFemme'][$cabinet->getId()][1]);

                        $sheet->setCellValue('T'.$k, $tabDataGlobal['asHomme'][$cabinet->getId()][0]);
                        $sheet->setCellValue('U'.$k, $tabDataGlobal['asHomme'][$cabinet->getId()][1]);

                        $sheet->setCellValue('V'.$k, $tabDataGlobal['asFemme'][$cabinet->getId()][0]);
                        $sheet->setCellValue('W'.$k, $tabDataGlobal['asFemme'][$cabinet->getId()][1]);

                        $sheet->setCellValue('X'.$k, $tabDataGlobal['nbClient'][$cabinet->getId()][0]);
                        $sheet->setCellValue('Y'.$k, $tabDataGlobal['nbClient'][$cabinet->getId()][1]);

                        $sheet->setCellValue('A'.$k, $tabDataGlobal['dateCloture'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AA'.$k, $tabDataGlobal['dateCloture'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AB'.$k, $tabDataGlobal['effectif'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AC'.$k, $tabDataGlobal['effectif'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AD'.$k, $tabDataGlobal['nbBureau'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AE'.$k, $tabDataGlobal['nbBureau'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AF'.$k, ($tailleCabinet)?$tailleCabinet->getLocalisation():"");
                        $sheet->setCellValue('AG'.$k, $listGroupe);
                        $sheet->setCellValue('AH'.$k, $listeReseau);
                        $sheet->setCellValue('AI'.$k, $cabinet->getDestinataire());
                        $sheet->setCellValue('AJ'.$k, $cabinet->getEmailDestinataire());

                        $sheet->setCellValue('AK'.$k, $tabDataGlobal['juridique'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AL'.$k, $tabDataGlobal['juridique'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AM'.$k, $tabDataGlobal['fiscal'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AN'.$k, $tabDataGlobal['fiscal'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AO'.$k, $tabDataGlobal['conseil'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AP'.$k, $tabDataGlobal['conseil'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AQ'.$k, $tabDataGlobal['autre'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AR'.$k, $tabDataGlobal['autre'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AS'.$k, $tabDataGlobal['total'][$cabinet->getId()][0]);
                        $sheet->setCellValue('AT'.$k, $tabDataGlobal['total'][$cabinet->getId()][1]);

                        $sheet->setCellValue('AU'.$k, $cabinet->getCode());
                        $sheet->setCellValue('AV'.$k, $cabinet->isPluridiscplinaire());
                        $sheet->setCellValue('AW'.$k, strip_tags($cabinet->getCommentaire()));
                        $sheet->setCellValue('AX'.$k, strip_tags($cabinet->getTxtPluridiscplinaire()));
                        $sheet->setCellValue('BA'.$k, ($cabinet->getFormeJuridique()!= "Séléctionner.....")?$cabinet->getFormeJuridique():"");
                        $sheet->setCellValue('BB'.$k, $cabinet->getSiret());
                        $sheet->setCellValue('BC'.$k, $cabinet->getSiren());
                        $sheet->setCellValue('BD'.$k, $cabinet->getAdresse());
                        $sheet->setCellValue('BE'.$k, $cabinet->getCodePostal());
                        $sheet->setCellValue('BF'.$k, $cabinet->getVille());
                        $sheet->setCellValue('BG'.$k, $cabinet->getDepartement());
                        $sheet->setCellValue('BH'.$k, $cabinet->getRegion());
                        $sheet->setCellValue('BI'.$k, $cabinet->getTelephone());
                        $sheet->setCellValue('BJ'.$k, $cabinet->getEmail());
                        $sheet->setCellValue('BK'.$k, $cabinet->getSite());

                    }else {
                        $sheet->setCellValue('A'.$k, $item);
                        $sheet->setCellValue('B'.$k, $cabinet->getNom());
                        $sheet->setCellValue('C'.$k, ($item == $annee -1 || $item == $annee -2)?$cabinet->getNomPresident().$otherPresident:"");

                        $sheet->setCellValue('D'.$k, ($ca)?$ca->getCaLettre():"");


                        $sheet->setCellValue('E'.$k, ($cac)?$cac->getCaLettre():"");
                        $sheet->setCellValue('F'.$k, ($caExpertise)?$caExpertise->getCaLettre():"");
                        $sheet->setCellValue('G'.$k, ($caExpertise)?$caExpertise->getALettre():"");
                        $sheet->setCellValue('H'.$k, ($caExpertise)?$caExpertise->getBLettre():"");
                        $sheet->setCellValue('I'.$k, ($caExpertise)?$caExpertise->getCLettre():"");
                        $sheet->setCellValue('J'.$k, ($tailleCabinet)?$tailleCabinet->getNbHommeEc():"");
                        $sheet->setCellValue('K'.$k, ($tailleCabinet)?$tailleCabinet->getNbFemmeEc():"");
                        $sheet->setCellValue('L'.$k, ($tailleCabinet)?$tailleCabinet->getNbHommeAs():"");
                        $sheet->setCellValue('M'.$k, ($tailleCabinet)?$tailleCabinet->getNbFemmeAs():"");
                        $sheet->setCellValue('N'.$k, ($tailleCabinet)?$tailleCabinet->getNbClient():"");
                        $sheet->setCellValue('O'.$k, ($tailleCabinet)?$tailleCabinet->getDateCloture():"");
                        $sheet->setCellValue('P'.$k, ($tailleCabinet)?$tailleCabinet->getEffectif():"");
                        $sheet->setCellValue('Q'.$k, ($tailleCabinet)?$tailleCabinet->getNbBureau():"");
                        $sheet->setCellValue('R'.$k, ($tailleCabinet)?$tailleCabinet->getLocalisation():"");
                        $sheet->setCellValue('S'.$k, $listGroupe);
                        $sheet->setCellValue('T'.$k, $listeReseau);
                        $sheet->setCellValue('U'.$k, $cabinet->getDestinataire());
                        $sheet->setCellValue('V'.$k, $cabinet->getEmailDestinataire());

                        $sheet->setCellValue('W'.$k, ($informationFiliale)?$informationFiliale->getJuridique():"");
                        $sheet->setCellValue('X'.$k, ($informationFiliale)?$informationFiliale->getFiscal():"");
                        $sheet->setCellValue('Y'.$k, ($informationFiliale)?$informationFiliale->getConseil():"");
                        $sheet->setCellValue('Z'.$k, ($informationFiliale)?$informationFiliale->getAutre():"");
                        $sheet->setCellValue('AA'.$k, ($informationFiliale)?$informationFiliale->getTotal():"");
                        $sheet->setCellValue('AB'.$k, $cabinet->getCode());
                        $sheet->setCellValue('AC'.$k, ($item == $annee -1)?$cabinet->isPluridiscplinaire():"");
                        $sheet->setCellValue('AD'.$k, ($item == $annee -1)?strip_tags($cabinet->getCommentaire()):"");
                        $sheet->setCellValue('AE'.$k, ($item == $annee -1)?strip_tags($cabinet->getTxtPluridiscplinaire()):"");
                        $sheet->setCellValue('AF'.$k, ($item == $annee -1 && $cabinet->getFormeJuridique()!= "Séléctionner.....")?$cabinet->getFormeJuridique():"");
                        $sheet->setCellValue('AG'.$k, ($item == $annee -1)?$cabinet->getSiret():"");
                        $sheet->setCellValue('AH'.$k, ($item == $annee -1)?$cabinet->getSiren():"");
                        $sheet->setCellValue('AI'.$k, ($item == $annee -1)?$cabinet->getAdresse():"");
                        $sheet->setCellValue('AJ'.$k, ($item == $annee -1)?$cabinet->getCodePostal():"");
                        $sheet->setCellValue('AK'.$k, ($item == $annee -1)?$cabinet->getVille():"");
                        $sheet->setCellValue('AL'.$k, ($item == $annee -1)?$cabinet->getDepartement():"");
                        $sheet->setCellValue('AM'.$k, ($item == $annee -1)?$cabinet->getRegion():"");
                        $sheet->setCellValue('AN'.$k, ($item == $annee -1)?$cabinet->getTelephone():"");
                        $sheet->setCellValue('AO'.$k, ($item == $annee -1)?$cabinet->getEmail():"");
                        $sheet->setCellValue('AP'.$k, ($item == $annee -1)?$cabinet->getSite():"");
                    }



                    $k++;

                    if ($item === ($annee -2)."_".($annee -1)) {

                    }else {
                        $tabDataGlobal['ca'][$cabinet->getId()][] = ($ca)?$ca->getCaLettre():"";

                        $tabDataGlobal['cac'][$cabinet->getId()][] = ($cac)?$cac->getCaLettre():"";

                        $tabDataGlobal['ec'][$cabinet->getId()][] = ($caExpertise)?$caExpertise->getCaLettre():"";

                        $tabDataGlobal['cacA'][$cabinet->getId()][] = ($caExpertise)?$caExpertise->getALettre():"";

                        $tabDataGlobal['cacB'][$cabinet->getId()][] = ($caExpertise)?$caExpertise->getBLettre():"";

                        $tabDataGlobal['cacC'][$cabinet->getId()][] = ($caExpertise)?$caExpertise->getCLettre():"";

                        $tabDataGlobal['ecHomme'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbHommeEc():"";

                        $tabDataGlobal['ecFemme'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbFemmeEc():"";

                        $tabDataGlobal['asHomme'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbHommeAs():"";

                        $tabDataGlobal['asFemme'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbFemmeAs():"";

                        $tabDataGlobal['nbClient'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbClient():"";

                        $tabDataGlobal['dateCloture'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getDateCloture():"";

                        $tabDataGlobal['effectif'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getEffectif():"";

                        $tabDataGlobal['nbBureau'][$cabinet->getId()][] = ($tailleCabinet)?$tailleCabinet->getNbBureau():"";

                        $tabDataGlobal['juridique'][$cabinet->getId()][] = ($informationFiliale)?$informationFiliale->getJuridique():"";

                        $tabDataGlobal['fiscal'][$cabinet->getId()][] = ($informationFiliale)?$informationFiliale->getFiscal():"";

                        $tabDataGlobal['conseil'][$cabinet->getId()][] = ($informationFiliale)?$informationFiliale->getConseil():"";

                        $tabDataGlobal['autre'][$cabinet->getId()][] = ($informationFiliale)?$informationFiliale->getAutre():"";

                        $tabDataGlobal['total'][$cabinet->getId()][] = ($informationFiliale)?$informationFiliale->getTotal():"";


                    }



                }
            }


            if ($item === ($annee -2)."_".($annee -1)) {
                $sheet->setTitle("classement_".($annee -2)."_".($annee -1));

                $sheet->getStyle('A1:BK'.$k)->applyFromArray($styleAlignArray);
                $sheet->getStyle('A1:BK1')->applyFromArray($styleArray);
                $sheet->getStyle('A2:BK2')->applyFromArray($styleArray);

                $sheet->getStyle("A1:BK1")->getFont()->setBold(true);
                $sheet->getStyle("A2:BK2")->getFont()->setBold(true);

                $sheet->getStyle('A1:BK'.$k)->getAlignment()->setWrapText(true);
                foreach (range('A1:BK'.$k, $sheet->getHighestDataColumn()) as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }

                $sheet->getStyle('D1:AG1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('AK1:AT1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            }else {
                $sheet->setTitle("classement_".$item);

                $sheet->getStyle('A1:AP'.$k)->applyFromArray($styleAlignArray);
                $sheet->getStyle('A1:AP1')->applyFromArray($styleArray);
                $sheet->getStyle("A1:AP1")->getFont()->setBold(true);
                $sheet->getStyle('A1:AP'.$k)->getAlignment()->setWrapText(true);
                foreach (range('A1:AP'.$k, $sheet->getHighestDataColumn()) as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
            }


            $inc++;
        }

        //Groupe
        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex($inc);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Nom');

        $groupes = $this->em->getRepository("App:Groupe")->findBy([], ['nom' => 'ASC']);

        $k=2;
        foreach ($groupes as $groupe) {
            $sheet->setCellValue('A'.$k, $groupe->getId());
            $sheet->setCellValue('B'.$k, $groupe->getNom());
            $k++;
        }

        $sheet->setTitle("Réseau, groupe");

        $sheet->getStyle('A1:B'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:B1")->getFont()->setBold(true);
        $sheet->getStyle('A1:B'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:B'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $inc++;

        //Reseau international
        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex($inc);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Nom');

        $reseaux = $this->em->getRepository("App:Regroupement")->findBy([], ['nom' => 'ASC']);

        $k=2;
        foreach ($reseaux as $reseau) {
            $sheet->setCellValue('A'.$k, $reseau->getId());
            $sheet->setCellValue('B'.$k, $reseau->getNom());
            $k++;
        }

        $sheet->setTitle("Réseau international");

        $sheet->getStyle('A1:B'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:B1")->getFont()->setBold(true);
        $sheet->getStyle('A1:B'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:B'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $inc++;

        //Departement
        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex($inc);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Nom');

        $departements = $this->em->getRepository("App:Departement")->findBy([], ['nom' => 'ASC']);

        $k=2;
        foreach ($departements as $departement) {
            $sheet->setCellValue('A'.$k, $departement->getId());
            $sheet->setCellValue('B'.$k, $departement->getNom());
            $k++;
        }

        $sheet->setTitle("Département");

        $sheet->getStyle('A1:B'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:B1")->getFont()->setBold(true);
        $sheet->getStyle('A1:B'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:B'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $inc++;

        //Region
        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex($inc);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Nom');

        $regions = $this->em->getRepository("App:Region")->findBy([], ['nom' => 'ASC']);

        $k=2;
        foreach ($regions as $region) {
            $sheet->setCellValue('A'.$k, $region->getId());
            $sheet->setCellValue('B'.$k, $region->getNom());
            $k++;
        }

        $sheet->setTitle("Region");

        $sheet->getStyle('A1:B'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:B1")->getFont()->setBold(true);
        $sheet->getStyle('A1:B'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:B'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $inc++;


        $spreadsheet->setActiveSheetIndex(0);



        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        return $temp_file;

    }

    public function exportBarometreDataBase($fileName)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nom');
        $sheet->setCellValue('B1', 'Prenom');
        $sheet->setCellValue('C1', 'Email');
        $sheet->setCellValue('D1', 'IP');
        $sheet->setCellValue('E1', 'Date');

        $barometres = $this->em->getRepository(Newsletter::class)->findBarometre();

        if ($barometres) {
            $k=2;
            foreach ($barometres as $barometre) {

                $sheet->setCellValue('A'.$k, $barometre->getNom());
                $sheet->setCellValue('B'.$k, $barometre->getPrenom());
                $sheet->setCellValue('C'.$k, $barometre->getEmail());
                $sheet->setCellValue('D'.$k, $barometre->getIp());
                $sheet->setCellValue('E'.$k, $barometre->getCreatedAt()->format("d/m/Y"));
                
                $k++;
            }
        }

        $sheet->setTitle("Baromètre LPC");

        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => array('argb' => '1ab394'),
                )
            )
        );
        $styleAlignArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' =>[
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ]
            ],
        ];

        $sheet->getStyle('A2:E'.$k)->applyFromArray($styleAlignArray);
        $sheet->getStyle('A1:E1')->applyFromArray($styleArray);
        $sheet->getStyle("A1:E1")->getFont()->setBold(true);
        $sheet->getStyle('A1:E'.$k)->getAlignment()->setWrapText(true);
        foreach (range('A1:E'.$k, $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        return $temp_file;

    }


}
