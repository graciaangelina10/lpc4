<?php

namespace App\Service;

use App\Entity\PanierProduit;
use Doctrine\Common\Persistence\ObjectManager;
use App\Manager\BaseManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Services\Mailer;

use App\Entity\Panier;
use App\Entity\Produit;

class PanierService
{
    protected $em;
    protected $session;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->em = $entityManager;
        $this->session = $session;
    }

    public function getPanierForUser($user = null)
    {
        if (!$user) {
            return $this->getSessionPanier();
        }

        if (!$user->getPanier()) {
            $this->createPanierForUser($user);
        }

        return $user->getPanier();
    }

    public function getSessionPanier()
    {
        $sessionPanierId = $this->session->get('app.session_panier_id');

        if ($sessionPanierId) {
            $repoPanier = $this->em->getRepository(Panier::class);
            $sessionPanier = $repoPanier->findOneById(['id' => $sessionPanierId, 'paid' => false]);

            if ($sessionPanier) {
                return $sessionPanier;
            }
        }

        return $this->createSessionPanier();
    }

    public function createSessionPanier()
    {
        $panier = new Panier();
        $this->em->persist($panier);
        $this->em->flush();
        $this->session->set('app.session_panier_id', $panier->getId());

        return $panier;
    }

    public function createPanierForUser($user)
    {
        $panier = new Panier();
        $panier->setUser($user);
        $this->em->persist($panier);
        $this->em->flush();
        $user->setPanier($panier);

        return $panier;
    }

    public function removeSessionPanier()
    {
        $this->session->set('app.session_panier_id', null);
    }

    /**
     * Prend le panier en session et ajoute tout ses produits au panier de l'user
     * passé en paramètre
     */
    public function transferSessionToUser($user)
    {
        $sessionPanier = $this->getSessionPanier();
        $userPanier = $this->getPanierForUser($user);

        if (!$sessionPanier || !$userPanier) {
            return;
        }

        foreach ($sessionPanier->getProduitList() as $produit) {
            $userPanier->addProduit($produit);
        }

        return $userPanier;
    }

    /**
     * Supprime les produit qui sont déjà acheté par l'user (improbable mais serait très chiant à gérer)
     */
    public function cleanPanier($panier)
    {
        $user = $panier->getUser();

        if (!$user) {
            return;
        }

        $achatList = $user->getAchatList();

		foreach ($achatList as $achat) {
			$produit = $achat->getProduit();

			if ($panier->contains($produit)) {
				$panier->removeProduit($produit);
			}
		}

		$this->em->flush();
    }

    public function updatePanier($panier, $produitPanier)
    {
        $taux = Produit::TVA;

        if ($produitPanier) {
            $PrixTotal = 0;
            $tva = 0;
            foreach ($produitPanier as $produit) {
                $prix = $produit->getProduit()->getPrix() * $produit->getQtt();
                $PrixTotal = $PrixTotal + ( $produit->getProduit()->getPrix() * $produit->getQtt()) ;
                $tva = $tva + round($prix - ($prix/(1+($taux/100))), 2);
            }

            $panier->setPrixTotal($PrixTotal);
            $panier->setTvaTotal($tva);

            $this->em->persist($panier);
            $this->em->flush();
        }

        return $panier;
    }

    public function setFraisTransport($panier, $ipAdresse = null)
    {
        //$ipAdresse = "212.85.152.125"; //Ip french test
        $ipAdresse = "23.221.76.66"; //Ip United States

        $fraisTransport =  $this->em->getRepository(Produit::class)->findOneBy(['rubrique' => 'frais-transport', 'disponible' => 1]);

        $panierFrais = $this->em->getRepository(PanierProduit::class)->findBy(array("produit"=>$fraisTransport, "panier"=>$panier));

        if ($fraisTransport && !$panierFrais) {

            $ipData = $this->ipData($ipAdresse);

            $ipData = json_decode($ipData);

            $country_code = (isset($ipData->country_code))?$ipData->country_code:"";

            if ($ipData && null != $country_code  && $country_code != "FR") {
                $panierProduit = new PanierProduit();
                $panierProduit->setProduit($fraisTransport);
                $panierProduit->setPanier($panier);
                $panierProduit->setQtt(1);
                $this->em->persist($panierProduit);
                $this->em->flush();
            }


        }

        return $fraisTransport;

    }

    public function removeFraisTransport($panier)
    {
        $panierProduits = $this->em->getRepository("App:PanierProduit")->findBy(array("panier"=>$panier));

        $hasProduit = 0;
        $hasTransport = 0;
        if ($panierProduits) {
            foreach ($panierProduits as $panierProduit) {
                if ($panierProduit->getProduit()->getRubrique() == "hors-serie" || $panierProduit->getProduit()->getRubrique() == "profession-comptable") {
                    $hasProduit++;
                }

                if ($panierProduit->getProduit()->getRubrique() == "frais-transport") {
                    $hasTransport++;
                }
            }

            if ($hasProduit == 0 && $hasTransport > 0) {
                foreach ($panierProduits as $panierProduit) {
                    if ($panierProduit->getProduit()->getRubrique() == "frais-transport") {
                        $this->em->remove($panierProduit);
                        $panier->removeProduit($panierProduit->getProduit());
                    }
                }
                $this->em->flush();
            }
        }

        return $panier;
    }

    public function ipData($ipAdresse)
    {
        // Création d'une nouvelle ressource cURL
        $ch = curl_init();

        // Configuration de l'URL et d'autres options
        curl_setopt($ch, CURLOPT_URL, "https://api.ipdata.co/".$ipAdresse."?api-key=2130337e5a738e250053f2ab6b787325482d5ee844882c38a776aa80");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        // Récupération de l'URL et affichage sur le navigateur
        $result = curl_exec($ch);

        // Fermeture de la session cURL
        curl_close($ch);

        return $result;

    }
}
