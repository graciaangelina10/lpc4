<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 8/22/18
 * Time: 10:31 AM.
 */

namespace App\Service;

use App\Entity\Tags;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class TagsService.
 */
class TagsService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TagsService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Initialise les positions de tags
     *
     * @return null
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function initPosition()
    {
        //check if position 1 exist to know list tags are upDate
        $tags = $this->em->getRepository(Tags::class)->findBy(array('position' => '1'));

        if (!$tags) {
            $tagsAll = $this->em->getRepository(Tags::class)->findAll();

            if ($tagsAll) {
                $inc = 0;

                foreach ($tagsAll as $tag) {
                    $tag->setPosition($inc);
                    $this->em->persist($tag);

                    ++$inc;
                }

                $this->em->flush();
            }
        }

        return null;
    }
}
