<?php

namespace App\Service;

use App\Entity\Produit;
use Doctrine\Common\Persistence\ObjectManager;
use App\Manager\BaseManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\Mailer;

use App\Entity\Achat;
use App\Entity\Paiement;
use App\Entity\Panier;

class AchatService
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Renvoie la liste de tous les produits et alimente le currentAchat pour les 
     * produits qui ont été acheté par $user
     */
    public function getProduitsForUser($user)
    {
        $repoProduit = $this->em->getRepository(Produit::class);
        $repoAchat = $this->em->getRepository(Achat::class);
        $produits = $repoProduit->findAllIndexedById();
        $achats = $repoAchat->findBy(['user' => $user]);

        foreach ($achats as $achat) {
            $produitId = $achat->getProduit()->getId();
            $produits[$produitId]->setCurrentAchat($achat);
        }

        return $produits;
    }

    public function buyPanier(Paiement $paiement, Panier $panier, $typePaiement = null, $forProduct = null)
    {
        $user = $panier->getUser();
        $panier->setPaid();
        $panier->setUser(null);
        $panier->setFormerUserId($user->getId());
        $now = new \DateTime();

        $panierProduit = $this->em->getRepository("App:PanierProduit")->findBy(array("panier"=>$panier));

        foreach ($panierProduit as $produit) {
            if (null != $forProduct && $forProduct == "forProduct") {
                if ($produit->getProduit()->getRubrique() != "abonnement-prime") {
                    $achat = new Achat();
                    $achat->setUser($user);
                    $achat->setProduit($produit->getProduit());
                    $achat->setDateAchat($now);
                    $achat->setPaiement($paiement);
                    $achat->setQtt($produit->getQtt());
                    $achat->setPrixUnitaire($produit->getProduit()->getPrix());
                    $achat->setPrixTotale($produit->getProduit()->getPrix() * $produit->getQtt());
                    $achat->setTva($panier->getTvaTotal());
                    $achat->setTypePaiement($typePaiement);
                    $this->em->persist($achat);
                }
            }else{
                $achat = new Achat();
                $achat->setUser($user);
                $achat->setProduit($produit->getProduit());
                $achat->setDateAchat($now);
                $achat->setPaiement($paiement);
                $achat->setQtt($produit->getQtt());
                $achat->setPrixUnitaire($produit->getProduit()->getPrix());
                $achat->setPrixTotale($produit->getProduit()->getPrix() * $produit->getQtt());
                $achat->setTva($panier->getTvaTotal());
                $achat->setTypePaiement($typePaiement);
                $this->em->persist($achat);
            }


        }

        $this->em->flush();
    }

    public function getProduit($produits, $rubrique)
    {
        $tabProduct = array();

        if ($produits) {
            foreach ( $produits as $item) {
                if ($item->getRubrique() == $rubrique) {
                    $tabProduct[] = $item;
                }

            }
        }

        return $tabProduct;
    }

    public function checkProduitUser($user, $produit)
    {
        $repoAchat = $this->em->getRepository("App:Achat");
        $produitUser = $repoAchat->findOneBy(['user' => $user, 'produit' => $produit]);

        return $produitUser;

    }
}
