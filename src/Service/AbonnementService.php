<?php

namespace App\Service;

use App\Entity\Produit;
use Doctrine\Common\Persistence\ObjectManager;
use App\Manager\BaseManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\Mailer;

use App\Entity\Abonnement;
use App\Entity\Paiement;
use App\Entity\Panier;

class AbonnementService
{
    protected $em;
    protected $repoAbonnement;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->repoAbonnement = $this->em->getRepository(Abonnement::class);
    }

    public function createAbonnementFromAgreement($agreement, $user)
    {
        $now = new \DateTime();
        $oneYearLater = new \DateTime();
        $oneYearLater->modify('+1 year');

        $abonnement = new Abonnement();
        $abonnement->setUser($user);
        $user->addAbonnement($abonnement);
        $abonnement->setApprovalUrl($agreement->getApprovalLink());
        $abonnement->setAgreementPaypalId($agreement->getId());
        $abonnement->setDateStart($now);
        $abonnement->setDateEnd($oneYearLater);

        $this->em->persist($abonnement);
        $this->em->flush();

        return $abonnement;
    }

    public function getNotExecutedYetAbonnement($user)
    {
        return $this->repoAbonnement->findOneBy([
            'user' => $user,
            'agreed' => false,
            'paid' => false,
        ]);
    }

    public function getPrixAbonnement()
    {
        $prixAbonnement = "0";
        $abonnementPrime = $this->em->getRepository(Produit::class)->findBy(array("rubrique"=>"abonnement-prime"));

        if ($abonnementPrime) {
            $prixAbonnement = $abonnementPrime[0]->getPrix();
        }

        return $prixAbonnement;
    }
}
