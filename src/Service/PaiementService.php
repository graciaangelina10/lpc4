<?php

namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use App\Manager\BaseManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\Mailer;

use App\Entity\Paiement;

class PaiementService
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function createPaiement($user, $paypalPaymentId, $paypalPayerId, $totalPrice, $state = null)
    {
        $now = new \DateTime();
        $paiement = new Paiement();
        $paiement->setUser($user);
        $paiement->setPaymentId($paypalPaymentId);
        $paiement->setPayerId($paypalPayerId);
        $paiement->setNom($user->getNom());
        $paiement->setPrenom($user->getPrenom());
        $paiement->setProfession($user->getProfession());
        $paiement->setSociete($user->getSociete());
        $paiement->setCodePostal($user->getCodePostal());
        $paiement->setVille($user->getVille());
        $paiement->setTelephone($user->getTelephone());
        $paiement->setAdresseFacturation1($user->getAdresseFacturation1());
        $paiement->setAdresseFacturation2($user->getAdresseFacturation2());
        if (null!= $user->getAdresseLivraison1()) {
            $paiement->setAdresseLivraison1($user->getAdresseLivraison1());
        }else {
            $paiement->setAdresseLivraison1($user->getAdresseFacturation1());
        }

        if (null!= $user->getAdresseLivraison2()) {
            $paiement->setAdresseLivraison2($user->getAdresseLivraison2());
        }else {
            $paiement->setAdresseLivraison2($user->getAdresseFacturation2());
        }

        if (null!= $user->getNomLivraison()) {
            $paiement->setNomLivraison($user->getNomLivraison());
        }else {
            $paiement->setNomLivraison($user->getNom());
        }

        if (null!= $user->getPrenomlivraison()) {
            $paiement->setPrenomlivraison($user->getPrenomlivraison());
        }else {
            $paiement->setPrenomlivraison($user->getPrenom());
        }

        if (null!= $user->getSocieteLivraison()) {
            $paiement->setSocieteLivraison($user->getSocieteLivraison());
        }else {
            $paiement->setSocieteLivraison($user->getSociete());
        }

        if (null!= $user->getCodePostalLivraison()) {
            $paiement->setCodePostalLivraison($user->getCodePostalLivraison());
        }else {
            $paiement->setCodePostalLivraison($user->getCodePostal());
        }

        if (null!= $user->getVilleLivraison()) {
            $paiement->setVilleLivraison($user->getVilleLivraison());
        }else {
            $paiement->setVilleLivraison($user->getVille());
        }

        if (null != $state) {
            $paiement->setState($state);
        }

        $paiement->setAmountPaid($totalPrice);
        $paiement->setMailPaiement(1);
        $paiement->setProfession($user->getProfession());
        $paiement->setDatePaiement(new \DateTime('now'));

        $this->em->persist($paiement);

        $this->em->flush();

        return $paiement;
    }
}
