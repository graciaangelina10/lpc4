<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 8/22/18
 * Time: 10:31 AM.
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class AgendaService.
 */
class AgendaService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * RevueService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @return array
     */
    public function listDate()
    {
        $article = $this->getEventPublie();
        $tabDateEvent = [];
        $tabDateNbEvent = [];
        if ($article) {
            foreach ($article as $un_article) {
                if (null!=$un_article->getDateDebutEvent()) {
                    $dateEvent = $un_article->getDateDebutEvent();
                    $tabDateEvent[] = $dateEvent->format("Y-m");
                }
            }
            $tabDateEventUnique = array_unique($tabDateEvent);
            if ($tabDateEventUnique) {
                foreach ($tabDateEventUnique as $dateUnique) {
                    $nbDate = 0;
                    foreach ($tabDateEvent as $un_dateEvent) {
                        if ($dateUnique == $un_dateEvent) {
                            $nbDate ++;
                        }
                    }
                    $tabDateNbEvent[$dateUnique] = $nbDate;
                }
            }
        }


        $m = array('','Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');
        $m_chiffre = array('','01','02','03','04','05','06','07','08','09','10','11','12');

        if ($this->session->get('dateCurrentEvent')) {
            $dateAgenda = $this->session->get('dateCurrentEvent');
            $date1 = date("n-Y",strtotime($dateAgenda."01"));
            //$date1 = date("n-Y");
        }else{
            $date1 = date("n-Y");
        }

        $decoupe_date1 = explode('-', $date1);
        $mois_date1 = $decoupe_date1[0];
        $annee_date1 = $decoupe_date1[1];

        $annee = $annee_date1;
        $tabDate = [];
        $k = 0;
        for($i=($mois_date1-1); $i<($mois_date1-1+4); $i++){
            //echo $m[1+($i%12)].' '.$annee.'<br />';

            $tabDate[$k]['anneeMois'] = $m[1+($i%12)]." ".$annee;
            $tabDate[$k]['anneeMoisCourt'] = $annee.$m_chiffre[1+($i%12)];
            if (isset($tabDateNbEvent[($annee."-".$m_chiffre[1+($i%12)])])) {
                $tabDate[$k]['nbEvent'] = $tabDateNbEvent[($annee."-".$m_chiffre[1+($i%12)])];

            } else {
                $tabDate[$k]['nbEvent'] = 0;


            }
            // Décembre ? on change d'année
            if(1+($i%12)==12) { $annee++; }

            $k++;

        }

        return $tabDate;
    }

    public function getEventPublie()
    {
        $limit = 100;
        $articleEvent = $this->em->getRepository("App:Article")->getEventCenter($limit);

        return $articleEvent;
    }
}
