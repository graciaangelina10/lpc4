<?php
/**
 * ArticleService Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 * 
 * @license http://www.gnu.org/copyleft/gpl.html General Public
 * 
 * @see ****
 */

namespace App\Service;

use App\Entity\Categorie;
use App\Entity\PositionArticleRevue;
use App\Entity\Tags;
use App\Entity\UneArticle;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commentaire;
use Doctrine\ORM\Mapping\Entity;

/**
 * Class ArticleService.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */
class ArticleService
{
    /**
     * EntityManager.
     *
     * @var EntityManageInterface;
     */
    private $em;

    /**
     * ArticleService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Add tag article.
     *
     * @param ArrayCollection $data    data
     * @param Entity          $article article
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function setTagArticle($data, $article)
    {
        $allTags = $this->em->getRepository(Tags::class)->findAll();

        //mise à jour tag article
        if (isset($data['App_article']['alltags'])
            and null != $data['App_article']['alltags']
        ) {
            $tagsArticle = $article->getTags();
            //suppression tags qui ne sont pas dans la liste
            if ($tagsArticle) {
                foreach ($allTags as $tag) {
                    if (false == $tagsArticle->contains($tag)) {
                        $article->removeTag($tag);
                    } elseif (true == $tagsArticle->contains($tag)) {
                        $article->addTag($tag);
                    }
                }
            }
        } else {
            if ($allTags) {
                foreach ($allTags as $tag) {
                    $article->removeTag($tag);
                }
            }
        }

        //ajout si nouveau tag
        if (isset($data['App_article']['tagnew'])
            and null != $data['App_article']['tagnew']
        ) {
            $tagNew = $data['App_article']['tagnew'];
            foreach ($tagNew as $tag) {
                $checkTag = $this->em->getRepository('App:Tags')
                    ->findBy(array('nom' => $tag));
                if (!$checkTag) {
                    $newTag = new Tags();
                    $newTag->setNom($tag);

                    $this->em->persist($newTag);

                    $article->addTag($newTag);
                }
            }
        }

        //Test s'il y a revue
        if (isset($data['App_article']['revue'])) {
            $revues = $data['App_article']['revue'];
            //check if article a déjà des revue

        }

        return $article;
    }

    /**
     * Add comment.
     *
     * @param Entity $user        user to add comment
     * @param Entity $article     article to set comment
     * @param string $commentaire commentaire
     *
     * @return Commentaire
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addComment($user, $article, $commentaire)
    {
        $comment = new Commentaire();
        $comment->setUser($user);
        $comment->setArticle($article);
        $comment->setComment($commentaire);
        $comment->setApprouve(true);

        $this->em->persist($comment);
        $this->em->flush();

        return $comment;
    }

    /**
     * Initialise les catégories
     *
     * @return null
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function initCategorie()
    {
        $categories = $this->em->getRepository(Categorie::class)->findBy(array('treeLeft' => '0'));

        if ($categories) {
            $categories = $this->em->getRepository(Categorie::class)->findAll();

            $inc = 1;

            foreach ($categories as $categorie) {
                $categorie->setTreeLeft($inc);
                ++$inc;
                $categorie->setTreeRight($inc);

                $this->em->persist($categorie);
                $this->em->flush();
                ++$inc;
            }
        }

        return null;
    }

    public function getLatestVideoArticle()
    {
        return $this->em->getRepository('App:Article')->getLatestVideosArticle();
    }

    public function getArticleFullContent($slug) 
    {
        return $this->em->getRepository('App:Article')->getArticleFullContent($slug);
    }

    public function getArticleTag($slug) 
    {
        return $this->em->getRepository('App:Article')->getArticleTag($slug);
    }

    public function getArticles($limit = null, $page = null, $category = null, $types = array(), $revue = null, $tag = null, $search = null, $home= null, $dateEvent= null)
    {

        return $this->em->getRepository(Article::class)->getArticles($limit, $page, $category, $types, $revue , $tag, $search, $home, $dateEvent);
    }

    public function saveThumbnails($article)
    {
        $lien = $article->getVideoArticle()->getLien();

        $type = $article->getVideoArticle()->getType();

        $basename = "";

        $rep =  __DIR__.'/../../../web/uploads/images/articles/thumbnails/';

        if ($lien) {
            $parseLien = explode("/",$lien);
            $sizeofTab = sizeof($parseLien);

            $codeVideo = $parseLien[($sizeofTab - 1)];

            $imageThumb = "";

           if ($type == "Vimeo" || preg_match("/vimeo/",$lien)) {
               $xmlVimeo = simplexml_load_file("http://vimeo.com/api/v2/video/".$codeVideo.".xml");
                if ($xmlVimeo) {
                    $imageThumb = trim($xmlVimeo->video->thumbnail_large);
                }

           }else if($type == "Youtube" || preg_match("/youtu/",$lien)) {

              if (preg_match("/watch\?v=/", $codeVideo,$matches)) {
                   $codeVideo = preg_replace("/watch\?v=/", "", $codeVideo);
               }
                     $imageThumb = 'https://img.youtube.com/vi/'.$codeVideo.'/hqdefault.jpg';
           }

           if ($imageThumb !="") {
               $pathinfo = pathinfo($imageThumb);

               $image = @file_get_contents($imageThumb );

               $basename = $pathinfo['filename']."_".$codeVideo.".".$pathinfo['extension'];

               file_put_contents($rep.$basename, $image);

               $article->setThumbnails($basename);
           }

        }

        return $basename;


    }

    /**
     * @param $entity
     * @param $type
     * @return array
     */
    public function duplicateContent($entity, $type)
    {
        $new_entity = [];
        if ($type == "article") {
            $article = $entity;
            $new_entity = clone $article;
            $new_entity->setSlug($article->getSlug().date("Y-m-d h:i:s"));
            $new_entity->setTitre($article->getTitre()."[Duplicate]");
            $new_entity->setStatus("en_cours");
            if (null!= $article->getImage()) {
                copy(__DIR__."/../../../web/uploads/images/articles/".$article->getImage(), __DIR__."/../../../web/uploads/images/articles/".date("Y-m-d")."_".$article->getImage());
                $new_entity->setImage(date("Y-m-d")."_".$article->getImage());
            }
            $new_entity->setDateCreation(new \DateTime('now'));
            $new_entity->setDatePublication(null);

            $this->em->persist($new_entity);
            $this->em->flush();
        } else if ($type == "revue"){
            $revue = $entity;
            $new_entity = clone $revue;
            $new_entity->setSlug($revue->getSlug().date("Y-m-d h:i:s"));
            $new_entity->setTitre($revue->getTitre()."[Duplicate]");

            $sommaires = $this->em->getRepository("App:SommaireRevue")->findByRevue($revue);

            if ($sommaires) {
                foreach ($sommaires as $sommaire) {
                    $new_entity_sommaire = clone $sommaire;
                    $new_entity_sommaire->setRevue($new_entity);
                    $this->em->persist($new_entity_sommaire);
                }

            }

            if (null!= $revue->getImage()) {
                copy(__DIR__."/../../../web/uploads/images/revue/".$revue->getImage(), __DIR__."/../../../web/uploads/images/revue/".date("Y-m-d")."_".$revue->getImage());
                $new_entity->setImage(date("Y-m-d")."_".$revue->getImage());
            }

            if (null!= $revue->getDocument()) {
                copy(__DIR__."/../../../web/uploads/pdf/revue/".$revue->getDocument(), __DIR__."/../../../web/uploads/pdf/revue/".date("Y-m-d")."_".$revue->getDocument());
                $new_entity->setDocument(date("Y-m-d")."_".$revue->getDocument());
            }

            $new_entity->setDateCreation(new \DateTime('now'));
            $new_entity->setActive(false);
            $new_entity->setIsCheck(false);
            $this->em->persist($new_entity);
            $this->em->flush();
            //Add article revue + add position revue
            $articles = $new_entity->getArticle();
            if ($articles) {
                $position = 1;
                foreach ($articles as $unArticle) {
                    $positionArticlerevue = new PositionArticleRevue();
                    $positionArticlerevue->setPosition($position);
                    $positionArticlerevue->setRevue($new_entity);
                    $positionArticlerevue->setArticle($unArticle);
                    $position++;
                    $this->em->persist($positionArticlerevue);
                }
            }

            $this->em->flush();


        } else if ($type == "produit"){
            $produit = $entity;
            $new_entity = clone $produit;
            $new_entity->setNom($produit->getNom()."[Duplicate]");

            if (null!= $produit->getImage()) {
                copy(__DIR__."/../../../web/uploads/images/produit/".$produit->getImage(), __DIR__."/../../../web/uploads/images/produit/".date("Y-m-d")."_".$produit->getImage());
                $new_entity->setImage(date("Y-m-d")."_".$produit->getImage());
            }

            if (null!= $produit->getPdf()) {
                copy(__DIR__."/../../../web/uploads/pdf/produit/".$produit->getPdf(), __DIR__."/../../../web/uploads/pdf/produit/".date("Y-m-d")."_".$produit->getPdf());
                $new_entity->setPdf(date("Y-m-d")."_".$produit->getPdf());
            }

            $new_entity->setDisponible(false);
            $new_entity->setDateParution(new \DateTime('now'));

            $this->em->persist($new_entity);
            $this->em->flush();
        }

        return $new_entity;
    }

    public function setDataFormList($articleForm, $data) {

        if (isset($data['article_filter']['titre']) && null != $data['article_filter']['titre']) {
            $articleForm->setTitre($data['article_filter']['titre']);
        }
        if (isset($data['article_filter']['categorie']) && null != $data['article_filter']['categorie']) {
            $categorie = $data['article_filter']['categorie'];
            foreach ($categorie as $unCat) {
                $categorie = $this->em->getRepository(Categorie::class)->find($unCat);
                $articleForm->addCategorie($categorie);
            }

        }

        if (isset($data['article_filter']['type'])  && null != $data['article_filter']['type']) {
            $type = $data['article_filter']['type'];

            foreach ($type as $unType) {
                $articleForm->addType($unType);
            }


        }
        if (isset($data['article_filter']['status']) && null != $data['article_filter']['status']) {
            $status = $data['article_filter']['status'];
            foreach ($status as $unStatus) {
                $articleForm->addStatus($unStatus);
            }

        }
        if (isset($data['article_filter']['auteur']) && null != $data['article_filter']['auteur']) {
            $auteur = $data['article_filter']['auteur'];

            foreach ($auteur as $unAuteur) {
                $articleForm->addAuteur($unAuteur);
            }
        }

        if (isset($data['article_filter']['premium']) && null!=$data['article_filter']['premium']) {
            $premium = ($data['article_filter']['premium'] ==1)?true:false;
            $articleForm->setPremium($premium);
        }

        /*

        $articleForm->setHomePage($data->getHomePage());
        $articleForm->setRevuePage($data->getRevuePage());
        $articleForm->setDateCreation($data->getDateCreation());
        $articleForm->setDatePublication($data->getDatePublication());*/

        return $articleForm;
    }

    /**
     * @param array $article
     * @return array
     */
    public function formatArticleData(array $article)
    {
        $articleNew = [];

        if ($article) {
            foreach ($article as $key => $unArticle) {
                $articleNew[$key]['premium'] = $unArticle->getPremium();
                $articleNew[$key]['auteur'] = $unArticle->getAuteur();
                $articleNew[$key]['type'] = $unArticle->getType();
                $articleNew[$key]['categorie'] = $unArticle->getCategorie();
                $articleNew[$key]['titre'] = $unArticle->getTitre();
                $articleNew[$key]['resume'] = $unArticle->getResume();
                $articleNew[$key]['image'] = $unArticle->getImage();
                $articleNew[$key]['videoArticle'] = $unArticle->getVideoArticle();
                $articleNew[$key]['thumbnails'] = $unArticle->getThumbnails();
            }
        }

        return $articleNew;
    }

    /**
     * @param array $articleUne
     * @return array
     */
    public function formatArticleUneData (array $articleUne)
    {
        $articleUneNew = [];

        if ($articleUne) {
            foreach ($articleUne as $key => $unArticleUne) {
                $articleUneNew[$key]['article'] = $unArticleUne->getArticle();
            }
        }

        return $articleUneNew;
    }

    public function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false) {
        if (is_array($ending)) {
            extract($ending);
        }
        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0 , $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }

        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</'.$tag.'>';
            }
        }

        return $truncate;
    }
}
