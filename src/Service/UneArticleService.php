<?php
namespace App\Service;

use App\Entity\Tags;
use App\Entity\UneArticle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commentaire;
use Doctrine\ORM\Mapping\Entity;

class UneArticleService
{
    /**
     * EntityManagerInterface
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ArticleService constructor.
     *
     * @param EntityManagerInterface $em em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repoUne = $em->getRepository(UneArticle::class);
    }

    /**
     * Add tag article
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function checkDefaultUnesExistence()
    {
        $unes = $this->repoUne->findAll();
        $requiredPositions = [1, 2, 3, 4, 5];

        foreach ($unes as $une) {
            if (($key = array_search($une->getPosition(), $requiredPositions)) !== false) {
                unset($requiredPositions[$key]);
            }
        }

        foreach ($requiredPositions as $position) {
            $une = new UneArticle();
            $une->setPosition($position);
            $this->em->persist($une);
        }

        $this->em->flush();
    }

    /**
     * Retourne toutes les unes
     *
     * @return ArrayCollection
     */
    public function getAll()
    {
        return $this->repoUne->getAll();
    }
}
