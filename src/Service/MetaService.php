<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 10/16/18
 * Time: 10:25 AM
 */

namespace App\Service;


class MetaService
{

    public function generateMeta($routeName, $title =null, $description= null)
    {
        $metaCode = $this->openFileText(__DIR__.'/../../../web/meta/meta.txt');

        $meta = array("title"=>"La profession comptable","description"=>"La profession comptable");
        if ($metaCode) {
            foreach ($metaCode as $key=>$data) {
                if ($key == "title_".$routeName){
                    if (null != $title){
                        $data = str_replace("#title#",$title,$data);
                    }
                    $meta['title'] = $data;
                }

                if ($key == "description_".$routeName){
                    if (null != $description){
                        $data = str_replace("#description#",$description,$data);
                    }
                    if (null != $title){
                        $data = str_replace("#title#",$title,$data);
                    }
                    $meta['description'] = $data;
                }
            }
        }

        return $meta;

    }

    public function openFile($fichier, $utf8=true)
    {
        if(is_file($fichier))
        {
            return file_get_contents($fichier);
            //if(CHARSET=='utf-8' && $utf8===false) debug(CHARSET);
            //return (CHARSET=='utf-8' && $utf8===true)?utf8_encode(@file_get_contents($fichier)):@file_get_contents($fichier);
        } else return false;
    }

    public function makeArray( $txt, $type = "ASSO")
    {
        // retourne un tableau du type { ["nom_tableau"]=> array(4) { ["nom_clef1"]=> string(xx) "valeur" ...
        // ï¿½ partir de nom_clef1->valeur|nom_clef2->valeur|nom_clef3->valeur...
        $tableau = array();
        $tmp_tableau = explode( "|", $txt);
        foreach( $tmp_tableau as $element)
        {
            list($clef, $valeur) = explode( "->", $element);
            //$tableau[$clef] = (CHARSET=='utf-8')?utf8_encode($valeur):$valeur;
            $tableau[$clef] = $valeur;
        }
        return $tableau;
    }

    /**
     * Permet de convertir les fichiers *.txt en tableau multidimentionnel
     *
     * @param string $fichier
     * @param bool $utf8
     * @return array
     */
    public function openFileText($fichier, $utf8=true){
        // ouvre le fichier texte qui contient tous les textes et les variables.
        $texte 	= $this->openFile( $fichier);
        if(!$texte) return false;
        $tab_var 	= explode( "\n--\n", $texte);
        foreach($tab_var as $value){
            if(@preg_match("#^//#", $value)) continue;
            //voir si on peut remplacer les 2 lignes suivante par une expression rï¿½guliï¿½re
            list($nomvar) 	= explode("=", $value);
            $contenuvar 	= substr(strstr($value, '='),1);
            //$tab_final[$nomvar] = ( ereg( "^tab_", $nomvar))?$this->makeArray($contenuvar):((CHARSET=='utf-8' && $utf8===true)?utf8_encode($contenuvar):$contenuvar);
            $tab_final[$nomvar] = ( @preg_match("#^tab_#", $nomvar))?$this->makeArray($contenuvar):$contenuvar;
        }
        return $tab_final;
    }
}