<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 11/9/18
 * Time: 3:46 PM
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;


class SiteMapService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * SiteMapService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function encodeUrl($chaine)
    {
        $chaine = str_replace('/','-',$this->superencode($chaine));
        $chaine = str_replace('---','-',$chaine);
        $chaine=str_replace("   ", " ", $chaine);
        $chaine=str_replace("  ", " ", $chaine);
        $chaine=str_replace(" - ", "-", $chaine);
        $chaine=str_replace(" ?", "", $chaine);
        $chaine=str_replace(", ", "-", $chaine);
        $chaine=str_replace(" , ", "-", $chaine);
        $chaine=str_replace(" ,", "-", $chaine);
        $chaine=str_replace(",", "-", $chaine);
        $chaine=str_replace("«", "-", $chaine);
        $chaine=str_replace("»", "-", $chaine);
        $chaine=str_replace(":", "-", $chaine);
        $chaine=str_replace(" ", "-", $chaine);
        $chaine=str_replace("' ", "-", $chaine);
        $chaine=str_replace(" ' ", "-", $chaine);
        $chaine=str_replace(" '", "-", $chaine);
        $chaine=str_replace("'", "-", $chaine);
        $chaine=str_replace("\" ", "-", $chaine);
        $chaine=str_replace(" \" ", "-", $chaine);
        $chaine=str_replace(" \"", "-", $chaine);
        $chaine=str_replace("\"", "-", $chaine);
        $chaine=str_replace("/", "-", $chaine);
        $chaine=str_replace(" /", "-", $chaine);
        //$chaine=str_replace("*", "", $chaine);
        $chaine=str_replace("’", "-", $chaine);
        $chaine=str_replace("&", "-", $chaine);
        $chaine=str_replace("%", "-", $chaine);
        $chaine=str_replace("!", "", $chaine);
        $chaine=str_replace("----", "-", $chaine);
        $chaine=str_replace("---", "-", $chaine);
        $chaine=str_replace("--", "-", $chaine);
        $chaine=str_replace("°", "-", $chaine);
        $chaine=str_replace("#328;", "n", $chaine);
        $chaine=str_replace("#337;", "o", $chaine);
        $chaine=str_replace("#268;", "c", $chaine);
        $chaine=str_replace("’", "-", $chaine);
        $chaine=str_replace("?", "-", $chaine);

        $chaine= rtrim($chaine, '-');

        return  $chaine;
    }
    public function superencode($chaine)
    {
        return  str_replace(" ", "-", strtolower($this->suppr_accents($chaine)));
    }
    private function suppr_accents($str, $encoding='utf-8')
    {
        // transformer les caractères accentués en entités HTML
        $str = htmlentities($str, ENT_NOQUOTES, $encoding);

        // remplacer les entités HTML pour avoir juste le premier caractères non accentués
        // Exemple : "&ecute;" => "e", "&Ecute;" => "E", "à" => "a" ...
        $str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);

        // Remplacer les ligatures tel que : , Æ ...
        // Exemple "œ" => "oe"
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        // Supprimer tout le reste
        $str = preg_replace('#&[^;]+;#', '', $str);

        return $str;
    }

    public function genererURL($container)
    {

        $urls = array();
        //accueil
        $url_accueil="https://".$_SERVER['SERVER_NAME'];
        $urls[] = $url_accueil;

        //liste article
        $articles = $this->em->getRepository("App:Article")->findBy(array("status"=>"publie"));
        $tabCategorie = array();
        $tags = array();
        if ($articles) {
            foreach ($articles as $article) {

                if (method_exists($article, "getSlug") && null != $article->getCategorie()) {
                    $url = $container->get('router')->generate(
                        'show_article',
                        array('slug' => $article->getSlug(), "categorie"=>$article->getCategorie()->getSlug())
                    );

                    $urls[] = $url_accueil.$url;
                }


                $categorie = $article->getCategorie();

                if(!in_array($categorie, $tabCategorie)) {
                    $tabCategorie[] = $categorie;
                }


            }
        }
        //categorie
        if ($tabCategorie) {
            foreach ($tabCategorie as $categorie) {
                if (method_exists($categorie, "getSlug") && null != $categorie->getSlug()) {
                    $url = $container->get('router')->generate(
                        'show_article_categorie',
                        array('slug' => $categorie->getSlug())
                    );

                    $urls[] = $url_accueil.$url;
                }

            }
        }

        //tag
        $tags = $this->em->getRepository("App:Tags")->findBy(array("home"=>1));
        if ($tags) {
            foreach ($tags as $tag) {
                $url = $container->get('router')->generate(
                    'show_article_tag',
                    array('slug' => $tag->getSlug())
                );

                $urls[] = $url_accueil.$url;
            }
        }
        //boutique
        $urls[] = $url_accueil."/boutique";

        //Pages
        $pages = $this->em->getRepository("App:Page")->findAll();
        if ($pages) {
            foreach ($pages as $page) {
                $url = $container->get('router')->generate(
                    'page_information',
                    array('slug' => $page->getSlug())
                );

                $urls[] = $url_accueil.$url;
            }
        }


        return $urls;
    }
}