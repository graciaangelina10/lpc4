<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 8/22/18
 * Time: 10:31 AM.
 */

namespace App\Service;

use App\Entity\PositionArticleRevue;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class RevueService.
 */
class RevueService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * RevueService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $revue
     * @return array
     */
    public function getAnneRevue($revue, $anneeActive)
    {
        $tabDateAnnee = array();
        $anneePrevious = "";
        $anneeNext = "";
        if ($revue) {
            foreach ($revue as $unRevue){
                $dateRevue = $unRevue->getDateCreation();
                $explodeDate = explode("-", $dateRevue->format("Y-m-d"));
                $anneeRevue = $explodeDate[0];

                if(!in_array($anneeRevue, $tabDateAnnee)) {
                    $tabDateAnnee[] = $anneeRevue;
                }
            }
            asort($tabDateAnnee);
        }

        $dateRevue = $tabDateAnnee;
        //check annee avant le plus proche dans le tableau $dateRevue
        $tab_final_previous= array();
        if ($dateRevue) {
            foreach ($dateRevue as $unDate) {
                //si le dernier element du tab_final previous est inferieur a l'ecart, on crash le tab final et on injecte la nouvelle valeur
                if($unDate<$anneeActive)
                {
                    $tab_final_previous=array();
                    $tab_final_previous[]= $unDate;
                }
            }
        }
        if ($tab_final_previous) {
            $anneePrevious = $tab_final_previous[0];
        }
        //check annee après le plus proche dans le tableau $dateRevue
        $tab_final_next = array();
        if ($dateRevue) {
            foreach ($dateRevue as $unDate) {
                //on voit si le tableau est vide
                $count=count($tab_final_next);
                //s'il est vide, on injecte la 1ere valeur qui passe
                if($count==0 && $unDate>$anneeActive)
                {
                    $tab_final_next[]= $unDate;
                }
                //si le dernier element du tab_final previous est inferieur a l'ecart, on crash le tab final et on injecte la nouvelle valeur
                if(isset($tab_final_next[0]) && $tab_final_next[0]>$unDate)
                {
                    $tab_final_next=array();
                    $tab_final_next[]= $unDate;
                }
            }
        }

        if ($tab_final_next) {
            $anneeNext = $tab_final_next[0];
        }

        $tabListDateRevue = array(
            "tabAnneeRevue"=> $tabDateAnnee,
            "anneePrevious"=> $anneePrevious,
            "anneeNext"=> $anneeNext)
        ;

        return $tabListDateRevue;
    }

    public function getRevueDate($revues, $anneeActive)
    {
        $listeRevue = array();
        if ($revues) {
            foreach ($revues as $unRevue){
                $dateRevue = $unRevue->getDateCreation();
                $explodeDate = explode("-", $dateRevue->format("Y-m-d"));
                $anneeRevue = $explodeDate[0];

                if($anneeRevue == $anneeActive) {
                    $listeRevue[] = $unRevue;
                }
            }
        }
        return $listeRevue;
    }

    public function addPositionArticle($data, $article)
    {
        $revues = $article->getRevue();
        $positionArticleRevue = $this->em->getRepository(PositionArticleRevue::class)->findBy(array("article"=>$article));
        $tabRevue = array();
        $tabRevueNew = array();
        if ($positionArticleRevue) {
            foreach ($positionArticleRevue as $articleRevue){
                $tabRevue[] = $articleRevue->getRevue();
            }
        }

        if (isset($data['App_article']['revue']) && $article->getRevuePage() == 1) {

            foreach ($revues as $revue){
                $positionArticleRevue = $this->em->getRepository("App:PositionArticleRevue")->findBy(array("article"=>$article,"revue"=>$revue));
                if (!$positionArticleRevue) {
                    $position = $this->em->getRepository("App:PositionArticleRevue")->getMaxPosition($revue);
                    if ($position && !empty($position[0]['position'])) {
                        $position = $position[0]['position'];
                        $position ++;

                    } else {
                        $position = 1;
                    }
                    $positionArticleRevue = new PositionArticleRevue();
                    $positionArticleRevue->setRevue($revue);
                    $positionArticleRevue->setArticle($article);
                    $positionArticleRevue->setPosition($position);

                    $this->em->persist($positionArticleRevue);
                }
                $tabRevueNew[] = $revue->getId();
            }
            //Check tab revue old with tab revue new
            if ($tabRevue) {
                foreach ($tabRevue as $revue){

                    if (!in_array($revue->getId(), $tabRevueNew)) {
                        $positionArticleRevue = $this->em->getRepository("App:PositionArticleRevue")->findBy(array("article"=>$article, "revue"=>$revue));
                        if ($positionArticleRevue) {
                            $this->majPositionArticle('down', ($positionArticleRevue[0]->getPosition() +1), $revue);
                            $this->em->remove($positionArticleRevue[0]);
                        }

                    }
                }
            }

        }else {

            if ($positionArticleRevue) {
                foreach ($positionArticleRevue as $un_positionArticle) {
                    //maj position actuel
                    $this->majPositionArticle('down', ($un_positionArticle->getPosition() +1), $un_positionArticle->getRevue());
                    $this->em->remove($un_positionArticle);
                }
            }
        }

        return $revues;
    }

    public function majPositionArticle($direction, $position, $revue)
    {
        switch ($direction) {
            case 'up':
                $oldPosition = $position +1;
                break;

            case 'down':
                $oldPosition = $position - 1;
                break;
        }
        $positionArticle = $this->em->getRepository("App:PositionArticleRevue")->findBy(array("position"=>$position, "revue"=>$revue));

        if ($positionArticle) {
            $positionArticle[0]->setPosition($oldPosition);
            $this->em->persist($positionArticle[0]);
        }

        return $positionArticle;

    }
}
