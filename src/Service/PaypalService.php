<?php

namespace App\Service;

use App\Entity\Achat;
use Doctrine\Common\Persistence\ObjectManager;
use App\Manager\BaseManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Services\Mailer;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\Agreement;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Address;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;

use App\Entity\Produit;
use App\Entity\Paiement;

class PaypalService
{
    protected $panierService;
    protected $router;
    protected $clientId;
    protected $clientSecret;
    protected $debugLevel;
    protected $logPath;
    protected $paypalEnvironnement;
    protected $entityManager;
    protected $repoPlan;
    private $container;

    public function __construct(
        PanierService $panierService,
        RouterInterface $router,
        EntityManagerInterface $entityManager,
        $clientId,
        $clientSecret,
        $debugLevel,
        $logPath,
        $paypalEnvironnement,
    ContainerInterface $container
    ) {
        $this->panierService = $panierService;
        $this->router = $router;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->debugLevel = $debugLevel;
        $this->logPath = $logPath;
        $this->paypalEnvironnement = $paypalEnvironnement;
        $this->em = $entityManager;
        $this->repoPlan = $this->em->getRepository(\App\Entity\Plan::class);
        $this->container = $container;
    }

    public function createPlan($planName, $price)
    {
        $plan = new Plan();
        $plan->setName($planName);
        $plan->setType('fixed');
        $plan->setDescription($planName);

        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition
            ->setName('Paiement-annuel')
            ->setType('REGULAR')
            ->setFrequency('YEAR')
            ->setFrequencyInterval('1')
            ->setCycles('20')
            ->setAmount(
                new Currency(
                    [
                        'value' => $price,
                        'currency' => 'EUR',
                    ]
                )
            )
            ;

        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences
            ->setReturnUrl("https://www.laprofessioncomptable.com".$this->router->generate('payment_abonnement_execute', []))
            ->setCancelUrl("https://www.laprofessioncomptable.com".$this->router->generate('payment_abonnement_cancel', []))
            ->setAutoBillAmount('yes')
            ->setInitialFailAmountAction('CONTINUE')
            ->setMaxFailAttempts('3')
            ->setSetupFee(new Currency(['value' => 0, 'currency' => 'EUR']))
        ;

        $plan->addPaymentDefinition($paymentDefinition);
        $plan->setMerchantPreferences($merchantPreferences);
        $apiContext = $this->getApiContext();
        $result = $plan->create($apiContext);

        $patch = new Patch();
        $value = new PayPalModel('{"state":"ACTIVE"}');

        $patch->setOp('replace')
              ->setPath('/')
              ->setValue($value);

        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);

        $createdPlan = Plan::get($result->getId(), $apiContext);
        $createdPlan->update($patchRequest, $apiContext);

        return $result;
    }

    public function createPaymentForCurrentPanier($user)
    {
        $panier = $this->panierService->getPanierForUser($user);

        $panierProduit = $this->em->getRepository("App:PanierProduit")->findBy(array("panier"=>$panier));

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $items = [];

        //foreach ($panier->getProduitList() as $produit) {
        $priceTotale = 0;
        foreach ($panierProduit as $produit) {

            $item = new Item();
            $item->setName($produit->getProduit()->getNom())
                 ->setCurrency('EUR')
                 ->setQuantity($produit->getQtt())
                 ->setSku($produit->getProduit()->getId())
                 ->setPrice($produit->getProduit()->getNetPrice());
            $priceTotale = $priceTotale + ($produit->getProduit()->getNetPrice()*$produit->getQtt());
            $items[] = $item;
        }

        $itemList = new ItemList();
        $itemList->setItems($items);

        $details = new Details();
        $details->setTax($panier->getTvaTotal());
        $details->setSubtotal($priceTotale);

        $priceSetTotale = $priceTotale + $panier->getTvaTotal();

        if($priceSetTotale == $panier->getPrixTotal()) {

        }else {
            $panier->setPrixTotal(round($priceSetTotale,2));
        }

        $amount = new Amount();
        $amount->setCurrency('EUR')
               ->setTotal(round($panier->getPrixTotal(),2))
               ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription('Commande LPC')
                    ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($this->router->generate('payment_execute', [], UrlGeneratorInterface::ABSOLUTE_URL))
                     ->setCancelUrl($this->router->generate('payment_failed', [], UrlGeneratorInterface::ABSOLUTE_URL));

        $payment = new Payment();
        $payment->setIntent('sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions([$transaction]);

        $request = clone $payment;



        $apiContext = $this->getApiContext();

        $payment->create($apiContext);

        return $payment;
    }

    function getApiContext()
    {
        // #### SDK configuration
        // Register the sdk_config.ini file in current directory
        // as the configuration source.
    /*
    if(!defined("PP_CONFIG_PATH")) {
        define("PP_CONFIG_PATH", __DIR__);
    }
     */
        // ### Api context
        // Use an ApiContext object to authenticate
        // API calls. The clientId and clientSecret for the
        // OAuthTokenCredential class can be retrieved from
        // developer.paypal.com
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->clientId,
                $this->clientSecret
            )
        );

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration
        $apiContext->setConfig(
            [
                'mode' => $this->paypalEnvironnement,
                'log.LogEnabled' => true,
                'log.FileName' => $this->logPath,
                'log.LogLevel' => $this->debugLevel, // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => true,
                //'cache.FileName' => '/PaypalCache' // for determining paypal cache directory
                // 'http.CURLOPT_CONNECTTIMEOUT' => 30
                // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
                //'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
            ]
        );
        // Partner Attribution Id
        // Use this header if you are a PayPal partner. Specify a unique BN Code to receive revenue attribution.
        // To learn more or to request a BN Code, contact your Partner Manager or visit the PayPal Partner Portal
        // $apiContext->addRequestHeader('PayPal-Partner-Attribution-Id', '123123123');
        return $apiContext;
    }

    public function executePayment($paymentId, $payerId)
    {
        $apiContext = $this->getApiContext();
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        $result = $payment->execute($execution, $apiContext);
        $payment = Payment::get($paymentId, $apiContext);
    }

    public function createAgreement($user)
    {
        $localPlan = $this->repoPlan->findOneByName('abonnement-LPC');
        $agreement = new Agreement();
        $now = new \DateTime();
        $formattedNow = $now->format('Y-m-d\TH:i:s\Z');

        $agreement->setName('Abonnement')
                  ->setDescription('Abonnement')
                  ->setStartDate($formattedNow);

        $plan = new Plan();
        $plan->setId($localPlan->getPaypalPlanId());
        $agreement->setPlan($plan);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $shippingAddress = new ShippingAddress();

        if ($user->getAdresseLivraison1()) {
            $shippingAddress
                ->setLine1($user->getAdresseLivraison1())
                ->setLine2($user->getAdresseLivraison2())
                ->setCity($user->getVilleLivraison())
                ->setPostalCode($user->getCodePostalLivraison())
                ->setCountryCode('FR');
        } else {
            $shippingAddress
                ->setLine1($user->getAdresseFacturation1())
                ->setLine2($user->getAdresseFacturation2())
                ->setCity($user->getVille())
                ->setPostalCode($user->getCodePostal())
                ->setCountryCode('FR');
        }

        $agreement->setShippingAddress($shippingAddress);
        $agreement->setPayer($payer);

        $agreement = $agreement->create($this->getApiContext());

        return $agreement;
    }

    public function executeAgreement($token)
    {
        $agreement = new Agreement();
        $result = $agreement->execute($token, $this->getApiContext());
        return $result;
    }

    public function setNotification($resultEvents, $mailer)
    {
        if ($resultEvents) {
            foreach ($resultEvents['events'] as $event) {

                //Paiement produit
                if (isset($event['resource']['parent_payment'])) {
                    $payment_id = $event['resource']['parent_payment'];
                    $event_type = $event['event_type'];
                    $summaryEvent = $event['summary'];
                    $total_amount = $event['resource']['amount']['total'];
                    $datePaiement = new \DateTime($event['create_time']);
                    $state = $event['resource']['state'];

                    //get paiement

                    $paiement = $this->em->getRepository("App:Paiement")->findBy(array("paymentId"=>$payment_id));

                    if (!$paiement) {
                        continue;
                    } else{
                        if ($paiement[0]->getMailConfirmePaiement()!= 1) {
                            $paiement = $paiement[0];
                        }else {
                            continue;
                        }

                    }


                    switch ($event_type)
                    {
                        case "PAYMENT.SALE.COMPLETED":
                            // mise à jour base de donnée + envoie email client
                            $paiement->setState("completed");
                            $paiement->setDatePaiement($datePaiement);
                            $paiement->setSummary($summaryEvent);
                            $paiement->setMailConfirmePaiement(1);

                            $this->em->persist($paiement);

                            $this->sendMail($paiement->getUser(), "La profession comptable : Confirmation paiement", $mailer, $paiement);

                            break;
                        case "PAYMENT.SALE.DENIED":
                            // mise à jour base de donnée + envoie email client
                            $paiement->setState("denied");
                            $paiement->setDatePaiement($datePaiement);
                            $paiement->setSummary($summaryEvent);
                            $paiement->setMailConfirmePaiement(1);

                            $this->em->persist($paiement);

                            $this->sendMail($paiement->getUser(), "La profession comptable : Réfus paiement", $mailer, $paiement);

                            break;
                        default:
                            break;
                    }
                }

                //Abonnement
                if (isset($event['resource']['billing_agreement_id'])) {

                }



            }

            $this->em->flush();
        }
    }

    public function sendMail($user, $type, $mailer, $paiement, $to =null)
    {
        if (null == $to) {
            $to = $user->getEmail();
        }

        if ($type == "Confirmation abonnement sur LPC") {
            $message = (new \Swift_Message($type))
                ->setFrom('abonnement@laprofessioncomptable.com')
                ->setTo($to)
                ->setBody(
                    $this->container->get('templating')->render(
                    // app/Resources/views/Emails/registration.html.twig
                        'emails/completed_abonnement.html.twig',
                        array('user' => $user, 'abonnement'=> $paiement)
                    ),
                    'text/html'
                )

            ;
        }else {
            $message = (new \Swift_Message($type))
                ->setFrom('contact@laprofessioncomptable.com')
                ->setTo($to)
                ->setBody(
                    $this->container->get('templating')->render(
                    // app/Resources/views/Emails/registration.html.twig
                        'emails/completed_paiement.html.twig',
                        array('user' => $user, 'paiement'=> $paiement, 'achats'=>$this->em->getRepository("App:Achat")->findByPaiement($paiement))
                    ),
                    'text/html'
                )

            ;
        }


        return $mailer->send($message);

    }

    public function sendMailViaCB($user, $type, $mailer, $paiement, $to =null, $status = null, $montantTotal = null)
    {
        if (null == $to) {
            $to = $user->getEmail();
        }





        switch ($status) {
            case "ACCEPTED":

                if ($type == "Confirmation abonnement sur LPC") {
                    $message = (new \Swift_Message($type))
                        ->setFrom('abonnement@laprofessioncomptable.com')
                        ->setTo($to)
                        ->setBody(
                            $this->container->get('templating')->render(
                            // app/Resources/views/Emails/registration.html.twig
                                'emails/completed_abonnementCB.html.twig',
                                array('user' => $user, 'abonnement'=> $paiement, 'statut' => $type)
                            ),
                            'text/html'
                        )

                    ;
                }elseif ($type == "Confirmation commande + abonnement sur LPC") {
                $message = (new \Swift_Message($type))
                    ->setFrom('abonnement@laprofessioncomptable.com')
                    ->setTo($to)
                    ->setBody(
                        $this->container->get('templating')->render(
                        // app/Resources/views/Emails/registration.html.twig
                            'emails/completed_commande_abonnementCB.html.twig',
                            array('user' => $user, 'statut' => $type, 'abonnement' => $this->em->getRepository(Produit::class)->findOneBy(['rubrique' => "abonnement-prime" ]),
                                'paiement'=> $paiement, 'achats'=>$this->em->getRepository(Achat::class)->findByPaiement($paiement))
                        ),
                        'text/html'
                    )

                ;
                }else {
                    $message = (new \Swift_Message($type))
                        ->setFrom('contact@laprofessioncomptable.com')
                        ->setTo($to)
                        ->setBody(
                            $this->container->get('templating')->render(
                            // app/Resources/views/Emails/registration.html.twig
                                'emails/completed_paiementCB.html.twig',
                                array('user' => $user, 'statut' => $type, 'paiement'=> $paiement, 'achats'=>$this->em->getRepository("App:Achat")->findByPaiement($paiement))
                            ),
                            'text/html'
                        )

                    ;
                }

                return $mailer->send($message);

                break;

            case "AUTHORISED":
                //send email

                if ($type == "Confirmation abonnement sur LPC") {
                    $message = (new \Swift_Message($type))
                        ->setFrom('abonnement@laprofessioncomptable.com')
                        ->setTo($to)
                        ->setBody(
                            $this->container->get('templating')->render(
                            // app/Resources/views/Emails/registration.html.twig
                                'emails/completed_abonnementCB.html.twig',
                                array('user' => $user, 'abonnement'=> $paiement, 'statut' => $type)
                            ),
                            'text/html'
                        )

                    ;
                }elseif ($type == "Confirmation commande + abonnement sur LPC") {
                    $message = (new \Swift_Message($type))
                        ->setFrom('abonnement@laprofessioncomptable.com')
                        ->setTo($to)
                        ->setBody(
                            $this->container->get('templating')->render(
                            // app/Resources/views/Emails/registration.html.twig
                                'emails/completed_commande_abonnementCB.html.twig',
                                array('user' => $user, 'statut' => $type, 'abonnement' => $this->em->getRepository(Produit::class)->findOneBy(['rubrique' => "abonnement-prime" ]),
                                    'paiement'=> $paiement, 'achats'=>$this->em->getRepository(Achat::class)->findByPaiement($paiement))
                            ),
                            'text/html'
                        )

                    ;
                }else {
                    $message = (new \Swift_Message($type))
                        ->setFrom('contact@laprofessioncomptable.com')
                        ->setTo($to)
                        ->setBody(
                            $this->container->get('templating')->render(
                            // app/Resources/views/Emails/registration.html.twig
                                'emails/completed_paiementCB.html.twig',
                                array('user' => $user, 'statut' => $type,
                                    'paiement'=> $paiement,
                                    'achats'=>$this->em->getRepository("App:Achat")->findByPaiement($paiement)
                                )
                            ),
                            'text/html'
                        )

                    ;
                }
                return $mailer->send($message);

                break;

            case "ABANDONED":

                break;

            case "AUTHORISED_TO_VALIDATE":

                break;

            case "CANCELLED":

                break;

            case "CAPTURED":

                break;

            case "CAPTURE_FAILED":

                break;

            case "EXPIRED":

                break;

            case "INITIAL":

                break;

            case "NOT_CREATED":

                break;

            case "REFUSED":

                $message = (new \Swift_Message("LPC: commande refusé"))
                    ->setFrom('contact@laprofessioncomptable.com')
                    ->setTo($to)
                    ->setBody(
                        $this->container->get('templating')->render(
                        // app/Resources/views/Emails/registration.html.twig
                            'emails/refused_paiementCB.html.twig',
                            array('user' => $user,
                                'statut' => $type,
                                'paiement'=> $paiement,
                                'montantTotal' => $montantTotal,
                                'achats'=>$this->em->getRepository("App:Achat")->findByPaiement($paiement))
                        ),
                        'text/html'
                    )

                ;

                return $mailer->send($message);

                break;

            case "SUSPENDED":

                $message = (new \Swift_Message("LPC: commande suspendu"))
                    ->setFrom('contact@laprofessioncomptable.com')
                    ->setTo($to)
                    ->setBody(
                        $this->container->get('templating')->render(
                        // app/Resources/views/Emails/registration.html.twig
                            'emails/suspended_paiementCB.html.twig',
                            array('user' => $user,
                                'statut' => $type,
                                'paiement'=> $paiement,
                                'montantTotal' => $montantTotal,
                                'achats'=>$this->em->getRepository("App:Achat")->findByPaiement($paiement))
                        ),
                        'text/html'
                    )

                ;

                return $mailer->send($message);

                break;

            case "WAITING_AUTHORISATION":

                break;

            case "WAITING_AUTHORISATION_TO_VALIDATE":

                break;
        }


        return $mailer;

    }

    public function majTarifPlan($data, $prixInitial)
    {
        if ($data['App_produit']['rubrique'] == "abonnement-prime") {
            $tarif = $data['App_produit']['prix'];
            if ($prixInitial != $tarif) {
                //Mettre à jour le prix abonnement chez Paypal
                $prixAbonnement = $tarif;
                $plan = $this->em->getRepository(\App\Entity\Plan::class)->findByName("abonnement-LPC");
                $namePlan = "abonnement-LPC";
                if (!$plan) {
                    $plan = new Plan();
                    $plan->setName("abonnement-LPC");
                    $plan->setPrix($prixAbonnement);
                    $this->em->persist($plan);

                }else {
                    $plan = $plan[0];
                }

                try {
                    $result = $this->createPlan($namePlan, $prixAbonnement);
                } catch (\Exception $e) {
                    throw new \Exception('Erreur à la création du plan paypal');

                }
                $plan->setPaypalPlanId($result->getId());
                $plan->setPrix($prixAbonnement);

                return $plan;
            }

        }
        return null;
    }
}
