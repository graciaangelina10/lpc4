<?php

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CabinetResponseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $civilite = [
            "Mr" => "Mr",
            "Mme" => "Mme",
            "Mlle" => "Mlle"
        ];

        $tabFormJuridique = [
            ' ' => 'Séléctionner.....',
            'Association de gestion et de comptabilité' => 'Association de gestion et de comptabilité',
            'Entreprise individuelle (EI)' => 'Entreprise individuelle (EI)',
            'Entreprise individuelle à responsabilité limitée (EIRL)' => 'Entreprise individuelle à responsabilité limitée (EIRL)',
            'Entreprise unipersonnelle à responsabilité limitée (EURL)' => 'Entreprise unipersonnelle à responsabilité limitée (EURL)',
            'Société à responsabilité limitée (SARL)' => 'Société à responsabilité limitée (SARL)',
            'Société anonyme (SA)' => 'Société anonyme (SA)',
            'Société par actions simplifiée (SAS)' => 'Société par actions simplifiée (SAS)',
            'Société par actions simplifiée unipersonnelle (SASU)' => 'Société par actions simplifiée unipersonnelle (SASU)',
            'Société en nom collectif (SNC)' => 'Société en nom collectif (SNC)',
            'Société coopérative de production (Scop)' => 'Société coopérative de production (Scop)',
            'Société en commandite par actions (SCA)' => 'Société en commandite par actions (SCA)',
            'Société en commandite simple (SCS)' => 'Société en commandite simple (SCS)',
        ];

        $builder

            ->add('nom', null, [
                'label' => 'Intitulé juridique du cabinet'
            ])
            ->add('formeJuridique', ChoiceType::class, [
                'choices' => $tabFormJuridique,
            ])
            ->add('siret')
            ->add('siren')
            ->add('adresse', null, [
                'label' => 'Adresse'
            ])
            ->add('codePostal', null, [
                'label' => 'Code postal'
            ])
            ->add('ville')
            ->add('departement')
            ->add('region')
            ->add('telephone')
            ->add('email', null, [
                'label' => 'email'
            ])

            ->add('site')
            ->remove('civilite', ChoiceType::class, [
                'choices' => $civilite,

            ])
            ->add('nomPresident')
            ->remove('prenomPresident')
            ->add('txtPluridiscplinaire', TextareaType::class,
                array(
                    'attr' => array('class' => 'ckeditor')
                )
            )
            /*->add('txtPluridiscplinaire', TextareaType::class, [
                'attr' => [
                    'rows' => 10,
                    'cols' => 100
                ]
            ])*/
            ->add('commentaire', TextareaType::class,
                array(
                    'attr' => array('class' => 'ckeditor')
                )
            )
            /*->add('commentaire', TextareaType::class, [
                'attr' => [
                    'rows' => 10,
                    'cols' => 100
                ]
            ])*/

            ->remove('cas')
            ->remove('caCs')

            ->remove('caExpertises')

            ->remove('tailleCabinets')
            ->remove('informationFiliales')
            ->remove('etude', null, [
                'required' => true,
            ])

            ->add('personnes', CollectionType::class, [
                'entry_type' => PersonneType::class,
                'label' => false,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])

            ->add('save', SubmitType::class, ['label' => 'Valider', 'attr' =>['class' => 'btn btn-primary']])
            ;

    }

    public function getParent()
    {
        return CabinetType::class;
    }

    public function getName()
    {
        return 'cabinet_questionnaire_type';
    }


}
