<?php
/**
 * Created by PhpStorm.
 * User: marcel
 * Date: 10/11/18
 * Time: 3:52 PM
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImportUserType extends AbstractType
{

    /**
     * Construction du form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file',FileType::class, array(
            'label'=>"Fichier (csv)",
            'required'=>true,
        ))
            ->add("save",SubmitType::class,[
                'label' => 'Enregistrer',
                'attr'=>[
                    'class'=>'btn btn-primary'
                ]
            ]);
    }

    /**
     * Configuration des options.
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class'=>'App\Model\ImportUser']);
    }

}