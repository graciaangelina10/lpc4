<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom', TextType::class, ['required' => false])
            ->add('nom', TextType::class, ['required' => true])
            ->add('adresseFacturation1', TextType::class, ['required' => true])
            ->add('adresseFacturation2', TextType::class, ['required' => false])
            ->add('codePostal', TextType::class, ['required' => true])
            ->add('ville', TextType::class, ['required' => true])
            ->add('prenomLivraison', TextType::class, ['required' => false])
            ->add('nomLivraison', TextType::class, ['required' => false])
            ->add('adresseLivraison1', TextType::class, ['required' => false])
            ->add('adresseLivraison2', TextType::class, ['required' => false])
            ->add('codePostalLivraison', TextType::class, ['required' => false])
            ->add('villeLivraison', TextType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([ 'data_class' => 'App\Entity\User']);
    }
}
