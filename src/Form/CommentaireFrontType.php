<?php
/**
 * CommentaireType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class CommentaireType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */
class CommentaireFrontType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('user')
            ->remove('parent')
            ->remove('article')
            ->add(
                'comment', TextareaType::class, [
                    'label' => false,
                    'attr' => [
                        "class" => "txt-area",
                        "id" => "txt-area",
                        "placeholder" => "Votre commentaire...",
                        "onfocus" => "focusTextArea()",
                        "onfocusout" => "focusoutTextArea()",
                    ]
                ]

            )
            ->remove('approuve');

    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return CommentaireType::class;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'comment_front';
    }
}
