<?php
/**
 * AbonnementType Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;

/**
 * Class AbonnementType
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
class AbonnementType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nom', null, [
            'required'=>true
            ]
        )
            ->add(
                'prenom', null, [
                'required'=>true
                ]
            )
            ->add('profession')
            ->add('societe')
            ->add(
                'adresse', null, [
                'required'=>true
                ]
            )
            ->add(
                'codePostal', null, [
                'required'=>true
                ]
            )
            ->add(
                'ville', null, [
                'required'=>true
                ]
            )
            ->add(
                'telephone', null, [
                'required'=>true
                ]
            )

            ->remove('username');
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'abonnement_registration';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}
