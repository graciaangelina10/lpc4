<?php
/**
 * TagsType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Form;

use App\Entity\Tags;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TagsType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */
class TagsType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
            ->add(
                'slug', null, [
                    'label' => 'Slug (Url)',
                    'required'=>false
                    ]
            )
            ->add(
                'home',
                null,
                [
                        'label' => 'is Home',
                        'required' => false,
                    ]
            );
    }

    /**
     * {@inheritdoc}
     *
     * @param OptionsResolver $resolver resolver
     *
     * @return ArrayCollection
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'App\Entity\Tags',
                'data' => new Tags(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'App_tags';
    }
}
