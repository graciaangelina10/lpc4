<?php
/**
 * CategorieType Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
namespace App\Form;

use App\Entity\Categorie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Yavin\Symfony\Form\Type\TreeType;

/**
 * Class CategorieType
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
class CategorieType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'parent',
            EntityType::class,

            [
                'class' => Categorie::class,
//                'levelPrefix' => ' --- ',
//                'orderFields' => ['treeLeft' => 'asc'],
//                'prefixAttributeName' => 'data-level-prefix',
//                'treeLevelField' => 'treeLevel',
                'required' => false,
                'label' => 'Catégorie parent ',
            ]
        )
            ->add('nom')
            ->add('slug', null, ["label" => "Url (slug)", "required" => false])
            ->add('home')
        ;
    }

    /**
    * {@inheritdoc}
    *
    * @param OptionsResolver $resolver resolver
    *
    * @return ArrayCollection
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => 'App\Entity\Categorie',
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return String
     */
    public function getBlockPrefix()
    {
        return 'App_categorie';
    }
}
