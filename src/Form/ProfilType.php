<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;

class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $profession = array(
            User::PROFESSION_COLLABORATEUR_CABINET => User::PROFESSION_COLLABORATEUR_CABINET,
            User::PROFESSION_EXPERT_COMPTABLE => User::PROFESSION_EXPERT_COMPTABLE,
            User::PROFESSION_STAGIAIRE => User::PROFESSION_STAGIAIRE,
            User::PROFESSION_AUTRE => User::PROFESSION_AUTRE
        );

        $builder
            ->add('prenom', TextType::class, ['required' => true])
            ->add('nom', TextType::class, ['required' => true])
            ->add('profession', ChoiceType::class,array(
                'choices' => $profession,
                'placeholder' => 'Profession *',
                'required' => true,

            ))
            ->add('societe', TextType::class, ['required' => false])
            ->add('telephone', TextType::class, ['required' => true])
            ->add('email', TextType::class, ['required' => true])

            ->add('adresseFacturation1', TextType::class, ['required' => true])
            ->add('adresseFacturation2', TextType::class, ['required' => false])
            ->add('codePostal', TextType::class, ['required' => true])
            ->add('ville', TextType::class, ['required' => true])

            ->add('prenomLivraison', TextType::class, ['required' => false])
            ->add('nomLivraison', TextType::class, ['required' => false])
            ->add('adresseLivraison1', TextType::class, ['required' => false])
            ->add('adresseLivraison2', TextType::class, ['required' => false])
            ->add('codePostalLivraison', TextType::class, ['required' => false])
            ->add('villeLivraison', TextType::class, ['required' => false])
            ->add('autreProfession', TextType::class, ['required' => false])
            ->add('newsletterSubscription')
            ->add('subscriptionRenewal')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([ 'data_class' => 'App\Entity\User']);
    }
}
