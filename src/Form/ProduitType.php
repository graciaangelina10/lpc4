<?php

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rubrique = [
            'Abonnement Prime' => "abonnement-prime",
            'Hors série' => "hors-serie",
            'Les essentiels de la profession comptable' => "profession-comptable",
            'Frais de transport' => "frais-transport",
            'PDF Gratuit' => 'pdf-gratuit',
        ];

        $builder
            ->add("rubrique",ChoiceType::class,array(
                'choices' => $rubrique,
                'label' => 'Choisir Rubrique',
                'placeholder' => 'Choisir Rubrique',
                'required' => true,
            ))
            ->add('nom',null,array(
                'label'=> 'Titre'
            ))
            ->add(
                'description', TextareaType::class,
                array(
                    'label' => 'Descriptif',
                    'attr' => array('class' => 'ckeditor'),
                    'required'=>true
                )
            )
            ->add('prix')
            ->add('auteur')
            ->add(
                'imageFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'label' => 'Image (260px X 270px)',
                )
            )
            ->add(
                'pdfFile', VichFileType::class, array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'label' => '',
                )
            )
            ->add("dateParution",DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Parution',
                ])
            ->add("disponible")
            ->add("linkArticle", null, [
                'label' => 'Lien article'
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\Produit'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_produit';
    }


}
