<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ResetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $profession = array("Commissaire aux comptes"=>"Commissaire aux comptes",
            "Collaborateur dans un cabinet EC / CAC (autre que EC / CAC)"=>"Collaborateur dans un cabinet EC / CAC (autre que EC / CAC)",
            "Expert-comptable"=>"Expert-comptable",
            "Stagiaire EC / CAC"=>"Stagiaire EC / CAC",
            "Autre"=>"Autre"
            );
        $builder->remove('nom',null, array(
            'required' => true,
        ))
            ->remove('prenom',null,array(
                'required' => true,
            ))
            ->remove('profession', ChoiceType::class,
                array(
                    'choices' => $profession,
                    'label' => 'Choisir Profession',
                    'placeholder' => 'Choisir Profession',
                    'required' => true,

                ))
            ->remove('email',EmailType::class,array(
                "required"=>true
            ))
            ->remove('societe')
            ->remove('codePostal',null,array(
                "required"=>true
            ))
            ->remove('ville',null,array(
                "required"=>true
            ))
            ->remove('telephone',null,array(
                "required"=>true
            ))
            ->remove('adresseFacturation1',null,array(
                "label"=>"Adresse",
                "required"=>true
            ))
            ->remove('adresseFacturation2',null,array(
                "label"=>"Adresse ligne 2"
            ))
            ->remove('adresseLivraison1')
            ->remove('adresseLivraison2')
            ->remove('nomLivraison')
            ->remove('prenomLivraison')
            ->remove('societeLivraison')
            ->remove('codePostalLivraison')
            ->remove('villeLivraison')
            ->remove('paidSubscriber',null,array(
                'label'=>'Abonné'
            ))
            ->remove('endSubscriptionDate',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date fin Abonnement',
                'required'=>false,
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'label'=>false,
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ));
            ;



    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_user';
    }


}
