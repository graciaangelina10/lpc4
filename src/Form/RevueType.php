<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RevueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numero')
            ->add('titre')
            ->add(
                'descriptif', TextareaType::class,
                array(
                    'attr' => array('class' => 'ckeditor'),
                    'label' => 'Descriptif',
                )
            )
            ->add(
                'imageFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'label' => 'Image (270px X 345px)',
                )
            )
            ->add(
                'pdfFile', VichFileType::class, array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'label' => '',
                )
            )
            ->add(
                'tarif', null, array(
                'required'=>false
                )
            )
            ->add(
                'dateCreation', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date création',
                ]
            )
            ->add(
                'isCheck', null, array(
                'label'=>'is Check (Mettre sur le bloc droite?)'
                )
            )
            ->add(
                'active', null, array(
                    'label'=>'Activé (Mettre en ligne)'
                )
            )
            ->add(
                'sommaire',
                CollectionType::class,
                array(
                    'entry_type'=>SommaireRevueType::class,
                    'entry_options' => array('label' => false),
                    'allow_add' => true,
                    'prototype' => true,
                    'allow_delete' => true,
                    'attr' => array(
                        'class' => 'my-selector',
                    ),
                    'by_reference' => false,
                    'label' => 'Sommaire ',
                )
            )
            ->add(
                'slug', null, [
                    'label' => 'Slug (Url)',
                    'required'=>false
                ]
            )
            ->add("linkCalameo")
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\Revue'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_revue';
    }


}
