<?php

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtudeNationalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tabAnnee = [];

        $anneeEnCours = date("Y") + 1;

        for ($i=1;$i<=10;$i++) {
            $tabAnnee [$anneeEnCours] = $anneeEnCours;
            $anneeEnCours = $anneeEnCours -1;
        }

        $builder
            ->add('annee', ChoiceType::class, [
                'choices' => $tabAnnee,
                'label' => 'Année'
            ])
            ->add('titre')
            ->add('descriptif', TextareaType::class,
                array(
                    'label' => 'Introduction',
                    'attr' => array('class' => 'ckeditor'),
            ))

            ->add('titre1', null, [
                'label' => 'titre (partie I)'
            ])

            ->add('descriptif1', TextareaType::class,
                array(
                    'label' => 'Descriptif (partie I)',
                    'attr' => array('class' => 'ckeditor'),
            ))

            ->add('titre2', null, [
                'label' => 'titre (partie 2)'
            ])

            ->add('descriptif2', TextareaType::class,
                array(
                    'label' => 'Descriptif (partie II)',
                    'attr' => array('class' => 'ckeditor'),
            ))

            ->add('titre3', null, [
                'label' => 'titre (partie 3)'
            ])

            ->add('descriptif3', TextareaType::class,
                array(
                    'label' => 'Descriptif (partie II)',
                    'attr' => array('class' => 'ckeditor'),
            ))


            ->add('dateCloture', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de clôture',
            ])
            ->add('isActive')
            ->add('isReseau')
            ->add('save', SubmitType::class, ['label' => 'Valider'])
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\EtudeNational'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_etudenational';
    }


}
