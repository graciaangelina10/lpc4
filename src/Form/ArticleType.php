<?php

/**
 * ArticleType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Revue;
use App\Entity\Tags;
use App\Entity\VideoArticle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Article;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Yavin\Symfony\Form\Type\TreeType;

/**
 * Class ArticleType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */
class ArticleType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $validator = $options['validation_groups'];

        if ($validator) {
            $status = [
                    'En cours' => Article::STATUS_EN_COURS,
                    'A valider' => Article::STATUS_A_VALIDER,
                    'Validé' => Article::STATUS_VALIDE,
                    'Publié' => Article::STATUS_PUBLIE,
                ];
        } else {
            $status = array(
                'En cours' => Article::STATUS_EN_COURS,
                'A valider' => Article::STATUS_A_VALIDER,
            );
        }

        $typeArticle = [
            Article::TYPE_ARTICLE => Article::TYPE_ARTICLE,
            Article::TYPE_EVENEMENT => Article::TYPE_EVENEMENT,
            Article::TYPE_PODCAST => Article::TYPE_PODCAST,
            Article::TYPE_VIDEO => Article::TYPE_VIDEO,
            Article::TYPE_EDITO => Article::TYPE_EDITO,
            Article::TYPE_CALAMEO => Article::TYPE_CALAMEO

        ];

        $builder->add(
            'categorie',
            EntityType::class,
            array(
                'multiple' => false,
                'required' => false,
                'placeholder' => 'Choisir catégorie',
                'class' => Categorie::class, // tree class
//                'levelPrefix' => ' --- ',
//                'orderFields' => ['treeLeft' => 'asc'],
//                'prefixAttributeName' => 'data-level-prefix',
//                'treeLevelField' => 'treeLevel',
            )
        )
            ->add('titre',
                TextareaType::class,
                    array(
                        'attr' => array('class' => 'ckeditor')
                    )
            )
            ->add(
                'slug', null, [
                'label' => 'Slug (Url)',
                'required'=>false
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                array(
                'choices' => $typeArticle,
                'label' => 'Choisir Type',
                'placeholder' => 'Choisir Type',
                'required' => true,

                )
            )
            ->add(
                'resume',
                TextareaType::class,
                array(
                'label' => 'Introduction',
                'attr' => array('class' => 'ckeditor')
                )
            )
            ->add(
                'contenu',
                TextareaType::class,
                    array(
                        'attr' => array('class' => 'ckeditor')
                    )
            )
            ->add(
                'imageFile',
                VichImageType::class,
                array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'label' => 'Image (650px X 440px)',
                )
            )
            ->add(
                'alltags',
                EntityType::class,
                array(
                'class' => Tags::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.position', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
                'label' => 'Tags',
                'required' => false,
                )
            )
            ->add(
                'tagnew',
                CollectionType::class,
                array(
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
                'label' => 'Nouveau Tag ',
                )
            );

        $builder
            ->add('premium')
            ->add('auteur')
            ->add(
                'dateCreation',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date création',
                ]
            )
            ->add(
                'datePublication',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date publication',
                'required' => false,
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                array(
                'choices' => $status,
                'label' => 'Statut',
                'required' => true,
                )
            )
            ->add(
                'dateDebutEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Début événement',
                    'required'=>false,
                ]
            )
            ->add(
                'dateFinEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Fin événement',
                    'required'=>false,
                ]
            )
            ->add(
                'lieuEvent', null, [
                'label'=> 'Lieu événement',
                'required'=>false,
                ]
            )
            ->add(
                'videoArticle', VideoArticleType::class, array(
                'label'=> false

                )
            )
            ->add(
                'revue',
                EntityType::class,
                array(
                    'class' => Revue::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('r')
                            ->orderBy('r.dateCreation', 'DESC');
                    },
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Revue',
                    'required' => false,
                    'by_reference' => false,
                )
            )
            ->add("homePage")
            ->add("revuePage")
            ->add(
                'podcastFile', VichFileType::class, array(
                    'required' => false,
                    'allow_delete' => true,
                    'download_uri' => true,
                    'label' => 'Podcast (audio)',
                )
            )
            ->add("linkCalameo")
            ->add('timePublication', TimeType::class, [
                'input'  => 'datetime',
                'label'=> 'Heure Publication',

            ])

        ;
    }

    /**
     * {@inheritdoc}
     *
     * @param OptionsResolver $resolver resolver
     *
     * @return ArrayCollection
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'App\Entity\Article',
            )
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'App_article';
    }

    /**
     * Contructor
     *
     * @param  SessionInterface $session service session
     * @return null
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
}
