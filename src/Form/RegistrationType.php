<?php
/**
 * RegistrationType Class
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use App\Entity\User;

/**
 * Class RegistrationType
 *
 * @category Class
 *
 * @package App\Form
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @link ****
 */
class RegistrationType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $permissions = array(
            'Utilisateur'        => 'ROLE_USER',
            'Administrateur'     => 'ROLE_ADMIN',
            'Editeur'     => 'ROLE_EDITOR',
            'Abonne'    =>'ROLE_ABONNE',
            'Contributeur' => 'ROLE_CONTRIBUTEUR'

        );

        $profession = array(
            User::PROFESSION_COLLABORATEUR_CABINET => User::PROFESSION_COLLABORATEUR_CABINET,
            User::PROFESSION_EXPERT_COMPTABLE => User::PROFESSION_EXPERT_COMPTABLE,
            User::PROFESSION_STAGIAIRE => User::PROFESSION_STAGIAIRE,
            User::PROFESSION_AUTRE => User::PROFESSION_AUTRE
        );

        $builder->add(
            'nom', null, [
            'required'=>true
            ]
        )
            ->add(
                'prenom', null, [
                'required'=>true
                ]
            )
            ->add('profession')
            ->add('societe')
            ->add(
                'adresseFacturation1', null, [
                'required'=>true,
                'label' => 'Adresse (ligne 1)',
                ]
            )
            ->add(
                'adresseFacturation2', null, [
                'required'=>false,
                'label' => 'Adresse (ligne 2)',
                ]
            )
            ->add(
                'codePostal', null, [
                'required'=>true
                ]
            )
            ->add(
                'ville', null, [
                'required'=>true
                ]
            )
            ->add(
                'telephone', null, [
                'required'=>true
                ]
            )
        ->add(
            'roles',
            ChoiceType::class,
            [
                'label'   => 'Rôle',
                'choices' => $permissions,
                'multiple' => true,
                'expanded' => true,
            ]
        )
            ->add('profession', ChoiceType::class, array(
                'choices' => $profession,

                'placeholder' => 'Profession *',
                'required' => true,

            ))
            ->remove('username')
        ;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'bba_user_registration';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}
