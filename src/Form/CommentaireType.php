<?php
/**
 * CommentaireType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author  Marcel <mrazafimandimby@bocasay.com>
<<<<<<< HEAD
 * @license http://www.gnu.org/copyleft/gpl.html General Public
=======
 *
 * @license
>>>>>>> feat.categorieUrl.article
 *
 * @see ****
 */

namespace App\Form;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class CommentaireType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */
class CommentaireType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user')
            ->add('parent')
            ->add('article')
            ->add(
                'comment',
                TextareaType::class,
                [
                    'label' => 'Commentaire',
                    'attr' => ['class' => 'ckeditor'],
                ]
            )
            ->add('approuve');
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $commentaire = $event->getData();
                $form = $event->getForm();

                if (null != $commentaire->getId()) {
                    $user = $commentaire->getUser();
                    $form->add(
                        'user',
                        EntityType::class,
                        [
                            'class' => User::class,
                            'query_builder' => function (EntityRepository $er) use ($user) {
                                return $er->createQueryBuilder('u')
                                    ->where('u.id=:id')
                                    ->setParameter('id', $user->getId());
                            },
                        ]
                    )
                    ->add(
                        'article',
                        EntityType::class,
                        [
                            'class' => Article::class,
                            'query_builder' => function (EntityRepository $er) use ($commentaire) {
                                return $er->createQueryBuilder('a')
                                    ->where('a.id=:id')
                                    ->setParameter(
                                        'id',
                                        $commentaire->getArticle()->getId()
                                    );
                            },
                        ]
                    );
                }
            }
        );
    }

    /**
     * {@inheritdoc}
     *
     * @param OptionsResolver $resolver resolver
     *
     * @return ArrayCollection
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\Commentaire',
            )
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'App_commentaire';
    }
}
