<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use App\Entity\User;

class OrderRegistrationCommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'nom',
            null,
            [
                'required'=>true,
            ]
        )
                ->add(
                    'prenom',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->remove('profession')
                ->remove('societe')
                ->remove(
                    'adresseFacturation1'
                )
                ->remove(
                    'adresseFacturation2'
                )
                ->remove(
                    'codePostal'

                )
                ->remove(
                    'ville'
                )
                ->remove(
                    'telephone'
                )
                ->add(
                    'email',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->remove('societeLivraison')
                ->remove(
                    'adresseLivraison1'
                )
                ->remove(
                    'adresseLivraison2'
                )
                ->remove(
                    'codePostalLivraison'
                )
                ->remove(
                    'villeLivraison'
                )
                ->remove(
                    'nomLivraison'
                )
                ->remove(
                    'prenomLivraison',
                    null,
                    [
                        'required'=>false
                    ]
                )
                ->add('conditionsAccepted')
                ->remove('username')
                ->remove('autreProfession')
            ;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'bba_user_registration';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}
