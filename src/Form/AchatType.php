<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', null, [
            'attr' => [
                'readonly' => 'readonly',
                'disabled' => 'disabled'
            ]
        ])
            ->add('produit', null, [
                "attr" => [
                    'class' => 'une-select2'
                ]
            ])
            ->add('qtt')
            ->add('prixUnitaire')
            ->add('prixTotale')
            ->add('tva')
            ->add('dateAchat', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date Achat',
                'required'=>true,
            ])
            ->add('typePaiement', ChoiceType::class, [
                'choices'  => [
                    'Paypal' => 'Paypal',
                    'Par chèque' => 'Par chèque',
                    'Virement' => 'Virement',
                    'CB' => 'CB'
                ],
                'required' => false
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Achat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_achat';
    }


}
