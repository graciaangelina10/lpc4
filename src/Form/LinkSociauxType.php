<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkSociauxType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'reseaux', ChoiceType::class, [
            'choices' => array(
                'Facebook'=>'Facebook',
                'Twitter'=>'Twitter',
                'Instagramm'=>'Instagramm',
                'Linkedin'=>'Linkedin',
                'Vimeo'=>'Vimeo',
                'Youtube'=>'Youtube',
                'RSS'=>'RSS'
            ),
            'label' => 'Choisir Réseaux sociaux',
            'placeholder' => 'Choisir Réseaux sociaux',
            'required' => true,
            ]
        )
            ->add('lien');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\LinkSociaux'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_linksociaux';
    }


}
