<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TailleCabinetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tabAnnee = [];

        $anneeEnCours = date("Y");

        for ($i=1;$i<=10;$i++) {
            $tabAnnee [$anneeEnCours] = $anneeEnCours;
            $anneeEnCours = $anneeEnCours -1;
        }

        $builder
            ->add('annee', ChoiceType::class, [
                'choices' => $tabAnnee,
                'label' => 'Année'
            ])
            ->add('nbClient', null, [
                'label' => 'Nombre de clients'
            ])

            ->add('dateCloture'/*, DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date Clôture',
            ]*/)
            ->add('effectif', null, [
                'label' => 'Effectif total'
            ])

            ->add('nbHommeEc', null, [
                'label' => 'Experts-comptables Hommes'
            ])
            ->add('nbFemmeEc', null, [
                'label' => 'Experts-comptables Femmes'
            ])
            ->add('nbHommeAs', null, [
                'label' => 'Associés Hommes'
            ])
            ->add('nbFemmeAs', null, [
                'label' => 'Associés Femmes'
            ])
            ->add('nbBureau', null, [
                'label' => 'Nombre de bureaux'
            ])
            ->add('localisation')
            ->add('groupeNational', null, [
                'label' => 'Réseau Association technique ou groupement national'
            ])
            ->add('reseauNational', null, [
                'label' => 'Réseau international'
            ])

            //->add('cabinet')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\TailleCabinet'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_taillecabinet';
    }


}
