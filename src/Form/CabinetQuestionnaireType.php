<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CabinetQuestionnaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tabFormJuridique = [
            'Société à responsabilité limitée' => 'Société à responsabilité limitée',
            'Société civile' => 'Société civile',
            'Association déclarée' => 'Association déclarée',
            'Association loi 1901 ou assimilé' => 'Association loi 1901 ou assimilé',
            'Autre société civile' => 'Autre société civile',
            'Entrepreneur individuel' => 'Entrepreneur individuel',
            'Etablissement secondaire' => 'Etablissement secondaire',
            'Groupement d\'intérêt économique' => 'Groupement d\'intérêt économique',
            'Organisme mutualiste' => 'Organisme mutualiste',
            'Profession libérale' => 'Profession libérale',
            'SA à condition d\'administration' => 'SA à condition d\'administration',
            'SA à conseil d\'administration' => 'SA à conseil d\'administration',
            'SA à directoire' => 'SA à directoire',
            'SARL' => 'SARL',
            'SARL unipersonnelle' => 'SARL unipersonnelle',
            'SAS' => 'SAS',
            'SASU' => 'SASU',
            'Société anonyme (SA)' => 'Société anonyme (SA)',
            'Société anonyme à directoire' => 'Société anonyme à directoire',
        ];

        $builder
            ->remove('etude', null, [
                'required' => true,
            ])
            ->add('nom', null, [
                'label' => 'Nom Cabinet'
            ])
            ->add('destinataire', null, [
                'label' => 'Destinataire Nom'
            ])
            ->add('emailDestinataire', null, [
                'label' => 'Destinataire Email'
            ])
            ->remove('code', null, [
                'label' => 'Code'
            ])
            ->add('isPluridiscplinaire')
            ->add('isReseau')
            ->remove('codePostal')
            ->remove('email')
            ->remove('adresse')
            ->remove('ville')

            ->remove('departement')
            ->remove('region')
            ->remove('telephone')

            ->remove('site')
            ->remove('formeJuridique', ChoiceType::class, [
                'choses' => $tabFormJuridique
            ])
            ->remove('siret')
            ->remove('siren')
            ->remove('cas')
            ->remove('caCs')

            ->remove('caExpertises')

            ->remove('tailleCabinets')
            ->remove('informationFiliales')

            ->add('save', SubmitType::class, ['label' => 'Valider', 'attr' =>['class' => 'btn btn-primary']])
            ;

    }

    public function getParent()
    {
        return CabinetType::class;
    }

    public function getName()
    {
        return 'cabinet_questionnaire_type';
    }


}
