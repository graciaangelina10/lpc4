<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CabinetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('etude', null, [
                'required' => true,
            ])
            ->add('nom')

            ->add('adresse')
            ->add('email')
            ->add('ville')
            ->add('codePostal')
            ->add('departement')
            ->add('region')
            ->add('telephone')

            ->add('site')
            ->add('formeJuridique')
            ->add('siret')
            ->add('siren')
            ->add('cas', CollectionType::class, [
                'entry_type' => CaType::class,
                'label' => 'CA',
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])
            ->add('caCs', CollectionType::class, [
                'entry_type' => CaCType::class,
                'label' => "CAC",
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])

            ->add('caExpertises', CollectionType::class, [
                'entry_type' => CaExpertiseType::class,
                'label' => 'CA Expertise',
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])

            ->add('tailleCabinets', CollectionType::class, [
                'entry_type' => TailleCabinetType::class,
                'label' => 'Taille',
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])
            ->add('informationFiliales', CollectionType::class, [
                'entry_type' => InformationFilialeType::class,
                'label' => 'INFORMATIONS CONCERNANT LES FILIALES NON INSCRITES ',
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
            ])
            ;

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Cabinet'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_cabinet';
    }


}
