<?php

/**
 * ArticleContributeurType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\VideoArticle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Article;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Yavin\Symfony\Form\Type\TreeType;

/**
 * Class ArticleContributeurType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */
class ArticleContributeurType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $validator = $options['validation_groups'];

        if ($validator) {
            $status = [
                    'En cours' => Article::STATUS_EN_COURS,
                    'A valider' => Article::STATUS_A_VALIDER,
                    'Validé' => Article::STATUS_VALIDE,
                    'Publié' => Article::STATUS_PUBLIE,
                ];
        } else {
            $status = array(
                'En cours' => Article::STATUS_EN_COURS,
                'A valider' => Article::STATUS_A_VALIDER,
            );
        }

        $typeArticle = [
            Article::TYPE_ARTICLE => Article::TYPE_ARTICLE,
            Article::TYPE_PODCAST => Article::TYPE_PODCAST,
            Article::TYPE_VIDEO => Article::TYPE_VIDEO,

        ];

        $builder->remove(
            'categorie',
            TreeType::class,
            array(
                'multiple' => false,
                'required' => false,
                'placeholder' => 'Choisir catégorie',
                'class' => Categorie::class, // tree class
                'levelPrefix' => ' --- ',
                'orderFields' => ['treeLeft' => 'asc'],
                'prefixAttributeName' => 'data-level-prefix',
                'treeLevelField' => 'treeLevel',
            )
        )
            ->add('titre')
            ->remove(
                'slug', null, [
                'label' => 'Slug (Url)',
                'required'=>false
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                array(
                'choices' => $typeArticle,
                'label' => 'Choisir Type',
                'placeholder' => 'Choisir Type',
                'required' => true,

                )
            )
            ->add(
                'resume',
                CKEditorType::class,
                array(
                'config' => array(
                    'filebrowserBrowseRoute' => 'elfinder',
                    'filebrowserBrowseRouteParameters' => array(
                        'instance' => 'default',
                    ),
                ),
                'label' => 'Introduction',
                'config_name' => 'base_config',
                )
            )
            ->add(
                'contenu',
                CKEditorType::class,
                array(
                'config' => array(
                    'filebrowserBrowseRoute' => 'elfinder',
                    'filebrowserBrowseRouteParameters' => array(
                        'instance' => 'default',
                    ),
                ),
                'config_name' => 'avance_config',
                )
            )
            ->add(
                'imageFile',
                VichImageType::class,
                array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'label' => 'Image (650px X 440px)',
                )
            )
            ->remove(
                'alltags',
                EntityType::class,
                array(
                'class' => 'App:Tags',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.position', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
                'label' => 'Tags',
                'required' => false,
                )
            )
            ->remove(
                'tagnew',
                CollectionType::class,
                array(
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'my-selector',
                ),
                'by_reference' => false,
                'label' => 'Nouveau Tag ',
                )
            );

        $builder
            ->remove('premium')
            ->add('auteur')
            ->add(
                'dateCreation',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date création',
                ]
            )
            ->add(
                'datePublication',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date publication',
                'required' => false,
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                array(
                'choices' => $status,
                'label' => 'Statut',
                'required' => true,
                )
            )
            ->remove(
                'dateDebutEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Début événement',
                    'required'=>false,
                ]
            )
            ->remove(
                'dateFinEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Fin événement',
                    'required'=>false,
                ]
            )
            ->remove(
                'lieuEvent', null, [
                'label'=> 'Lieu événement',
                'required'=>false,
                ]
            )
            ->add(
                'videoArticle', VideoArticleType::class, array(
                'label'=> false

                )
            )
            ->remove(
                'revue',
                EntityType::class,
                array(
                    'class' => 'App:Revue',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('r')
                            ->orderBy('r.dateCreation', 'DESC');
                    },
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Revue',
                    'required' => false,
                    'by_reference' => false,
                )
            )
            ->remove("homePage")
            ->remove("revuePage")
            ->add(
                'podcastFile', VichFileType::class, array(
                    'required' => false,
                    'allow_delete' => true,
                    'download_uri' => true,
                    'label' => 'Podcast (audio)',
                )
            )
            ->remove('linkCalameo')
            ->add('timePublication', TimeType::class, [
                'input'  => 'datetime',
                'label'=> 'Heure Publication',

            ])

        ;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'article_contributeur';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return ArticleType::class;
    }


}
