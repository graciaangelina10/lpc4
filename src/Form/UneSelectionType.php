<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Article;

class UneSelectionType extends AbstractType
{
    /**
     * Construction du form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'article1',
                EntityType::class,
                [
                    'label' => 'Position 1 :',
                    'class' => Article::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where("a.status='publie'")
                            ->orderBy('a.id', 'DESC');
                    },
                    'choice_label' => 'titre',
                    'required' => false,
                    'placeholder' => '[aucun]',
                ]
            )
            ->add(
                'article2',
                EntityType::class,
                [
                    'label' => 'Position 2 :',
                    'class' => Article::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where("a.status='publie'")
                            ->orderBy('a.id', 'DESC');
                    },
                    'choice_label' => 'titre',
                    'required' => false,
                    'placeholder' => '[aucun]',
                ]
            )
            ->add(
                'article3',
                EntityType::class,
                [
                    'label' => 'Position 3 :',
                    'class' => Article::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where("a.status='publie'")
                            ->orderBy('a.id', 'DESC');
                    },
                    'choice_label' => 'titre',
                    'required' => false,
                    'placeholder' => '[aucun]',
                ]
            )
            ->add(
                'article4',
                EntityType::class,
                [
                    'label' => 'Position 4 :',
                    'class' => Article::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where("a.status='publie'")
                            ->orderBy('a.id', 'DESC');
                    },
                    'choice_label' => 'titre',
                    'required' => false,
                    'placeholder' => '[aucun]',
                ]
            )
            ->add(
                'article5',
                EntityType::class,
                [
                    'label' => 'Position 5 :',
                    'class' => Article::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->where("a.status='publie'")
                            ->orderBy('a.id', 'DESC');
                    },
                    'choice_label' => 'titre',
                    'required' => false,
                    'placeholder' => '[aucun]',
                ]
            )
            ->add('save', SubmitType::class, ['label' => 'Enregistrer']);
    }

    /**
     * Configuration des options.
     *
     * @param OptionsResolver $resolver resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\Model\UneSelection']);
    }
}
