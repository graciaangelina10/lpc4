<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use App\Entity\User;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $profession = array(
            User::PROFESSION_COLLABORATEUR_CABINET => User::PROFESSION_COLLABORATEUR_CABINET,
            User::PROFESSION_EXPERT_COMPTABLE => User::PROFESSION_EXPERT_COMPTABLE,
            User::PROFESSION_STAGIAIRE => User::PROFESSION_STAGIAIRE,
            User::PROFESSION_AUTRE => User::PROFESSION_AUTRE
        );

        $permissions = array(
            'Utilisateur'        => 'ROLE_USER',
            'Administrateur'     => 'ROLE_ADMIN',
            'Editeur'     => 'ROLE_EDITOR',
            'Abonné'    =>'ROLE_ABONNE',
            'Contributeur' => 'ROLE_CONTRIBUTEUR'

        );

        $builder->add('nom',null, array(
            'required' => true,
        ))
            ->add('prenom',null,array(
                'required' => true,
            ))
            ->add('profession', ChoiceType::class,
                array(
                    'choices' => $profession,
                    'label' => 'Choisir Profession',
                    'placeholder' => 'Choisir Profession',
                    'required' => true,

                ))
            ->add('email',EmailType::class,array(
                "required"=>true
            ))
            ->add('societe')
            ->add('codePostal',null,array(
                "required"=>true
            ))
            ->add('ville',null,array(
                "required"=>true
            ))
            ->add('telephone',null,array(
                "required"=>true
            ))
            ->add('adresseFacturation1',null,array(
                "label"=>"Adresse",
                "required"=>true
            ))
            ->add('adresseFacturation2',null,array(
                "label"=>"Adresse ligne 2"
            ))
            ->add('adresseLivraison1')
            ->add('adresseLivraison2')
            ->add('nomLivraison')
            ->add('prenomLivraison')
            ->add('societeLivraison')
            ->add('codePostalLivraison')
            ->add('villeLivraison')
            ->add('paidSubscriber',null,array(
                'label'=>'Abonné'
            ))
            ->add('endSubscriptionDate',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date fin Abonnement',
                'required'=>false,
            ])
            ->add('enabled', null, array(
                "label"=>"Activé",
                "required"=>false
            ))
            ->add(
                'roles',
                ChoiceType::class,
                [
                    'label'   => 'Rôle',
                    'choices' => $permissions,
                    'multiple' => true,
                    'expanded' => true,
                ]
            )
            ->add('autreProfession')
            ->add('newsletterSubscription',null,[
                'required'=>false,
                'label'=> 'Newsletter Premium'
            ])
            ->add('subscriptionRenewal', null, [
                'label'=> 'Renouvellement automatique'
            ])
            ->add('dateCreation',DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date d\'inscription',
                'required'=>false,
            ])

            ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();

                if (!$data->getId()) {

                    $form->add('plainPassword', RepeatedType::class, array(
                        'type' => PasswordType::class,
                        'options' => array(
                            'translation_domain' => 'FOSUserBundle',
                            'attr' => array(
                                'autocomplete' => 'new-password',
                            ),
                        ),
                        'label'=>false,
                        'first_options' => array('label' => 'form.password'),
                        'second_options' => array('label' => 'form.password_confirmation'),
                        'invalid_message' => 'fos_user.password.mismatch',
                    ));
                }

            }
        );

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_user';
    }


}
