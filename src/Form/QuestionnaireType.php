<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionnaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('etude')
            ->add('cabinet')
            ->add('destinataire', null, [
                'label' => 'Destinataire Nom'
            ])
            ->add('email', null, [
                'label' => 'Destinataire Email'
            ])
            ->add('code')
            ->add('save', SubmitType::class, ['label' => 'Valider', 'attr' =>['class' => 'btn btn-primary']])


        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Questionnaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_questionnaire';
    }


}
