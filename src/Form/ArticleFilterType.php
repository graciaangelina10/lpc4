<?php

/**
 * ArticleFilterType Class.
 *
 * PHP version 7.1.20
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @license
 *
 * @see ****
 */

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\VideoArticle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Article;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Yavin\Symfony\Form\Type\TreeType;

/**
 * Class ArticleFilterType.
 *
 * @category Class
 *
 * @author Marcel <mrazafimandimby@bocasay.com>
 *
 * @see ****
 */
class ArticleFilterType extends AbstractType
{
    /**
     * Build form.
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder variable to build
     * @param ArrayCollection                              $options options variable
     *                                                              {@inheritdoc}
     *
     * @return FormBuilder
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $status = [
                'En cours' => Article::STATUS_EN_COURS,
                'A valider' => Article::STATUS_A_VALIDER,
                'Validé' => Article::STATUS_VALIDE,
                'Publié' => Article::STATUS_PUBLIE,
            ];


        $typeArticle = [
            Article::TYPE_ARTICLE => Article::TYPE_ARTICLE,
            Article::TYPE_EVENEMENT => Article::TYPE_EVENEMENT,
            Article::TYPE_PODCAST => Article::TYPE_PODCAST,
            Article::TYPE_VIDEO => Article::TYPE_VIDEO,
            Article::TYPE_EDITO => Article::TYPE_EDITO,
            Article::TYPE_CALAMEO => Article::TYPE_CALAMEO

        ];

        $choicesPremium = [
            'Oui' => '1',
            'Non' => '0',
        ];

        $builder->add(
            'categorie',
            EntityType::class,
            array(
                'multiple' => true,
                'required' => false,
                'placeholder' => 'Choisir catégorie',
                'class' => Categorie::class, // tree class
//                'levelPrefix' => ' --- ',
//                'orderFields' => ['treeLeft' => 'asc'],
//                'prefixAttributeName' => 'data-level-prefix',
//                'treeLevelField' => 'treeLevel',
            )
        )
            ->add('titre', null, [
                'required'=>false
            ])
            ->remove(
                'slug', null, [
                'label' => 'Slug (Url)',
                'required'=>false
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                array(
                'choices' => $typeArticle,
                'label' => 'Choisir Type',
                'placeholder' => 'Choisir Type',
                'required' => false,
                    'multiple'  => true,

                )
            )
            ->remove(
                'resume'
            )
            ->remove(
                'contenu'
            )
            ->remove(
                'imageFile'
            )
            ->remove(
                'alltags'
            )
            ->remove(
                'tagnew'
            );

        $builder
            ->add('premium', ChoiceType::class, [
                'choices' => $choicesPremium,
                'expanded' => false,
                'required'=> false,



            ])
            ->add('auteur',EntityType::class, [
                'class' => Article::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->where('a.auteur is not null')
                        ->distinct('a.auteur')
                        ->orderBy('a.auteur','ASC')
                        ;
                },
                'choice_label' => 'auteur',
                'choice_value' => 'auteur',
                'multiple'  => true,
                'required' =>false,
            ])
            ->remove(
                'dateCreation',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date création',
                    'required' => false,
                ]

            )
            ->remove(
                'datePublication',
                DateType::class,
                [
                'widget' => 'single_text',
                'label' => 'Date publication',
                'required' => false,
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                array(
                'choices' => $status,
                'label' => 'Status',
                'required' => false,
                    'multiple'  => true,
                )
            )
            ->remove(
                'dateDebutEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Début événement',
                    'required'=>false,
                ]
            )
            ->remove(
                'dateFinEvent',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'label' => 'Date Fin événement',
                    'required'=>false,
                ]
            )
            ->remove(
                'lieuEvent', null, [
                'label'=> 'Lieu événement',
                'required'=>false,
                ]
            )
            ->remove(
                'videoArticle'
            )
            ->remove(
                'revue'
            )
            ->remove("homePage",null,[

            ])
            ->remove("revuePage")
            ->remove(
                'podcastFile'
            )
            ->remove('linkCalameo')
            ->remove('timePublication');


        ;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'bba_article_filter';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return ArticleType::class;
    }

}
