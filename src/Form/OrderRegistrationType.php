<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use App\Entity\User;

class OrderRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $profession = array(
            User::PROFESSION_COLLABORATEUR_CABINET => User::PROFESSION_COLLABORATEUR_CABINET,
            User::PROFESSION_EXPERT_COMPTABLE => User::PROFESSION_EXPERT_COMPTABLE,
            User::PROFESSION_STAGIAIRE => User::PROFESSION_STAGIAIRE,
            User::PROFESSION_AUTRE => User::PROFESSION_AUTRE
        );

        $builder->add(
            'nom',
            null,
            [
                'required'=>true,
            ]
        )
                ->add(
                    'prenom',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->add('profession', ChoiceType::class, array(
                    'choices' => $profession,

                    'placeholder' => 'Profession *',
                    'required' => true,

                ))
                ->add('societe')
                ->add(
                    'adresseFacturation1',
                    null,
                    [
                        'required'=>true,
                        'label' => 'Adresse (ligne 1)',
                    ]
                )
                ->add(
                    'adresseFacturation2',
                    null,
                    [
                        'required'=>false,
                        'label' => 'Adresse (ligne 2)',
                    ]
                )
                ->add(
                    'codePostal',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->add(
                    'ville',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->add(
                    'telephone',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->add(
                    'email',
                    null,
                    [
                        'required'=>true
                    ]
                )
                ->add('societeLivraison')
                ->add(
                    'adresseLivraison1',
                    null,
                    [
                        'required'=>false,
                        'label' => 'Adresse (ligne 1)',
                    ]
                )
                ->add(
                    'adresseLivraison2',
                    null,
                    [
                        'required'=>false,
                        'label' => 'Adresse (ligne 2)',
                    ]
                )
                ->add(
                    'codePostalLivraison',
                    null,
                    [
                        'required'=>false
                    ]
                )
                ->add(
                    'villeLivraison',
                    null,
                    [
                        'required'=>false
                    ]
                )
                ->add(
                    'nomLivraison',
                    null,
                    [
                        'required'=>false
                    ]
                )
                ->add(
                    'prenomLivraison',
                    null,
                    [
                        'required'=>false
                    ]
                )
                ->add('conditionsAccepted')
                ->remove('username')
                ->add('autreProfession')
            ;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'bba_user_registration';
    }

    /**
     * Get parent form
     *
     * @return null|string
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}
