<?php

namespace App\Form;

use App\Entity\VideoArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VideoArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $typeVideo = [
            VideoArticle::TYPE_VIMEO => VideoArticle::TYPE_VIMEO,
            VideoArticle::TYPE_YOU_TUBE => VideoArticle::TYPE_YOU_TUBE,

        ];
        $builder->add(
            'type',
            ChoiceType::class,
            array(
                'choices' => $typeVideo,
                'label' => 'Choisir Type video',
                'placeholder' => 'Choisir Type',
                'required'=> false

            )
        )
            ->add(
                'lien', null, array(
                 'required'=> false
                )
            );
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\VideoArticle'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_videoarticle';
    }


}
