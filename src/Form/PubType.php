<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PubType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')
            ->add(
                'position', ChoiceType::class, [
                "choices"=> array(
                    "Haut (Taille: 728px X 91px)"=>"haut",
                    "Droite (Taille: 236px X 472px  )"=>"droite",
                ),
                'placeholder' => 'Choisir Position',
                'required' => true,
                ]
            )
            ->add(
                'imageFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'label' => '',
                )
            )
            ->add('lien')
            ->add('active')
            ->add('forAbonnee', null, [
                'label' => 'Espace abonné'
            ])
            ->add('codeAdserver')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'App\Entity\Pub'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_pub';
    }


}
