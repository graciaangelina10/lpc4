<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tabAnnee = [];

        $anneeEnCours = date("Y");

        for ($i=1;$i<=10;$i++) {
            $tabAnnee [$anneeEnCours] = $anneeEnCours;
            $anneeEnCours = $anneeEnCours -1;
        }

        $builder->add('annee', ChoiceType::class, [
            'choices' => $tabAnnee,
            'label' => 'Année'
        ])
            ->add('caLettre', null, [
                'label' => 'CA (en K€)'
            ])
           // ->add('cabinet')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Ca'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'App_ca';
    }


}
