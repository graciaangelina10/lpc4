<?php

namespace App\JMSNamingStrategy;

use JMS\Serializer\Metadata\PropertyMetadata;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class IdenticalUnlessSpecifiedPropertyNamingStrategy implements NameConverterInterface
{

    public function normalize($propertyName)
    {

    }

    public function denormalize($propertyName)
    {

    }
    /**
     * Fonction qui transforme les noms d'attribut
     *
     * @param PropertyMetadata $property l'attribut
     *
     * @return string
     */
    public function translateName(PropertyMetadata $property)
    {
        $name = $property->serializedName;

        if (null !== $name) {
            return $name;
        }

        return $property->name;
    }
}
