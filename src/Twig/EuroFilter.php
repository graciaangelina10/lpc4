<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EuroFilter extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('euro', array($this, 'euroFilter')),
        );
    }

    public function euroFilter($number, $decimals = 2, $decPoint = ',', $thousandsSep = '.')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = $price . '€';

        return $price;
    }
}
