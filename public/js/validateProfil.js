$(function () {
        $('.renouvellement_auto input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });

        $('.icheckbox_square-blue').on('ifChanged', function(event) {
            //alert('checked = ' + event.target.checked);
            $(".alert-success").css("display","none");
            $(".alert-success").fadeOut();
            $(".alert-danger").css("display","block");
            $(".alert-danger").css("position","fixed");
            $(".alert-danger").css("z-index","99999");
            $(".alert-danger").css("margin-right","10px");

            $(".alert-danger").fadeOut();
            $(".alert-danger").fadeIn();

        });

        $('#profil_nom, #profil_prenom, #profil_societe, #profil_autreProfession, ' +
            '#profil_email, #profil_adresseFacturation1,' +
            ' #profil_adresseFacturation2, #profil_ville, #profil_nomLivraison,' +
            '#profil_prenomLivraison, #profil_adresseLivraison1, #profil_adresseLivraison2,' +
            '#profil_codePostalLivraison, #profil_villeLivraison, #show-shipping-address').on('input', function() {

            $(".alert-success").css("display","none");
            $(".alert-danger").css("display","block");
            $(".alert-danger").css("position","fixed");
            $(".alert-danger").css("z-index","99999");
            $(".alert-danger").css("margin-right","10px");



        });

        $('#profil_email, #profil_telephone').on('input', function() {

            $(".alert-success").css("display","none");
            $(".alert-danger").css("display","block");
            $(".alert-danger").css("position","fixed");
            $(".alert-danger").css("z-index","99999");
            $(".alert-danger").css("margin-right","10px");

            var emailaddress = $("#profil_email").val();
            var telephoneNumber = $("#profil_telephone").val();
            var codePostale = $("#profil_codePostal").val();
            if( !validatePhone(telephoneNumber)) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre numero de téléphone !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

                return false;

            }else if( !validateEmail(emailaddress)) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre adresse email !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

            }else if(!$.isNumeric(codePostale) ) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre code postale !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

            }else {
                $(".alert-champ").css("display","none");
                $(".alert-danger").css("display","block");
            }



        });

        $('select').change(function(e) { // select element changed
            $(".alert-success").css("display","none");
            $(".alert-danger").css("display","block");
            $(".alert-danger").css("position","fixed");
            $(".alert-danger").css("z-index","99999");
            $(".alert-danger").css("margin-right","10px");
        });

        $('.modif-data').click(function() {


            var emailaddress = $("#profil_email").val();
            var telephoneNumber = $("#profil_telephone").val();
            var codePostale = $("#profil_codePostal").val();
            if( !validatePhone(telephoneNumber)) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre numero de téléphone !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

                return false;

            }else if( !validateEmail(emailaddress)) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre adresse email !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

                return false;

            }else if(!$.isNumeric(codePostale) ) {
                $(".alert-danger").css("display","none");
                $(".alert-champ").empty();
                $(".alert-champ").append("Merci de vérifier votre code postale !!");
                $(".alert-champ").css("display","block");
                $(".alert-champ").css("position","fixed");
                $(".alert-champ").css("z-index","99999");
                $(".alert-champ").css("margin-right","10px");

                return false;

            }

        });




    });

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

function validatePhone($num) {
    var valide=new RegExp(/^[\+|0|1-9][0-9-(-)]{9,20}$/);
    return valide.test( $num );
}
