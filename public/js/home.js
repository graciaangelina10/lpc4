function _toConsumableArray(arr) {if (Array.isArray(arr)) {for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {arr2[i] = arr[i];}return arr2;} else {return Array.from(arr);}}new Vue({
    el: '#app',
    delimiters: ['<%', '%>'],
    data: {
        baseUrl: base_url,
        apiBaseUrl: api_base_url,
        user: '',
        articles: articlesFirstBatch,
        articlesTop: frontPageArticle,
        limit: 10,
        page: 1,
        showButtonMoreArticles: false,
        categories: categoriesFirstBatch,
        selectedTypeFilters: [],
        typesFilterState: [],
        allFilterSelected: false,
        allTypes: ['Articles', 'Vidéos', 'Podcasts', 'Événements'],
        scrollPos: '',
        currentArticleUrl: '',
        firstArticleToShow: firstArticle,
        articleShow: false,
        currentCategorieUrl: '',
        ignoreEndOfScrollDuration: 1500,
        articleOpenedAt: 0,
        disableRedirections: disableRedirections,
        hideUnesBloc: hideUnesBloc,
        tagHome: tagHome,
        currentTagUrl: '',
        events: eventsTop,
        brefs: bref,
        videos: videosLatest,
        agendas: agendas,
        IdRevue: IdRevue,
        tag: tagSlug,
        categoryFiteredID: categoryFiteredID,
        categorieParentID: categorieParentID,
        showLoading: false,
        search: search,
        home: home,
        curentUrl: curentUrl },

    methods: {
        getArticles: function getArticles() {var _this = this;
            this.page = 1;
            this.showButtonMoreArticles = false;

            this.$http.
            get(this.apiBaseUrl + '/articles', {
                params: {
                    limit: this.limit,
                    page: this.page,
                    category: this.categoryFiteredID,
                    types: this.selectedTypeFilters,
                    revue: this.IdRevue,
                    tag: this.tag,
                    search: this.search,
                    home: this.home } }).


            then(
                function (response) {
                    _this.articles = response.body;

                    _this.articles.some(function (article) {
                        _this.$set(article, 'fullContent', false);
                    });

                    _this.handleArticlesTop();

                    _this.checkRestItems();
                },
                function (error) {
                    if (error.body.status === '404') {
                        _this.articles = [];
                        if (_this.articlesTop.length > 0) {
                            _this.handleArticlesTop();
                        }
                    }
                });

        },
        getMoreArticles: function getMoreArticles() {var _this2 = this;
            this.page += 1;
            this.showLoading = true;

            this.$http.
            get(this.apiBaseUrl + '/articles', {
                params: {
                    limit: this.limit,
                    page: this.page,
                    category: this.categoryFiteredID,
                    types: this.selectedTypeFilters,
                    revue: this.IdRevue,
                    tag: this.tag,
                    home: this.home } }).


            then(
                function (response) {
                    newArticles = response.body;
                    _this2.showLoading = false;
                    if (newArticles.length > 0) {var _articles;
                        (_articles = _this2.articles).push.apply(_articles, _toConsumableArray(newArticles));
                    } else {
                        _this2.showButtonMoreArticles = false;
                        return;
                    }
                    _this2.checkRestItems();
                },
                function (error) {
                    console.log(error.response.data);
                });

        },
        checkRestItems: function checkRestItems() {var _this3 = this;
            this.$http.
            get(this.apiBaseUrl + '/articles', {
                params: {
                    limit: this.limit,
                    page: this.page + 1,
                    category: this.categoryFiteredID,
                    types: this.selectedTypeFilters,
                    revue: this.IdRevue,
                    tag: tagSlug,
                    search: this.search,
                    home: this.home } }).


            then(function (res) {
                if (res.body.length > 0) {
                    _this3.showButtonMoreArticles = true;
                    return;
                }

                _this3.showButtonMoreArticles = false;
            });
        },
        filterByCategory: function filterByCategory(category) {
            this.hideUnesBloc = true;
            this.categoryFiteredID = category.id;
            this.getArticles();

            this.currentCategorieUrl = this.baseUrl + '/categorie/' + category.slug;
            history.replaceState(null, null, this.currentCategorieUrl);
        },
        getCategories: function getCategories() {var _this4 = this;
            this.$http.get(this.apiBaseUrl + '/categories').then(function (response) {
                _this4.categories = response.body;
            });
        },
        // Copie une version "front" des articles Top dans la liste normale d'article
        // Les articles "front" ne sont pas visibles dans la liste et ne sont affichés
        //  que quand on clique sur leur version "Une" ou que c'est l'article qui est
        //  fourni avec un accès via la route d'un article
        handleArticlesTop: function handleArticlesTop() {var _this5 = this;
            this.articlesTop.map(function (articleTop) {
                if (!articleTop.article) {
                    return;
                }

                articleTop.article.front = true;
                _this5.articles.unshift(articleTop.article);
            });
        },
        showFullContent: function showFullContent(article) {var _this6 = this;
            /*if (article.premium && !this.user) {
                var popupPremium = document.getElementById('popup-premium');
                popupPremium.classList.remove('fade');
                popupPremium.classList.add('open-popup-premium');
                $('.fixed-top_sub').css('display', 'none');
                $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article/' + article.categorie.slug + '/' + article.slug);
                return;
            }*/
            //recuperer contenu article
            this.$http.get(this.apiBaseUrl + '/articleContenu', {
                params: {
                    id: article.id } }).

            then(function (response) {
                _this6.$set(article, 'contenu', response.body);
            });

            if (article.front) {
                this.articleShow = true;
                $('#article-front-' + article.id).css('display', 'block');
            }


            this.articles.some(function (article) {return article.fullContent = false;});
            this.$set(article, 'fullContent', true);

            var articleElement = document.getElementById('article-' + article.id);
            console.log(articleElement);

            var currentArticleTop = 0;
            var currentArticleheight = 0;

            if (document.getElementById('article-' + article.id)) {
                currentArticleTop = articleElement.offsetTop;
                currentArticleheight = articleElement.scrollHeight;
            }

            this.scrollPos = currentArticleTop + currentArticleheight;
            this.currentArticleUrl = this.baseUrl + '/article/' + article.categorie.slug + '/' + article.slug;

            if (!this.disableRedirections) {
                history.replaceState(null, null, this.currentArticleUrl);
            }

            this.$nextTick(function () {return twttr.widgets.load();});
            this.articleOpenedAt = new Date().getTime();


            if (!article.front) {
                /*setTimeout(function () {
                    // 580 : offset dû au header et au bloc top
                    $([document.documentElement, document.body]).animate(
                        {
                            scrollTop: articleElement.offsetTop },

                        300,
                        'easeInQuart');

                }, 600);*/
            }
            //show comment
            this.$http.
            get('/comment/list/'+article.id, {
                params: {
                    'routeToArticle': '/article/' + article.categorie.slug + '/' + article.slug
                     } }).


            then(function (res) {
                if (res.body.length > 0) {
                    $("#CommentSidenav").empty();
                    $("#CommentSidenav").append(res.body);
                    return;
                }


            });

        },
        handleEndOfScroll: function handleEndOfScroll(article) {var _this7 = this;
            // On ignore le endOfScroll pour une durée donnée après l'ouverture de l'article
            // C'est à cause du scroll auto après les redimensionnements/annimations
            if (
                new Date().getTime() <
                this.articleOpenedAt + this.ignoreEndOfScrollDuration)
            {
                return;
            }

            setTimeout(
                function () {return document.documentElement.scrollTop = _this7.scrollPos;},
                200);

            this.hideFullContent(article);
        },
        hideFullContent: function hideFullContent(article) {
            this.$set(article, 'fullContent', false);
            if (this.home == 1)
                this.articleShow = false;else

                this.articleShow = true;
            this.currentArticleUrl = this.baseUrl;
            //$('#article-front-' + article.id).css('display', 'none');

            if (!this.disableRedirections) {
                if (this.currentCategorieUrl != '') {
                    history.replaceState(null, null, this.currentCategorieUrl);
                } else if (this.curentUrl != '') {
                    history.replaceState(null, null, this.curentUrl);
                } else {
                    history.replaceState(null, null, '/');
                }
            }



            /*if (this.firstArticleToShow) {
                if (this.home == 1) {
                    this.firstArticleToShow = false;
                    this.articles.splice(this.articles.indexOf(article), 1);
                }

            }*/
        },
        filterByTypes: function filterByTypes(event) {var _this8 = this;
            this.$nextTick(function () {return _this8.getArticles();});
        },
        getNextArticleElementSelector: function getNextArticleElementSelector(article) {
            return function () {
                // Si on est sur un article front, c'est le 1e article de la liste centrale qui
                // est juste après (c'est lui qui est juste après le bloc une)
                if (article.front) {
                    return document.querySelector('.article-section:not(.front-article)');
                }

                // Sinon, c'est que c'est l'article suivant dans la liste centrale
                var articleElement = document.querySelector(
                    '[article-id="' + article.id + '"]');


                return articleElement.nextElementSibling;
            };
        },
        closePremiumModal: function closePremiumModal() {
            var popupPremium = document.getElementById('popup-premium');
            popupPremium.classList.remove('open-popup-premium');
            popupPremium.classList.add('fade');
            $('.fixed-top_sub').css('display', 'block');
        },
        getToLogin: function getToLogin() {
            window.location = '/login';
        },
        getToSubscribe: function getToSubscribe() {
            window.location = '/register';
        },
        startFrom: function startFrom(arr, idx) {
            return arr.slice(idx);
        },
        tagPage: function tagPage(tag) {var _this9 = this;
            this.$http.get('/api/tag/' + tag.slug, {
                params: {
                    limit: this.limit,
                    page: 1,
                    category: this.categoryFiteredID,
                    types: this.selectedTypeFilters,
                    revue: this.IdRevue,
                    tag: tag.slug } }).

            then(function (response) {
                _this9.hideUnesBloc = true;
                _this9.articles = response.body;

                _this9.checkRestItems();

                _this9.currentTagUrl = _this9.baseUrl + '/tag/' + tag.slug;
                history.replaceState(null, null, _this9.currentTagUrl);
            });
        },
        isAuthenticated: function isAuthenticated(event) {
            return this.user ? true : false;
        } },

    beforeMount: function beforeMount() {
        this.user = document.getElementById('user').value;
        this.$set(this.articles[0], 'contenu', this.articles[0].contenuCache);
        if (this.firstArticleToShow) {
            //show comment
            this.$http.
            get('/comment/list/'+this.articles[0].id, {
                params: {
                    'routeToArticle': '/article/' + this.articles[0].categorie.slug + '/' + this.articles[0].slug
                } }).
            then(function (res) {
                if (res.body.length > 0) {
                    $("#CommentSidenav").empty();
                    $("#CommentSidenav").append(res.body);
                    return;
                }
            });
        }

        this.handleArticlesTop();

        this.agendas = this.agendas.filter(function (agenda) {return !!agenda.article;});
        this.videos = this.videos.filter(function (video) {return !!video.article;});

    },
    mounted: function mounted() {
        if (this.firstArticleToShow) {
            this.articleShow = true;


        }

        this.checkRestItems();

    } });