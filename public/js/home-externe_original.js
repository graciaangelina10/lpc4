new Vue({
  el: '#app',
  delimiters: ['<%', '%>'],
  data: {
    showPaymentButton: false,
    showShippingAddress: false,
    baseUrl: '..',
    apiBaseUrl: '/api',
    user: '',
    categories: categoriesFirstBatch,
    scrollPos: '',
    currentCategorieUrl: '',
    tagHome: tagHome,
    events: eventsTop,
    currentTagUrl: '',
    categoryFiteredID: '',
    agendas: agendas,
      categorieParentID: categorieParentID
  },
  methods: {
      filterByCategory(category) {
          this.hideUnesBloc = true;
          this.categoryFiteredID = category.id;
          //this.getArticles();

          this.currentCategorieUrl = this.baseUrl + '/categorie/' + category.slug;
          history.replaceState(null, null, this.currentCategorieUrl);
      },
    tagPage: function(tag) {
      this.$http.get('/api/tag/' + tag.slug).then(response => {
        this.hideUnesBloc = true;
        this.articles = response.body;
        this.currentTagUrl = this.baseUrl + '/tag/' + tag.slug;
        history.replaceState(null, null, this.currentTagUrl);
      });
    },
      isAuthenticated(event) {
          return this.user ? true : false;
      },
      closePremiumModal() {
          let popupPremium = document.getElementById('popup-premium');
          popupPremium.classList.remove('open-popup-premium');
          popupPremium.classList.add('fade');
          $('.fixed-top_sub').css('display', 'block');
      },
      getToLogin() {
          window.location = '/login';
      },
      getToSubscribe() {
          window.location = '/register';
      },
      beforeMount() {
          this.user = document.getElementById('user').value;

      }

  }
});
