new Vue({
  el: '#app',
  delimiters: ['<%', '%>'],
  data: {
    baseUrl: base_url,
    apiBaseUrl: api_base_url,
    user: '',
    articles: articlesFirstBatch,
    articlesTop: frontPageArticle,
    limit: 10,
    page: 1,
    showButtonMoreArticles: false,
    categories: categoriesFirstBatch,
    selectedTypeFilters: [],
    typesFilterState: [],
    allFilterSelected: false,
    allTypes: ['Articles', 'Vidéos', 'Podcasts', 'Événements'],
    scrollPos: '',
    currentArticleUrl: '',
    firstArticleToShow: firstArticle,
    articleShow: false,
    currentCategorieUrl: '',
    ignoreEndOfScrollDuration: 1500,
    articleOpenedAt: 0,
    disableRedirections: disableRedirections,
    hideUnesBloc: hideUnesBloc,
    tagHome: tagHome,
    currentTagUrl: '',
    events: eventsTop,
    brefs: bref,
    videos: videosLatest,
    agendas: agendas,
      IdRevue:IdRevue,
      tag: tagSlug,
      categoryFiteredID:categoryFiteredID,
      categorieParentID: categorieParentID,
      showLoading: false,
      search: search,
      home: home,
      curentUrl:curentUrl
  },
  methods: {
    getArticles() {
      this.page = 1;
      this.showButtonMoreArticles = false;

      this.$http
        .get(this.apiBaseUrl + '/articles', {
          params: {
            limit: this.limit,
            page: this.page,
            category: this.categoryFiteredID,
            types: this.selectedTypeFilters,
              revue:this.IdRevue,
              tag: this.tag,
              search: this.search,
              home: this.home
          }
        })
        .then(
          response => {
            this.articles = response.body;

            this.articles.some(article => {
              this.$set(article, 'fullContent', false);
            });

            this.handleArticlesTop();

            this.checkRestItems();
          },
          error => {
            if (error.body.status === '404') {
              this.articles = [];
              if (this.articlesTop.length > 0) {
                this.handleArticlesTop();
              }
            }
          }
        );
    },
    getMoreArticles() {
      this.page += 1;
      this.showLoading = true;

      this.$http
        .get(this.apiBaseUrl + '/articles', {
          params: {
            limit: this.limit,
            page: this.page,
            category: this.categoryFiteredID,
            types: this.selectedTypeFilters,
              revue:this.IdRevue,
              tag: this.tag,
              home: this.home
          }
        })
        .then(
          response => {
            newArticles = response.body;
            this.showLoading = false;
            if (newArticles.length > 0) {
              this.articles.push(...newArticles);
            } else {
              this.showButtonMoreArticles = false;
              return;
            }
            this.checkRestItems();
          },
          error => {
            console.log(error.response.data);
          }
        );
    },
    checkRestItems() {
      this.$http
        .get(this.apiBaseUrl + '/articles', {
          params: {
            limit: this.limit,
            page: this.page + 1,
            category: this.categoryFiteredID,
            types: this.selectedTypeFilters,
              revue:this.IdRevue,
              tag: tagSlug,
              search: this.search,
              home: this.home
          }
        })
        .then(res => {
          if (res.body.length > 0){
              this.showButtonMoreArticles = true;
              return;
          }

          this.showButtonMoreArticles = false;
        });
    },
    filterByCategory(category) {
      this.hideUnesBloc = true;
      this.categoryFiteredID = category.id;
      this.getArticles();

      this.currentCategorieUrl = this.baseUrl + '/categorie/' + category.slug;
      history.replaceState(null, null, this.currentCategorieUrl);
    },
    getCategories() {
      this.$http.get(this.apiBaseUrl + '/categories').then(response => {
        this.categories = response.body;
      });
    },
    // Copie une version "front" des articles Top dans la liste normale d'article
    // Les articles "front" ne sont pas visibles dans la liste et ne sont affichés
    //  que quand on clique sur leur version "Une" ou que c'est l'article qui est
    //  fourni avec un accès via la route d'un article
    handleArticlesTop() {
      this.articlesTop.map(articleTop => {
        if (!articleTop.article) {
          return;
        }

        articleTop.article.front = true;
        this.articles.unshift(articleTop.article);
      });
    },
    showFullContent(article) {
      if (article.premium && !this.user) {
        let popupPremium = document.getElementById('popup-premium');
        popupPremium.classList.remove('fade');
        popupPremium.classList.add('open-popup-premium');
        $('.fixed-top_sub').css('display', 'none');
          $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article/'+ article.categorie.slug+'/'+ article.slug);
        return;
      }
          //recuperer contenu article
            this.$http.get(this.apiBaseUrl + '/articleContenu', {
                params: {
                    id: article.id,
                }
            }).then(response => {
                this.$set(article, 'contenu', response.body);
            });

      if (article.front) {
        this.articleShow = true;
        $('#article-front-' + article.id).css('display', 'block');
      }


      this.articles.some(article => (article.fullContent = false));
      this.$set(article, 'fullContent', true);

      let articleElement = document.getElementById('article-' + article.id);
      console.log(articleElement);

      let currentArticleTop = 0;
      let currentArticleheight = 0;

      if (document.getElementById('article-' + article.id)) {
        currentArticleTop = articleElement.offsetTop;
        currentArticleheight = articleElement.scrollHeight;
      }

      this.scrollPos = currentArticleTop + currentArticleheight;
      this.currentArticleUrl = this.baseUrl + '/article/' + article.categorie.slug+'/'+article.slug;

      if (!this.disableRedirections) {
        history.replaceState(null, null, this.currentArticleUrl);
      }

      this.$nextTick(() => twttr.widgets.load());
      this.articleOpenedAt = new Date().getTime();


      if (!article.front) {
        setTimeout(() => {
          // 580 : offset dû au header et au bloc top
          $([document.documentElement, document.body]).animate(
            {
              scrollTop: articleElement.offsetTop
            },
            300,
            'easeInQuart'
          );
        }, 600);
      }

      this.$http
          .get('/comment/list/'+article.id, {
            params: {
              'routeToArticle': '/article/' + article.categorie.slug + '/' + article.slug
            }
          })
          .then(res => {
            if (res.body.length > 0){
              $("#CommentSidenav").empty();
              $("#CommentSidenav").append(res.body);
              return;
            }


          });
    },
    handleEndOfScroll: function(article) {
      // On ignore le endOfScroll pour une durée donnée après l'ouverture de l'article
      // C'est à cause du scroll auto après les redimensionnements/annimations
      if (
        new Date().getTime() <
        this.articleOpenedAt + this.ignoreEndOfScrollDuration
      ) {
        return;
      }

      setTimeout(
        () => (document.documentElement.scrollTop = this.scrollPos),
        200
      );
      this.hideFullContent(article);
    },
    hideFullContent(article) {
      this.$set(article, 'fullContent', false);
      if (this.home == 1)
          this.articleShow = false;
        else
          this.articleShow = true;
      this.currentArticleUrl = this.baseUrl;
      $('#article-front-' + article.id).css('display', 'none');

      if (!this.disableRedirections) {
        if (this.currentCategorieUrl != '') {
          history.replaceState(null, null, this.currentCategorieUrl);
        }else if (this.curentUrl !='') {
           history.replaceState(null, null, this.curentUrl);
        }else {
          history.replaceState(null, null, '/');
        }
      }

      if (this.firstArticleToShow) {
        if (this.home == 1){
            this.firstArticleToShow = false;
            this.articles.splice(this.articles.indexOf(article), 1);
        }

      }
    },
    filterByTypes(event) {
      this.$nextTick(() => this.getArticles());
    },
    getNextArticleElementSelector: function(article) {
      return function() {
        // Si on est sur un article front, c'est le 1e article de la liste centrale qui
        // est juste après (c'est lui qui est juste après le bloc une)
        if (article.front) {
          return document.querySelector('.article-section:not(.front-article)');
        }

        // Sinon, c'est que c'est l'article suivant dans la liste centrale
        let articleElement = document.querySelector(
          '[article-id="' + article.id + '"]'
        );

        return articleElement.nextElementSibling;
      };
    },
    closePremiumModal() {
      let popupPremium = document.getElementById('popup-premium');
      popupPremium.classList.remove('open-popup-premium');
      popupPremium.classList.add('fade');
      $('.fixed-top_sub').css('display', 'block');
    },
    getToLogin() {
      window.location = '/login';
    },
    getToSubscribe() {
      window.location = '/register';
    },
    startFrom(arr, idx) {
      return arr.slice(idx);
    },
    tagPage: function(tag) {
      this.$http.get('/api/tag/' + tag.slug,{
          params: {
              limit: this.limit,
              page: 1,
              category: this.categoryFiteredID,
              types: this.selectedTypeFilters,
              revue:this.IdRevue,
              tag: tag.slug
          }
      }).then(response => {
        this.hideUnesBloc = true;
        this.articles = response.body;

        this.checkRestItems();

        this.currentTagUrl = this.baseUrl + '/tag/' + tag.slug;
        history.replaceState(null, null, this.currentTagUrl);
      });
    },
    isAuthenticated(event) {
      return this.user ? true : false;
    }
  },
  beforeMount() {
    this.user = document.getElementById('user').value;
    this.$set(this.articles[0], 'contenu', this.articles[0].contenuCache);
    if (this.firstArticleToShow) {
      //show comment
      this.$http.
      get('/comment/list/'+this.articles[0].id, {
        params: {
          'routeToArticle': '/article/' + this.articles[0].categorie.slug + '/' + this.articles[0].slug
        } }).
      then(function (res) {
        if (res.body.length > 0) {
          $("#CommentSidenav").empty();
          $("#CommentSidenav").append(res.body);
          return;
        }
      });
    }
    this.handleArticlesTop();

    this.agendas = this.agendas.filter(agenda => !!agenda.article);
    this.videos = this.videos.filter(video => !!video.article);
  },
  mounted() {
    if (this.firstArticleToShow) {
      this.articleShow = true;
    }

     this.checkRestItems();

  }
});
