new Vue({
    el: '#app',
    delimiters: ['<%', '%>'],
    data: {
        showPaymentButton: false,
        showShippingAddress: false,
        baseUrl: '..',
        apiBaseUrl: '/api',
        user: '',
        categories: categoriesFirstBatch,
        scrollPos: '',
        currentCategorieUrl: '',
        tagHome: tagHome,
        events: eventsTop,
        currentTagUrl: '',
        categoryFiteredID: '',
        agendas: agendas,
        brefs: bref,
        categorieParentID: categorieParentID },


    methods: {
        filterByCategory: function filterByCategory(category) {
            this.hideUnesBloc = true;
            this.categoryFiteredID = category.id;
            //this.getArticles();

            this.currentCategorieUrl = this.baseUrl + '/categorie/' + category.slug;
            history.replaceState(null, null, this.currentCategorieUrl);
        },
        tagPage: function tagPage(tag) {var _this = this;
            this.$http.get('/api/tag/' + tag.slug).then(function (response) {
                _this.hideUnesBloc = true;
                _this.articles = response.body;
                _this.currentTagUrl = _this.baseUrl + '/tag/' + tag.slug;
                history.replaceState(null, null, _this.currentTagUrl);
            });
        },
        isAuthenticated: function isAuthenticated(event) {
            return this.user ? true : false;
        },
        closePremiumModal: function closePremiumModal() {
            var popupPremium = document.getElementById('popup-premium');
            popupPremium.classList.remove('open-popup-premium');
            popupPremium.classList.add('fade');
            $('.fixed-top_sub').css('display', 'block');
        },
        getToLogin: function getToLogin() {
            window.location = '/login';
        },
        getToSubscribe: function getToSubscribe() {
            window.location = '/register';
        },
        beforeMount: function beforeMount() {
            this.user = document.getElementById('user').value;

        } } });