<?php

ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Toolbox initialisation, using Sogecommerce account informations
 * Required : ShopID + CERTIFICATE + platform URL
 */
$toolbox = require "config/config_lpc.php";

//Choix carte
$vads_payment_cards = "CB";

if (isset($_POST['cardId']) && !empty($_POST['cardId'])) {
    $vads_payment_cards = $_POST['cardId'];
}

//Total commande
$vads_amount = 0;

if (isset($_POST['amount']) && !empty($_POST['amount'])) {
    $vads_amount = $_POST['amount'];
}else {
    return "pb_price";
    exit;
}

//Numero de commande

$vads_order_id = "";

if (isset($_POST['numCommande']) && !empty($_POST['numCommande'])) {
    $vads_order_id = $_POST['numCommande'];
}

//Nom prenom
$vads_cust_name = "";

if (isset($_POST['nom']) && !empty($_POST['nom'])) {
    $vads_cust_name = $_POST['nom'];
}

$vads_cust_address = "";

if (isset($_POST['adresse']) && !empty($_POST['adresse'])) {
    $vads_cust_address = $_POST['adresse'];
}

$vads_cust_zip = "";

if (isset($_POST['codePostal']) && !empty($_POST['codePostal'])) {
    $vads_cust_zip = $_POST['codePostal'];
}

$vads_cust_city = "";
if (isset($_POST['ville']) && !empty($_POST['ville'])) {
    $vads_cust_city = $_POST['ville'];
}

$vads_cust_country = "FR";

if (isset($_POST['pays']) && !empty($_POST['pays'])) {
    $vads_cust_country = $_POST['pays'];
}

$vads_cust_phone = "";
if (isset($_POST['telephone']) && !empty($_POST['telephone'])) {
    $vads_cust_phone = $_POST['telephone'];
}

$vads_cust_email = "";

if (isset($_POST['email']) && !empty($_POST['email'])) {
    $vads_cust_email = $_POST['email'];
}

//abonnement
$vads_sub_desc = "";
if (isset($_POST['abonnement']) && !empty($_POST['abonnement'])) {
    $vads_sub_desc = $_POST['abonnement'];
}

$vads_page_action = "REGISTER_SUBSCRIBE";
/**
 * Payment arguments
 * If none is mentioned, defaults will be used
 * You can check defaults and formats : vads-payment-php\lib\payzenFormToolbox.php  in function "getFormFields"
 */

//GET return URL
$protocol = ( (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://' ;
$url =  $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$return_url = str_replace('sogecommerce/paiement.php','boutique/paiement',$url);

$args = array(
    "vads_cust_title" => "",
    "vads_order_id" => $vads_order_id, //numero de commande
    "vads_cust_name" => $vads_cust_name, //Nom prenom
    "vads_cust_address" => $vads_cust_address, //Adresse
    "vads_cust_zip" => $vads_cust_zip, //Code postal
    "vads_cust_city" => $vads_cust_city, //Ville
    "vads_cust_country" => $vads_cust_country, // Pays
    "vads_cust_phone" => $vads_cust_phone, // Téléphone,
    "vads_cust_email" => $vads_cust_email, //Email
    "vads_amount" => $vads_amount, //The amount of the transaction presented in the smallest unit of the currency (cents for Euro).
    "vads_currency" => "978", // An ISO 4217 numerical code of the payment currency.
    "vads_payment_cards" => $vads_payment_cards, // Contains the list of card types proposed to the buyer, separated by a ";".
    'vads_redirect_error_timeout'   => '10',//delay in seconds before an automatic redirection to the merchant website at the end of a declined payment.
    'vads_redirect_success_timeout' => '10',// delay in seconds before an automatic redirection to the merchant website at the end of an accepted payment.
    'vads_url_return'   => $return_url //Default URL to where the buyer will be redirected. If this field has not been transmitted, the Back Office configuration will be taken into account.
);

if ($vads_sub_desc != "") {
    $args = [
        "vads_sub_desc" => $vads_sub_desc, //Abonnement,
        "vads_page_action" => $vads_page_action,
    ];
}

/**
 * Retrieve FORM DATA
 */
$formData = $toolbox->getFormData($args);

/**
 * Output the form in html
 */
$form = '<form name="lpcPaiement" action="'.$formData['form']['action'].'" method="'.$formData['form']['method'].'" accept-charset="'.$formData['form']['accept-charset'].'">';
foreach ($formData['fields'] as $name => $value) {
    //$form .= '<label for="'. $name. '">'.$name.'</label>';
    $form .= '<input type="hidden" readonly="readonly"  name="'.$name.'" value="'.$value.'" /><br />';
}
//$form .= '<input type="submit" name="pay-submit" id="pay-submit" value="'.'Pay'.'"/>';
$form .= '</form>';

echo $form;

?>


<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

</body>
</html>
