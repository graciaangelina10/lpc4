breves = Vue.component('breves', {
    template: '\n    <div class="en-bref-bloc">\n        <h4>En bref</h4>\n        <div class="listing-bref">\n            <div class="bloc-bref" v-for="breve in brevef" :key="breve.id">\n                <span class="date">{{ breve.datePublication | formatDateNumeric }}</span>\n                <a target="blank" :href="breve.link">{{ breve.titre }}</a>\n            </div>\n        </div>\n    </div>\n    ',










    props: [
        'feed',
        'amount',
    'brevef'],

     });