Vue.component('videos', {
    props: ['videos', 'user'],
    methods: {
        transform: function transform(_url) {
            if (!_url) {
                return '';
            }

            if (_url.match(/youtu\.?be/) || _url.match(/vimeo.com/)) {
                _url = _url.replace(
                    'https://youtu.be/',
                    'https://www.youtube.com/embed/');


                _url = _url.replace(
                    'https://vimeo.com/',
                    'https://player.vimeo.com/video/');


                _url = _url.replace('watch?v=', 'embed/');
            }

            return _url + '?autoplay=1&loop=1';
        },

        showFullContent: function showFullContent(article) {
            if (article.premium && !this.user) {
                var popupPremium = document.getElementById('popup-premium');
                popupPremium.classList.remove('fade');
                popupPremium.classList.add('open-popup-premium');
                $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article/' + article.categorie.slug + '/' + article.slug);
                $('.fixed-top_sub').css('display', 'none');
                return;
            } else {
                window.location = '/article/' + article.categorie.slug + '/' + article.slug;
            }
        } },

    template: '\n    <section class="video-section">\n      <div class="offset-left-width col-md-12 col-lg-10">\n        <h2>Nos derni\xE8res videos</h2>\n      </div>\n      <!--listing-video  -->\n      <div class="listing-video">\n        <div class="offset-left-width col-md-12 col-lg-10">\n          <div class="row">\n            <!--video-thumb-->\n            <div class="col-md-4 col-lg-4 col-sm-12 col-xl-4 un-video" v-for="video in videos" :key="video.id">\n              <a v-if="video.article.premium" class="video-box" @click="showFullContent(video.article, $event)">\n                \n                  <iframe width="300" height="169" :id="\'iframeVideo\'+video.id"  v-if="video.article.videoArticle && video.article.premium && user"\n                        :src="video.article.videoArticle.lien | transformVideoUrl"\n                        frameborder="0"\n                        webkitallowfullscreen\n                        mozallowfullscreen\n                        allowfullscreen \n                        \n                        >\n                </iframe>\n                <div class="video-thumb" v-if="!video.article.videoArticle && !video.article.thumbnails">\n                  <img  class="img-cover" :src="\'/uploads/images/articles/\' + video.article.image"  width="300px" height="169px" alt="">\n                </div>\n                <div class="video-thumb" v-if="video.article.thumbnails && !user">\n                  <img  class="img-cover" :src="\'/uploads/images/articles/thumbnails/\' + video.article.thumbnails"  width="300px" height="169px" alt="">\n                </div>\n                <h3>{{ video.article.titre }} <span v-if="video.article.premium" class="premium-icon-star">\n            <i class="fas fa-star"></i>\n          </span></h3>\n                <p v-html="video.article.resume">..</p>\n              </a>\n              \n              <a :href="\'/article/\'+video.article.categorie.slug+\'/\'+video.article.slug" v-if="!video.article.premium" class="video-box" @click="showFullContent(video.article, $event)">\n                \n                <iframe width="300" height="169" :id="\'iframeVideo\'+video.id" v-if="video.article.videoArticle"\n                        :src="video.article.videoArticle.lien | transformVideoUrl"\n                        frameborder="0"\n                        webkitallowfullscreen\n                        mozallowfullscreen\n                        allowfullscreen>\n                </iframe>\n                <div class="video-thumb" v-if="!video.article.videoArticle" >\n                  <img class="img-cover" :src="\'/uploads/images/articles/\' + video.article.image"  width="300px" height="169px" alt="">\n                </div>\n                <h3>{{ video.article.titre }} <span v-if="video.article.premium" class="premium-icon-star">\n            <i class="fas fa-star"></i>\n          </span></h3>\n                <p v-html="video.article.resume">..</p>\n              </a>\n            </div>\n            <!--.video-thumb-->\n            \n          </div>\n        \n        </div>\n      </div>            \n      <!--listing-video  -->\n    </section>\n    \n    ' });