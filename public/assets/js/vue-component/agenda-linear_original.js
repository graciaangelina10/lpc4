Vue.component('agenda-linear', {
  props: ['events', 'isAuthenticated'],
  methods: {
    onClickEvent(event) {
      if (!this.isAuthenticated()) {
        let popupPremium = document.getElementById('popup-premium');
        popupPremium.classList.remove('fade');
        popupPremium.classList.add('open-popup-premium');
        $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article-agenda/'+ event.categorie.slug+'/'+ event.slug);
        $('.fixed-top_sub').css('display', 'none');
        return;
      }

      window.location = '/article-agenda/' + event.categorie.slug+'/'+event.slug;
    }
  },
  template: `
    
    <div class="inter-article">
      <div class="titre-section">
        <h2>VOS PROCHAINS RENDEZ-VOUS</h2>
      </div>
      <!--listing-agenda-->
      <div class="listing-agenda">
        <div class="row">
          <!--bloc-agenda-->
          <a style="cursor:pointer;" class="bloc-agenda col-6 col-md-3" v-for="event in events" :key="event.id" @click="onClickEvent(event)">
            <h3>{{ event.titre }}</h3>
            <div class="date" v-if="event.dateDebutEvent == event.dateFinEvent">
                {{ event.dateFinEvent | formatDateShowDayAndMounth }}
            </div>
            <div class="date" v-if="event.dateDebutEvent != event.dateFinEvent">
              {{ event.dateDebutEvent | formatDateShowDayOnly }} - {{ event.dateFinEvent | formatDateShowDayAndMounth }}
            </div>
            <div class="lieu">{{ event.lieuEvent }}</div>
          </a>
          <!--.bloc-agenda-->
          
        </div>    
        
      </div>
      <!--.listing-agenda-->
    </div>
    
    `
});
