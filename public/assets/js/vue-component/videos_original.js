Vue.component('videos', {
  props: ['videos','user'],
  methods: {
    transform(_url) {
      if (!_url) {
        return '';
      }

      if (_url.match(/youtu\.?be/) || _url.match(/vimeo.com/)) {
        _url = _url.replace(
          'https://youtu.be/',
          'https://www.youtube.com/embed/'
        );

        _url = _url.replace(
          'https://vimeo.com/',
          'https://player.vimeo.com/video/'
        );

        _url = _url.replace('watch?v=', 'embed/');
      }

      return _url+'?autoplay=1&loop=1';
    },

    showFullContent(article) {
      if (article.premium && !this.user) {
        let popupPremium = document.getElementById('popup-premium');
        popupPremium.classList.remove('fade');
        popupPremium.classList.add('open-popup-premium');
        $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article/'+ article.categorie.slug+'/'+ article.slug);
        $('.fixed-top_sub').css('display', 'none');
        return;
      }else {
          window.location = '/article/' + article.categorie.slug+'/'+ article.slug;
      }
    }
  },
  template: `
    <section class="video-section">
      <div class="offset-left-width col-md-12 col-lg-10">
        <h2>Nos dernières videos</h2>
      </div>
      <!--listing-video  -->
      <div class="listing-video">
        <div class="offset-left-width col-md-12 col-lg-10">
          <div class="row">
            <!--video-thumb-->
            <div class="col-md-6 col-lg-6 col-xl-4 un-video" v-for="video in videos" :key="video.id">
              <a v-if="video.article.premium" class="video-box" @click="showFullContent(video.article, $event)">
                
                  <iframe width="300" height="169" :id="'iframeVideo'+video.id"  v-if="video.article.videoArticle && video.article.premium && user"
                        :src="video.article.videoArticle.lien | transformVideoUrl"
                        frameborder="0"
                        webkitallowfullscreen
                        mozallowfullscreen
                        allowfullscreen 
                        
                        >
                </iframe>
                <div class="video-thumb" v-if="!video.article.videoArticle && !video.article.thumbnails">
                  <img  class="img-cover" :src="'/uploads/images/articles/' + video.article.image"  width="300px" height="169px" alt="">
                </div>
                <div class="video-thumb" v-if="video.article.thumbnails && !user">
                  <img  class="img-cover" :src="'/uploads/images/articles/thumbnails/' + video.article.thumbnails"  width="300px" height="169px" alt="">
                </div>
                <h3>{{ video.article.titre }} <span v-if="video.article.premium" class="premium-icon-star">
            <i class="fas fa-star"></i>
          </span></h3>
                <p v-html="video.article.resume">..</p>
              </a>
              
              <a :href="'/article/'+video.article.categorie.slug+'/'+video.article.slug" v-if="!video.article.premium" class="video-box" @click="showFullContent(video.article, $event)">
                
                <iframe width="300" height="169" :id="'iframeVideo'+video.id" v-if="video.article.videoArticle"
                        :src="video.article.videoArticle.lien | transformVideoUrl"
                        frameborder="0"
                        webkitallowfullscreen
                        mozallowfullscreen
                        allowfullscreen>
                </iframe>
                <div class="video-thumb" v-if="!video.article.videoArticle" >
                  <img class="img-cover" :src="'/uploads/images/articles/' + video.article.image"  width="300px" height="169px" alt="">
                </div>
                <h3>{{ video.article.titre }} <span v-if="video.article.premium" class="premium-icon-star">
            <i class="fas fa-star"></i>
          </span></h3>
                <p v-html="video.article.resume">..</p>
              </a>
            </div>
            <!--.video-thumb-->
            
          </div>
        
        </div>
      </div>            
      <!--listing-video  -->
    </section>
    
    `
});
