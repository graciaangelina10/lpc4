categorieTree = Vue.component('categorie-tree', {
    name: 'categorie-tree',
    template: '\n  <li>\n    <a \n        @click="handleClick"\n        style="cursor: pointer"\n        href="#"\n        v-if="categorie.children.length >0"\n        >\n      {{ categorie.nom }}\n    </a>\n    <a v-if="categorie.children.length == 0" :href="\'/categorie/\'+categorie.slug">{{ categorie.nom }}</a>\n    <ul v-if="categorie.children.length >0">\n        <span class="return" @click="returnClick"><</span>\n        <categorie-tree\n        v-if="showChildren"\n        v-for="children in categorie.children"\n        :key="children.id"\n        :categorie="children"\n        :depth="depth + 1"\n        :click-handler="clickHandler"\n        :id="\'id_cat_\'+children.id"\n        >\n        </categorie-tree>\n    </ul>\n  </li>\n    ',

























    props: [
        'categorie',
        'depth',
        'clickHandler'],

    data: function data() {
        return {
            showChildren: true };

    },
    mounted: function mounted() {
        // apres chargement element
        if ($("#id_cat_" + this.categorie.id).hasClass("active")) {
            this.showChildren = true;
        }
    },
    computed: {
        indent: function indent() {
            return { transform: 'translate(' + this.depth * 15 + 'px)' };
        } },

    methods: {
        handleClick: function handleClick() {
            //   $(".cat_menu li").removeClass('active');
            if ($(window).width() < 768) {
                $(".cat_menu").addClass('extended');
            }

            this.clickHandler(this.categorie);
        },
        returnClick: function returnClick() {

        },
        toggleChildren: function toggleChildren() {
            this.showChildren = !this.showChildren;
        } } });