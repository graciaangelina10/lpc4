Vue.component('agenda-lateral', {
  props: ['agendas', 'isAuthenticated'],
  methods: {
    onClickEvent(agenda) {
      if (this.isAuthenticated === "") {
        let popupPremium = document.getElementById('popup-premium');
        popupPremium.classList.remove('fade');
        popupPremium.classList.add('open-popup-premium');
        $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article-agenda/'+ agenda.article.categorie.slug+'/'+ agenda.article.slug);
        $('.fixed-top_sub').css('display', 'none');
        return;
      }

      window.location = '/article-agenda/' + agenda.article.categorie.slug+'/'+ agenda.article.slug;
    }
  },
  template: `
    <div class="agenda-box mobile-hide" v-if="agendas.length > 0">
        <div class="titre-section">
            <h2 class="titre-filter">Agenda</h2>
        </div>
        <div class="slider-agenda">
            <div class="item" v-for="agenda in agendas" :key="agenda.id" @click="onClickEvent(agenda)" style="cursor:pointer;">
                <span>{{ agenda.article.titre }}</span><br/>
                <span class="date" v-if="agenda.article.dateDebutEvent == agenda.article.dateFinEvent">
                     {{ agenda.article.dateFinEvent | formatDateShowDayAndMounth }}
                </span>
                <span class="date" v-if="agenda.article.dateDebutEvent != agenda.article.dateFinEvent">
                     {{ agenda.article.dateDebutEvent | formatDateShowDayOnly }} - {{ agenda.article.dateFinEvent | formatDateShowDayAndMounth }}
                  
                </span>
            </div>
        </div>
    </div>
    `
});
