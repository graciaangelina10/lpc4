categorieTree = Vue.component('categorie-tree', {
  name: 'categorie-tree',
  template: `
  <li>
    <a 
        @click="handleClick"
        style="cursor: pointer"
        href="#"
        v-if="categorie.children.length >0"
        >
      {{ categorie.nom }}
    </a>
    <a v-if="categorie.children.length == 0" :href="'/categorie/'+categorie.slug">{{ categorie.nom }}</a>
    <ul v-if="categorie.children.length >0">
        <span class="return" @click="returnClick"><</span>
        <categorie-tree
        v-if="showChildren"
        v-for="children in categorie.children"
        :key="children.id"
        :categorie="children"
        :depth="depth + 1"
        :click-handler="clickHandler"
        :id="'id_cat_'+children.id"
        >
        </categorie-tree>
    </ul>
  </li>
    `,
  props: [
    'categorie',
    'depth',
    'clickHandler'
  ],
  data: () => {
      return {
          showChildren: true
      };
  },
    mounted: function () {
        // apres chargement element
        if ($( "#id_cat_"+this.categorie.id).hasClass( "active")){
            this.showChildren = true;
        }
    },
  computed: {
      indent: function () {
          return { transform: `translate(${this.depth * 15}px)` };
      },
  },
  methods: {
      handleClick: function () {
        //   $(".cat_menu li").removeClass('active');
          if($(window).width() < 768 ){
            $(".cat_menu").addClass('extended');
          }
          
          this.clickHandler(this.categorie);    
      },
      returnClick: function() {

      },
      toggleChildren: function () {
          this.showChildren = !this.showChildren;
      }
  }
});
