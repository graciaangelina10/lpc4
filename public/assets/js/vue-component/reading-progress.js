readingProgress = Vue.component('reading-progress', {
    template: '<div class="reading-wrapper"\n     :class="{sticky: sticky}"\n     >\n     <div class="rs-box-article">\n        <div class="ouvrir">\n        <span class="ouvrir-img"></span>\n        </div>\n        <div class="share-buttons-in-article">\n            <ul class="ul-btn-viewshare"><li>\n              <a class="rs twitter clicktwitter"\n                  target="_blank"\n                  :href="\'https://twitter.com/share?text=\'+article.titre+\'&url=https://www.laprofessioncomptable.com/article/\'+article.categorie.slug+\'/\'+article.slug+\'&hashtags=laprofessioncomptable&via=LaProfComptable\'"\n                  data-size="large"\n                  :data-text="article.titre"\n                  data-hashtags="laprofessioncomptable"\n                  data-via="La profession comptable"\n              >\n                <span class="twitter-ico"></span>\n              </a>\n            </li>\n            <li>\n              <a :href="\'http://www.linkedin.com/shareArticle?mini=true&url=https://www.laprofessioncomptable.com/article/\'+article.categorie.slug+\'/\'+article.slug+\'&title=\'+article.titre+\'&source=@LaProfComptable\'" target="_blank" class="rs linkedin clicklinkedin">\n                <span class="linkedin-ico"></span>\n              </a>\n            </li>\n       </ul>\n  </div>\n    </div>\n  <svg :width="diameter"\n       :height="diameter"\n       :viewbox="\'0 0 \' + diameter + \' \' + diameter"\n       @click="emitClick"\n       style="\n         -webkit-transform: rotate(-90deg);\n         -ms-transform: rotate(-90deg);\n         transform: rotate(-90deg);\n         position: absolute;\n         left: 50%;\n         top: 50%;\n         border-radius: 50%;\n         box-shadow: -3px 0px 7px gray;\n         background: white;\n         z-index: 1000;\n         cursor: pointer;\n       "\n       >\n\n    <circle class="progress-circle"\n            id="progress-circle"\n            :cx="radius"\n            :cy="radius"\n            :r="progressRadius - (strokeWidth / 2)"\n            :stroke-width="strokeWidth"\n            @click="emitClick"\n            fill="white"\n            :style="{\n                \'stroke-dashoffset\': dashOffset,\n                \'stroke-dasharray\': dashArray\n            }"\n            style="stroke-linecap: butt; cursor: pointer"\n            v-if="!full"\n            :stroke="strokeColor"\n            >\n    </circle>\n\n    <line :x1="radius * 5/8" :y1="radius * 5/8"\n          :x2="radius * 11/8" :y2="radius * 11/8"\n          :stroke-width="radius / 10"\n          @click="emitClick"\n          v-if="!full"\n          style="stroke-linecap: round; cursor: pointer" \n          :style="{stroke: strokeColor}"\n          />\n\n    <line :x1="radius * 11/8" :y1="radius * 5/8"\n          :x2="radius * 5/8" :y2="radius * 11/8"\n          :stroke-width="radius / 10"\n          @click="emitClick"\n          v-if="!full"\n          style="stroke-linecap: round; cursor: pointer"\n          :style="{stroke: strokeColor}"\n          />\n\n\n    <circle id="finished-circle"\n            :cx="radius"\n            :cy="radius"\n            :r="progressRadius - (strokeWidth / 2)"\n            v-if="full"\n            @click="emitClick"\n            style="\n                transition: all 1s;\n                cursor: pointer\n            "\n            :style="{fill: strokeColor}"\n            >\n    </circle>\n\n    <line :x1="radius * 8/8" :y1="radius * 5/8"\n          :x2="radius * 23/32" :y2="radius * 29/32"\n          :stroke-width="radius / 10"\n          @click="emitClick"\n          v-if="full"\n          style="stroke: white; cursor: pointer" />\n\n    <line :x1="radius * 6/8" :y1="radius * 7/8"\n          :x2="radius * 10/8" :y2="radius * 11/8"\n          :stroke-width="radius / 10"\n          @click="emitClick"\n          v-if="full"\n          style="stroke: white; cursor: pointer" />\n  </svg>\n</div>',















































































































    props: [
        'radius',
        'nextElementSelector',
        'article',
        'currentArticleUrl'],

    data: function data() {
        return {
            full: false,
            progress: 0,
            sticky: false,
            strokeColor: '#39CCCC',
            coef: 0,
            headerHeight: 200,
            distanceBeforeEndOfRead: 375,
            overflowThreshold: 300 };

    },
    methods: {
        emitClick: function emitClick() {
            this.$emit('click');
        },
        handleScroll: function handleScroll(e) {
            var progressOverflow = 0;
            var nextElement = this.nextElementSelector();
            var currentScroll = this.$el.parentNode.getBoundingClientRect().top - this.headerHeight;
            var nextElementScroll = nextElement.getBoundingClientRect().top - this.headerHeight - this.distanceBeforeEndOfRead;
            var viewportHeight = this.getViewportHeight();
            this.sticky = currentScroll <= 25;

            this.progress = 100 *
                -currentScroll / (
                    nextElementScroll - currentScroll);

            if (this.progress < 0) {
                this.progress = 0;
            }

            if (this.progress > 100) {
                this.progress = 100;
                progressOverflow = -nextElementScroll;
            }

            if (progressOverflow >= this.overflowThreshold) {
                this.$emit('end-of-scroll');
            }
        },
        getViewportHeight: function getViewportHeight() {
            return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        },
        htmlEntities: function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        } },

    watch: {
        progress: function progress(val) {
            this.full = this.progress >= 100;
        } },

    computed: {
        progressRadius: function progressRadius() {
            return this.radius * 0.9;
        },
        diameter: function diameter() {
            return this.radius * 2;
        },
        strokeWidth: function strokeWidth() {
            return this.radius * 0.07;
        },
        dashArray: function dashArray() {
            return 2 * this.progressRadius * Math.PI;
        },
        dashOffset: function dashOffset() {
            return this.dashArray - this.progress / 100 * this.dashArray + this.strokeWidth;
        } },

    created: function created() {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed: function destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    },
    mounted: function mounted() {
        console.log('url ', this.currentArticleUrl);
    } });