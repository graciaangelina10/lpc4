categorieTree = Vue.component('categorie-tree', {
    name: 'categorie-tree',
    template: `
  <li>
    <a 
        @click="handleClick"
        style="cursor: pointer"
        href="#"
        v-if="categorie.children.length >0"
        >
      {{ categorie.nom }}
    </a>
    <a v-if="categorie.children.length == 0" :href="'/categorie/'+categorie.slug">{{ categorie.nom }}</a>
    <ul v-if="categorie.children.length >0">
        <categorie-tree
        v-if="showChildren"
        v-for="children in categorie.children"
        :key="children.id"
        :categorie="children"
        :depth="depth + 1"
        :click-handler="clickHandler"
        :id="'id_cat_'+children.id"
        >
        </categorie-tree>
    </ul>
  </li>
    `,
    props: [
        'categorie',
        'depth',
        'clickHandler'
    ],
    data: () => {
        return {
            showChildren: true
        };
    },
    mounted: function () {
        // apres chargement element
        if ($( "#id_cat_"+this.categorie.id).hasClass( "active")){
            this.showChildren = true;
        }
    },
    computed: {
        indent: function () {
            return { transform: `translate(${this.depth * 15}px)` };
        },
    },
    methods: {
        handleClick: function () {
            // this.toggleChildren();
            // $(".cat_menu li").removeClass('active');
            this.clickHandler(this.categorie);

            // if(this.showChildren){
            //     $("#id_cat_"+this.categorie.id+" ul").css("border-top","1px solid #1e2839");
            //     $("#id_cat_"+this.categorie.id+" ul").css("border-right","1px solid #1e2839");
            //     $("#id_cat_"+this.categorie.id+" ul").css("border-bottom","1px solid #1e2839");
            //     $("#id_cat_"+this.categorie.id).addClass('active');

            // }else {
            //     $("#id_cat_"+this.categorie.id+" ul").css("border","0");
            //     $(".cat_menu li").removeClass('active');
            // }
        },
        toggleChildren: function () {
            this.showChildren = !this.showChildren;
        }
    }
});
