breves = Vue.component('breves', {
  template: `
    <div class="en-bref-bloc">
        <h4>En bref</h4>
        <div class="listing-bref">
            <div class="bloc-bref" v-for="breve in breves" :key="breve.id">
                <span class="date">{{ breve.datePublication | formatDateNumeric }}</span>
                <a target="blank" :href="breve.link">{{ breve.titre }}</a>
            </div>
        </div>
    </div>
    `,
  props: [
    'feed',
    'amount',
  ],
  data: () => {
      return {
          breves: [],
      };
  },
  methods: {
      refresh: function () {
          this.breves = [];

        this.$http.get(this.feed).then(response => {
            console.log(response);
            let parser = new DOMParser();
            let xml = parser.parseFromString(response.body, "text/xml");
            let items = xml.querySelectorAll('item');
            let nbBreve = (items.length-1);

            //for (var i = nbBreve; i > (nbBreve -3); --i) {
            for (var i = 0; i < 3; i++) {
                let item = items[i];

                if (!item) {
                    break;
                }

                let id = null;
                let titre = null;
                let link = null;
                let pubDate = null;

                let idElement = item.querySelector('Record_num');
                let titreElement = item.querySelector('title');
                let linkElement = item.querySelector('Lien URL');
                let pubDateElement = item.querySelector('pubDate');

                if (titreElement) {
                    titre = titreElement.innerHTML;
                }
                if (linkElement) {
                    link = linkElement.innerHTML;
                }
                if (idElement) {
                    id = idElement.innerHTML;
                }
                if (pubDateElement) {
                    let dateElt = pubDateElement.innerHTML;
                    pubDate = new Date(dateElt.replace("GMT",""));
                }

                this.breves.push({
                    id: id,
                    titre: titre,
                    link: link,
                    datePublication: pubDate,
                });
            }
        });
      },
  },
  created () {
      this.refresh();
  },
});
