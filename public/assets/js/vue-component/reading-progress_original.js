readingProgress = Vue.component('reading-progress', {
  template: `

<div class="reading-wrapper"
     :class="{sticky: sticky}"
     >
     <div class="rs-box-article">
        <div class="ouvrir">
        <span class="ouvrir-img"></span>
        </div>
        <div class="share-buttons-in-article">
         <ul class="ul-btn-viewshare">
            <li>
              <a class="rs twitter clicktwitter"
                  target="_blank"
                  :href="'https://twitter.com/share?text='+article.titre+'&url=https://www.laprofessioncomptable.com/article/'+article.categorie.slug+'/'+article.slug+'&hashtags=laprofessioncomptable&via=LaProfComptable'"
                  data-size="large"
                  :data-text="article.titre"
                  data-hashtags="laprofessioncomptable"
                  data-via="La profession comptable"
              >
                <span class="twitter-ico"></span>
              </a>
            </li>
            <li>
              <a :href="'http://www.linkedin.com/shareArticle?mini=true&url=https://www.laprofessioncomptable.com/article/'+article.categorie.slug+'/'+article.slug+'&title='+article.titre+'&source=@LaProfComptable'" target="_blank" class="rs linkedin clicklinkedin">
                <span class="linkedin-ico"></span>
              </a>
            </li>
            </ul>
        </div> 
    </div>
  <svg :width="diameter"
       :height="diameter"
       :viewbox="'0 0 ' + diameter + ' ' + diameter"
       @click="emitClick"
       style="
         -webkit-transform: rotate(-90deg);
         -ms-transform: rotate(-90deg);
         transform: rotate(-90deg);
         position: absolute;
         left: 50%;
         top: 50%;
         border-radius: 50%;
         box-shadow: -3px 0px 7px gray;
         background: white;
         z-index: 1000;
         cursor: pointer;
       "
       >

    <circle class="progress-circle"
            id="progress-circle"
            :cx="radius"
            :cy="radius"
            :r="progressRadius - (strokeWidth / 2)"
            :stroke-width="strokeWidth"
            @click="emitClick"
            fill="white"
            :style="{
                'stroke-dashoffset': dashOffset,
                'stroke-dasharray': dashArray
            }"
            style="stroke-linecap: butt; cursor: pointer"
            v-if="!full"
            :stroke="strokeColor"
            >
    </circle>

    <line :x1="radius * 5/8" :y1="radius * 5/8"
          :x2="radius * 11/8" :y2="radius * 11/8"
          :stroke-width="radius / 10"
          @click="emitClick"
          v-if="!full"
          style="stroke-linecap: round; cursor: pointer" 
          :style="{stroke: strokeColor}"
          />

    <line :x1="radius * 11/8" :y1="radius * 5/8"
          :x2="radius * 5/8" :y2="radius * 11/8"
          :stroke-width="radius / 10"
          @click="emitClick"
          v-if="!full"
          style="stroke-linecap: round; cursor: pointer"
          :style="{stroke: strokeColor}"
          />


    <circle id="finished-circle"
            :cx="radius"
            :cy="radius"
            :r="progressRadius - (strokeWidth / 2)"
            v-if="full"
            @click="emitClick"
            style="
                transition: all 1s;
                cursor: pointer
            "
            :style="{fill: strokeColor}"
            >
    </circle>

    <line :x1="radius * 8/8" :y1="radius * 5/8"
          :x2="radius * 23/32" :y2="radius * 29/32"
          :stroke-width="radius / 10"
          @click="emitClick"
          v-if="full"
          style="stroke: white; cursor: pointer" />

    <line :x1="radius * 6/8" :y1="radius * 7/8"
          :x2="radius * 10/8" :y2="radius * 11/8"
          :stroke-width="radius / 10"
          @click="emitClick"
          v-if="full"
          style="stroke: white; cursor: pointer" />
  </svg>
</div>
    `,
  props: [
    'radius',
    'nextElementSelector',
    'article',
    'currentArticleUrl'
  ],
  data: () => {
      return {
          full: false,
          progress: 0,
          sticky: false,
          strokeColor: '#39CCCC',
          coef: 0,
          headerHeight: 200,
          distanceBeforeEndOfRead: 375,
          overflowThreshold: 300,
      };
  },
  methods: {
      emitClick: function () {
          this.$emit('click');
      },
      handleScroll: function (e) {
          let progressOverflow = 0;
          let nextElement = this.nextElementSelector();
          let currentScroll = this.$el.parentNode.getBoundingClientRect().top - this.headerHeight;
          let nextElementScroll = nextElement.getBoundingClientRect().top - this.headerHeight - this.distanceBeforeEndOfRead;
          let viewportHeight = this.getViewportHeight();
          this.sticky = currentScroll <= 25;

          this.progress = 100 *
              - currentScroll
              / (nextElementScroll - currentScroll);

          if (this.progress < 0) {
              this.progress = 0;
          }

          if (this.progress > 100) {
              this.progress = 100;
              progressOverflow = - nextElementScroll;
          }

          if (progressOverflow >= this.overflowThreshold) {
              this.$emit('end-of-scroll');
          }
      },
      getViewportHeight: function () {
        return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      },
      htmlEntities: function (str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
      },
  },
  watch: {
    progress: function (val) {
        this.full = this.progress >= 100;
    },
  },
  computed: {
      progressRadius: function () {
          return this.radius * 0.9;
      },
    diameter: function () {
      return this.radius * 2;
    },
    strokeWidth: function () {
      return this.radius * 0.07;
    },
    dashArray: function () {
      return 2 * this.progressRadius * Math.PI;
    },
    dashOffset: function () {
      return this.dashArray - ((this.progress / 100) * this.dashArray) + this.strokeWidth;
    },
  },
  created () {
    window.addEventListener('scroll', this.handleScroll);
  },
  destroyed () {
    window.removeEventListener('scroll', this.handleScroll);
  },
  mounted() {
      console.log('url ', this.currentArticleUrl);
  }

});
