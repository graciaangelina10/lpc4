Vue.component('agenda-linear', {
    props: ['events', 'isAuthenticated'],
    methods: {
        onClickEvent: function onClickEvent(event) {
            if (!this.isAuthenticated()) {
                var popupPremium = document.getElementById('popup-premium');
                popupPremium.classList.remove('fade');
                popupPremium.classList.add('open-popup-premium');
                $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article-agenda/' + event.categorie.slug + '/' + event.slug);
                $('.fixed-top_sub').css('display', 'none');
                return;
            }

            window.location = '/article-agenda/' + event.categorie.slug + '/' + event.slug;
        } },

    template: '\n    \n    <div class="inter-article">\n      <div class="titre-section">\n        <h2>VOS PROCHAINS RENDEZ-VOUS</h2>\n      </div>\n      <!--listing-agenda-->\n      <div class="listing-agenda">\n        <div class="row row-bloc-agenda">\n          <!--bloc-agenda-->\n          <a style="cursor:pointer;" class="bloc-agenda col-6 col-md-3" v-for="event in events" :key="event.id" @click="onClickEvent(event)">\n            <h3>{{ event.titre }}</h3>\n            <div class="date" v-if="event.dateDebutEvent == event.dateFinEvent">\n                {{ event.dateFinEvent | formatDateShowDayAndMounth }}\n            </div>\n            <div class="date" v-if="event.dateDebutEvent != event.dateFinEvent">\n              {{ event.dateDebutEvent | formatDateShowDayOnly }} - {{ event.dateFinEvent | formatDateShowDayAndMounth }}\n            </div>\n            <div class="lieu">{{ event.lieuEvent }}</div>\n          </a>\n          <!--.bloc-agenda-->\n          \n        </div>    \n        \n      </div>\n      <!--.listing-agenda-->\n    </div>\n    \n    ' });