Vue.component('agenda-lateral', {
    props: ['agendas', 'isAuthenticated'],
    methods: {
        onClickEvent: function onClickEvent(agenda) {
            if (this.isAuthenticated === "") {
                var popupPremium = document.getElementById('popup-premium');
                popupPremium.classList.remove('fade');
                popupPremium.classList.add('open-popup-premium');
                $("#popup-premium .inverted").attr("href", '/login?urltoredirect=/article-agenda/' + agenda.article.categorie.slug + '/' + agenda.article.slug);
                $('.fixed-top_sub').css('display', 'none');
                return;
            }

            window.location = '/article-agenda/' + agenda.article.categorie.slug + '/' + agenda.article.slug;
        } },

    template: '\n    <div class="agenda-box mobile-hide" v-if="agendas.length > 0">\n        <div class="titre-section">\n            <h2 class="titre-filter">Agenda</h2>\n        </div>\n        <div class="slider-agenda">\n            <div class="item" v-for="agenda in agendas" :key="agenda.id" @click="onClickEvent(agenda)" style="cursor:pointer;">\n                <span>{{ agenda.article.titre }}</span><br/>\n                <span class="date" v-if="agenda.article.dateDebutEvent == agenda.article.dateFinEvent">\n                     {{ agenda.article.dateFinEvent | formatDateShowDayAndMounth }}\n                </span>\n                <span class="date" v-if="agenda.article.dateDebutEvent != agenda.article.dateFinEvent">\n                     {{ agenda.article.dateDebutEvent | formatDateShowDayOnly }} - {{ agenda.article.dateFinEvent | formatDateShowDayAndMounth }}\n                  \n                </span>\n            </div>\n        </div>\n    </div>\n    ' });