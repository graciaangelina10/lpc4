Vue.filter('capitalize', function(value) {
  if (!value) return '';
  value = value.toString();
  return value.toUpperCase();
});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD MMMM YYYY');
  }
});

Vue.filter('formatDateShowDayOnly', function(value) {
  if (value) {
    return moment(String(value)).format('D');
  }
});

Vue.filter('formatDateShowDayAndMounth', function(value) {
  if (value) {
    return moment(String(value)).format('D MMMM');
  }
});

Vue.filter('formatDateNumeric', function(value) {
  if (value) {
    return moment(String(value)).format('DD.MM.YY');
  }
});

Vue.filter('transformVideoUrl', function(_url) {
  if (!_url) {
    return '';
  }

  if (_url.match(/youtu\.?be/) || _url.match(/vimeo.com/)) {
    _url = _url.replace('https://youtu.be/', 'https://www.youtube.com/embed/');

    _url = _url.replace(
      'https://vimeo.com/',
      'https://player.vimeo.com/video/'
    );

    _url = _url.replace('watch?v=', 'embed/');
  }

  return _url+"?title=0&byline=0&portrait=0";
});

function htmlEntities (str) {
  return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

Vue.filter('htmlEntities', htmlEntities);

Vue.filter('truncate', function (text, stop, clamp) {
  return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})
Vue.filter('stripTags', function (text) {
  return text.replace(/(<([^>]+)>)/ig, '');
})

